<?php
    require_once 'josys/db_connect.php';
    include_once 'josys/class/Database.php';
    $database 	= new Database($db);
    $action= empty($_GET['c']) ? FALSE : $_GET['c'] ; 
    switch ($action) {
        case 'categories':
            $categories= $database->select('*','categories','ORDER BY category_title','all');
            echo json_encode($categories);
            break;
        case 'layers':
            $layers= $database->select('*','layers','ORDER BY layer_title','all');
            echo json_encode($layers);
            break;
        
        default:
            # code...
            echo "{null}";
            break;
    }