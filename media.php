<?php
include "josys/function/time_load.php";
// include "config.php";
time_load();
//start session
ob_start();
session_start();
// error_reporting(0);
//require Import System files
require_once 'josys/db_connect.php';
include_once 'josys/class/Database.php';
include_once 'josys/class/Rupiah.php';
include_once 'josys/class/Tanggal.php';
include_once 'josys/function/simple_captcha.php';
include_once 'josys/class/Security.php';
include_once 'josys/function/YoutubeVideoData.php';

$database 	= new Database($db);
$rupiah 	= new Rupiah();
$security   = new Security();

//Config Website
$config = array();
$config['web_name']				= 'Sistem Jaringan Irigasi';
$config['web_link']				= 'www.sijari.com';
$config['link_home']			= 'sistem-jaringan-irigasi';
$config['link_portofolio_kat']	= 'daerah-irigasi';
$config['link_portofolio_det']	= 'detail-daerah-irigasi';

$config['link_tentang']			= 'tentang-akustik-ruang';
$config['link_portofolio']		= 'portofolio-akustik-ruang';
// $config['link_portofolio_kat']	= 'portofolio';
// $config['link_portofolio_det']	= 'detail-portofolio';
$config['link_produk']			= 'produk-akustik-ruang';
$config['link_produk_kat']		= 'produk';
$config['link_produk_det']		= 'detail-produk';
$config['link_blog']			= 'blog-akustik-ruang';
$config['link_blog_det']		= 'detail-blog';
$config['link_konsultasi']		= 'konsultasi-akustik-ruang';
$config['link_galeri_foto']		= 'galeri-foto-akustik-ruang';
$config['link_galeri_video']	= 'galeri-video-akustik-ruang';
$config['link_testimoni']		= 'testimoni-akustik-ruang';
$config['link_kontak']			= 'kontak-akustik-ruang';

include "template.php";
ob_end_flush();
