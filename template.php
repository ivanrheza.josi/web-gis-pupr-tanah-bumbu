<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "joinc/statistic.php"; ?>
        <?php include "joinc/metadata_tag.php"; ?>

        <!-- FAVICON -->
        <link rel="icon" type="image/png" href="assets/images/icon/favicon.png" />
        <link rel="apple-touch-icon" href="assets/images/icon/touch-icon.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/images/icon/touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/images/icon/touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/images/icon/touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="assets/images/icon/touch-icon-152x152.png" />
        <link rel="apple-touch-icon" sizes="167x167" href="assets/images/icon/touch-icon-167x167.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icon/touch-icon-180x180.png" />
        
        <!-- Base Css -->
        <link href="assets/css/base.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/mega-menu.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css?v=<?php echo date('Ymdhis')?>" rel="stylesheet" type="text/css"/>
        <link href="assets/css/i-custom.css?v=<?php echo date('Ymdhis')?>" rel="stylesheet" type="text/css"/>
        <!-- Print -->
        <link rel="stylesheet" href="assets/css/print.css" type="text/css" media="print">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <!-- jQuery -->
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        
    </head>
    <body>
        <div class="se-pre-con"></div>
        <div id="page-content">
        <?php 
            include('joinc/header.php');
            include('joinc/contents.php');
        ?>
        </div>
        <?php 
            // include('joinc/footer.php');
        ?>

        <div class="containerXXX">
            <footer>
                <div class="sub-footer">
                        <div class="row roww">
                            <div class="col-sm-12">
                                <div class="contact-details">
                                    <div class="foot-socmedd text-center">
                                    <?php 
                                        $sosm = $database->select($fields = "*", $table = "sosmed", $where_clause = "ORDER BY id_sosmed DESC", $fetch = "all");
                                        foreach ($sosm as $key => $vsosm) {
                                            echo '
                                                <a href="'.$vsosm['link'].'" target="_blank"><img src="joimg/sosmed/'.$vsosm['gambar'].'" alt="'.$vsosm['nama'].'" title="'.$vsosm['nama'].'"></a>&nbsp;&nbsp;&nbsp;
                                            ';
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 ifooter">
                                <p class="foot-copyrighttXXX text-center">Copyright &copy; 2018 <?php echo $config['web_name']; ?> - SIJARI. All rights reserved.</p>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
            </footer>

        </div>
        
        <!-- jquery ui js -->
        <script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
        <!-- bootstrap js -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- fraction slider js -->
        <script src="assets/js/jquery.fractionslider.js" type="text/javascript"></script>
        <!-- owl carousel js --> 
        <script src="assets/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
        <!-- counter -->
        <script src="assets/js/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="assets/js/waypoints.js" type="text/javascript"></script>
        <!-- filter portfolio -->
        <script src="assets/js/jquery.shuffle.min.js" type="text/javascript"></script>
        <script src="assets/js/portfolio.js" type="text/javascript"></script>
        <!-- magnific popup -->
        <script src="assets/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <!-- range slider -->
        <script src="assets/js/ion.rangeSlider.min.js" type="text/javascript"></script>
        <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
        <!-- custom -->
        <script src="assets/js/custom.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".dropdown").hover(            
                    function() {
                        $('.dropdown-menu', this).not('.in .dropdown-menu').stop(false,false).slideDown("400");
                        $(this).toggleClass('open');        
                    },
                    function() {
                        $('.dropdown-menu', this).not('.in .dropdown-menu').stop(false,false).slideUp("400");
                        $(this).toggleClass('open');       
                    }
                );
            });
            

        </script>
        
    </body>
</html>