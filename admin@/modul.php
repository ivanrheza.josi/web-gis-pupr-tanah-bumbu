<?php
// Bagian Home
if ($_GET['module']=='home'){
	if ($_SESSION['level']=='admin'){

      error_reporting(0);
        // Statistik user
        $ip      = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
        $tanggal = date("Y-m-d");// Mendapatkan tanggal sekarang
        $waktu   = time(); //

        // Mencek berdasarkan IPnya, apakah user sudah pernah mengakses hari ini
        //$statistik_count = $database->count_rows($table="statistik", $where_clause="WHERE ip='$ip' AND tanggal='$tanggal'");
        // Kalau belum ada, simpan data user tersebut ke database
        //if(mysql_num_rows($s) == 0){
       // }
        //else{
        //}

        $pengunjung       = $database->count_rows($table="statistik", $where_clause="WHERE tanggal='$tanggal' GROUP BY ip");
        $totalpengunjung  = $database->select($fields="COUNT(hits)", $table="statistik", $where_clause="", $fetch="");
        $hits             = $database->select($fields="SUM(hits)", $table="statistik", $where_clause="WHERE tanggal='$tanggal'", $fetch="");
        $totalhits        = $database->select($fields="SUM(hits)", $table="statistik", $where_clause="", $fetch="");

        //$tothitsgbr       = mysql_result(mysql_query("SELECT SUM(hits) FROM statistik"), 0);
        //$bataswaktu       = time() - 300;
       // $pengunjungonline = $database->count_rows($fields="statistik", $table="statistik", $where_clause="WHERE online > '$bataswaktu'");
      ?>

          <div class="card">
              <div class="card-header">
                  <h2>Statistic Visitors</h2>
              </div>

              <div class="alert alert-success " role="alert">
                  Selamat Datang, di halaman Admin Panel <?php echo $config['web_name'];?>.
              </div>

              <div class="card-body card-padding">
                  <div class="mini-charts">
                      <div class="row">
                          <div class="col-sm-6 col-md-3">
                              <div class="mini-charts-item bgm-cyan">
                                  <div class="clearfix">
                                      <div class="chart stats-bar"></div>
                                      <div class="count">
                                          <h4 class="putih">Today Views</h4>
                                          <h2><?php if(empty($hits[0])){echo "0";}else{echo $hits[0];}?></h2>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-6 col-md-3">
                              <div class="mini-charts-item bgm-lightgreen">
                                  <div class="clearfix">
                                      <div class="chart stats-bar-2"></div>
                                      <div class="count">
                                          <h4 class="putih">Today Visits</h4>
                                          <h2><?php if(empty($pengunjung)){echo "0";}else{echo $pengunjung;}?></h2>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-6 col-md-3">
                              <div class="mini-charts-item bgm-orange">
                                  <div class="clearfix">
                                      <div class="chart stats-line"></div>
                                      <div class="count">
                                          <h4 class="putih">Total Views</h4>
                                          <h2><?php echo $totalhits[0];?></h2>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-6 col-md-3">
                              <div class="mini-charts-item bgm-bluegray">
                                  <div class="clearfix">
                                      <div class="chart stats-line-2"></div>
                                      <div class="count">
                                          <h4 class="putih">Total Visitor</h4>
                                          <h2><?php echo $totalpengunjung[0];?></h2>
                                      </div>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>

              </div>
          </div>
<?php
  }
}

// Tentang Kami
elseif ($_GET['module']=='pages') {
  if ($_SESSION['level']=='admin') {
    include "modul/mod_pages/pages_v.php";
  }
}

// Portofolio
elseif ($_GET['module']=='portofolio_kategori'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_portofolio_kategori/portofolio_kategori.php";
  }
}
elseif ($_GET['module']=='portofolio'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_portofolio/portofolio.php";
  }
}
elseif ($_GET['module']=='galeri_foto_porto'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_galeri_foto_porto/galeri_foto_porto_v.php";
  }
}

// Produk
elseif ($_GET['module']=='produk_kategori'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_produk_kategori/produk_kategori.php";
  }
}
elseif ($_GET['module']=='produk'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_produk/produk.php";
  }
}

// Blog
elseif ($_GET['module']=='blog'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_blog/blog.php";
  }
}

// Konsultasi
elseif ($_GET['module']=='messages_konsul'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_messages_konsul/messages_konsul_v.php";
  }
}

// Galeri Foto
elseif ($_GET['module']=='galeri_foto'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_galeri_foto/galeri_foto_v.php";
  }
}

// Galeri Video
elseif ($_GET['module']=='galeri_video'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_galeri_video/galeri_video.php";
  }
}

// Testimoni
elseif ($_GET['module']=='testimoni'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_testimoni/testimoni.php";
  }
}

// Kontak
elseif ($_GET['module']=='kontak'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_kontak/kontak.php";
  }
}
elseif ($_GET['module']=='sosmed'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_sosmed/sosmed_v.php";
  }
}
elseif ($_GET['module']=='map'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_map/map_v.php";
  }
}
elseif ($_GET['module']=='messages'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_messages/messages_v.php";
  }
}

// Setting Website
elseif ($_GET['module']=='slideshow'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_slideshow/slideshow_v.php";
  }
}
elseif ($_GET['module']=='menu'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_menu/menu_v.php";
  }
}
elseif ($_GET['module']=='client'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_client/client_v.php";
  }
}

// Setting SEO
elseif ($_GET['module']=='title'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_title/title_v.php";
  }
}
elseif ($_GET['module']=='keyword'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_keyword/keyword_v.php";
  }
}
elseif ($_GET['module']=='description'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_description/description_v.php";
  }
}

// Admin Profile
elseif ($_GET['module']=='profile'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_profile/profile_v.php";
  }
}
elseif ($_GET['module']=='login'){
  if ($_SESSION['level']=='admin'){
    include "modul/mod_profile/login_v.php";
  }
}

elseif ( $_GET['module']=='maps_categories' ){
  if ( $_SESSION['level']=='admin' ){
    include "modul/mod_maps_categories/maps_categories_v.php";
  }
}
elseif ( $_GET['module']=='maps_sijari' ){
  if ( $_SESSION['level']=='admin' ){
    include "modul/mod_maps_sijari/maps_sijari_v.php";
  }
}
else{
  echo "<p><b><center>MODUL BELUM ADA ATAU BELUM LENGKAP</center></b></p>";
}
?>
