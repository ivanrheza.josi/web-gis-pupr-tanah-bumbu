<?php
require_once "config.php";

if(isset($_GET['act'])):
	switch ($_GET['act']) {
		case 'merkGetType':
			$id_merk = $_POST['merkJS'];
			$merkGetType = $database->select($fields="id_type, nama", $table="type", $where_clause="WHERE id_merk='$id_merk' AND status='1'", $fetch="all");
			if(!empty($merkGetType)) {
				echo '<option value="" disabled="disable" selected="select">- Pilih Type -</option>';
				foreach ($merkGetType as $key => $value) {
					echo '<option value="'.$value[id_type].'">'.$value[nama].'</option>';
				}
			}else {
					echo '<option value="" disabled="disable">- Pilih Type -</option>';
			}
        break;

        case 'provinsiGetKabupaten':
			$id_provinsi = $_POST['provinsiJS'];
			$provinsiGetKabupaten = $database->select($fields="id_kabupaten, nama", $table="kabupaten", $where_clause="WHERE id_provinsi='$id_provinsi' AND status='1'", $fetch="all");
			if(!empty($provinsiGetKabupaten)) {
				echo '<option value="" disabled="disable" selected="select">- Pilih Kabupaten -</option>';
				foreach ($provinsiGetKabupaten as $key => $value) {
					echo '<option value="'.$value[id_kabupaten].'">'.$value[nama].'</option>';
				}
			}else {
					echo '<option value="" disabled="disable">- Pilih Kabupaten -</option>';
			}
        break;

		//modul type
		case 'kategoriGetMerk':
			$kategori = $_POST['kat'];
			$kategoriGetMerk = $database->select($fields="id_merk, nama", $table="merk", $where_clause="WHERE kategori='$kategori' AND status='1'", $fetch="all");
			if(!empty($kategoriGetMerk)) {
				echo '<option value="" disabled="disable" selected="select">- Pilih Merk -</option>';
				foreach ($kategoriGetMerk as $key => $value) {
					echo '<option value="'.$value[id_merk].'">'.$value[nama].'</option>';
				}
			}else {
					echo '<option value="" disabled="disable">- Pilih Merk -</option>';
			}
		break;
    }
endif;
