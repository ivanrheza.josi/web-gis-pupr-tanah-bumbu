<?php
  	session_start();

 	// remove all session variables
	session_unset();

	// destroy the session
	session_destroy();
	
  	echo "<script>alert('Anda Telah Keluar dari Halaman Admin Panel'); window.location = 'index.php'</script>";
?>
