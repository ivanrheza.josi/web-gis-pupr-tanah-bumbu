<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
    require_once '../../../josys/db_connect.php';
    include_once '../../../josys/class/Database.php';
    include_once '../../../josys/class/Upload.php';
    include_once '../../../josys/function/Seo.php';

    $database 	= new Database($db);
    $upload     = new Upload();

    $module	= $_GET['module'];
    $act	= $_GET['act'];

if($act=='insert')
{
	// Insert
	if ($module=='news' AND $act=='insert')
	{
		$lokasi_file	= $_FILES['fupload']['tmp_name'];
		$tipe_file 		= $_FILES['fupload']['type'];
	  	$nama_file 		= $_FILES['fupload']['name'];

	  	$nama_seo 		= substr(seo($_POST['title']), 0, 75);
	  	$acak           = rand(000,999);
	  	$nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;

	  	$contents       = stripslashes($_POST['contents']);

		if(!empty($lokasi_file))
		{

			 if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/png"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG atau *.PNG'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

		  	//Proses Upload Image
            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/berita/", $inputName="");

			//Proses Resize / thumbnail Image
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/berita/", $thumbDirectory="../../../joimg/berita/thumbnail/", $thumbWidth="150");

		    //data yang akan di insert berbentuk array
			$form_data = array(
			    "title"          => "$_POST[title]",
                "seo"			 => "$nama_seo",
			    "deskripsi"       => "$contents",
			    "image"          => "$nama_file_unik",
			    "status"		 => "$_POST[status]",
			    "autor"			=>"$_POST[autor]",
			    "subject"			=>"$_POST[subject]"
			);

			//proses insert ke database
            $database->insert($table="berita", $array=$form_data);
			//db_insert($table="berita", $form_data);
		}
		else
		{
			//data yang akan di insert berbentuk array
            $form_data = array(
			    "title"          => "$title",
                "seo"			 => "$nama_seo",
			    "deskripsi"       => "$contents",
			    "status"		 => "$_POST[status]",
			    "autor"			=>"$_POST[autor]",
			    "subject"			=>"$_POST[subject]"
			);

			//proses insert ke database
            $database->insert($table="berita", $array=$form_data);
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='update')
{
	// Update
	if ($module=='news' AND $act=='update')
	{
	 	$lokasi_file   		= $_FILES['fupload']['tmp_name'];
	  	$tipe_file     		= $_FILES['fupload']['type'];
	  	$nama_file     	 	= $_FILES['fupload']['name'];

		$nama_seo			= substr(seo($_POST['title']), 0, 75);
	  	$acak           	= rand(000,999);
	  	$nama_file_unik		= $nama_seo.'-'.$acak.'-'.$nama_file;

        $contents           = stripslashes($_POST['contents']);

		if(!empty($lokasi_file))
		{

			 if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/png"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG atau *.PNG'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

            $show   = $database->select($fields="image", $table="berita", $where_clause="WHERE id_berita = '$_POST[id]'");
			if($show['image'] != '')
			{
				unlink("../../../joimg/berita/$show[image]");
				unlink("../../../joimg/berita/thumbnail/$show[image]");
			}

			//Proses Upload Image
            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/berita/", $inputName="");

			//Proses Resize / thumbnail Image
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/berita/", $thumbDirectory="../../../joimg/berita/thumbnail/", $thumbWidth="150");

	   		//data yang akan diupdate berbentuk array
            $form_data = array(
			    "title"          => "$_POST[title]",
                "seo"			 => "$nama_seo",
			    "deskripsi"       => "$contents",
			    "image"          => "$nama_file_unik",
			    "status"		 => "$_POST[status]",
			     "autor"			=>"$_POST[autor]",
			    "subject"			=>"$_POST[subject]"
			);

			//proses update ke database
			$database->update($table="berita", $array=$form_data, $fields_key="id_berita", $id="$_POST[id]");
		}
		else
		{
			//data yang akan diupdate berbentuk array
            $form_data = array(
			    "title"          => "$_POST[title]",
                "seo"			 => "$nama_seo",
			    "deskripsi"       => "$contents",
			    "status"		 => "$_POST[status]",
			     "autor"			=>"$_POST[autor]",
			    "subject"			=>"$_POST[subject]"
			);

			//proses update ke database
            $database->update($table="berita", $array=$form_data, $fields_key="id_berita", $id="$_POST[id]");
			//db_update($table="berita", $form_data, $where_clause="id_berita = $_POST[id]");
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}


if($act=='delete')
{
	// Delete
	if ($module=='news' AND $act=='delete')
	{
	//	$show	= db_get_one("SELECT * FROM berita WHERE id_berita='$_GET[id]'");
        $show   = $database->select($fields="image", $table="berita", $where_clause="WHERE id_berita = '$_GET[id]'");
        if($show['image'] != '')
        {
            unlink("../../../joimg/berita/$show[image]");
            unlink("../../../joimg/berita/thumbnail/$show[image]");
            $database->delete($table="berita", $fields_key="id_berita", $id="$_GET[id]");
        }
        else
        {
        	$database->delete($table="berita", $fields_key="id_berita", $id="$_GET[id]");
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

}
?>
