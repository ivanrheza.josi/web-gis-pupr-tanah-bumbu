


<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

<?php
    $aksi="modul/mod_video/aksi_video.php?module=video";
    switch($_GET['act']){
    default:
?>

                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Video
                            <a href="?module=video&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                        </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="7%">Video</th>
                                        <th>Judul</th>
                                        <th>DateTime</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                  <?php
                                    function youtube($url){
                                    $link=str_replace('http://www.youtube.com/watch?v=', '', $url);
                                    $link=str_replace('https://www.youtube.com/watch?v=', '', $link);
                                    $data='<object width="260" height="200" data="http://www.youtube.com/v/'.$link.'" type="application/x-shockwave-flash">
                                    <param name="src" value="http://www.youtube.com/v/'.$link.'" />
                                    </object>';
                                    return $data;
                                  }
                                    $no=1;
                                    //$articles = db_get_all("SELECT * FROM articles ORDER BY id_articles DESC");
                                    $articles = $database->select($fields="*",$table="video", $where_clause="ORDER BY id_video DESC", $fetch="all");
                                    foreach ($articles as $key => $value) {
                                ?>

                                    <tr>
                                        <th><?php echo $no; ?></th>
                                        <td> <?php echo youtube("".$value['video']."");?></td>
                                        <td><?php echo $value['judul']; ?></td>
                                        <td><?php echo $value['tgl_posting']; ?></td>
                                        
                                        <td>
                                            <a href="?module=video&act=edit&id=<?php echo $value['id_video'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_video]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "add":
?>

                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Video | Add New</p>
                            <hr class="line">

                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=input">
                                <input type="hidden" name="category" value="<?php echo $_GET['kateg'];?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Judul</label>
                                                <input type="text" name="judul" class="form-control" placeholder="Enter Judul" maxlength="150" required="require">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="form-group">
                                            <div class="fg-line">
                                                <label>Video</label>
                                                <input type="text" name="video" class="form-control" placeholder="Url yang diambil di youtube" required="require">
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                                <br>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id     = $_GET['id'];
    //$value  = db_get_one("SELECT * FROM articles WHERE id_articles='$id'");
    $value  = $database->select($fields="*", $table="video", $where_clause="WHERE id_video = '$id'");
 
      function youtube($url){
      $link=str_replace('http://www.youtube.com/watch?v=', '', $url);
      $link=str_replace('https://www.youtube.com/watch?v=', '', $link);
      $data='<object width="260" height="200" data="http://www.youtube.com/v/'.$link.'" type="application/x-shockwave-flash">
      <param name="src" value="http://www.youtube.com/v/'.$link.'" />
      </object>';
      return $data;
    }
?>
 
  
                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Video | Edit</p>
                            <hr class="line">

                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $id;?>">
                                        <input type="hidden" name="category" value="<?php echo $_GET['kateg'];?>">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label>Judul Video</label>
                                                    <input type="text" name="judul" class="form-control" placeholder="Enter Judul" value="<?php echo $value['judul'];?>" maxlength="128">
                                                </div>
                                            </div>
                                            
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Video</label> <br>
                                                 <?php echo youtube("".$value['video']."");?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Ganti Video</label>
                                                <input type="text" name="video" class="form-control" placeholder="Url yang diambil di youtube"  value='<?php echo $value[video];?>' required="require">
                                            </div>
                                        </div>
                                    </div>
                                 
                                </div>
                                <br>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>

                        </div>
                    </div>

<?php
    break;
    }
}
?>

