<?php
// session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable({
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        } );
    } );
    </script>
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_destination/aksi_destination.php?module=destination";
    if (!isset($_GET['act'])) {
        $_GET['act'] = '';
    }
    switch($_GET['act']){
    default:
?>

                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Destination
                            <a href="?module=destination&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                        </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama</th>
                                        <th>Length</th>
                                        <th>Price</th>
                                        <th>Gambar</th>
                                        <th>Peta</th>
                                        <th>Status</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $posts = $database->select($fields="*", $table="tour", $where_clause="ORDER BY id_tour DESC", $fetch="all");
                                    foreach ($posts as $key => $value) {
                                ?>

                                    <tr>
                                        <th><?php echo $no; ?></th>
                                        <td><?php echo $value['nama']; ?></td>
                                        <td><?php echo $value['length']; ?></td>
                                        <td><?php echo $value['harga']; ?></td>
                                        <td><img src="../joimg/tour/thumbnail/<?php echo $value['image'];?>" alt="" width="50px"></td>
                                        <td><img src="../joimg/peta_tour/thumbnail/<?php echo $value['peta'];?>" alt="" width="50px"></td>
                                        <td><?php if($value['status'] == 'Y'){echo "Publish";}else{echo "Hidden";} ?></td>
                                        <td>
                                            <a href="?module=destination&act=edit&id=<?php echo $value['id_tour'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_tour]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                            <a href="?module=subimg_destination&id=<?php echo $value['id_tour'];?>" class="btn btn-info waves-effect"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Sub Image</a>
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "add":
?>
                   <div class="card">
                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Destination / Add New</p>
                            <hr class="line">

                            <form id="destination-form" method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Judul Destination</label>
                                                <input type="text" name="judul" class="form-control" placeholder="Enter title" maxlength="150" required="require">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Kategori <span class="color-red">*</span></label>
                                                        <div class="select">
                                                            <select class="form-control" name="kategori" required="require">
                                                                <option disabled="disable" selected="select">- Pilih -</option>
                                                                <?php
                                                                    $tk = $database -> select($fields='*', $table='tour_kategori', $where_clause='ORDER BY id_tour_kategori DESC', $fetch='all'); 
                                                                    foreach ($tk as $key => $value_tk) {
                                                                        echo '
                                                                            <option value="'.$value_tk['id_tour_kategori'].'">'.$value_tk['nama'].'</option>
                                                                        ';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Length</label>
                                                        <input type="text" name="length" class="form-control" placeholder="5 Day 1 Night..." maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Price <span class="color-red">*</span></label>
                                                        <input type="text" name="harga" class="form-control" placeholder="Rp. 500 K / Person..." maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Short Description</label>
                                                <textarea name="deskripsi" class="form-control" rows="10" placeholder="Enter short description trip..."></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Overview</label>
                                                <textarea name="konten" class="form-control" rows="10" placeholder="Enter overview trip..."></textarea>
                                            </div>
                                        </div>
                                                                                
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Price Details</label>
                                                <textarea name="konten_harga" class="form-control" rows="10" placeholder="Type Price details..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image <span class="color-red">*</span></label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload" required="require">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Peta <span class="color-red">*</span></label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload_peta" required="require">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Hotel</label>
                                                <select class="selectpicker" multiple="multiple" data-selected-text-format="count" data-style="btn-info" id="multi-hotel" name="id_hotel[]">
                                                <?php
                                                    $hk = $database -> select($fields='*', $table='hotel', $where_clause='ORDER BY id_hotel DESC', $fetch='all'); 
                                                    foreach ($hk as $key => $value_hk) {
                                                        echo '
                                                            <option value="'.$value_hk['id_hotel'].'">'.$value_hk['nm_hotel'].'</option>
                                                        ';
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="Y" checked>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="N">
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <br>
                                <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG *.</div>

                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="tour", $where_clause="WHERE id_tour = '$id'", $fetch="");
    /*$id_author  = $value['id_author'];
    $author     = $database->select($fields="*", $table="author", $where_clause="WHERE id_author = '$id_author'", $fetch="");*/
?>

                   <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Destination / Edit</p>
                            <hr class="line">

                            <form id="destination-form" method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $id;?>">
                                        <input type="hidden" name="category" value="<?php echo $_GET['kateg'];?>">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label>Judul Destination</label>
                                                    <input type="text" name="judul" class="form-control" placeholder="Enter title"  value="<?php echo $value['nama'];?>" maxlength="128">
                                                </div>
                                            </div>
                                            <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Kategori <span class="color-red">*</span></label>
                                                       <div class="select">
                                                            <select class="form-control" name="kategori" required="require">
                                                                <option disabled="disable" selected="select">- Pilih -</option>
                                                                <?php
                                                                    $tk = $database -> select($fields='*', $table='tour_kategori', $where_clause='ORDER BY id_tour_kategori DESC', $fetch='all'); 
                                                                    foreach ($tk as $key => $value_tk) {
                                                                        $selected = ($value['id_tour_kategori'] === $value_tk['id_tour_kategori']) ? 'selected' : '' ;
                                                                        echo '
                                                                            <option value="'.$value_tk['id_tour_kategori'].'" '.$selected.'>'.$value_tk['nama'].'</option>
                                                                        ';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Length</label>
                                                        <input type="text" name="length" class="form-control" placeholder="5 Day 1 Night..." maxlength="150" required="require" value="<?php echo $value['length']; ?>">
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Price <span class="color-red">*</span></label>
                                                        <input type="text" name="harga" class="form-control" placeholder="Rp. 500 K / Person..." maxlength="150" required="require" value="<?php echo $value['harga']; ?>">
                                                    </div>
                                                </div>                                            
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Short Description</label>
                                                <textarea name="deskripsi" class="form-control" rows="10" placeholder="Enter short description trip..."><?php echo $value['deskripsi'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Overview</label>
                                                <textarea name="konten" class="form-control" rows="10" placeholder="Enter overview trip..."><?php echo $value['konten'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Price Details</label>
                                                <textarea name="konten_harga" class="form-control" rows="10" placeholder="Type Price details..."><?php echo $value['konten_harga'];?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <img src="../joimg/tour/thumbnail/<?php echo $value['image'];?>" alt="<?php echo $value['image'];?>" width="300px">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Peta</label>
                                                <br>
                                                <img src="../joimg/peta_tour/thumbnail/<?php echo $value['peta'];?>" alt="<?php echo $value['peta'];?>" width="300px">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload_peta">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Hotel</label>
                                                <select class="selectpicker" multiple="multiple" data-selected-text-format="count" data-style="btn-info"  id="multi-hotel" name="id_hotel[]">
                                                <?php
                                                    $hk = $database -> select($fields='*', $table='hotel', $where_clause='ORDER BY id_hotel DESC', $fetch='all');
                                                    $ex = explode(',', $value['id_hotel']);
                                                    foreach ($hk as $key => $value_hk) {
                                                        $checked = (in_array($value_hk['id_hotel'], $ex)) ? 'selected' : '' ;
                                                        echo '
                                                            <option value="'.$value_hk['id_hotel'].'" '.$checked.'>'.$value_hk['nm_hotel'].'</option>
                                                        ';
                                                    }                                                         
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>

                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="Y" <?php if($value['status'] == 'Y'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="N" <?php if($value['status'] == 'N'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <br>

                                <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. <b>Image Size :</b> 750 x 320 pixels.</div>

                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>

                        </div>
                    </div>
<?php
    break;
    }
}
?>