<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        });
    } );
    </script>
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "#konten",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_produk/aksi_produk.php?module=produk";
    switch($_GET['act']){
    default:
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Produk / Produk
                    <a href="?module=produk&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                </p>
                <hr class="line">
                <div class="table-responsive">
                    <table class='display' id='datatables'>
                        <thead>
                            <tr class="inamakolom">
                                <th width="5%">No</th>
                                <th>Code</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Image</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th width="18%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no=1;
                            $posts = $database->select($fields="*", $table="produk", $where_clause="ORDER BY id_produk DESC", $fetch="all");
                            foreach ($posts as $key => $value) {
                                $posts_cat = $database->select($fields="*", $table="produk_kategori", $where_clause="WHERE id_produk_kategori = '$value[id_produk_kategori]'", $fetch="");
                        ?>
                                <tr>
                                    <th style="text-align: center;"><?php echo $no; ?></th>
                                    <td><?php echo $value['code']; ?></td>
                                    <td><?php echo $value['judul']; ?></td>
                                    <td><?php echo $rupiah->koma_strip($value['price']); ?></td>
                                    <td style="text-align: center;"><img src="../joimg/produk/<?php echo $value['image']; ?>" alt="" height="50px"></td>
                                    <td><?php echo $posts_cat['nama']; ?></td>
                                    <td style="text-align: center;"><?php echo $value['date']; ?></td>
                                    <td style="text-align: center;"><?php if($value['status'] == 'Y'){echo "Publish";}else{echo "Hidden";} ?></td>
                                    <td style="text-align: center;">
                                        <a href="?module=produk&act=edit&id=<?php echo $value['id_produk'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                        <a href="<?php echo "$aksi&act=delete&id=$value[id_produk]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                    </td>
                                </tr>
                        <?php
                                $no++;
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php
    break;
    case "add":
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Produk / Produk / Add New</p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Code</label>
                                    <input type="text" name="code" class="form-control" placeholder="Enter code" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Title</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control" placeholder="Enter price" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Category</label>
                                    <div class="select">
                                        <select class="form-control" name="kategori" required="require">
                                            <option disabled="disable" selected="select">- Select One -</option>
                                            <?php
                                                $tk = $database -> select($fields='*', $table='produk_kategori', $where_clause='ORDER BY id_produk_kategori DESC', $fetch='all'); 
                                                if (!empty($tk)) {
                                                    foreach ($tk as $key => $value_tk) {
                                                        echo '
                                                            <option value="'.$value_tk['id_produk_kategori'].'">'.$value_tk['nama'].'</option>
                                                        ';
                                                    }
                                                } else {
                                                    echo '<option disabled="disable">Oops, it seems no Kategori in database. Try to input it First, Please.</option>';
                                                }
                                                
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Content</label>
                                    <textarea id="konten" name="konten" class="form-control" rows="10" placeholder="Enter konten"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Image</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload" required="require">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-4">
                            <p class="c-black f-500 m-b-20">Publish Post ?</p>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="Y" checked>
                                <i class="input-helper"></i>
                                Yes
                            </label>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="N">
                                <i class="input-helper"></i>
                                No
                            </label>
                        </div>
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="produk", $where_clause="WHERE id_produk = '$id'", $fetch="");
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Produk / Edit</p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                    <div class="row">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Code</label>
                                    <input type="text" name="code" class="form-control" placeholder="Enter code" value="<?php echo $value['code'];?>" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Title</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title" value="<?php echo $value['judul'];?>" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control" placeholder="Enter price" value="<?php echo $value['price'];?>" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Category</label>
                                    <div class="select">
                                        <select class="form-control" name="kategori" required="require">
                                            <option disabled="disable" selected="select">- Select One -</option>
                                            <?php
                                                $tk = $database -> select($fields='*', $table='produk_kategori', $where_clause='ORDER BY id_produk_kategori DESC', $fetch='all');
                                                if (!empty($tk)) {
                                                    foreach ($tk as $key => $value_tk) {
                                                        $selected = ($value['id_produk_kategori'] === $value_tk['id_produk_kategori']) ? 'selected' : '' ;
                                                        echo '
                                                            <option value="'.$value_tk['id_produk_kategori'].'" '.$selected.'>'.$value_tk['nama'].'</option>
                                                        ';
                                                    }
                                                } else {
                                                    echo '<option disabled="disable">Oops, it seems no Kategori in database. Try to input it First, Please.</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Content</label>
                                    <textarea id="konten" name="konten" class="form-control" rows="10" placeholder="Enter konten"><?php echo $value['konten'];?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Image</label>
                                    <br>
                                    <img src="../joimg/produk/<?php echo $value['image'];?>" alt="" height="200px">
                                    <br><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-4">
                            <p class="c-black f-500 m-b-20">Publish Post ?</p>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="Y" <?php if($value['status'] == 'Y'){echo "checked";}?> >
                                <i class="input-helper"></i>
                                Yes
                            </label>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="N" <?php if($value['status'] == 'N'){echo "checked";}?> >
                                <i class="input-helper"></i>
                                No
                            </label>
                        </div>
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
<?php
    break;
    }
}
?>