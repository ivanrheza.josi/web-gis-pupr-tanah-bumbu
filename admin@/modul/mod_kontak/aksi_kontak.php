<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/Seo.php';
include_once '../../../josys/class/Upload.php';

$database 	= new Database($db);
$upload     = new Upload();

$database 	= new Database($db);

$module	= $_GET['module'];
$act	= $_GET['act'];


if($act=='insert')
{
	// Insert
	if ($module=='kontak' AND $act=='insert')
	{
		 if (empty($_POST['kategori'])){
                echo "<script>alert('Data tidak tersimpan! Anda belum memilih kategori . Cek kembali!'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

        $lokasi_file	= $_FILES['fupload']['tmp_name'];
		$tipe_file 		= $_FILES['fupload']['type'];
	  	$nama_file 		= $_FILES['fupload']['name'];

	  	$nama_seo 		= substr(seo($_POST['nama']), 0, 75);
	  	$acak           = rand(000,999);
	  	$nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;
	  	$now = date("Y-m-d h:i:s");

		if(!empty($lokasi_file))
		{

			if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
				echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
				die();
			}

            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/banner/");

		    //data yang akan di insert berbentuk array
			$form_data = array(
			    "nama"  		=> "$_POST[nama]",
			    "judul"			=> "$_POST[title]",
	            "deskripsi"     => "$_POST[deskripsi]",
	            "tgl_posting" 	=> "$now",
	            "jenis"  		=> "$_POST[kategori]",
			    "image" 		=> "$nama_file_unik"
			);

			//proses insert ke database
            $database->insert($table="kontak", $array=$form_data);
		}else{
			//data yang akan di insert berbentuk array
			$form_data = array(
			    "nama"  		=> "$_POST[nama]",
			    "judul"			=> "$_POST[title]",
	            "deskripsi"     => "$_POST[deskripsi]",
	            "tgl_posting" 	=> "$now",
	            "jenis"  		=> "$_POST[kategori]"
			   
			);

	        //proses insert ke database
	        $database->insert($table="kontak", $array=$form_data);
		}

	  	//$nama_seo 		= substr(seo($_POST['nama']), 0, 75);
	  	

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='update')
{
	// Update
	if ($module=='kontak' AND $act=='update')
	{
		 // if (empty($_POST['kategori'])){
   //              echo "<script>alert('Data tidak tersimpan! Anda belum memilih kategori . Cek kembali!'); window.location = '../../media.php?module=$module';</script>";
   //              die();
   //          }
    //     $lokasi_file    = $_FILES['fupload']['tmp_name'];
	  	// $tipe_file      = $_FILES['fupload']['type'];
	  	// $nama_file      = $_FILES['fupload']['name'];

		// $nama_seo		= substr(seo($_POST['nama']), 0, 50);
	 //  	$acak           = rand(000,999);
	 //  	$nama_file_unik	= $nama_seo.'-'.$acak.'-'.$nama_file;
	  	$now = date("Y-m-d h:i:s");


		if(!empty($lokasi_file))
		{

			if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
				echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
				die();
			}

            $show   = $database->select($fields="image", $table="kontak", $where_clause="WHERE id_kontak = '$_POST[id]'", $fetch='');
			if($show['image'] != '')
			{
				unlink("../../../joimg/banner/$show[image]");
			}

            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/banner/");

            //data yang akan diupdate berbentuk array
			$form_data = array(
				"nama"  		=> "$_POST[nama]",
			    "judul"			=> "$_POST[title]",
	            "deskripsi"     => "$_POST[deskripsi]",
	            "tgl_posting" 	=> "$now",
	            "jenis"  		=> "$_POST[kategori]",
			    "image" 		=> "$nama_file_unik"
			);

			//proses update ke database
            $database->update($table="kontak", $array=$form_data, $fields_key="id_kontak", $id="$_POST[id]");
		}else{
			
			//data yang akan diupdate berbentuk array
			$form_data = array(
	           // "nama"  			=> "$_POST[nama]",
			    "judul"			=> "$_POST[title]",
	            // "deskripsi"     => "$_POST[deskripsi]",
	            "tgl_posting" 	=> "$now",
	            // "jenis"  		=> "$_POST[kategori]",
			);

			//proses update ke database
	        $database->update($table="kontak", $array=$form_data, $fields_key="id_kontak", $id="$_POST[id]");
		}


		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}


if($act=='delete')
{
	// Delete
	if ($module=='kontak' AND $act=='delete')
	{
		$show   = $database->select($fields="image", $table="kontak", $where_clause="WHERE id_kontak = '$_GET[id]'", $fetch='');
		if($show['image'] != '')
		{
			unlink("../../../joimg/banner/$show[image]");
            $database->delete($table="kontak", $fields_key="id_kontak", $id="$_GET[id]");
		}
		else
		{
             $database->delete($table="kontak", $fields_key="id_kontak", $id="$_GET[id]");
		}
       
        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";    
    }
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

}
?>
