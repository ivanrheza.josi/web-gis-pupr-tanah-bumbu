<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->

    <script src="assets/vendors/input-mask/input-mask.min.js"></script>

<?php
    $aksi="modul/mod_kontak/aksi_kontak.php?module=kontak";
    switch($_GET['act']){
    default:
    // include_once '../josys/class/Rupiah.php';
?>

                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Kontak / Detail Kontak
                            <!-- <a href="?module=kontak&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a> -->
                        </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr class="inamakolom">
                                        <th width="5%">No</th>
                                        <!-- <th>Image</th> -->
                                        <th>Title </th>
                                        <th>Content</th>
                                        <!-- <th>Kategori</th> -->
                                        <!-- <th>tgl</th> -->
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $promo = $database->select($fields="*", $table="kontak", $where_clause="ORDER BY tgl_posting DESC", $fetch="all");
                                    foreach ($promo as $key => $value) {
                                ?>

                                    <tr>
                                        <th style="text-align: center;"><?php echo $no; ?></th>
                                         <!-- <td><img src="../joimg/banner/<?php //echo $value['image'];?>" alt="" width="50px"></td> -->
                                        <td style="text-align: center;"><?php echo $value['nama'];?></td>
                                         <td><?php echo $value['judul'];?></td>
                                        <!-- <td><?php //echo $value['jenis']; ?></td> -->
                                        <!-- <td><?php //echo $value['tgl_posting']; ?></td> -->
                                        <td style="text-align: center;">
                                            <a href="?module=kontak&act=edit&id=<?php echo $value['id_kontak'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                            <!-- <a href="<?php echo "$aksi&act=delete&id=$value[id_kontak]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a> -->
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "add":
?>
                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Setting Contack / Contact / Add New</p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Nama</label>
                                                <input type="text" name="nama" class="form-control" placeholder="masukkan nama kontak" maxlength="50" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Konten</label>
                                                <input type="text" name="title" class="form-control input-mask" placeholder="masukkan konten" maxlength="150" required="require" >
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-3">
                                        <div class="form-group">
                                                <div class="fg-line">
                                                    <label>Kategori <span class="color-red">*</span></label>
                                                   <div class="select">
                                                        <select class="form-control" name="kategori" required="require">
                                                            <option disabled="disable" selected="select">- Pilih -</option>
                                                            <?php
                                                            $getKategori = $database->get_enum($table="kontak", $fields="jenis");
                                                            foreach ($getKategori as $key_getKat => $value_getKat) {
                                                                echo '
                                                                <option value="'.$value_getKat.'">'.$value_getKat.'</option>
                                                                ';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <!-- <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Deskripsi</label>
                                                <textarea name="deskripsi" class="form-control" rows="5" placeholder="deskripsi kontak"></textarea>
                                            </div>
                                        </div>
                                    </div> -->
                                    <input type="hidden" name="deskripsi">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id     = $_GET['id'];
    $value  = $database->select($fields="*", $table="kontak", $where_clause="WHERE id_kontak = '$id'", $fetch='');
?>

                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Kontak / Detail Kontak / Edit</p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">

                                <input type="hidden" name="id" value="<?php echo $id;?>">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Name</label>
                                                <input type="text" name="nama" class="form-control" placeholder="masukkan nama kontak" maxlength="50" value="<?php echo $value['nama'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Konten</label>
                                                <input type="text" name="title" class="form-control input-mask" placeholder="masukkan konten" maxlength="150" required="require"  value="<?php echo $value['judul'];?>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Kategori <span class="color-red">*</span></label>
                                            <div class="select">
                                                <select class="form-control" name="kategori" required="require">
                                                    <option disabled="disable" selected="select">- Pilih -</option>
                                                    <?php
                                                    // $getKategori = $database->get_enum($table="kontak", $fields="jenis");
                                                    // foreach ($getKategori as $key_getKat => $value_getKat) {
                                                    //     $s = ($value_getKat == $value['jenis']) ? 'selected' : '' ;
                                                    //     echo '
                                                    //     <option value="'.$value_getKat.'" '.$s.'>'.$value_getKat.'</option>
                                                    //     ';
                                                    // }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-sm-8">
                                       <div class="form-group">
                                            <div class="fg-line">
                                                <label>Deskripsi</label>
                                                <textarea name="deskripsi" class="form-control" rows="5" placeholder="deskripsi kontak"><?php //echo $value['deskripsi'];?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <input type="hidden" name="deskripsi">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <img src="../joimg/banner/<?php //echo $value['image'];?>" alt="" width="60px">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    }
}
?>
