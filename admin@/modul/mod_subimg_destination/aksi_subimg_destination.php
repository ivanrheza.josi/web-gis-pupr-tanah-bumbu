<?php
session_start();
error_reporting(0);
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/class/Upload.php';
include_once '../../../josys/class/Security.php';
include_once '../../../josys/function/Seo.php';
include_once '../../../josys/watermark/watermark.php';

$database   = new Database($db);
$upload     = new Upload();
$security   = new Security();

$module = $_GET['module'];
$act    = $_GET['act'];

// echo $module.'-'.$act.'-'.$_POST['id_tour'];die();
if($act=='insert')
{
    // Insert
    if ($module=='subimg_destination' AND $act=='insert')
    {
       /* if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg"){
            echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG'); window.location = '../../media.php?module=$module';</script>";
            die();
        }*/

        //$nama_seo       = substr(seo($_POST['judul']), 0, 75);
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
            if((!empty($lokasi_file[0])) || ($lokasi_file[0] != ''))//(!empty($lokasi_file)) {
            { 
               $id_tour = $_POST['id_tour'];
                //====MULTI UPLOAD IMAGE
                $valid_formats = array("jpg");
                //$max_file_size = 1024*100; //100 kb
                $path = "../../../joimg/subimg_destination/"; // Upload directory

                if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
                // Loop $_FILES to execute all files
                    foreach ($_FILES['fupload']['name'] as $f => $name) {
                        if ($_FILES['fupload']['error'][$f] == 4) {
                            continue; // Skip file if any error found
                        }

                        if ($_FILES['fupload']['error'][$f] == 0) {
                            if( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
                                $message[] = "$name is not a valid format";
                                continue; // Skip invalid file formats
                            } else {
                                // No error found! Move uploaded files
                                $nama_file         = $_FILES['fupload']['name'][$f];
                                $acak              = rand(000,999);
                                $nama_file_unik    = $id_tour.'-'.$acak.'-'.$nama_file;

                                //proses upload gambar
                                if(move_uploaded_file($_FILES["fupload"]["tmp_name"][$f], $path.$nama_file_unik)) {

                                    //watermak image
                                    //watermark_image("../../../joimg/subimg_destination/".$nama_file_unik,"../../../joimg/subimg_destination/".$nama_file_unik);

                                    $vfile_upload = $path . $nama_file_unik;

                                    //identitas file asli
                                    $im_src = imagecreatefromjpeg($vfile_upload);
                                    $src_width = imageSX($im_src);
                                    $src_height = imageSY($im_src);

                                    //Set ukuran gambar hasil perubahan
                                    $dst_width = 600;
                                    $dst_height = ($dst_width/$src_width)*$src_height;

                                    //proses perubahan ukuran
                                    $im = imagecreatetruecolor($dst_width,$dst_height);
                                    imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

                                    //Simpan gambar thumbnail
                                    imagejpeg($im,"../../../joimg/subimg_destination/thumbnail/".$nama_file_unik);

                                    //Hapus gambar di memori komputer
                                    imagedestroy($im_src);
                                    imagedestroy($im);

                                    //insert banyak gambar ke database produkgambar
                                    $array = array(
                                        'id_tour'  => $id_tour,
                                        'image'     => $nama_file_unik
                                    );
                                    $database->insert($table="subimg_destination", $array);
                                }
                            }
                        }
                    }
                }
                //END MULTI UPLOAD IMAGE
            }
        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$id_tour';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$id_tour';</script>";
    }
}
if($act == 'update_image') {
    // Update Image
    if (($module=='subimg_destination') AND ($act=='update_image'))
    {
        //$module = "subimg_destination&act=edit&id=$_POST[id_tour]";

        $lokasi_file    = $_FILES['fupload_name']['tmp_name'];
        $tipe_file      = $_FILES['fupload_name']['type'];
        $nama_file      = $_FILES['fupload_name']['name'];
       /* $ukuran_file = $_FILES["fupload_name"]["size"];
        $ukuran = 6000000; //ukuran gambar 2MB*/

        //$nama_seo       = substr(seo($_POST['judul']), 0, 75);
        $acak           = rand(000,999);
        $nama_file_unik = $_POST["id_tour"].'-'.$acak.'-'.$nama_file;

        if(!empty($lokasi_file))
        {
            /*if ($ukuran_file >= $ukuran){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan ukuran image maksimal 2MB'); window.location = '../../media.php?module=$module';</script>";
                die();
            }*/
            /*    if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG'); window.location = '../../media.php?module=$module';</script>";
                die();
            }*/
           

            $show   = $database->select($fields="image", $table="subimg_destination", $where_clause="WHERE id_sub = '$_POST[id_sub]'");
            if($show['image'] != '')
            {
                unlink("../../../joimg/subimg_destination/$show[image]");
                unlink("../../../joimg/subimg_destination/thumbnail/$show[image]");
            }

            //Proses Upload Image
            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/subimg_destination/", $inputName="fupload_name");                      

            //watermak
            //watermark_image("../../../joimg/subimg_destination/".$nama_file_unik,"../../../joimg/subimg_destination/".$nama_file_unik);

            //Proses Resize Image
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory='../../../joimg/subimg_destination/', $thumbDirectory='../../../joimg/subimg_destination/thumbnail/', $thumbWidth='600');

            //update gambar ke database produkgambar
            $array = array(
                'image'     => $nama_file_unik
            );
            $database->update($table="subimg_destination", $array, $fields_key="id_sub", $id=$_POST['id_sub']);

            echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$_POST[id_tour]';</script>";
        }else {
            echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$_POST[id_tour]';</script>";
        }
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$_POST[id_tour]';</script>";
    }
}

if($act == 'delete_image') {
    // Delete Image
    if ($module=='subimg_destination' AND $act=='delete_image')
    {
        //$module = "subimg_destination&act=edit&id=$_GET[id_tour]";
        $show   = $database->select($fields="image", $table="subimg_destination", $where_clause="WHERE id_sub = '$_GET[id_sub]'");
        if($show['image'] != '')
        {
            unlink("../../../joimg/subimg_destination/$show[image]");
            unlink("../../../joimg/subimg_destination/thumbnail/$show[image]");
            $database->delete($table="subimg_destination", $fields_key="id_sub", $id=$_GET['id_sub']);
        }
        else
        {
            $database->delete($table="subimg_destination", $fields_key="id_sub", $id=$_GET['id_sub']);
        }
        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module&id=$_GET[id_tour]';</script>";
    }
    else
    {
        //$module = "subimg_destination&act=edit&id=$_GET[id_tour]";
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$_GET[id_tour]';</script>";
    }
}

if($act=='update')
{
    // Update
    if ($module=='subimg_destination' AND $act=='update')
    {
        $module = "subimg_destination&act=edit&id=$_POST[id_tour]";

        //$nama_seo        = substr(seo($_POST['judul']), 0, 75);
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
        echo $lokasi_file[0];
      

        if((!empty($lokasi_file[0])) || ($lokasi_file[0] != '')) { //!empty(lokasi_file);
            //proses insert data ke table author
            $lokasi_file      = $_FILES['fupload']['tmp_name'];
            $tipe_file        = $_FILES['fupload']['type'];

            //====MULTI UPLOAD IMAGE
            $valid_formats = array("image/jpg","image/png","image/jpeg","image/gif");
            //$max_file_size = 1024*100; //100 kb
            $path = "../../../joimg/subimg_destination/"; // Upload directory

            if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
            // Loop $_FILES to execute all files
                foreach ($_FILES['fupload']['name'] as $f => $name) {
                    // if ($_FILES['fupload']['error'][$f] == 4) {
                    //     continue; // Skip file if any error found
                    // }

                    if ($_FILES['fupload']['error'][$f] == 0) {
                        if( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
                            $message[] = "$name is not a valid format";
                            continue; // Skip invalid file formats
                        } else {
                            // No error found! Move uploaded files
                            $nama_file         = $_FILES['fupload']['name'][$f];
                            $acak              = rand(000,999);
                            $nama_file_unik    = $_POST["id_tour"].'-'.$acak.'-'.$nama_file;

                            //proses upload gambar
                            if(move_uploaded_file($_FILES["fupload"]["tmp_name"][$f], $path.$nama_file_unik)) {

                                $vfile_upload = $path . $nama_file_unik;

                                //identitas file asli
                                $im_src = imagecreatefromjpeg($vfile_upload);
                                $src_width = imageSX($im_src);
                                $src_height = imageSY($im_src);

                                //Set ukuran gambar hasil perubahan
                                $dst_width = 600;
                                $dst_height = ($dst_width/$src_width)*$src_height;

                                //proses perubahan ukuran
                                $im = imagecreatetruecolor($dst_width,$dst_height);
                                imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

                                //Simpan gambar thumbnail
                                imagejpeg($im,"../../../joimg/subimg_destination/thumbnail/".$nama_file_unik);

                                //Hapus gambar di memori komputer
                                imagedestroy($im_src);
                                imagedestroy($im);

                                //insert banyak gambar ke database produkgambar
                                $array = array(
                                    'id_tour'  => $_POST["id_tour"],
                                    'image'     => $nama_file_unik
                                );
                                $database->insert($table="subimg_destination", $array);
                            }
                        }
                    }
                }
            }
            //END MULTI UPLOAD IMAGE
        }
        else
        {
           
            $data_posts = array(
                "judul"         => "$_POST[judul]",
                "seo"           => "$nama_seo",
                "status"        => "$_POST[status]"
            );
            $database->update($table="subimg_destination", $array = $data_posts, $fields_key="id_tour", $id=$_POST['id_tour']);
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=subimg_destination';</script>";
    }
    else
    {
        $module = "subimg_destination&act=edit&id=$_POST[id_tour]";
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}


if($act == 'delete') {
    // Delete Data
    if ($module=='subimg_destination' AND $act=='delete')
    {
        $show_img   = $database->select($fields="*", $table="subimg_destination", $where_clause="WHERE id_tour = '$_GET[id]'", $fetch="all");
        if((!empty($show_img)) || ($show_img != '0')) {
            foreach ($show_img as $key_si => $value_si) {
                if($value_si['image'] != '')
                {
                    unlink("../../../joimg/subimg_destination/$value_si[image]");
                    unlink("../../../joimg/subimg_destination/thumbnail/$value_si[image]");
                    $database->delete($table="subimg_destination", $fields_key="id_sub", $id=$value_si['id_sub']);
                }
                else
                {
                    $database->delete($table="subimg_destination", $fields_key="id_sub", $id=$value_si['id_sub']);
                }
            }
        }
        
       
        //delete data in table posts
        $database->delete($table="subimg_destination", $fields_key="id_tour", $id=$_GET['id']);
        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

}
?>
