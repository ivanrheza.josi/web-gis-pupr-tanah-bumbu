<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
    $module = $_GET['module'];
    if ($module === 'subimg_destination') {
                
    ?>
        <script src="assets/vendors/input-mask/input-mask.min.js"></script>
        <!--Begin DataTables-->
        <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
        <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
        <script>
        $(document).ready(function() {
            $('#datatables').DataTable();
        } );
        </script>

        <style>
        .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
            margin-left: 0px;
        }
        </style>
       
        <?php
            $aksi="modul/mod_subimg_destination/aksi_subimg_destination.php?module=subimg_destination";
            
            $id = $_GET['id'];
            $nm_k = $database->select($fields="nama", $table="tour", $where_clause="WHERE id_tour=$id", $fetch="");
        ?>

        <div class="card">

            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Sub Image Destination  / <?php echo $nm_k['nama']; ?></p>
                <hr class="line">

                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                    <input type="hidden" name="id_tour" value="<?php echo $id;?>">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="c-black f-500 m-b-20">Upload Foto <span class="color-red">*</span> <small>Tambah foto</small></p>
                        </div>
                        <?php
                        $posts_image = $database->select($fields="*", $table="subimg_destination", $where_clause="WHERE id_tour = '$_GET[id]'", $fetch="all");
                        foreach ($posts_image as $key_pi => $value_pi) {
                        echo '
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                    <img src="../joimg/subimg_destination/thumbnail/'.$value_pi["image"].'" style="width:100%; height:150px;">
                                </div><br>
                                <a data-toggle="modal" href="#change_image'.$value_pi["id_sub"].'" class="btn btn-sm bgm-cyan waves-effect">Change</a>
                                ';?>
                                <a href="<?php echo "$aksi&act=delete_image&id_tour=$_GET[id]&id_sub=$value_pi[id_sub]";?>" class="btn btn-sm btn-danger waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');">Remove</a>
                                <?php echo '
                            </div>
                        </div>
                        ';
                        }

                        $count_image = $database->count_rows($table="subimg_destination", $where_clause="WHERE id_tour = '$_GET[id]'");
                        for($no_fupload = 1; $no_fupload <= (1); $no_fupload++) {
                        echo '
                        <div class="col-md-3">
                           
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="fupload[]" required>
                                    </span>
                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>

                        </div>
                        ';
                     }
                        ?>
                    </div>
                    <hr class="garis">
                    <br>
                    <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. | Ukuran Max 1 MB</div>

                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>

                </form>
            </div>
        </div>

        <?php
            $posts_image_modal = $database->select($fields="id_sub, image", $table="subimg_destination", $where_clause="WHERE id_tour = '$_GET[id]'", $fetch="all");
            foreach ($posts_image_modal as $key_pim => $value_pim) {
            echo '
            <!-- Modal Update Image -->
            <div class="modal fade" id="change_image'.$value_pim["id_sub"].'" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="text-align: center;">
                        <form method="post" enctype="multipart/form-data" action="'.$aksi.'&act=update_image">
                            <input type="hidden" name="id_sub" value="'.$value_pim["id_sub"].'">
                            <input type="hidden" name="id_tour" value="'.$_GET["id"].'">
                            <div class="modal-header" style="border-bottom: 1px solid #9C27B0;">
                                <h4 class="modal-title">'.$value_pim["image"].'</h4>
                            </div>
                            <div class="modal-body">
                                <br>
                                <div class="alert alert-info alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. <b>Ukuran Max :</b> 1 MB.
                                </div>
                                <div class="fileinput fileinput-new" data-provides="fileinput" style="padding-right: initial;">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                        <img src="../joimg/subimg_destination/thumbnail/'.$value_pim["image"].'" style="width:100%; height:150px;">
                                    </div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload_name" required>
                                        </span>
                                        <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="border-top: 1px solid #9C27B0;">
                                <button type="submit" class="btn btn-primary">Save changes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal Update Image -->
            ';
            }
    } else {
            echo "Modul Tidak ditemukan";
        }
}
?>
