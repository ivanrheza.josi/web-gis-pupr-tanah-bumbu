<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>

    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "#konten",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_galeri_video/aksi_galeri_video.php?module=galeri_video";
    switch($_GET['act']){
    default:
?>
        <div class="card">
            <div class="card-body card-padding">

                <p class="c-black f-500 m-b-20">Galeri / Galeri Video
                    <a href="?module=galeri_video&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                </p>

                <hr class="line">

                <div class="table-responsive">
                    <table class='display' id='datatables'>
                        <thead>
                            <tr class="inamakolom">
                                <th width="5%">No</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Youtube Video ID</th>
                                <th width="18%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no=1;
                            $posts = $database->select($fields="*", $table="galeri_video", $where_clause="ORDER BY id_galeri_video DESC", $fetch="all");
                            foreach ($posts as $key => $value) {
                                $yt_link        = "https://www.youtube.com/watch?v=".$value['nama'];
                                $yt_title       = youtube_data($yt_link,'title');
                                $yt_thumb       = youtube_data($yt_link,'thumbnail_url');
                                $yt_thumbnail   = str_replace("default", "mqdefault", $yt_thumb);
                        ?>
                                <tr>
                                    <th><?php echo $no; ?></th>
                                    <td style="text-align: center;"><img src="<?php echo $yt_thumbnail; ?>" height="100px"></td>
                                    <td><?php echo $yt_title; ?></td>
                                    <td><?php echo $value['nama']; ?></td>
                                    <td style="text-align: center;">
                                        <a href="?module=galeri_video&act=edit&id=<?php echo $value['id_galeri_video'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                        <a href="<?php echo $aksi.'&act=delete&id='.$value['id_galeri_video'];?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                    </td>
                                </tr>
                        <?php
                                $no++;
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php
    break;
    case "add":
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Galeri / Galeri Video / Add New</p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                    <input type="hidden" name="category" value="<?php echo $_GET['kateg'];?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Youtube Video ID</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert">
                                <b>Attention!</b><p style="font-size: 13px;"><b>Contoh link url video youtube:</b> https://www.youtube.com/watch?v=026OeYh7Q04<br>
                                <b>ID Video:</b> 026OeYh7Q04</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="galeri_video", $where_clause="WHERE id_galeri_video = '$id'", $fetch="");
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Kategori Produk / Edit <?php echo $value['nama'];?></p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Title</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title"  value="<?php echo $value['nama'];?>" maxlength="128">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
<?php
    break;
    }
}
?>