<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

<?php
    $aksi="modul/mod_testimoni/aksi_testimoni.php?module=testimoni";
    switch($_GET['act']){
    default:
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Testimoni
                    <a href="?module=testimoni&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                </p>
                <hr class="line">
                <div class="table-responsive">
                    <table class='display' id='datatables'>
                        <thead>
                            <tr class="inamakolom">
                                <th width="5%">No</th>
                                <th width="7%">Image</th>
                                <th>Company</th>
                                <th>Comment</th>
                                <th>Datetime</th>
                                <th>Status</th>
                                <th width="18%">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        <?php
                            $no=1;
                            //$articles = db_get_all("SELECT * FROM articles ORDER BY id_articles DESC");
                            $articles = $database->select($fields="*",$table="tb_testimoni", $where_clause="ORDER BY id_testimoni DESC", $fetch="all");
                            foreach ($articles as $key => $value) {
                        ?>

                            <tr>
                                <th style="text-align: center;"><?php echo $no; ?></th>
                                <td><img src="../joimg/testimoni/<?php echo $value['image'];?>" alt="" width="100%"></td>
                                
                                 <td><?php echo $value['company']; ?></td>
                                <td><?php if(strlen($value["comment"]) <= "85"){echo $value["comment"];}else{echo substr($value["comment"], 0, 85); echo '...';} ?></td>
                                <td style="text-align: center;"><?php echo $value['dateTime']; ?></td>
                                <td style="text-align: center;"><?php if($value['status'] == '1'){echo "Publish";}else{echo "Hidden";} ?></td>
                                <td style="text-align: center;">
                                    <a href="?module=testimoni&act=edit&id=<?php echo $value['id_testimoni'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                    <a href="<?php echo "$aksi&act=delete&id=$value[id_testimoni]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                </td>
                            </tr>
                        <?php
                            $no++;
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php
    break;
    case "add":
?>
                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Testimoni / Add New</p>
                            <hr class="line">

                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                                <div class="fg-line">
                                                    <label>Name</label>
                                                    <input type="text" name="nama" class="form-control" placeholder="Enter Name" maxlength="128" autofocus>
                                                </div>
                                            </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Company</label>
                                                <input type="text" name="title" class="form-control" placeholder="Enter Company" maxlength="150" required="require">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="fg-line">
                                                <label>Website</label>
                                                <input type="text" name="website" class="form-control" placeholder="Enter Url Website" maxlength="150" required="require">
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Comment</label>
                                                <textarea name="contents" class="form-control" rows="5" placeholder="Enter description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload" required="require">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type:</b> JPG, JPEG dan PNG. <b>Image Size:</b> 512 x 512 pixels.</p></div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="1" checked>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="0">
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div> -->
                                </div>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id     = $_GET['id'];
    $value  = $database->select($fields="*", $table="tb_testimoni", $where_clause="WHERE id_testimoni = '$id'");
?>

                    <div class="card">
                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Testimoni / Edit</p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $id;?>">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Name</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Enter Name" value="<?php echo $value['nama'];?>" maxlength="128">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Company</label>
                                                <input type="text" name="title" class="form-control" placeholder="Enter Company" value="<?php echo $value['company'];?>" maxlength="128">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="fg-line">
                                                <label>Website</label>
                                                <input type="text" name="website" class="form-control" placeholder="Enter Url Website" value="<?php //echo $value['website'];?>" maxlength="128">
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Comment</label>
                                                <textarea name="contents" class="form-control" rows="5" placeholder="Enter description"><?php echo $value['comment'];?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <img src="../joimg/testimoni/<?php echo $value['image'];?>" alt="" height="300px">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type:</b> JPG, JPEG dan PNG. <b>Image Size:</b> 512 x 512 pixels.</p></div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>

                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="1" <?php //if($value['status'] == '1'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="0" <?php //if($value['status'] == '0'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div> -->
                                </div>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>

                        </div>
                    </div>

<?php
    break;
    }
}
?>
