<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
    require_once '../../../josys/db_connect.php';
    include_once '../../../josys/class/Database.php';
    include_once '../../../josys/function/ImageResizeSinur.php';

    $database 	= new Database($db);

    $module	= $_GET['module'];
    $act	= $_GET['act'];

if($act=='insert')
{
	// Insert
	if ($module=='testimoni' AND $act=='insert')
	{
		$lokasi_file	= $_FILES['fupload']['tmp_name'];
	  	$contents       = stripslashes($_POST['contents']);

		if(!empty($lokasi_file))
		{
			$image_name = img_resize($_FILES['fupload'],512,'../../../joimg/testimoni/','akustik-ruang');

		    //data yang akan di insert berbentuk array
			$form_data = array(
			    "nama"      => "$_POST[nama]",
			    "company"	=> "$_POST[title]",
			    "comment"   => "$contents",
			    "image"     => "$image_name"
			);

			//proses insert ke database
            $database->insert($table="tb_testimoni", $array=$form_data);
			//db_insert($table="articles", $form_data);
		}
		else
		{
			//data yang akan di insert berbentuk array
            $form_data = array(
			    "nama"      => "$_POST[nama]",
			    "company"	=> "$_POST[title]",
			    "comment"   => "$contents"
			);

			//proses insert ke database
            $database->insert($table="tb_testimoni", $array=$form_data);
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='update')
{
	// Update
	if ($module=='testimoni' AND $act=='update')
	{
	 	$lokasi_file   		= $_FILES['fupload']['tmp_name'];
        $contents           = stripslashes($_POST['contents']);

		if(!empty($lokasi_file))
		{
            $show   = $database->select($fields="image", $table="tb_testimoni", $where_clause="WHERE id_testimoni = '$_POST[id]'");
			if($show['image'] != '')
			{
				unlink("../../../joimg/testimoni/$show[image]");
			}

			$image_name = img_resize($_FILES['fupload'],512,'../../../joimg/testimoni/','akustik-ruang');

	   		//data yang akan diupdate berbentuk array
            $form_data = array(
				"nama"      => "$_POST[nama]",
			    "company"	=> "$_POST[title]",
			    "comment"   => "$contents",
			    "image"     => "$image_name"
			);

			//proses update ke database
			$database->update($table="tb_testimoni", $array=$form_data, $fields_key="id_testimoni", $id="$_POST[id]");
		}
		else
		{
			//data yang akan diupdate berbentuk array
            $form_data = array(
				"nama"      => "$_POST[nama]",
			    "company"	=> "$_POST[title]",
			    "comment"   => "$contents"
			);

			//proses update ke database
            $database->update($table="tb_testimoni", $array=$form_data, $fields_key="id_testimoni", $id="$_POST[id]");
			//db_update($table="articles", $form_data, $where_clause="id_articles = $_POST[id]");
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='delete')
{
	// Delete
	if ($module=='testimoni' AND $act=='delete')
	{
	//	$show	= db_get_one("SELECT * FROM articles WHERE id_articles='$_GET[id]'");
        $show   = $database->select($fields="image", $table="tb_testimoni", $where_clause="WHERE id_testimoni = '$_GET[id]'");
        if($show['image'] != '')
        {
            unlink("../../../joimg/testimoni/$show[image]");
            $database->delete($table="tb_testimoni", $fields_key="id_testimoni", $id="$_GET[id]");
        }
        else
        {
        	$database->delete($table="tb_testimoni", $fields_key="id_testimoni", $id="$_GET[id]");
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

}
?>
