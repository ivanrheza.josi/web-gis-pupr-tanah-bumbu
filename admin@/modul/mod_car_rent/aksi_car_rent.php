<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/class/Upload.php';
include_once '../../../josys/function/Seo.php';

$database   = new Database($db);
$upload     = new Upload();

$module = $_GET['module'];
$act    = $_GET['act'];

if ($module=='car_rent' AND $act=='updatedesc')
{
    $id         = $_POST['id'];
    $content    = stripslashes($_POST['content']);
    //data yang akan diupdate berbentuk array
    $form_data  = array(
        "static_content" => "$content"
    );
    //proses update ke database
    $database->update($table="modul", $array=$form_data, $fields_key="id_modul", $id=$id);
    echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$id';</script>";
}

if($act=='insert')
{
    // Insert
    if ($module=='car_rent' AND $act=='insert')
    {
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
        $tipe_file      = $_FILES['fupload']['type'];
        $nama_file      = $_FILES['fupload']['name'];

        $nama_seo       = substr(seo($_POST['nama']), 0, 75);
        $acak           = rand(000,999);
        $nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;

        if(!empty($lokasi_file))
        {

            if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/car_rent/");
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/car_rent", $thumbDirectory="../../../joimg/car_rent/thumbnail", $thumbWidth="500");

            //data yang akan di insert berbentuk array
            $form_data = array(
                "nama"      => "$_POST[nama]",
                "seo"       => "$nama_seo",
                // "deskripsi" => "$_POST[deskripsi]",
                "harga1"      => "$_POST[harga1]",
                "harga2"      => "$_POST[harga2]",
                "harga3"      => "$_POST[harga3]",
                "harga4"      => "$_POST[harga4]",
                "gambar"    => "$nama_file_unik",
                "status"    => "$_POST[status]"
            );

            //proses insert ke database
            $database->insert($table="car_rent", $array=$form_data);
        }
        else
        {
            //data yang akan di insert berbentuk array
            $form_data = array(
                "nama"      => "$_POST[nama]",
                "seo"       => "$nama_seo",
                // "deskripsi" => "$_POST[deskripsi]",
                "harga1"      => "$_POST[harga1]",
                "harga2"      => "$_POST[harga2]",
                "harga3"      => "$_POST[harga3]",
                "harga4"      => "$_POST[harga4]",
                "status"    => "$_POST[status]"
            );

            //proses insert ke database
            $database->insert($table="car_rent", $array=$form_data);
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

if($act=='update')
{
    // Update
    if ($module=='car_rent' AND $act=='update')
    {
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
        $tipe_file      = $_FILES['fupload']['type'];
        $nama_file      = $_FILES['fupload']['name'];

        $nama_seo       = substr(seo($_POST['nama']), 0, 50);
        $acak           = rand(000,999);
        $nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;

        if(!empty($lokasi_file))
        {

            if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

            $show   = $database->select($fields="gambar", $table="car_rent", $where_clause="WHERE id_car_rent = '$_POST[id]'", $fetch='');
            if($show['gambar'] != '')
            {
                unlink("../../../joimg/car_rent/$show[gambar]");
                unlink("../../../joimg/car_rent/thumbnail/$show[gambar]");
            }

            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/car_rent/");
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/car_rent", $thumbDirectory="../../../joimg/car_rent/thumbnail", $thumbWidth="500");

            //data yang akan diupdate berbentuk array
            $form_data = array(
                "nama"      => "$_POST[nama]",
                "seo"       => "$nama_seo",
                // "deskripsi" => "$_POST[deskripsi]",
                "harga1"      => "$_POST[harga1]",
                "harga2"      => "$_POST[harga2]",
                "harga3"      => "$_POST[harga3]",
                "harga4"      => "$_POST[harga4]",
                "gambar"    => "$nama_file_unik",
                "status"    => "$_POST[status]"
            );

            //proses update ke database
            $database->update($table="car_rent", $array=$form_data, $fields_key="id_car_rent", $id="$_POST[id]");
        }
        else
        {
            //data yang akan diupdate berbentuk array
            $form_data = array(
                "nama"      => "$_POST[nama]",
                "seo"       => "$nama_seo",
                // "deskripsi" => "$_POST[deskripsi]",
                "harga1"      => "$_POST[harga1]",
                "harga2"      => "$_POST[harga2]",
                "harga3"      => "$_POST[harga3]",
                "harga4"      => "$_POST[harga4]",
                "status"    => "$_POST[status]"
            );

            //proses update ke database
            $database->update($table="car_rent", $array=$form_data, $fields_key="id_car_rent", $id="$_POST[id]");
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}


if($act=='delete')
{
    // Delete
    if ($module=='car_rent' AND $act=='delete')
    {
    //  $show   = db_get_one("SELECT * FROM sosmed WHERE id_sosmed='$_GET[id]'");
        $show   = $database->select($fields="gambar", $table="car_rent", $where_clause="WHERE id_car_rent = '$_GET[id]'", $fetch='');
        if($show['gambar'] != '')
        {
            unlink("../../../joimg/car_rent/$show[gambar]");
            unlink("../../../joimg/car_rent/thumbnail/$show[gambar]");
            $database->delete($table="car_rent", $fields_key="id_car_rent", $id="$_GET[id]");
        }
        else
        {
            $database->delete($table="car_rent", $fields_key="id_car_rent", $id="$_GET[id]");
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

}
?>
