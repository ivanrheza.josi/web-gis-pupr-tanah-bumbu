<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
    
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

<?php
    $aksi="modul/mod_car_rent/aksi_car_rent.php?module=car_rent";
    switch($_GET['act']){
    default:
?>
        <div class="card">
        <?php
            $desc  = $database->select($fields="*", $table="modul", $where_clause="WHERE id_modul = '24'");
        ?>
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Car Short Description</p>
                <hr class="line">
                <form method='post' action='<?php echo $aksi;?>&act=updatedesc'>
                    <input type="hidden" name="id" value="<?php echo $desc['id_modul']; ?>">
                    <div class="form-group" style="margin-bottom: 10px;">
                        <div class="fg-line">
                            <textarea name="content" class="form-control" rows="3" placeholder="Enter your content"><?php echo $desc['static_content'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group kaki" style="border-top: none; padding: 0; margin: 0;">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <!-- <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button> -->
                    </div>
                </form>
            </div>
        </div>
                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Car / Car
                            <a href="?module=car_rent&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                        </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr class="inamakolom">
                                        <th width="5%">No</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Car + Driver (12 Hours)</th>
                                        <th>Car + Driver (Full Day)</th>
                                        <th>Car + Driver + BBM (12 Hours)</th>
                                        <th>Car + Driver + BBM (Full Day)</th>
                                        <th>Status</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $car = $database->select($fields="*", $table="car_rent", $where_clause="ORDER BY id_car_rent DESC", $fetch="all");
                                    foreach ($car as $key => $value) {
                                ?>

                                    <tr>
                                        <th style="text-align: center;"><?php echo $no; ?></th>
                                        <td style="text-align: center;"><img src="../joimg/car_rent/thumbnail/<?php echo $value['gambar'];?>" alt="" style="height: 80px;"></td>
                                        <td><?php echo $value['nama']; ?></td>
                                        <td><?php echo $value['harga1']; ?></td>
                                        <td><?php echo $value['harga2']; ?></td>
                                        <td><?php echo $value['harga3']; ?></td>
                                        <td><?php echo $value['harga4']; ?></td>
                                        <td style="text-align: center;"><?php if($value['status'] == 'Y'){echo "Publish";}else{echo "Hidden";} ?></td>
                                        <td style="text-align: center;">
                                            <a href="?module=car_rent&act=edit&id=<?php echo $value['id_car_rent'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_car_rent]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "add":
?>
                   <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Car / Car / Add New</p>
                            <hr class="line">

                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                                <!-- <input type="hidden" name="category" value="<?php //echo $_GET['kateg'];?>"> -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Car Name</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Enter name" maxlength="150" required="require" autofocus>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="fg-line">
                                                <label>Description</label>
                                                <textarea id="konten" name="deskripsi" class="form-control" rows="10" placeholder="Enter description"></textarea>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fg-line">
                                            <label>Price (Car + Driver)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">12 Hours</label>
                                                    <input type="text" name="harga1" class="form-control" placeholder="Enter price" maxlength="150" required="require">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">Full Day</label>
                                                    <input type="text" name="harga2" class="form-control" placeholder="Enter price" maxlength="150" required="require">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fg-line">
                                            <label>Price (Car + Driver + BBM)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">12 Hours</label>
                                                    <input type="text" name="harga3" class="form-control" placeholder="Enter price" maxlength="150" required="require">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">Full Day</label>
                                                    <input type="text" name="harga4" class="form-control" placeholder="Enter price" maxlength="150" required="require">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="alert alert-info" role="alert"><b>Attention for Price!</b> <br><p style="font-size: 13px;"><b>Number only,</b> without point and/ comma.</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload" required="require">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                                <div class="alert alert-info" role="alert"><b>Attention for Image!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG *.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="Y" checked>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="N">
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="car_rent", $where_clause="WHERE id_car_rent = '$id'", $fetch="");
    /*$id_author  = $value['id_author'];
    $author     = $database->select($fields="*", $table="author", $where_clause="WHERE id_author = '$id_author'", $fetch="");*/
?>

                   <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Home / Car / Edit</p>
                            <hr class="line">

                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                 <input type="hidden" name="id" value="<?php echo $value['id_car_rent'];?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Car Name</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Enter name" maxlength="150" value="<?php echo $value['nama'];?>" required="require">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="fg-line">
                                                <label>Description</label>
                                                <textarea id="konten" name="deskripsi" class="form-control" rows="10" placeholder="Enter description"></textarea>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fg-line">
                                            <label>Price (Car + Driver)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">12 Hours</label>
                                                    <input type="text" name="harga1" class="form-control" placeholder="Enter price" maxlength="150" value="<?php echo $value['harga1'];?>" required="require">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">Full Day</label>
                                                    <input type="text" name="harga2" class="form-control" placeholder="Enter price" maxlength="150" value="<?php echo $value['harga2'];?>" required="require">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fg-line">
                                            <label>Price (Car + Driver + BBM)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">12 Hours</label>
                                                    <input type="text" name="harga3" class="form-control" placeholder="Enter price" maxlength="150" value="<?php echo $value['harga3'];?>" required="require">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label style="font-weight: normal; font-size: 12px;">Full Day</label>
                                                    <input type="text" name="harga4" class="form-control" placeholder="Enter price" maxlength="150" value="<?php echo $value['harga4'];?>" required="require">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                <!-- </div>

                                <div class="row"> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <img src="../joimg/car_rent/thumbnail/<?php echo $value['gambar'];?>" alt="" width="300px">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                                <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. <b>Image Size :</b> 750 x 320 pixels.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>

                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="Y" <?php if($value['status'] == 'Y'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="N" <?php if($value['status'] == 'N'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>

                        </div>
                    </div>
<?php
    break;
    }
}
?>