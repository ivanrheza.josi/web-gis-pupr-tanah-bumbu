<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/Seo.php';
include_once '../../../josys/function/ImageResizeSinur.php';

$database   = new Database($db);

$module = $_GET['module'];
$act    = $_GET['act'];

if($act=='insert')
{
    // Insert
    if ($module=='blog' AND $act=='insert')
    {
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
        $nama_seo       = substr(seo($_POST['judul']), 0, 75);
        $contents       = stripslashes($_POST['konten']);
        $date           = date('Y-m-d');

        if(!empty($lokasi_file))
        {
            $image_name = img_resize($_FILES['fupload'],840,'../../../joimg/blog/','akustik-ruang');

            //data yang akan di insert berbentuk array
            $form_data = array(
                // "id_blog_kategori"    => "$_POST[kategori]",
                "judul"                     => "$_POST[judul]",
                "nama"                      => "$_POST[nama]",
                "konten"                    => "$contents",
                "image"                     => "$image_name",
                "date"                      => "$date",
                "seo"                       => "$nama_seo",
                "status"                    => "$_POST[status]"
            );

            //proses insert ke database
            $database->insert($table="blog", $array=$form_data);
        }
        else
        {
            //data yang akan di insert berbentuk array
            $form_data = array(
                // "id_blog_kategori"    => "$_POST[kategori]",
                "judul"                     => "$_POST[judul]",
                "nama"                      => "$_POST[nama]",
                "konten"                    => "$contents",
                "date"                      => "$date",
                "seo"                       => "$nama_seo",
                "status"                    => "$_POST[status]"
            );

            //proses insert ke database
            $database->insert($table="blog", $array=$form_data);
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

if($act=='update')
{
    // Update
    if ($module=='blog' AND $act=='update')
    {
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
        $nama_seo       = substr(seo($_POST['judul']), 0, 75);
        $contents       = stripslashes($_POST['konten']);
        $date           = date('Y-m-d');

        if(!empty($lokasi_file))
        {
            $show   = $database->select($fields="image", $table="blog", $where_clause="WHERE id_blog = '$_POST[id]'", $fetch='');
            if($show['image'] != '')
            {
                unlink("../../../joimg/blog/$show[image]");
            }

            $image_name = img_resize($_FILES['fupload'],840,'../../../joimg/blog/','akustik-ruang');

            // $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/blog/");
            // $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/blog", $thumbDirectory="../../../joimg/blog/thumbnail", $thumbWidth="500");

            //data yang akan diupdate berbentuk array
            $form_data = array(
                // "id_blog_kategori"    => "$_POST[kategori]",
                "judul"                     => "$_POST[judul]",
                "nama"                      => "$_POST[nama]",
                "konten"                    => "$contents",
                "image"                     => "$image_name",
                "date"                      => "$date",
                "seo"                       => "$nama_seo",
                "status"                    => "$_POST[status]"
            );

            //proses update ke database
            $database->update($table="blog", $array=$form_data, $fields_key="id_blog", $id="$_POST[id]");
        }
        else
        {
            //data yang akan diupdate berbentuk array
            $form_data = array(
                // "id_blog_kategori"    => "$_POST[kategori]",
                "judul"                     => "$_POST[judul]",
                "nama"                      => "$_POST[nama]",
                "konten"                    => "$contents",
                "date"                      => "$date",
                "seo"                       => "$nama_seo",
                "status"                    => "$_POST[status]"
            );

            //proses update ke database
            $database->update($table="blog", $array=$form_data, $fields_key="id_blog", $id="$_POST[id]");
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}


if($act=='delete')
{
    // Delete
    if ($module=='blog' AND $act=='delete')
    {
        $show   = $database->select($fields="image", $table="blog", $where_clause="WHERE id_blog = '$_GET[id]'", $fetch='');
        if($show['image'] != '')
        {
            unlink("../../../joimg/blog/$show[image]");
            $database->delete($table="blog", $fields_key="id_blog", $id="$_GET[id]");
        }
        else
        {
            $database->delete($table="blog", $fields_key="id_blog", $id="$_GET[id]");
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

}
?>