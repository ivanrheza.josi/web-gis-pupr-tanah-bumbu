<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/Seo.php';

$database   = new Database($db);

$module = $_GET['module'];
$act    = $_GET['act'];


if($act=='insert')
{
    // Insert
    if ($module=='maps_sijari' AND $act=='insert')
    {
        //data yang akan di insert berbentuk array
        $form_data = array(
            "layer_title"      => $_POST['judul'],
            // "layer_name"      => $_FILES['fupload'],
            "relation_categories"      => $_POST['stat']
        );

        $target_dir = "../../../src/maps/kmz/";
        $target_file = $target_dir . basename($_FILES["fupload"]["name"]);
        $uploadOk = 1;
        $mapsFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Allow certain file formats
        if($mapsFileType != "kmz" && $mapsFileType != "kml" ) {
            $uploadOk = 0;
            echo "<script>alert('Sorry, onli KMZ or KML files are allowed'); window.location = '../../media.php?module=$module';</script>";
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fupload"]["tmp_name"], $target_dir.round(microtime(true)).'_'.$_FILES["fupload"]["name"] )) {
                $form_data['layer_name']= basename( round(microtime(true)).'_'.$_FILES["fupload"]["name"]);
                //proses insert ke database
                $database->insert($table="layers", $array=$form_data);

                echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";

            } else {
                echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
            }
        }
        
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

if($act=='update')
{
    // Update
    if ($module=='maps_sijari' AND $act=='update')
    {
        //data yang akan diupdate berbentuk array
        $form_data = array(
            "layer_title"      => $_POST['judul'],
            // "layer_name"      => $_FILES['fupload'],
            "relation_categories"      => $_POST['stat']
        );

        # with files
        if ( ! empty( $_FILES['fupload']['tmp_name'] ) )
        {
            $target_dir = "../../../src/maps/kmz/";
            $target_file = $target_dir . basename($_FILES["fupload"]["name"]);
            $uploadOk = 1;
            $mapsFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    
            // Allow certain file formats
            if($mapsFileType != "kmz" && $mapsFileType != "kml" ) {
                $uploadOk = 0;
                echo "<script>alert('Sorry, onli KMZ or KML files are allowed'); window.location = '../../media.php?module=$module';</script>";
            }
    
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["fupload"]["tmp_name"], $target_dir.round(microtime(true)).'_'.$_FILES["fupload"]["name"] )) {
                    $form_data['layer_name']= basename( round(microtime(true)).'_'.$_FILES["fupload"]["name"]);
                    //proses update ke database

                    $layers= $database->select('*','layers','WHERE layer_id='.$_POST['id']);
                    $target_layer= $target_dir.basename($layers['layer_name']);
                    if ( file_exists($target_layer) ) {
                        unlink($target_layer);
                    }
                    
                    $database->update($table="layers", $array=$form_data, $fields_key="layer_id", $id="$_POST[id]");
    
                    echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    
                } else {
                    echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
                }
            }
        }
        #without files
        else {
            $database->update($table="layers", $array=$form_data, $fields_key="layer_id", $id="$_POST[id]");
            echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
        }
        


        //proses update ke database
        $database->update($table="categories", $array=$form_data, $fields_key="category_id", $id="$_POST[id]");

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}


if($act=='delete')
{
    // Delete
    if ($module=='maps_sijari' AND $act=='delete')
    {
        $layers= $database->select('*', 'layers', 'WHERE layer_id='.$_GET["id"]);
        $target_dir = "../../../src/maps/kmz/";
        $target_file = $target_dir . basename($layers['layer_name']);
        if ( file_exists($target_file) ) {
            unlink( $target_file );
        }
        $database->delete($table="layers", $fields_key="layer_id", $id="$_GET[id]"); 
        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";    
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

}
?>
