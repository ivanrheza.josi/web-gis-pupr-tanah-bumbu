<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>


    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_maps_sijari/maps_sijari_c.php?module=maps_sijari";
    switch($_GET['act']){
    default:
?>
        <div class="card">
            <div class="card-body card-padding">

                <p class="c-black f-500 m-b-20">Daerah Irigasi
                    <a href="?module=maps_sijari&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                </p>

                <hr class="line">

                <div class="table-responsive">
                    <table class='display' id='datatables'>
                        <thead>
                            <tr class="inamakolom">
                                <th width="5%">No</th>
                                <th>Title</th>
                                <th>Kategori Daerah Irigasi</th>
                                <th width="18%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no=1;
                            $posts = $database->select($fields="*", $table="layers", $where_clause="ORDER BY layer_title ASC", $fetch="all");
                            foreach ($posts as $key => $value) {
                            
                            ?>
                                <tr>
                                    <th><?php echo $no; ?></th>
                                    <td><?php echo $value['layer_title']; ?></td>
                                    <td>
                                        <?php
                                            $categories= $database->select($fields="*", $table="categories", $where_clause="WHERE category_id=".$value['relation_categories'], $fetch="all");
                                            echo $categories[0]['category_title'];
                                        ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="?module=maps_sijari&act=edit&id=<?php echo $value['layer_id'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                        <a href="<?php echo $aksi.'&act=delete&id='.$value['layer_id'];?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                    </td>
                                </tr>
                        <?php
                                $no++;
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php
    break;
    case "add":
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Daerah Irigasi / Add New</p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Title</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Kategori Daerah Irigasi</label>
                                    <div class="select">
                                        <select class="form-control" name="stat" required>
                                            <option value="" disabled="disable" selected="select">- Pilih Kategori Daerah Irigasi -</option>
                                            <?php
                                                $categories= $database->select('*','categories','ORDER BY category_title ASC','all');
                                                foreach ($categories as $key => $value) {
                                                    echo "<option value='{$value["category_id"]}'>{$value["category_title"]}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
 
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Import File</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload" required>
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>File Type :</b> KML/KMZ. <b></p></div>
                        </div>
 
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="layers", $where_clause="WHERE layer_id = '$id'", $fetch="");
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Maps SIJARI / Edit / <?php echo $value['layer_title'];?></p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Title</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title"  value="<?php echo $value['layer_title'];?>" maxlength="128">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Kategori Daerah Irigasi</label>
                                    <div class="select">
                                        <select class="form-control" name="stat" required="require">
                                            <option disabled="disable">- Pilih Kategori Daerah Irigasi -</option>
                                            <?php
                                                $categories= $database->select('*','categories','ORDER BY category_title ASC','all');
                                                foreach ($categories as $key => $value_c) {
                                                    echo "<option value='{$value_c["category_id"]}' ".($value['relation_categories'] == $value_c['category_id'] ? 'selected' : FALSE ).">{$value_c["category_title"]}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Update KML/KMZ File</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="dsgfsdfsdf" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>File Type :</b> KML/KMZ. <b></p></div>
                        </div>
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
<?php
    break;
    }
}
?>