<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {

    $aksi="modul/mod_map/map_c.php?module=map";
    switch($_GET['act']){
    default:
    $map  = $database->select($fields="id_modul, static_content", $table="modul", $where_clause="WHERE id_modul = '6'", $fetch='');
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Kontak / Peta</p>
                <hr class="line">
                <form method='post' action='<?php echo $aksi;?>&act=update'>
                    <input type="hidden" name="id" value="<?php echo $map['id_modul'];?>" />
                    <div class="form-group" style="margin-bottom: 30px;">
                        <div class="fg-line">
                            <label>Link Embed Google Maps</label>
                            <textarea name="content" class="form-control" rows="3" placeholder="Enter your link from google maps"><?php echo $map['static_content'];?></textarea>
                        </div>
                    </div>

                    <div class="alert alert-info" role="alert">
                        <b>Attention!</b>
                        <p style="font-size: 13px;">&#42;&#41; Cara menggunakan MAPS : Buka website <b>Google Maps</b> &#45;&#45;&#62; Cari lokasi/titik yang akan ditampilkan di website &#45;&#45;&#62; klik menu <b>SHARE</b> &#45;&#45;&#62; Pilih tab <b>Embed a map</b> &#45;&#45;&#62; Salin/copy link yang berada diantara tanda petik <b>src=""</b></p>
                        <?php
                            $cnth_link = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15810.5824272133!2d110.3792777!3d-7.8272818!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac8d90905871c1b5!2sJogjaSite.com!5e0!3m2!1sen!2sid!4v1531721517024" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
                        ?>
                        <p style="padding-top: 10px;">Contoh link embed map:<div style="font-size: 13px;"><?php echo htmlentities($cnth_link); ?></div></p>
                        <br>
                        <p>
                        Yang disalin/copy yang ada dalam tanda petik src (src=""):<br>
                        <b style=" font-size: 13px;">https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15810.5824272133!2d110.3792777!3d-7.8272818!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac8d90905871c1b5!2sJogjaSite.com!5e0!3m2!1sen!2sid!4v1531721517024</b>
                        </p>
                    </div>

                    <div class="form-group kaki" style="border-top: none; padding: 0; margin: 0">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <!-- <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button> -->
                    </div>

                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Display On Website</p>
                <hr class="line">
                <iframe src="<?php echo $map['static_content']; ?>" width="100%" height="250px" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>

<?php
    break;
    }
}
?>
