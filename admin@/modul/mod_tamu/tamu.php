<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->


<?php
    $aksi="modul/mod_tamu/aksi_tamu.php?module=buku-tamu";
    switch($_GET['act']){
    default:
?>

                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Support Web - Guest Book </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>DateTime</th>
                                        <th>Jawaban</th>
                                        <th>Status</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $messages = $database->select($fields="*", $table="buku_tamu", $where_clause="ORDER BY id_tamu DESC", $fetch="all");
                                    foreach ($messages as $key => $value) {
                                ?>

                                    <tr>
                                        <th><?php echo $no; ?></th>
                                        <td><?php echo $value['nama']; ?></td>
                                        <td><?php echo $value['email']; ?></td>
                                        <td><?php echo $value['tgl']; ?></td>
                                        <td><?php echo substr($value['jawab'], 0, 75); ?> ...</td>
                                        <td><?php if($value['status'] == 'Y'){echo "Public";}else{echo "Hidden";}?></td>
                                        <td>
                                            <a href="?module=buku-tamu&act=detail&id=<?php echo $value['id_tamu'];?>" class="btn bgm-cyan waves-effect"><i class="zmdi zmdi-eye zmdi-hc-fw"></i> Detail</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_tamu]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "detail":
    $id     = $_GET['id'];
    $value  = $database->select($fields="*", $table="buku_tamu", $where_clause="WHERE id_tamu='$id'");


    include "../josys/function/Download.php";
?>

                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Support Web / Detail Guest Book / <?php echo $value['nama'];?> </p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">

                                <input type="hidden" name="id" value="<?php echo $id;?>">

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Nama </label>
                                                <input type="text" name="name" class="form-control" maxlength="100" value="<?php echo $value['nama'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>

                                   
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Email </label>
                                                <input type="email" name="email" class="form-control" maxlength="150" value="<?php echo $value['email'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                   
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>DateTime </label>
                                                <input type="text" name="dateTime" class="form-control" maxlength="150" value="<?php echo $value['tgl'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Messages </label>
                                                <textarea name="message" class="form-control" rows="5" disabled><?php echo $value['pesan'];?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                   
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label>Jawab Pesan ini ? </label>
                                                    <textarea name="jawaban" class="form-control" rows="5" placeholder="tulis jawaban disini"><?php echo $value['jawab'];?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <p class="c-black f-500 m-b-20">Publish Post ?</p>

                                            <label class="radio radio-inline m-r-20">
                                                <input type="radio" name="status" value="Y" <?php if($value['status'] == 'Y'){echo "checked";}?>>
                                                <i class="input-helper"></i>
                                                Yes
                                            </label>
                                            <label class="radio radio-inline m-r-20">
                                                <input type="radio" name="status" value="N" <?php if($value['status'] == 'N'){echo "checked";}?>>
                                                <i class="input-helper"></i>
                                                No
                                            </label>
                                        </div>
                                   
                                </div>

                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    }
}
?>
