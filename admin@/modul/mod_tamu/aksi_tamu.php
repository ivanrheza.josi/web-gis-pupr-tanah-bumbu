<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';

$database 	= new Database($db);

$module	= $_GET['module'];
$act	= $_GET['act'];

if($act=='delete')
{
	// Delete
	if ($module=='buku-tamu' AND $act=='delete')
	{
      /*  $show	= $database->select($fields="*", $table="buku_tamu", $where_clause="WHERE id_tamu = '$_GET[id]'");
*/
		
            $database->delete($table="buku_tamu", $fields_key="id_tamu", $id=$_GET['id']);
			//db_delete($table="slide", $where_clause="id_slide = $_GET[id]");
		echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='update')
{
	// Delete
	if ($module=='buku-tamu' AND $act=='update')
	{
        
    //data yang akan diupdate berbentuk array
		$tgl = date("Y-m-d h:i:s");
    $form_data = array(
        "status"	=> "$_POST[status]",
        "jawab"     => "$_POST[jawaban]",
        "tgl_jawab" => "$tgl"
    );
    //proses update ke database
    $database->update($table="buku_tamu", $array=$form_data, $fields_key="id_tamu", $id="$_POST[id]");

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

}
?>
