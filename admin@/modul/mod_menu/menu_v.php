<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->


<?php
    $aksi="modul/mod_menu/menu_c.php?module=menu";
    switch($_GET['act']){
    default:
?>
                <div class="card">
                    <div class="card-body card-padding">
                        <p class="c-black f-500 m-b-20">Setting Website / Menu
                            <a href="?module=menu&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                        </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr class="inamakolom">
                                        <th width="5%">No</th>
                                        <th>Title</th>
                                        <th width="22%">Image</th>
                                        <th>Description</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $slide = $database->select($fields="*", $table="menu", $where_clause="ORDER BY id_menu ASC", $fetch="all");
                                    foreach ($slide as $key => $value) {
                                ?>
                                    <tr>
                                        <th style="text-align: center;"><?php echo $no; ?></th>
                                        <td><?php echo $value['nama']; ?></td>
                                        <td style="text-align: center;"><img src="../joimg/menu/<?php echo $value['gambar'];?>" height="120px"></td>
                                        <td><?php echo $value['deskripsi']; ?></td>
                                        <td style="text-align: center;">
                                            <a href="?module=menu&act=edit&id=<?php echo $value['id_menu'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_menu]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "add":
?>
                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Setting Website / Menu / Add New</p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Title</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Enter title" maxlength="100" autofocus>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Description</label>
                                                <textarea name="deskripsi" class="form-control" rows="5" placeholder="Enter short description" maxlength="350"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Link</label>
                                                <input type="text" name="link" class="form-control" placeholder="Enter link" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. <b>Size :</b> 512 x 350 pixels.</p></div>
                                    </div>
                                </div>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id     = $_GET['id'];
    $value   = $database->select($fields="*", $table="menu", $where_clause="WHERE id_menu = '$id'", $fetch="");
?>

                    <div class="card">
                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Setting Website / Menu / Edit</p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Title </label>
                                                <input type="text" name="nama" class="form-control" placeholder="Enter title" maxlength="100" value="<?php echo $value['nama']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Description</label>
                                                <textarea name="deskripsi" class="form-control" rows="5" placeholder="Enter sort description slideshow" maxlength="350"><?php echo $value['deskripsi'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Link</label>
                                                <input type="text" name="link" class="form-control" placeholder="Enter link" maxlength="100" value="<?php echo $value['link']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image</label>
                                                <br>
                                                <img src="../joimg/menu/<?php echo $value['gambar'];?>" alt="" width="100%">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. <b>Size :</b> 512 x 350 pixels.</p></div>
                                    </div>
                                </div>
                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>

<?php
    break;
    }
}
?>
