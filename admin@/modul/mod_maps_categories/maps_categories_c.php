<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/Seo.php';

$database   = new Database($db);

$module = $_GET['module'];
$act    = $_GET['act'];


if($act=='insert')
{
    // Insert
    if ($module=='maps_categories' AND $act=='insert')
    {
        //data yang akan di insert berbentuk array
        $form_data = array(
            "category_title"      => "$_POST[title]"
        );

        //proses insert ke database
        $database->insert($table="categories", $array=$form_data);

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

if($act=='update')
{
    // Update
    if ($module=='maps_categories' AND $act=='update')
    {
        //data yang akan diupdate berbentuk array
        $form_data = array(
            "category_title"      => "$_POST[title]",
        );

        //proses update ke database
        $database->update($table="categories", $array=$form_data, $fields_key="category_id", $id="$_POST[id]");

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}


if($act=='delete')
{
    // Delete
    if ($module=='maps_categories' AND $act=='delete')
    {
        $count_rows_table_layers= $database->count_rows_new($table="layers", $where_clause='WHERE relation_categories ='.$_GET["id"]);
        if ( $count_rows_table_layers > 0 ) {
            echo "<script>alert('Sorry! data is being used.'); window.location = '../../media.php?module=$module';</script>"; 
        }else {
            $database->delete($table="categories", $fields_key="category_id", $id="$_GET[id]"); 
            echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
        }
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

}
?>
