    <?php
    session_start();
    if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
        echo "
        <center>Untuk mengakses modul, Anda harus login <br>
        <a href=../../index.php><b>LOGIN</b></a></center>";
    }
    else {
    ?>
        <script src="assets/vendors/input-mask/input-mask.min.js"></script>
        <!--Begin DataTables-->
        <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
        <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
        <script>
        $(document).ready(function() {
            $('#datatables').DataTable();
        } );
        </script>
    
        <style>
        .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
            margin-left: 0px;
        }
        </style>
      
    
    <?php
        $aksi="modul/mod_maps_categories/maps_categories_c.php?module=maps_categories";
        switch($_GET['act']){
        default:
    ?>
    
                    <div class="card">
                        <div class="card-body card-padding">
    
                            <p class="c-black f-500 m-b-20">Daerah Irigasi / Kategori
                                <a href="?module=maps_categories&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                            </p>
    
                            <hr class="line">
    
                            <div class="table-responsive">
                                <table class='display' id='datatables'>
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Title</th>
                                            <th width="18%">Actions</th>
                                        </tr>
                                    </thead>
    
                                    <tbody>
    
                                    <?php
                                        $no=1;
                                        $posts = $database->select($fields="*", $table="categories", $where_clause="ORDER BY category_title ASC", $fetch="all");
                                        foreach ($posts as $key => $value) {
                                    ?>
    
                                        <tr>
                                            <th><?php echo $no; ?></th>
                                            <td><?php echo $value['category_title']; ?></td>
                                            <td>
                                                <a href="?module=maps_categories&act=edit&id=<?php echo $value['category_id'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                                <a href="modul/mod_maps_categories/maps_categories_c.php?module=maps_categories&act=delete&id=<?php echo $value['category_id'] ?>" class="btn bgm-pink waves-effect waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                                
                                            </td>
                                        </tr>
    
                                    <?php
                                        $no++;
                                        }
                                    ?>
    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
    
    <?php
        break;
        case "add":
    ?>
                       <div class="card">
    
                            <div class="card-body card-padding">
                                <p class="c-black f-500 m-b-20">Daerah Irigasi / Kategori / Add New</p>
                                <hr class="line">
    
                                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                                    <input type="hidden" name="category" value="<?php echo $_GET['kateg'];?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="fg-line">
                                                    <label>Title</label>
                                                    <input type="text" name="title" class="form-control" placeholder="Enter title" maxlength="150" required="require">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group kaki">
                                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                    </div>
    
                                </form>
                            </div>
                        </div>
    
    <?php
        break;
        case "edit":
        $id         = $_GET['id'];
        $value      = $database->select($fields="*", $table="categories", $where_clause="WHERE category_id = '$id'", $fetch="");
    ?>
    
                       <div class="card">
    
                            <div class="card-body card-padding">
                                <p class="c-black f-500 m-b-20">Daerah Irigasi / Kategori / Edit</p>
                                <hr class="line">
    
                                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="id" value="<?php echo $id;?>">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Title</label>
                                                        <input type="text" name="title" class="form-control" placeholder="Enter title"  value="<?php echo $value['category_title'];?>" maxlength="128">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
    
                                    <div class="form-group kaki">
                                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                    </div>
    
                                </form>
    
                            </div>
                        </div>
    <?php
        break;
        }
    }
    ?>