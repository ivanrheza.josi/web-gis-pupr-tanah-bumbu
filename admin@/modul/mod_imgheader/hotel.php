<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        });
    } );
    </script>
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "#desc",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_imgheader/aksi_hotel.php?module=imgheader";
    switch($_GET['act']){
    default:
?>

        <div class="card">
            <div class="card-body card-padding">

                <p class="c-black f-500 m-b-20">Setting Website / Image Header
                    <!-- <a href="?module=hotel&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a> -->
                </p>

                <hr class="line">

                <div class="table-responsive">
                    <table class='display' id='datatables'>
                        <thead>
                            <tr class="inamakolom">
                                <th width="5%">No</th>
                                <th>Name</th>
                                <!-- <th>Description</th> -->
                                <th>Image</th>
                                <!-- <th>Date</th>
                                <th>Status</th> -->
                                <th width="12%">Actions</th>
                            </tr>
                        </thead>

                        <tbody>

                        <?php
                            $no=1;
                            $posts = $database->select($fields="*", $table="img_header", $where_clause="ORDER BY urutan ASC", $fetch="all");
                            foreach ($posts as $key => $value) {
                        ?>

                            <tr>
                                <th style="text-align: center;"><?php echo $no; ?></th>
                                <td style="text-align: center;"><?php echo $value['name']; ?></td>
                                <!-- <td><?php //echo substr($value['des'], 0, 65); ?> ...</td> -->
                                <td style="text-align: center;"><img src="../joimg/imgheader/thumbnail/<?php echo $value['image'];?>" alt="" style="height: 200px;"></td>
                                <!-- <td><?php //echo $value['date']; ?></td> -->
                                <!-- <td><?php //if($value['status'] == 'Y'){echo "Publish";}else{echo "Hidden";} ?></td> -->
                                <td style="text-align: center;">
                                    <a href="?module=imgheader&act=edit&id=<?php echo $value['id_img'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                    <!-- <a href="<?php //echo "$aksi&act=delete&id=$value[id_img]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a> -->
                                    <!-- <a href="?module=subimg_hotel&id=<?php //echo $value['id_hotel'];?>" class="btn btn-info waves-effect"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Sub Image</a> -->
                                </td>
                            </tr>

                        <?php
                            $no++;
                            }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php
    break;
    case "add":
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Destination / Add New</p>
                <hr class="line">

                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Destination Name <span class="color-red">*</span></label>
                                    <input type="text" name="nm_hotel" class="form-control" placeholder="Enter name" maxlength="150" required="require" autofocus>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <div class="fg-line">
                                    <label>Description</label>
                                    <textarea id="desc" name="desc" class="form-control" rows="10" placeholder="Enter desc"></textarea>
                                </div>
                            </div> -->
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Image</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                    <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG *.</div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>

                </form>
            </div>
        </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="img_header", $where_clause="WHERE id_img = '$id'", $fetch="");
    /*$id_author  = $value['id_author'];
    $author     = $database->select($fields="*", $table="author", $where_clause="WHERE id_author = '$id_author'", $fetch="");*/
?>

        <div class="card">

            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Image Header / Edit</p>
                <hr class="line">

                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <!-- <input type="hidden" name="category" value="<?php //echo $_GET['kateg'];?>"> -->
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Name <span class="color-red">*</span></label>
                                    <input type="text" name="nm_hotel" class="form-control" placeholder="Enter name" value="<?php echo $value['name'];?>" maxlength="128" required="require">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <div class="fg-line">
                                    <label>Description</label>
                                    <textarea id="desc" name="desc" class="form-control" rows="10" placeholder="Enter desc"><?php //echo $value['des'];?></textarea>
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Image <span class="color-red">*</span></label>
                                    <br>
                                    <img src="../joimg/imgheader/thumbnail/<?php echo $value['image'];?>" alt="<?php echo $value['image'];?>" width="300px">
                                    <br><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                    <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG/JPEG. <b>Image Size :</b> 1690 x 1000 pixels.</div>
                                </div>
                            </div>
                        </div>                            
                    </div>
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>

                </form>

            </div>
        </div>
<?php
    break;
    }
}
?>