<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/class/Upload.php';
include_once '../../../josys/function/Seo.php';

$database   = new Database($db);
$upload     = new Upload();

$module = $_GET['module'];
$act    = $_GET['act'];


if($act=='insert')
{
    // Insert
    if ($module=='imgheader' AND $act=='insert')
    {
        // $lokasi_file    = $_FILES['fupload']['tmp_name'];
        // $tipe_file      = $_FILES['fupload']['type'];
        // $nama_file      = $_FILES['fupload']['name'];

        $nama_seo       = substr(seo($_POST['name']), 0, 75);
        // $acak           = rand(000,999);
        // $nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;
        // $deskripsi      = stripslashes($_POST['desc']);
        // $contents       = stripslashes($_POST['konten']);
        // $date = date('Y-m-d');

        if(!empty($lokasi_file))
        {

            if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/imgheader/");
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/imgheader", $thumbDirectory="../../../joimg/imgheader/thumbnail", $thumbWidth="500");

            //data yang akan di insert berbentuk array
            $form_data = array(               
                "name"          => "$_POST[nm_hotel]",
                // "des"               => "$deskripsi",
                // "image"             => "$nama_file_unik",
                // "link"              => "$_POST[link]",
                "seo"               => "$nama_seo"
            );

            //proses insert ke database
            $database->insert($table="img_header", $array=$form_data);
        }
        else
        {
            //data yang akan di insert berbentuk array
            $form_data = array(               
                "name"          => "$_POST[nm_hotel]",                
                // "des"               => "$deskripsi",
                // "link"              => "$_POST[link]",
                "seo"               => "$nama_seo"
            );
            //proses insert ke database
            $database->insert($table="img_header", $array=$form_data);
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

if($act=='update')
{
    // Update
    if ($module=='imgheader' AND $act=='update')
    {
        $lokasi_file    = $_FILES['fupload']['tmp_name'];
        $tipe_file      = $_FILES['fupload']['type'];
        $nama_file      = $_FILES['fupload']['name'];

        $nama_seo       = substr(seo($_POST['nm_hotel']), 0, 50);
        $acak           = rand(000,999);
        $nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;
        // $contents       = stripslashes($_POST['konten']);
        // $deskripsi      = stripslashes($_POST['desc']);
        // $date = date('Y-m-d');

        if(!empty($lokasi_file))
        {

            if ($tipe_file != "image/jpg" AND $tipe_file != "image/jpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.JPEG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                die();
            }

            $show   = $database->select($fields="image", $table="img_header", $where_clause="WHERE id_img = '$_POST[id]'", $fetch='');
            if($show['image'] != '')
            {
                unlink("../../../joimg/imgheader/$show[image]");
                unlink("../../../joimg/imgheader/thumbnail/$show[image]");
            }

            $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/imgheader/");
            $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/imgheader", $thumbDirectory="../../../joimg/imgheader/thumbnail", $thumbWidth="500");

            //data yang akan diupdate berbentuk array
            $form_data = array(                
                "name"          => "$_POST[nm_hotel]",
                // "des"               => "$deskripsi",
                "image"             => "$nama_file_unik",
                // "link"              => "$_POST[link]",
                "seo"               => "$nama_seo"
            );

            //proses update ke database
            $database->update($table="img_header", $array=$form_data, $fields_key="id_img", $id="$_POST[id]");
        }
        else
        {
            //data yang akan diupdate berbentuk array
            $form_data = array(                
                "name"          => "$_POST[nm_hotel]",
                // "des"               => "$deskripsi",
                // "link"              => "$_POST[link]",
                "seo"               => "$nama_seo"
            );

            //proses update ke database
            $database->update($table="img_header", $array=$form_data, $fields_key="id_img", $id="$_POST[id]");
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}


if($act=='delete')
{
    // Delete
    if ($module=='imgheader' AND $act=='delete')
    {
    //  $show   = db_get_one("SELECT * FROM sosmed WHERE id_sosmed='$_GET[id]'");
        $show   = $database->select($fields="image", $table="img_header", $where_clause="WHERE id_img = '$_GET[id]'", $fetch='');
        if($show['image'] != '')
        {
            unlink("../../../joimg/imgheader/$show[image]");
            unlink("../../../joimg/imgheader/thumbnail/$show[image]");
            $database->delete($table="img_header", $fields_key="id_img", $id="$_GET[id]");
        }
        else
        {
            $database->delete($table="img_header", $fields_key="id_img", $id="$_GET[id]");
        }

        echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
    }
    else
    {
        echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
    }
}

}
?>