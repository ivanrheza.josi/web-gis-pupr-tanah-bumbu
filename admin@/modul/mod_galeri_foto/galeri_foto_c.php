<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/ImageResizeSinur.php';

$database 	= new Database($db);

$module	= $_GET['module'];
$act	= $_GET['act'];

if($act=='insert')
{
	// Insert
	if ($module=='galeri_foto' AND $act=='insert')
	{
		$lokasi_file	= $_FILES['fupload']['tmp_name'];
		
		if(!empty($lokasi_file))
		{
			$image_name = img_resize($_FILES['fupload'],840,'../../../joimg/galeri/','akustik-ruang');
			
		    //data yang akan di insert berbentuk array
			$form_data = array(
			    "nama" 		=> "$_POST[nama]",
			    "image" 	=> "$image_name"
			);

			//proses insert ke database
            $database->insert($table="galeri_foto", $array=$form_data);
		}
		else
		{
			//data yang akan di insert berbentuk array
			$form_data = array(
			    "nama" 		=> "$_POST[nama]"
			);

			//proses insert ke database
            $database->insert($table="galeri_foto", $array=$form_data);
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='update')
{
	// Update
	if ($module=='galeri_foto' AND $act=='update')
	{
	 	$lokasi_file    = $_FILES['fupload']['tmp_name'];
	  	
		if(!empty($lokasi_file))
		{
			$image_name = img_resize($_FILES['fupload'],840,'../../../joimg/galeri/','akustik-ruang');
			
			$show	= $database->select($fields="image", $table="galeri_foto", $where_clause="WHERE id_galeri_foto = '$_POST[id]'");
			if($show['image'] != '')
			{
				unlink("../../../joimg/galeri/$show[image]");
			}

            $image_name = img_resize($_FILES['fupload'],840,'../../../joimg/galeri/','akustik-ruang');

	   		//data yang akan diupdate berbentuk array
			$form_data = array(
				"nama" 		=> "$_POST[nama]",
				"image" 	=> "$image_name"
			);

			//proses update ke database
            $database->update($table="galeri_foto", $array=$form_data, $fields_key="id_galeri_foto", $id="$_POST[id]");
		}
		else
		{
			//data yang akan diupdate berbentuk array
			$form_data = array(
				"nama" 		=> "$_POST[nama]"
			);

			//proses update ke database
            $database->update($table="galeri_foto", $array=$form_data, $fields_key="id_galeri_foto", $id="$_POST[id]");
		}
		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}


if($act=='delete')
{
	// Delete
	if ($module=='galeri_foto' AND $act=='delete')
	{
		$show	= $database->select($fields="image", $table="galeri_foto", $where_clause="WHERE id_galeri_foto = '$_GET[id]'");

		if($show['image'] != '')
		{
			unlink("../../../joimg/galeri/$show[image]");
            $database->delete($table="galeri_foto", $fields_key="id_galeri_foto", $id=$_GET['id']);
		}
		else
		{
            $database->delete($table="galeri_foto", $fields_key="id_galeri_foto", $id=$_GET['id']);
		}
		echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

}
?>
