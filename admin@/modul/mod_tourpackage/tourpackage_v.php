<?php
// session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable({
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        } );
    } );
    </script>
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_tourpackage/aksi_tourpackage.php?module=tourpackage";
    if (!isset($_GET['act'])) {
        $_GET['act'] = '';
    }
    switch($_GET['act']){
    default:
?>
        <div class="card">
        <?php
            $desc  = $database->select($fields="*", $table="modul", $where_clause="WHERE id_modul = '25'");
        ?>
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20"><?php echo $desc['nama_modul'];?></p>
                <hr class="line">
                <form method='post' action='<?php echo $aksi;?>&act=updatedesc'>
                    <input type="hidden" name="id" value="<?php echo $desc['id_modul']; ?>">
                    <div class="form-group" style="margin-bottom: 10px;">
                        <div class="fg-line">
                            <textarea name="content" class="form-control" rows="3" placeholder="Enter your content"><?php echo $desc['static_content'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group kaki" style="border-top: none; padding: 0; margin: 0;">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <!-- <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button> -->
                    </div>
                </form>
            </div>
        </div>
                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Tour / Tour Package
                            <a href="?module=tourpackage&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                        </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr class="inamakolom">
                                        <th width="5%">No</th>
                                        <th>Name</th>
                                        <th>Duration</th>
                                        <th>Departure</th>
                                        <th>Image</th>
                                        <!-- <th>Peta</th> -->
                                        <th>Status</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $posts = $database->select($fields="*", $table="tour", $where_clause="ORDER BY id_tour DESC", $fetch="all");
                                    foreach ($posts as $key => $value) {
                                ?>

                                    <tr>
                                        <th style="text-align: center;"><?php echo $no; ?></th>
                                        <td><?php echo $value['nama']; ?></td>
                                        <td><?php echo $value['length']; ?></td>
                                        <td><?php echo $value['harga']; ?></td>
                                        <td style="text-align: center;"><img src="../joimg/tour/thumbnail/<?php echo $value['image'];?>" alt="" style="height: 60px;"></td>
                                        <!-- <td><img src="../joimg/peta_tour/thumbnail/<?php //echo $value['peta'];?>" alt="" width="50px"></td> -->
                                        <td style="text-align: center;"><?php if($value['status'] == 'Y'){echo "Publish";}else{echo "Hidden";} ?></td>
                                        <td style="text-align: center;">
                                            <a href="?module=tourpackage&act=edit&id=<?php echo $value['id_tour'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_tour]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                            <!-- <a href="?module=subimg_destination&id=<?php //echo $value['id_tour'];?>" class="btn btn-info waves-effect"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Sub Image</a> -->
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "add":
?>
                   <div class="card">
                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Tour / Tour Package / Add New</p>
                            <hr class="line">

                            <form id="destination-form" method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Package Name <span class="color-red">*</span></label>
                                                        <input type="text" name="judul" class="form-control" placeholder="Enter name" maxlength="150" required="require" autofocus>
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Duration <span class="color-red">*</span></label>
                                                        <input type="text" name="length" class="form-control" placeholder="6 jam" maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Departure <span class="color-red">*</span></label>
                                                        <input type="text" name="harga" class="form-control" placeholder="Pukul 08.00 WIB" maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>                                         
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Accommodation <span class="color-red">*</span></label>
                                                <textarea name="deskripsi" class="form-control" rows="10" placeholder="Enter facilities"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image <span class="color-red">*</span></label>
                                                <br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload" required="require">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                                <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG *.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Destination <span class="color-red">*</span></label>
                                                <select class="selectpicker" multiple="multiple" data-selected-text-format="count" data-style="btn-info" id="multi-hotel" name="id_hotel[]">
                                                <?php
                                                    $hk = $database -> select($fields='*', $table='hotel', $where_clause='ORDER BY id_hotel DESC', $fetch='all'); 
                                                    foreach ($hk as $key => $value_hk) {
                                                        echo '
                                                            <option value="'.$value_hk['id_hotel'].'">'.$value_hk['nm_hotel'].'</option>
                                                        ';
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="Y" checked>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="N">
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group kaki" style="margin-top: 0;">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="tour", $where_clause="WHERE id_tour = '$id'", $fetch="");
    /*$id_author  = $value['id_author'];
    $author     = $database->select($fields="*", $table="author", $where_clause="WHERE id_author = '$id_author'", $fetch="");*/
?>

                   <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Tour Package / Edit</p>
                            <hr class="line">

                            <form id="destination-form" method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Package Name <span class="color-red">*</span></label>
                                                        <input type="text" name="judul" class="form-control" placeholder="Enter name" value="<?php echo $value['nama'];?>" maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Duration <span class="color-red">*</span></label>
                                                        <input type="text" name="length" class="form-control" placeholder="6 jam" value="<?php echo $value['length'];?>" maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="fg-line">
                                                        <label>Departure <span class="color-red">*</span></label>
                                                        <input type="text" name="harga" class="form-control" placeholder="Pukul 08.00 WIB" value="<?php echo $value['harga'];?>" maxlength="150" required="require">
                                                    </div>
                                                </div>                                            
                                            </div>                                         
                                        </div>
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Accommodation <span class="color-red">*</span></label>
                                                <textarea name="deskripsi" class="form-control" rows="10" placeholder="Enter facilities"><?php echo $value['deskripsi'];?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Image <span class="color-red">*</span></label>
                                                <br>
                                                <img src="../joimg/tour/thumbnail/<?php echo $value['image'];?>" alt="<?php echo $value['image'];?>" width="300px">
                                                <br><br>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-success btn-file m-r-10">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="fupload">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                </div>
                                                <div class="alert alert-info" role="alert"><b>Attention!</b> <br><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG *.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Destination <span class="color-red">*</span></label>
                                                <select class="selectpicker" multiple="multiple" data-selected-text-format="count" data-style="btn-info"  id="multi-hotel" name="id_hotel[]">
                                                <?php
                                                    $hk = $database -> select($fields='*', $table='hotel', $where_clause='ORDER BY id_hotel DESC', $fetch='all');
                                                    $ex = explode(',', $value['id_hotel']);
                                                    foreach ($hk as $key => $value_hk) {
                                                        $checked = (in_array($value_hk['id_hotel'], $ex)) ? 'selected' : '' ;
                                                        echo '
                                                            <option value="'.$value_hk['id_hotel'].'" '.$checked.'>'.$value_hk['nm_hotel'].'</option>
                                                        ';
                                                    }                                                         
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="c-black f-500 m-b-20">Publish Post ?</p>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="Y" <?php if($value['status'] == 'Y'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            Yes
                                        </label>
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="status" value="N" <?php if($value['status'] == 'N'){echo "checked";}?>>
                                            <i class="input-helper"></i>
                                            No
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group kaki">
                                    <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>

                        </div>
                    </div>
<?php
    break;
    }
}
?>