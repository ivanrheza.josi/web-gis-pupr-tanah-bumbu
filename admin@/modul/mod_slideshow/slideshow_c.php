<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/ImageResizeSinur.php';

$database 	= new Database($db);

$module	= $_GET['module'];
$act	= $_GET['act'];

if($act=='insert')
{
	// Insert
	if ($module=='slideshow' AND $act=='insert')
	{
		$lokasi_file	= $_FILES['fupload']['tmp_name'];
		
		if(!empty($lokasi_file))
		{
			$image_name = img_resize($_FILES['fupload'],1024,'../../../joimg/slide/','akustik-ruang');

		    //data yang akan di insert berbentuk array
			$form_data = array(
			    "nama" 		=> "$_POST[nama]",
			    "gambar" 	=> "$image_name"
			);

			//proses insert ke database
            $database->insert($table="slide", $array=$form_data);
		}
		else
		{
			//data yang akan di insert berbentuk array
			$form_data = array(
			    "nama" 		=> "$_POST[nama]"
			);

			//proses insert ke database
            $database->insert($table="slide", $array=$form_data);
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

if($act=='update')
{
	// Update
	if ($module=='slideshow' AND $act=='update')
	{
	 	$lokasi_file    = $_FILES['fupload']['tmp_name'];
	  	
		if(!empty($lokasi_file))
		{
			$show	= $database->select($fields="gambar", $table="slide", $where_clause="WHERE id_slide = '$_POST[id]'");
			if($show['gambar'] != '')
			{
				unlink("../../../joimg/slide/$show[gambar]");
			}

            $image_name = img_resize($_FILES['fupload'],1024,'../../../joimg/slide/','akustik-ruang');

	   		//data yang akan diupdate berbentuk array
			$form_data = array(
				"nama" 		=> "$_POST[nama]",
				"gambar" 	=> "$image_name"
			);

			//proses update ke database
            $database->update($table="slide", $array=$form_data, $fields_key="id_slide", $id="$_POST[id]");
		}
		else
		{
			//data yang akan diupdate berbentuk array
			$form_data = array(
				"nama" 		=> "$_POST[nama]"
			);

			//proses update ke database
            $database->update($table="slide", $array=$form_data, $fields_key="id_slide", $id="$_POST[id]");
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}


if($act=='delete')
{
	// Delete
	if ($module=='slideshow' AND $act=='delete')
	{
		$show	= $database->select($fields="gambar", $table="slide", $where_clause="WHERE id_slide = '$_GET[id]'");

		if($show['gambar'] != '')
		{
			unlink("../../../joimg/slide/$show[gambar]");
            $database->delete($table="slide", $fields_key="id_slide", $id=$_GET['id']);
		}
		else
		{
            $database->delete($table="slide", $fields_key="id_slide", $id=$_GET['id']);
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
	}
}

}
?>
