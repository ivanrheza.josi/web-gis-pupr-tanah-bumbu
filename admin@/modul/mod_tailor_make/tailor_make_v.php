<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->


<?php
    $aksi="modul/mod_tailor_make/tailor_make_c.php?module=tailor_make";
    switch($_GET['act']){
    default:
?>

    <div class="card">
        <div class="card-body card-padding">

            <p class="c-black f-500 m-b-20">Reservation / Special Interest </p>

            <hr class="line">

            <div class="table-responsive">
                <table class='display' id='datatables'>
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Destination</th>
                            <th>Contact 1</th>
                            <th>Contact 2</th>
                            <th>Departure Date</th>
                            <th>Duration</th>
                            <th>Numb. Adults</th>
                            <th>Numb. Children</th>
                            <th>DateTime</th>
                            <th>Readed?</th>
                            <th width="18%">Actions</th>
                        </tr>
                    </thead>

                    <tbody>

                    <?php
                        $no=1;
                        $rsi = $database->select($fields="*", $table="tailor_make", $where_clause="ORDER BY id_tailor_make DESC", $fetch="all");
                        foreach ($rsi as $key => $value) {
                    ?>

                        <tr>
                            <th><?php echo $no; ?></th>
                            <td><?php echo $value['nama']; ?></td>
                            <td><?php echo $value['email']; ?></td>
                            <td><?php echo $value['destination']; ?></td>
                            <td><?php echo $value['contact1']; ?></td>
                            <td><?php echo $value['contact2']; ?></td>
                            <td><?php echo $value['checkin']; ?></td>
                            <td><?php echo $value['duration']; ?></td>
                            <td><?php echo $value['adults']; ?></td>
                            <td><?php echo $value['children']; ?></td>
                            <td><?php echo $value['waktu']; ?></td>
                            <td><?php if($value['status'] == '1'){echo "Sudah dibaca";}else{echo "Belum dibaca";}?></td>
                            <td>
                                <a href="?module=tailor_make&act=detail&id=<?php echo $value['id_tailor_make'];?>" class="btn bgm-cyan waves-effect"><i class="zmdi zmdi-eye zmdi-hc-fw"></i> Detail</a>
                                <a href="<?php echo "$aksi&act=delete&id=$value[id_tailor_make]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                            </td>
                        </tr>

                    <?php
                        $no++;
                        }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
    break;
    case "detail":
    $id     = $_GET['id'];
    $value  = $database->select($fields="*", $table="tailor_make", $where_clause="WHERE id_tailor_make='$id'");
    //var_dump($value); echo $_GET['act'];
    //var_dump($tailor_make);
    //data yang akan diupdate berbentuk array
    $form_data = array(
        "status"    => "1"
    );
    //proses update ke database
    $database->update($table="tailor_make", $array=$form_data, $fields_key="id_tailor_make", $id="$id");

    // include "../josys/function/Download.php";
?>

    <div class="card">

        <div class="card-body card-padding">
            <p class="c-black f-500 m-b-20">Reservation / Special Interest / </p>
            <hr class="line">
            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">

                <input type="hidden" name="id" value="<?php echo $id;?>">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Name </label>
                                <input type="text" name="name" class="form-control" maxlength="100" value="<?php echo $value['nama'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Email </label>
                                <input type="email" name="email" class="form-control" maxlength="150" value="<?php echo $value['email'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Destination </label>
                                <input type="text" name="destination" class="form-control" maxlength="100" value="<?php echo $value['destination'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Contact 1 </label>
                                <input type="text" name="contact1" class="form-control" maxlength="150" value="<?php echo $value['contact1'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Contact 2 </label>
                                <input type="text" name="contact2" class="form-control" maxlength="150" value="<?php echo $value['contact2'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Departure Date </label>
                                <input type="text" name="subject" class="form-control" maxlength="150" value="<?php echo $value['checkin'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Duration </label>
                                <input type="text" name="duration" class="form-control" maxlength="100" value="<?php echo $value['duration'];?>" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Number of Adults </label>
                                <input type="text" name="adults" class="form-control" maxlength="100" value="<?php echo $value['adults'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Number of Children </label>
                                <input type="text" name="children" class="form-control" maxlength="100" value="<?php echo $value['children'];?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>DateTime </label>
                                <input type="text" name="dateTime" class="form-control" maxlength="150" value="<?php echo $value['waktu'];?>" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="fg-line">
                                <label>Any Other Requirements </label>
                                <textarea name="note" class="form-control" rows="5" readonly><?php echo $value['catatan'];?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group kaki">
                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                </div>

            </form>
        </div>
    </div>

<?php
    break;
    }
}
?>
