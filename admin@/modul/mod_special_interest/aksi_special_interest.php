<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
    //Import System
    require_once '../../../josys/db_connect.php';
    include_once '../../../josys/class/Database.php';
    include_once '../../../josys/class/Upload.php';
    include_once '../../../josys/function/Seo.php';

    $database   = new Database($db);
    $upload     = new Upload();

    $module = $_GET['module'];
    $act    = $_GET['act'];


    if($act=='insert')
    {
        // Insert
        if ($module=='special_interest' AND $act=='insert')
        {
            $lokasi_file    = $_FILES['fupload']['tmp_name'];
            $tipe_file      = $_FILES['fupload']['type'];
            $nama_file      = $_FILES['fupload']['name'];

            $nama_seo       = substr(seo($_POST['judul']), 0, 75);
            $acak           = rand(000,999);
            $nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;
            $deskripsi      = stripslashes($_POST['deskripsi']);
            $contents       = stripslashes($_POST['konten']);
            $contents_harga = stripslashes($_POST['konten_harga']);
            $date = date('Y-m-d');
            $id_hotel = "";
            foreach ($_POST['id_hotel'] as $key => $hotel) {
                $id_hotel .= $hotel.",";
            }
            $lokasi_peta    = $_FILES['fupload_peta']['tmp_name'];
            $tipe_peta      = $_FILES['fupload_peta']['type'];
            $nama_peta      = $_FILES['fupload_peta']['name'];
            $nama_file_unik_peta = $nama_seo.'-'.$acak.'-'.$nama_peta;

            if(!empty($lokasi_file) && !empty($lokasi_peta))
            {

                if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }
                if ($tipe_peta != "image/jpeg" AND $tipe_peta != "image/pjpeg" AND $tipe_peta != "image/gif" AND $tipe_peta != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File peta yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }

                $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/special_interest/");
                $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/special_interest", $thumbDirectory="../../../joimg/special_interest/thumbnail", $thumbWidth="500");

                $upload->berkas($fileName=$nama_file_unik_peta, $fileDirectory="../../../joimg/peta_special_interest/", $inputName="fupload_peta");
                $upload->thumbnail($imageName=$nama_file_unik_peta, $imageDirectory="../../../joimg/peta_special_interest", $thumbDirectory="../../../joimg/peta_special_interest/thumbnail", $thumbWidth="500");

                //data yang akan di insert berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "image"             => "$nama_file_unik",
                    "peta"              => "$nama_file_unik_peta",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses insert ke database
                $database->insert($table="special_interest", $array=$form_data);
            }

            else if(!empty($lokasi_file) && empty($lokasi_peta))
            {

                if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }

                $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/special_interest/");
                $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/special_interest", $thumbDirectory="../../../joimg/special_interest/thumbnail", $thumbWidth="500");

                //data yang akan di insert berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "image"             => "$nama_file_unik",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses insert ke database
                $database->insert($table="special_interest", $array=$form_data);
            }
            else if(empty($lokasi_file) && !empty($lokasi_peta))
            {

                if ($tipe_peta != "image/jpeg" AND $tipe_peta != "image/pjpeg" AND $tipe_peta != "image/gif" AND $tipe_peta != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File Peta yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }

                $upload->berkas($fileName=$nama_file_unik_peta, $fileDirectory="../../../joimg/peta_special_interest/", $inputName="fupload_peta");
                $upload->thumbnail($imageName=$nama_file_unik_peta, $imageDirectory="../../../joimg/peta_special_interest", $thumbDirectory="../../../joimg/peta_special_interest/thumbnail", $thumbWidth="500");

                //data yang akan di insert berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "peta"              => "$nama_file_unik_peta",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses insert ke database
                $database->insert($table="special_interest", $array=$form_data);
            }
            else
            {
                //data yang akan di insert berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",        
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses insert ke database
                $database->insert($table="special_interest", $array=$form_data);
            }

            echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
        }
        else
        {
            echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        }
    }

    if($act=='update')
    {
        // Update
        if ($module=='special_interest' AND $act=='update')
        {
            $lokasi_file    = $_FILES['fupload']['tmp_name'];
            $tipe_file      = $_FILES['fupload']['type'];
            $nama_file      = $_FILES['fupload']['name'];

            $nama_seo       = substr(seo($_POST['judul']), 0, 50);
            $acak           = rand(000,999);
            $nama_file_unik = $nama_seo.'-'.$acak.'-'.$nama_file;
            $contents       = stripslashes($_POST['konten']);
            $deskripsi      = stripslashes($_POST['deskripsi']);
            $contents_harga = stripslashes($_POST['konten_harga']);
            $date = date('Y-m-d');
            $id_hotel = "";
            foreach ($_POST['id_hotel'] as $key => $hotel) {
                $id_hotel .= $hotel.",";
            }

            $lokasi_peta    = $_FILES['fupload_peta']['tmp_name'];
            $tipe_peta      = $_FILES['fupload_peta']['type'];
            $nama_peta      = $_FILES['fupload_peta']['name'];
            $nama_file_unik_peta = $nama_seo.'-'.$acak.'-'.$nama_peta;

            if(!empty($lokasi_file) && !empty($lokasi_peta))
            {

                if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }
                if ($tipe_peta != "image/jpeg" AND $tipe_peta != "image/pjpeg" AND $tipe_peta != "image/gif" AND $tipe_peta != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File Peta yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }


                $show   = $database->select($fields="image, peta", $table="special_interest", $where_clause="WHERE id_special_interest = '$_POST[id]'", $fetch='');
                if($show['image'] != '')
                {
                    unlink("../../../joimg/special_interest/$show[image]");
                    unlink("../../../joimg/special_interest/thumbnail/$show[image]");
                }
                if($show['peta'] != '')
                {
                    unlink("../../../joimg/peta_special_interest/$show[peta]");
                    unlink("../../../joimg/peta_special_interest/thumbnail/$show[peta]");
                }

                $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/special_interest/");
                $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/special_interest", $thumbDirectory="../../../joimg/special_interest/thumbnail", $thumbWidth="500");

                $upload->berkas($fileName=$nama_file_unik_peta, $fileDirectory="../../../joimg/peta_special_interest/", $inputName="fupload_peta");
                $upload->thumbnail($imageName=$nama_file_unik_peta, $imageDirectory="../../../joimg/peta_special_interest", $thumbDirectory="../../../joimg/peta_special_interest/thumbnail", $thumbWidth="500");

                //data yang akan diupdate berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "image"             => "$nama_file_unik",
                    "peta"              => "$nama_file_unik_peta",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses update ke database
                $database->update($table="special_interest", $array=$form_data, $fields_key="id_special_interest", $id="$_POST[id]");
            }
            else if(!empty($lokasi_file) && empty($lokasi_peta))
            {

                if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/gif" AND $tipe_file != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }

                $show   = $database->select($fields="image", $table="special_interest", $where_clause="WHERE id_special_interest = '$_POST[id]'", $fetch='');
                if($show['image'] != '')
                {
                    unlink("../../../joimg/special_interest/$show[image]");
                    unlink("../../../joimg/special_interest/thumbnail/$show[image]");
                }

                $upload->berkas($fileName=$nama_file_unik, $fileDirectory="../../../joimg/special_interest/");
                $upload->thumbnail($imageName=$nama_file_unik, $imageDirectory="../../../joimg/special_interest", $thumbDirectory="../../../joimg/special_interest/thumbnail", $thumbWidth="500");

                //data yang akan diupdate berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "image"             => "$nama_file_unik",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses update ke database
                $database->update($table="special_interest", $array=$form_data, $fields_key="id_special_interest", $id="$_POST[id]");
            }
            else if(empty($lokasi_file) && !empty($lokasi_peta))
            {

                if ($tipe_peta != "image/jpeg" AND $tipe_peta != "image/pjpeg" AND $tipe_peta != "image/gif" AND $tipe_peta != "image/png"){
                    echo "<script>alert('Data tidak tersimpan! Upload Gagal, Pastikan File Peta yang di Upload bertipe *.JPG, *.GIF, *.PNG.'); window.location = '../../media.php?module=$module';</script>";
                    die();
                }

                $show   = $database->select($fields="peta", $table="special_interest", $where_clause="WHERE id_special_interest = '$_POST[id]'", $fetch='');
                if($show['peta'] != '')
                {
                    unlink("../../../joimg/peta_special_interest/$show[peta]");
                    unlink("../../../joimg/peta_special_interest/thumbnail/$show[peta]");
                }

                $upload->berkas($fileName=$nama_file_unik_peta, $fileDirectory="../../../joimg/peta_special_interest/", $inputName="fupload_peta");
                $upload->thumbnail($imageName=$nama_file_unik_peta, $imageDirectory="../../../joimg/peta_special_interest", $thumbDirectory="../../../joimg/peta_special_interest/thumbnail", $thumbWidth="500");

                //data yang akan diupdate berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "peta"              => "$nama_file_unik_peta",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses update ke database
                $database->update($table="special_interest", $array=$form_data, $fields_key="id_special_interest", $id="$_POST[id]");
            }
            else
            {
                //data yang akan diupdate berbentuk array
                $form_data = array(
                    "id_special_interest_kategori"  => "$_POST[kategori]",
                    "nama"              => "$_POST[judul]",
                    "length"            => "$_POST[length]",
                    "harga"             => "$_POST[harga]",
                    "konten"            => "$contents",
                    "konten_harga"      => "$contents_harga",
                    "deskripsi"         => "$deskripsi",
                    "id_hotel"          => "$id_hotel",
                    "date"              => "$date",
                    "seo"               => "$nama_seo",
                    "status"            => "$_POST[status]"
                );

                //proses update ke database
                $database->update($table="special_interest", $array=$form_data, $fields_key="id_special_interest", $id="$_POST[id]");
            }

            echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
        }
        else
        {
            echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        }
    }


    if($act=='delete')
    {
        // Delete
        if ($module=='special_interest' AND $act=='delete')
        {
        //  $show   = db_get_one("SELECT * FROM sosmed WHERE id_sosmed='$_GET[id]'");
            $show   = $database->select($fields="image, peta", $table="special_interest", $where_clause="WHERE id_special_interest = '$_GET[id]'", $fetch='');
            if($show['image'] != '')
            {
                unlink("../../../joimg/special_interest/$show[image]");
                unlink("../../../joimg/special_interest/thumbnail/$show[image]");
                $database->delete($table="special_interest", $fields_key="id_special_interest", $id="$_GET[id]");
            }
            if($show['peta'] != '')
            {
                unlink("../../../joimg/peta_special_interest/$show[peta]");
                unlink("../../../joimg/peta_special_interest/thumbnail/$show[peta]");
                $database->delete($table="special_interest", $fields_key="id_special_interest", $id="$_GET[id]");
            }
            else
            {
                $database->delete($table="special_interest", $fields_key="id_special_interest", $id="$_GET[id]");
            }

            echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
        }
        else
        {
            echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        }
    }

}
?>