<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable();
    } );
    </script>
    <!--End DataTables-->


<?php
    $aksi="modul/mod_review/review_c.php?module=review";
    switch($_GET['act']){
    default:
?>

                <div class="card">
                    <div class="card-body card-padding">

                        <p class="c-black f-500 m-b-20">Support Web - Review </p>

                        <hr class="line">

                        <div class="table-responsive">
                            <table class='display' id='datatables'>
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Comment</th>
                                        <th>On</th>
                                        <th>Readed?</th>
                                        <th width="18%">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                <?php
                                    $no=1;
                                    $review = $database->select($fields="*", $table="review", $where_clause="ORDER BY id_review DESC", $fetch="all");
                                    foreach ($review as $key => $value) {
                                        if ($value['category'] == 'destination') {
                                            $tour = $database->select($fields = "*", $table = "tour", $where_clause = "WHERE id_tour = $value[id_special]", $fetch = "");
                                            $link = '<a href="../tour-destination-'.$tour['seo'].'-'.$tour['id_tour'].'" target="_blank">'.$tour['nama'].'</a>';
                                        } else {                                            
                                            $tour = $database->select($fields = "*", $table = "special_interest", $where_clause = "WHERE id_special_interest = $value[id_special]", $fetch = "");
                                            $link = '<a href="../detail-special-interest-'.$tour['seo'].'-'.$tour['id_special_interest'].'" target="_blank">'.$tour['nama'].'</a>';
                                        }                                        
                                ?>
                                    <tr>
                                        <th><?php echo $no; ?></th>
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo $value['email']; ?></td>
                                        <td><?php echo $value['comment']; ?></td>
                                        <td><?php echo $link; ?></td>
                                        <td><?php if($value['status'] == '1'){echo "Sudah dibaca";}else{echo "Belum dibaca";}?></td>
                                        <td>
                                            <a href="?module=review&act=detail&id=<?php echo $value['id_review'];?>" class="btn bgm-cyan waves-effect"><i class="zmdi zmdi-eye zmdi-hc-fw"></i> Detail</a>
                                            <a href="<?php echo "$aksi&act=delete&id=$value[id_review]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                        </td>
                                    </tr>

                                <?php
                                    $no++;
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

<?php
    break;
    case "detail":
    $id     = $_GET['id'];
    $value  = $database->select($fields="*", $table="review", $where_clause="WHERE id_review='$id'");

    if ($value['category'] == 'destination') {
        $tour = $database->select($fields = "*", $table = "tour", $where_clause = "WHERE id_tour = $value[id_special]", $fetch = "");
        $link = '<a href="../tour-destination-'.$tour['seo'].'-'.$tour['id_tour'].'" target="_blank">'.$tour['nama'].'</a>';
    } else {                                            
        $tour = $database->select($fields = "*", $table = "special_interest", $where_clause = "WHERE id_special_interest = $value[id_special]", $fetch = "");
        $link = '<a href="../detail-special-interest-'.$tour['seo'].'-'.$tour['id_special_interest'].'" target="_blank">'.$tour['nama'].'</a>';
    }

    //data yang akan diupdate berbentuk array
    $form_data = array(
        "status"	=> "1"
    );
    //proses update ke database
    $database->update($table="review", $array=$form_data, $fields_key="id_review", $id="$id");

    include "../josys/function/Download.php";
?>

                    <div class="card">

                        <div class="card-body card-padding">
                            <p class="c-black f-500 m-b-20">Support Web / Detail Review / <?php echo $value['name'].' / On : '.$link;?></p>
                            <hr class="line">
                            <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">

                                <input type="hidden" name="id" value="<?php echo $id;?>">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Name </label>
                                                <input type="text" name="name" class="form-control" maxlength="100" value="<?php echo $value['name'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Email </label>
                                                <input type="email" name="email" class="form-control" maxlength="150" value="<?php echo $value['email'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Rating </label>
                                                <input type="text" name="phone" class="form-control" maxlength="150" value="<?php echo $value['rating'];?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="fg-line">
                                                <label>Comment </label>
                                                <textarea name="message" class="form-control" rows="5" disabled><?php echo $value['comment'];?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group kaki">
                                    <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                                </div>

                            </form>
                        </div>
                    </div>

<?php
    break;
    }
}
?>
