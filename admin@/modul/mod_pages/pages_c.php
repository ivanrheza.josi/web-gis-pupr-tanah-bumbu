<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
    require_once '../../../josys/db_connect.php';
    include_once '../../../josys/class/Database.php';
    include_once '../../../josys/function/Seo.php';

    $database 	= new Database($db);

	$module	= $_GET['module'];
	$act	= $_GET['act'];
	$id 	= $_POST['id'];
	// $idd 	= $_POST['idd'];

	// if ($module=='pages' AND $act=='updatedesc')
	// {
	// 	$content 	= stripslashes($_POST['content']);
 //    	//data yang akan diupdate berbentuk array
	// 	$form_data = array(
	// 		"static_content" => "$content"
	// 	);
	// 	//proses update ke database
 //        $database->update($table="modul", $array=$form_data, $fields_key="id_modul", $id=$idd);
 //        $id = $_POST['id'];
	// 	echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$id&idd=$idd';</script>";
	// }

	// Update pages
	if ($module=='pages' AND $act=='update_2')
	{
		$content 	= stripslashes($_POST['content']);
		$content_2 	= stripslashes($_POST['content_2']);

    	//data yang akan diupdate berbentuk array
		$form_data = array(
			"static_content" 	=> "$content", 
			"static_content_2" 	=> "$content_2"
		);

		//proses update ke database
        $database->update($table="modul", $array=$form_data, $fields_key="id_modul", $id=$id);

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$id';</script>";
	}

	if ($module=='pages' AND $act=='update')
	{
		$content 	= stripslashes($_POST['content']);

    	//data yang akan diupdate berbentuk array
		$form_data = array(
			"static_content" => "$content"
		);

		//proses update ke database
        $database->update($table="modul", $array=$form_data, $fields_key="id_modul", $id=$id);

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$id&idd=$idd';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$id&idd=$idd';</script>";
	}

}
?>
