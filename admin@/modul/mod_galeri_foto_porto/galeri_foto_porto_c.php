<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
//Import System
require_once '../../../josys/db_connect.php';
include_once '../../../josys/class/Database.php';
include_once '../../../josys/function/ImageResizeSinur.php';

$database 	= new Database($db);

$module		= $_GET['module'];
$act		= $_GET['act'];

if($act=='insert')
{
	$id_porto	= $_POST['id_porto'];
	
	// Insert
	if ($module=='galeri_foto_porto' AND $act=='insert')
	{
		$lokasi_file	= $_FILES['fupload']['tmp_name'];
		
		if(!empty($lokasi_file))
		{
			$image_name = img_resize($_FILES['fupload'],840,'../../../joimg/portofolio/','sijari-tanah-bumbu');
			
		    //data yang akan di insert berbentuk array
			$form_data = array(
			    "nama" 			=> "$_POST[nama]",
			    "id_portofolio" => "$id_porto",
			    "image" 		=> "$image_name"
			);

			//proses insert ke database
            $database->insert($table="galeri_foto_porto", $array=$form_data);
		}
		else
		{
			//data yang akan di insert berbentuk array
			$form_data = array(
			    "nama" 			=> "$_POST[nama]",
			    "id_portofolio" => "$id_porto"
			);

			//proses insert ke database
            $database->insert($table="galeri_foto_porto", $array=$form_data);
		}

		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id=$id_porto';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$id_porto';</script>";
	}
}

if($act=='update')
{
	// Update
	if ($module=='galeri_foto_porto' AND $act=='update')
	{
	 	if( ! empty($_FILES['fupload']['tmp_name']) )
		{			
			$show	= $database->select($fields="image", $table="galeri_foto_porto", $where_clause="WHERE id_galeri_foto_porto = '$_POST[id]'");
			if($show['image'] != '')
			{
				unlink("../../../joimg/portofolio/$show[image]");
			}

            $image_name = img_resize($_FILES['fupload'],840,'../../../joimg/portofolio/','sijari-tanah-bumbu');

	   		//data yang akan diupdate berbentuk array
			$form_data = array(
				"nama" 			=> "$_POST[nama]",
				"image" 		=> "$image_name"
			);

			//proses update ke database
            $database->update($table="galeri_foto_porto", $array=$form_data, $fields_key="id_galeri_foto_porto", $id="$_POST[id]");
		}
		else
		{
			//data yang akan diupdate berbentuk array
			$form_data = array(
				"nama" 			=> "$_POST[nama]",
			);

			//proses update ke database
            $database->update($table="galeri_foto_porto", $array=$form_data, $fields_key="id_galeri_foto_porto", $id="$_POST[id]");
		}
		echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module&id={$_POST["id_portofolio"]}';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id={$_POST["id_portofolio"]}';</script>";
	}
}


if($act=='delete')
{
	$id_porto	= $_GET['id_porto'];

	// Delete
	if ($module=='galeri_foto_porto' AND $act=='delete')
	{
		$show	= $database->select($fields="image", $table="galeri_foto_porto", $where_clause="WHERE id_galeri_foto_porto = '$_GET[id]'");

		if($show['image'] != '')
		{
			unlink("../../../joimg/portofolio/$show[image]");
            $database->delete($table="galeri_foto_porto", $fields_key="id_galeri_foto_porto", $id=$_GET['id']);
		}
		else
		{
            $database->delete($table="galeri_foto_porto", $fields_key="id_galeri_foto_porto", $id=$_GET['id']);
		}
		echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module&id=$id_porto';</script>";
	}
	else
	{
		echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module&id=$id_porto';</script>";
	}
}

}
?>
