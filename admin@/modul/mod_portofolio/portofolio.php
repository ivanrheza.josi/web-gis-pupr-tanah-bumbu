<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else {
?>
    <script src="assets/vendors/input-mask/input-mask.min.js"></script>
    <!--Begin DataTables-->
    <link rel="stylesheet" href="assets/vendors/dataTables/css/jquery.dataTables.css" type="text/css" media="screen" />
    <script type="text/javascript" charset="utf8" src="assets/vendors/dataTables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/vendors/dataTables/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#datatables').DataTable({
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        });
    } );
    </script>
    <!-- TinyMCE 4.x -->
    <script type="text/javascript" src="../jolib/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "#konten",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality jbimages",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],

        toolbar1: "undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media jbimages | link forecolor backcolor emoticons | pagebreak print preview",
        image_advtab: true,
        relative_urls: false
    });
    </script>
    <!-- /TinyMCE -->

    <style>
    .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
        margin-left: 0px;
    }
    </style>
  

<?php
    $aksi="modul/mod_portofolio/aksi_portofolio.php?module=portofolio";
    switch($_GET['act']){
    default:
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Daerah Irigasi / Daerah Irigasi
                    <a href="?module=portofolio&act=add" class="btn bgm-deeppurple btn-sm m-t-10 waves-effect pull-right"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> Add New</a>
                </p>
                <hr class="line">
                <div class="table-responsive">
                    <table class='display' id='datatables'>
                        <thead>
                            <tr class="inamakolom">
                                <th width="5%">No</th>
                                <th>Judul</th>
                                <th>Peta</th>
                                <th>Skema</th>
                                <th>Kecamatan</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th width="25%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no=1;
                            $posts = $database->select($fields="*", $table="portofolio", $where_clause="ORDER BY id_portofolio DESC", $fetch="all");
                            foreach ($posts as $key => $value) {
                                $posts_cat = $database->select($fields="*", $table="portofolio_kategori", $where_clause="WHERE id_portofolio_kategori = '$value[id_portofolio_kategori]'", $fetch="");
                        ?>
                                <tr>
                                    <th style="text-align: center;"><?php echo $no; ?></th>
                                    <td><?php echo $value['judul']; ?></td>
                                    <td style="text-align: center;"><img src="../joimg/portofolio/<?php echo $value['image']; ?>" alt="" height="50px"></td>
                                    <td style="text-align: center;"><img src="../joimg/portofolio/<?php echo $value['image_2']; ?>" alt="" height="50px"></td>
                                    <td><?php echo $posts_cat['nama']; ?></td>
                                    <td style="text-align: center;"><?php echo $value['date']; ?></td>
                                    <td style="text-align: center;"><?php if($value['status'] == 'Y'){echo "Publish";}else{echo "Hidden";} ?></td>
                                    <td style="text-align: center;">
                                        <a href="?module=galeri_foto_porto&id=<?php echo $value['id_portofolio'];?>" class="btn bgm-lightgreen waves-effect"><i class="zmdi zmdi-image zmdi-hc-fw"></i> Galeri</a>
                                        <a href="?module=portofolio&act=edit&id=<?php echo $value['id_portofolio'];?>" class="btn bgm-teal waves-effect"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> Edit</a>
                                        <a href="<?php echo "$aksi&act=delete&id=$value[id_portofolio]";?>" class="btn bgm-pink waves-effect" onclick="return confirm('Apakah anda yakin menghapus data ini?');"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Delete</a>
                                    </td>
                                </tr>
                        <?php
                                $no++;
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php
    break;
    case "add":
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Daerah Irigasi / Daerah Irigasi / Add New</p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=insert">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Judul</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title" maxlength="150" required="require">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Kategori Daerah Irigasi</label>
                                    <div class="select">
                                        <select class="form-control" name="stat" required="require" id="diSelection">
                                            <option value="" disabled="disable" selected="select"> - Pilih Kategori Daerah Irigasi - </option>
                                            <?php
                                                $categories_di= $database->select('*', 'categories', 'ORDER BY category_title ASC', 'all');
                                                foreach ($categories_di as $key => $value) {
                                                    echo "<option value='{$value["category_id"]}'>{$value["category_title"]}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Kecamatan</label>
                                    <div class="select">
                                        <select class="form-control" name="kategori" required="require" id="kecSelection">
                                            <option value="" disabled="disable" selected="select">- Pilih Kecamatan -</option>
                                            <option value="" disabled="disable">Maaf Anda Belum Memilih Kategori Daerah Irigasi.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Komentar</label>
                                    <textarea id="konten" name="konten" class="form-control" rows="10" placeholder="Enter konten"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Peta</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Skema</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload_2">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Identitas Daerah Irigasi</label>
                                    <br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload_3">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-12">
                            <p class="c-black f-500 m-b-20">Publish Post ?</p>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="Y" checked>
                                <i class="input-helper"></i>
                                Yes
                            </label>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="N">
                                <i class="input-helper"></i>
                                No
                            </label>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
                <!-- end form -->
            </div>
        </div>
        <!-- /.card -->
<script>
    (function(j){
        j('#diSelection').on('change', function(){
            j.get('daerah_irigasi.php', {"action": "kecamatan","category_id": j(this).val() }, function(dataKecamatan){
                var html= '';
                html += '<option value="" disabled="disable" selected="select">- Pilih Kecamatan -</option>';
                if ( dataKecamatan.length > 0 ) {
                    for ( i = 0; i < dataKecamatan.length; i++) {
                        html += '<option value="'+dataKecamatan[i].id_portofolio_kategori+'">'+dataKecamatan[i].nama+'</option>';
                    }
                }

                j('#kecSelection').html(html);
            }, "json");
        });
    })(jQuery)
</script>

<?php
    break;
    case "edit":
    $id         = $_GET['id'];
    $value      = $database->select($fields="*", $table="portofolio", $where_clause="WHERE id_portofolio = '$id'", $fetch="");
?>
        <div class="card">
            <div class="card-body card-padding">
                <p class="c-black f-500 m-b-20">Daerah Irigasi / Daerah Irigasi / Edit</p>
                <hr class="line">
                <form method="post" enctype="multipart/form-data" action="<?php echo $aksi;?>&act=update">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Judul</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Enter title" value="<?php echo $value['judul'];?>" maxlength="150" required="require">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Kategori Daerah Irigasi</label>
                                    <div class="select">
                                        <select class="form-control" name="stat" required="require" id="diSelection">
                                            <option value="" disabled="disable">- Pilih Kategori Daerah Irigasi -</option>
                                            <?php
                                                $categories= $database->select('*','categories','ORDER BY category_title ASC', 'all');
                                                foreach ($categories as $key => $value_c) { 
                                                    echo "<option value='{$value_c["category_id"]}' ".($value_c["category_id"]==$value["stat"] ? 'selected' : FALSE).">{$value_c["category_title"]}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Kecamatan</label>
                                    <div class="select">
                                        <select class="form-control" name="kategori" required="require" id="kecSelection">
                                            <option value="" disabled="disable" selected="select">- Pilih Kecamatan -</option>
                                            <?php
                                                $kecamatan = $database -> select($fields='*', $table='portofolio_kategori', $where_clause='WHERE stat='.$value['stat'].' ORDER BY nama ASC', $fetch='all');
                                                foreach ($kecamatan as $key => $value_k) {
                                                    echo "<option value='{$value_k["id_portofolio_kategori"]}' ".($value_k["id_portofolio_kategori"]==$value["id_portofolio_kategori"] ? 'selected' : FALSE).">{$value_k["nama"]}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Komentar</label>
                                    <textarea id="konten" name="konten" class="form-control" rows="10" placeholder="Enter konten"><?php echo $value['konten'];?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Peta</label>
                                    <br>
                                    <img src="../joimg/portofolio/<?php echo $value['image'];?>" alt="" height="200px">
                                    <br><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Skema</label>
                                    <br>
                                    <img src="../joimg/portofolio/<?php echo $value['image_2'];?>" alt="" height="200px">
                                    <br><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload_2">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Identitas Daerah Irigasi</label>
                                    <br>
                                    <img src="../joimg/portofolio/<?php echo $value['image_3'];?>" alt="" height="200px">
                                    <br><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-success btn-file m-r-10">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fupload_3">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert"><b>Attention!</b><p style="font-size: 13px;"><b>Image Type :</b> JPG, PNG, JPEG. <b>Image Size :</b> 840 x 512 pixels.</p></div>
                        </div>
                        <div class="col-md-12">
                            <p class="c-black f-500 m-b-20">Publish Post ?</p>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="Y" <?php if($value['status'] == 'Y'){echo "checked";}?> >
                                <i class="input-helper"></i>
                                Yes
                            </label>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="status" value="N" <?php if($value['status'] == 'N'){echo "checked";}?> >
                                <i class="input-helper"></i>
                                No
                            </label>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="form-group kaki">
                        <button type="submit" class="btn btn-primary btn-sm m-t-10 waves-effect"><i class="zmdi zmdi-check"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm m-t-10 waves-effect" onclick="self.history.back()"><i class="zmdi zmdi-close"></i> Cancel</button>
                    </div>
                </form>
                <!-- end form -->
            </div>
        </div>
        <!-- /.card -->
<script>
    (function(j){
        j('#diSelection').on('change', function(){
            j.get('daerah_irigasi.php', {"action": "kecamatan","category_id": j(this).val() }, function(dataKecamatan){
                var html= '';
                html += '<option value="" disabled="disable" selected="select">- Pilih Kecamatan -</option>';
                if ( dataKecamatan.length > 0 ) {
                    for ( i = 0; i < dataKecamatan.length; i++) {
                        html += '<option value="'+dataKecamatan[i].id_portofolio_kategori+'">'+dataKecamatan[i].nama+'</option>';
                    }
                }

                j('#kecSelection').html(html);
            }, "json");
        });
    })(jQuery)
</script>
<?php
    break;
    }
}
?>