<?php
session_start();
if (empty($_SESSION['id_users']) || empty($_SESSION['username']) || empty($_SESSION['password'])){
    echo "
    <center>Untuk mengakses modul, Anda harus login <br>
    <a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
    //Import System
    require_once '../../../josys/db_connect.php';
    include_once '../../../josys/class/Database.php';
    include_once '../../../josys/function/Seo.php';
    include_once '../../../josys/function/ImageResizeSinur.php';

    $database   = new Database($db);

    $module = $_GET['module'];
    $act    = $_GET['act'];

    if($act=='insert')
    {
        // Insert
        if ($module=='portofolio' AND $act=='insert')
        {
            $lokasi_file    = $_FILES['fupload']['tmp_name'];
            $lokasi_file_2  = $_FILES['fupload_2']['tmp_name'];
            $lokasi_file_3  = $_FILES['fupload_3']['tmp_name'];
            $nama_seo       = substr(seo($_POST['judul']), 0, 75);
            $contents       = stripslashes($_POST['konten']);
            $date           = date('Y-m-d');

            $image_name     = !empty($lokasi_file) ? img_resize($_FILES['fupload'],840,'../../../joimg/portofolio/','sijari-tanahbumbu') : '';
            $image_name_2   = !empty($lokasi_file_2) ? img_resize($_FILES['fupload_2'],840,'../../../joimg/portofolio/','sijari-tanahbumbu') : '';
            $image_name_3   = !empty($lokasi_file_3) ? img_resize($_FILES['fupload_3'],840,'../../../joimg/portofolio/','sijari-tanahbumbu') : '';
            
            //data yang akan di insert berbentuk array
            $form_data = array(
                "stat"                      => "$_POST[stat]",
                "id_portofolio_kategori"    => "$_POST[kategori]",
                "judul"                     => "$_POST[judul]",
                "konten"                    => "$contents",
                "image"                     => "$image_name",
                "image_2"                   => "$image_name_2",
                "image_3"                   => "$image_name_3",
                "date"                      => "$date",
                "seo"                       => "$nama_seo",
                "status"                    => "$_POST[status]"
            );

            //proses insert ke database
            $database->insert($table="portofolio", $array=$form_data);

            echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
        }
        else
        {
            echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        }
    }

    if($act=='update')
    {
        // Update
        if ($module=='portofolio' AND $act=='update')
        {
            $lokasi_file    = $_FILES['fupload']['tmp_name'];
            $lokasi_file_2  = $_FILES['fupload_2']['tmp_name'];
            $lokasi_file_3  = $_FILES['fupload_3']['tmp_name'];
            $nama_seo       = substr(seo($_POST['judul']), 0, 75);
            $contents       = stripslashes($_POST['konten']);
            $date           = date('Y-m-d');
            
            //data yang akan diupdate berbentuk array
            $form_data = array(
                "stat"                      => "$_POST[stat]",
                "id_portofolio_kategori"    => "$_POST[kategori]",
                "judul"                     => "$_POST[judul]",
                "konten"                    => "$contents",
                "date"                      => "$date",
                "seo"                       => "$nama_seo",
                "status"                    => "$_POST[status]"
            );
            $show   = $database->select($fields="image, image_2, image_3", $table="portofolio", $where_clause="WHERE id_portofolio = '$_POST[id]'", $fetch='');
            if( !empty($lokasi_file) )
            {
                if($show['image'] != '')
                {
                    unlink("../../../joimg/portofolio/$show[image]");
                }
                $form_data['image']= img_resize($_FILES['fupload'],840,'../../../joimg/portofolio/','sijari-tanahbumbu');

            }
            if( !empty($lokasi_file_2) )
            {
                if($show['image_2'] != '')
                {
                    unlink("../../../joimg/portofolio/$show[image_2]");
                }
                $form_data['image_2']= img_resize($_FILES['fupload_2'],840,'../../../joimg/portofolio/','sijari-tanahbumbu');

            }
            if( !empty($lokasi_file_3) )
            {
                if($show['image_3'] != '')
                {
                    unlink("../../../joimg/portofolio/$show[image_3]");
                }
                $form_data['image_3']= img_resize($_FILES['fupload_3'],840,'../../../joimg/portofolio/','sijari-tanahbumbu');

            }

            //proses update ke database
            $database->update($table="portofolio", $array=$form_data, $fields_key="id_portofolio", $id="$_POST[id]");

            echo "<script>alert('Sukses! Data Telah Berhasil Disimpan.'); window.location = '../../media.php?module=$module';</script>";
        }
        else
        {
            echo "<script>alert('Maaf! Data Gagal Disimpan, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        }
    }


    if($act=='delete')
    {
        // Delete
        if ($module=='portofolio' AND $act=='delete')
        {
            $show   = $database->select($fields="image, image_2, image_3", $table="portofolio", $where_clause="WHERE id_portofolio = '$_GET[id]'", $fetch='');
            if($show['image'] != '')
            {
                unlink("../../../joimg/portofolio/$show[image]");
            }
            if($show['image_2'] != '')
            {
                unlink("../../../joimg/portofolio/$show[image_2]");
            }
            if($show['image_3'] != '')
            {
                unlink("../../../joimg/portofolio/$show[image_3]");
            }
            $database->delete($table="portofolio", $fields_key="id_portofolio", $id="$_GET[id]");

            echo "<script>alert('Sukses! Data Telah Berhasil Dihapus.'); window.location = '../../media.php?module=$module';</script>";
        }
        else
        {
            echo "<script>alert('Maaf! Data Gagal Dihapus, Silahkan coba lagi.'); window.location = '../../media.php?module=$module';</script>";
        }
    }

}
?>