<script src="assets/vendors/input-mask/input-mask.min.js"></script>
<?php
    require_once "config.php";
    $kat_edit   = explode("-", $_POST['kat_edit']);
    $value      = $database->select($fields="*", $table="posts", $where_clause="WHERE id_posts = '$kat_edit[0]'", $fetch="");

    if($_POST['kat'] == "Mobil" || $_POST['kat'] == "Truck dan Mobil Niaga" || $_POST['kat'] == "Motor") {
        echo '
        <div class="col-md-12">
            <hr class="garis">
        </div>
        <hr class="garis">
        <div class="col-md-6">
            <div class="form-group">
                <div class="fg-line">
                    <label>Merk <span class="color-red">*</span></label>
                    <div class="select">
                        <select class="form-control" name="merk" id="merk" onchange="merkJS()" required="require">
                            <option disabled="disable" selected="select">- Pilih -</option>
                            ';
                            $merk = $database->select($fields="id_merk, nama", $table="merk", $where_clause="WHERE kategori = '$_POST[kat]' AND status = '1' ORDER BY nama ASC", $fetch="all");
                            foreach ($merk as $key_m => $value_m) {
                                echo '
                                <option value="'.$value_m["id_merk"].'">'.$value_m["nama"].'</option>
                                ';
                            }
                            echo '
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="fg-line">
                    <label>Type <span class="color-red">*</span></label>
                    <div class="select">
                        <select class="form-control" name="type" id="merkGetType" required="require">
                        </select>
                    </div>
                </div>
            </div>
        </div>
        ';
    }
