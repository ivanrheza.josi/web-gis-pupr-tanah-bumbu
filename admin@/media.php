<?php
ob_start(); // Added to avoid a common error of 'header already sent'
session_start();
error_reporting(0);
//Import System
require_once "config.php";
include "../josys/function/time_load.php";
include_once "../josys/class/Rupiah.php";
include_once "../josys/function/YoutubeVideoData.php";

time_load();
$rupiah = new Rupiah();

if (empty($_SESSION['id_users']) AND empty($_SESSION['username']) AND empty($_SESSION['passuser']))
{
	echo "
		<center>Anda harus login dulu <br>
		<a href=index.php><b>LOGIN</b></a></center>";
}
else
{
?>

<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Panel | <?php echo $config['web_name'];?></title>

        <!-- Icon -->
        <link rel="icon" type="image/png" href="../assets/images/icon/favicon.png" />

        <!-- Vendor CSS -->
        <link href="assets/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="assets/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="assets/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">

        <link href="assets/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">

        <!-- CSS -->
        <link href="assets/css/mdl.app.css" rel="stylesheet">
        <link href="assets/css/mdl.app.min.css" rel="stylesheet">

        <!-- Custom CSS Style -->
        <link href="assets/css/custom.css" rel="stylesheet">

        <!-- Javascript Libraries -->
        <script src="assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="assets/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="assets/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="assets/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>

    </head>
    <body class="sw-toggled">
        <header id="header">
            <ul class="header-inner">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="logo hidden-xs">
                    <a href="?module=home"><?php echo $config['web_name'];?> - Admin Panel</a>
                </li>

                <li class="pull-right">
					<ul class="top-menu">
						<li class="dropdown">
							<a data-toggle="dropdown" class="tm-message" href="#">
								<i class="tmn-counts">
									<?php
									$jmlh_inbox = $database->count_rows($table="messages", $where_clause="WHERE status = '0' ORDER BY dateTime DESC");
									echo $jmlh_inbox;
									?>
								</i>
							</a>
							<div class="dropdown-menu dropdown-menu-lg pull-right">
								<div class="listview">
									<div class="lv-header">
										Messages
									</div>
									<div class="lv-body">
										<?php
										$messages = $database->select($fields="id_messages, name, message", $table="messages", $where_clause="WHERE status = '0' ORDER BY dateTime DESC", $fetch="all");
										foreach ($messages as $key_pk => $val_pk) {
										echo '
										<a class="lv-item" href="?module=messages&act=detail&id='.$val_pk["id_messages"].'">
											<div class="media">
												<div class="media-body">
													<div class="lv-title">'.$val_pk["name"].'</div>
													<small class="lv-small">'.$val_pk["message"].'</small>
												</div>
											</div>
										</a>
										';
										}
										?>
									</div>
									<a class="lv-footer" href="?module=messages">View All</a>
								</div>
							</div>
						</li>
                        <li class="dropdown">
							<a data-toggle="dropdown" class="tm-settings" href="#"></a>
							<ul class="dropdown-menu dm-icon pull-right">
								<li class="hidden-xs">
									<a data-action="fullscreen" href="#"><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
								</li>
								<li>
	                                <a href="<?php echo $config["web_index"];?>" target="_blank"><i class="zmdi zmdi-window-restore"></i> View Website</a>
	                            </li>
								<li>
									<a href="?module=profile"><i class="zmdi zmdi-account"></i> Admin Profile</a>
								</li>
								<li>
									<a href="logout.php" onclick="return confirm('Apakah Anda yakin akan keluar dari Halaman Admin Panel?');"><i class="zmdi zmdi-power"></i> Logout</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>

            <!-- Top Search Content -->
            <div id="top-search-wrap">
                <input type="text">
                <i id="top-search-close">&times;</i>
            </div>
        </header>

        <section id="main">
            <aside id="sidebar" >
                <div class="sidebar-inner c-overflow">
                    <div class="profile-menu">
                        <a>
                            <div class="profile-pic">
                                <?php
                                $id_profile_nav = $_SESSION['id_users'];
                                $profile_nav    = $database->select($fields="*", $table="users", $where_clause="WHERE id_users = '$id_profile_nav'", $fetch="");
								?>
                                <img src="assets/img/profile/thumbnail/<?php echo $profile_nav['foto'];?>" alt="">
                            </div>

                            <div class="profile-info">
                                <?php echo $profile_nav['nama_lengkap'];?>
                            </div>
                        </a>
                    </div>

                    <ul class="main-menu">
                        <li><a href="?module=home"><i class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                        <li><a href="?module=slideshow"><i class="zmdi zmdi-collection-image"></i> Slideshow</a></li>
                        <li><a href="?module=pages&id=5"><i class="zmdi zmdi-assignment zmdi-hc-fw"></i> Tupoksi</a></li>
                        <li class="sub-menu <?php echo $t =($_GET['module'] === 'portofolio_kategori') || ($_GET['module'] === 'portofolio') ? 'active toggled' : '' ; ?>">
                            <a href="#"><i class="zmdi zmdi-google-maps zmdi-hc-fw"></i> Daerah Irigasi</a>
                            <ul>
                                <li><a href="?module=portofolio_kategori">Kecamatan</a></li>
                                <li><a href="?module=portofolio">Daerah Irigasi</a></li>
                                <li><a href="?module=maps_categories">Kategori</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu <?php echo $t =($_GET['module'] === 'maps_categories') || ($_GET['module'] === 'maps_sijari') ? 'active toggled' : '' ; ?>">
                            <a href="#"><i class="zmdi zmdi-map zmdi-hc-fw"></i> Map SIJARI</a>
                            <ul>
                                <li><a href="?module=maps_sijari">Maps</a></li>
                                <!-- <li><a href="?module=maps_sijari">Comment</a></li> -->
                            </ul>
                        </li>
                        <li><a href="?module=sosmed"><i class="zmdi zmdi-facebook-box"></i> Media Sosial</a></li>
                        <li class="sub-menu <?php echo $t =($_GET['module'] === 'title') || ($_GET['module'] === 'keyword') || ($_GET['module'] === 'description') ? 'active toggled' : '' ; ?>">
                            <a href="#"><i class="zmdi zmdi-settings zmdi-hc-fw"></i>Setting SEO</a>
                            <ul>
                                <li><a href="?module=title">Title</a></li>
                                <li><a href="?module=keyword">Keyword</a></li>
                                <li><a href="?module=description">Description</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </aside>

            <section id="content">
                <div class="container">

                    <?php
                        require "modul.php";
                    ?>

                </div>
            </section>
        </section>

        <footer id="footer">
            Copyright &copy; 2018 <?php echo $config['web_name']; ?> - Panel Admin. All Rights Reserved. Developed By <a href="http://www.jogjasite.com/" target="_blank">JogjaSite</a>. Page rendered in <?php echo time_load();?> seconds
        </footer>

        <script src="assets/vendors/bower_components/autosize/dist/autosize.min.js"></script>
        <script src="assets/vendors/fileinput/fileinput.min.js"></script>
        <script src="assets/vendors/input-mask/input-mask.min.js"></script>

        <script src="assets/js/functions.js"></script>
        <script src="assets/js/mdl.style.js"></script>
    </body>
</html>




<?php
}
?>
