-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2019 at 09:49 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jogjaval_i-sijari`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id_blog` int(11) NOT NULL,
  `id_produk_kategori` int(11) DEFAULT NULL,
  `judul` text NOT NULL,
  `nama` varchar(100) NOT NULL,
  `popular` enum('Y','N') DEFAULT NULL,
  `short_desc` text,
  `konten` text,
  `image` text NOT NULL,
  `date` date NOT NULL,
  `seo` text NOT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id_blog`, `id_produk_kategori`, `judul`, `nama`, `popular`, `short_desc`, `konten`, `image`, `date`, `seo`, `status`) VALUES
(10, NULL, 'Pembuatan Akustik Peredam Suara Ruangan Studio Musik | Studio Recording | Karaoke Room | Bioskop | Home Theater | Hall | Gedung Pertunjukan', 'Admin', NULL, NULL, '<p>Berikut adalah salah satu pekerjaan Mystudio di TVRI Yogyakarta. Pekerjaan di studio 3 TVRI kali ini hanya pada akustik ruangnya. Spesifikasi acoustic treatment yang kami tawarkan adalah berupa panel jayabell dan panel absorber rockwool dengan finishing bungkus kain. Kombinasi antara panel jayabell dan abosrber bungkus kain ini dimaksudkan untuk memperoleh penyerapan yang seimbang dari frekuensi rendah hingga tinggi. Kita tahu bahwa bunyi ada yang berada di frekuensi rendah dan ada yang berada di frekuensi tinggi. Nah, kedua panel ini memiliki fungsi yang berbeda pula.</p>\r\n<p>Panel Jayabell dengan rockwool didalamnya berfungsi untuk menyerap bunyi di frekuensi rendah, papan jayabell berfungsi sebagai membran untuk menyerap frekuensi rendah melalui resonansi yang didukung juga oleh rockwool didalamnya, sedangkan lubang-lubang kecil pada papan jayabell berfungsi untuk menyerap bunyi di frekuensi menegah ke atas sekaligus menyebarkan bunyi frekuensi tinggi diserap sebaguan dan sisanya disebar. Panel jayabell mampu menyerap bunyi frekuensi 125Hz hingga 40% dan 500Hz hingga 95%.</p>\r\n<p>Panel rockwool bungkus kain ini berfungsi untuk menyerap bunyi di frekuensi menengah hingga tinggi. Panel ini dibuat dengan membungkus rockwool dengan kain yang berpori seperti kain sofa dengan menggunakan dudukan dan rangka multiplek. Panel rockwool bungkus kain mampu menyerap bunyi frekuensi 1000Hz hingga 92% dan frekuensi 4000Hz hingga 86%.</p>\r\n<p>Kombinasi kedua panel ini merupakan kombinasi yang cocok untuk mendapatkan waktu dengung yang lebih \"flat\" di semua frekuensi.</p>\r\n<p>Untuk plafonnya sendiri kami menggunakan amstrong Acoustic Board dengan ukuran 120cm x 60cm. Plafon akustik ini biasa digunakan di kantor-kantor sebagai penyerap noise. Karakter penyerapan dari amstrong ini sendiri cenderung seimbang menyerap bunyi frekuensi rendah hingga tinggi dengan tingkat penyerapan sebesar 40-60%. Karakter penyerapan yang seimbang inilah yang menjadikan panel amstrong cukup disenangi oleh banyak kalangan baik dari kalangan musisi hingga kalangan umum.</p>\r\n<p>Mengenai tema desainnya sendiri adalah bertema pixel televisi dimana sebuah garis miring dalam layar jika didekatkan sekali adalah berupa kombinasi pixel-pixel yang berbentuk persegi. Sedangkan tema warnanya sendiri adalah warna bitu, putih dan hijau yang merupakan warna khas dari TVRI.</p>', 'akustik-ruang-mystudio835631pembuatan-akustik-peredam-suara-ruangan-studio-musik--studio-recording--karjasa-pembuatan-studio-rekaman.jpg', '2018-11-27', 'pembuatan-akustik-peredam-suara-ruangan-studio-musik--studio-recording--kar', 'Y'),
(11, NULL, 'ABSORBER ATAU DIFFUSER? Jasa Pembuatan Studio Musik & Studio Recording 081226549069', 'Admin', NULL, NULL, '<p>Dalam desain akustik ruang, pemilihan absorber dan diffuser adalah hal yang penting dan seringkali membingungkan karena banyaknya faktor yang mempengaruhi bunyi. Berbagai bentuk dan produk absorber dan diffuser ditawarkan namun seringkali kita kurang mengetahui properti dan fungsi spesifik dari produk tersebut. Artikel kali ini akan membahas mengenai penerapan absorber dan diffuser dalam sebuah ruang akustik.</p>\r\n<p>Penentuan penggunaan absorber dan diffuser ditujukan untuk memperoleh waktu dengung yang ideal untuk fungsi tertentu, misal untuk studio musik, auditorium, atau untuk ruang kelas dan sebagainya. Waktu dengung dipengaruhi oleh bentuk, volume ruang, dan material bidang pembatas ruang. Setiap ruang yang tidak identik akan memiliki konfigurasi desain akustik ruang yang berbeda-beda. Pemilihan absorber dan diffuser yang tepat tidak hanya akan mendapatkan waktu dengung yang ideal namun juga mempengaruhi faktor estetika.</p>\r\n<p>Absorber atau diffuser? Untuk memahaminya maka sebelumnya kita pahami dahulu prinsip dari absorber dan diffuser. Absorber berfungsi untuk menyerap energi bunyi yang berlebih dalam ruang sehingga akan sangat berpengaruh dalam penurunan waktu dengung, sedangkan diffuser berfungsi untuk menyebar energi bunyi dengan sebisa mungkin meminimalkan penyerapan untuk mempertahankan waktu dengung. Penyebaran bunyi oleh diffuser juga berfungsi untuk menghindari adanya pantulan bolak balik atau yang disebut flutter echo yang dapat mempengaruhi kualitas bunyi. Dari prinsip tersebut, maka jelas fungsi dari absorber dan diffuser.</p>', 'akustik-ruang-mystudio883779absorber-atau-diffuserview-A-copy-copy-water.jpg', '2018-11-27', 'absorber-atau-diffuser-jasa-pembuatan-studio-musik--studio-recording-081226', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `blog_detail`
--

CREATE TABLE `blog_detail` (
  `id_blog` int(11) NOT NULL,
  `id_blog_kategori` int(11) DEFAULT NULL,
  `judul` text NOT NULL,
  `popular` enum('Y','N') DEFAULT NULL,
  `short_desc` text,
  `konten` text,
  `image` text NOT NULL,
  `date` date NOT NULL,
  `seo` text NOT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_detail`
--

INSERT INTO `blog_detail` (`id_blog`, `id_blog_kategori`, `judul`, `popular`, `short_desc`, `konten`, `image`, `date`, `seo`, `status`) VALUES
(7, 6, 'contoh artikel 1', 'Y', 'contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi ', '<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>\r\n<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>\r\n<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>', 'contoh-artikel-1-489-goa jomblang (1).jpg', '2017-11-21', 'contoh-artikel-1', 'Y'),
(8, 7, 'contoh artikel 2', 'Y', 'contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi ', '<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>\r\n<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>\r\n<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>', 'contoh-artikel-2-755-kalibiru.jpg', '2017-11-21', 'contoh-artikel-2', 'Y'),
(9, 6, 'contoh artikel 3', 'Y', 'contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi contoh deskripsi ', '<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>\r\n<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>\r\n<p style=\"text-align: justify;\">contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel,&nbsp;contoh isi artikel.</p>', 'contoh-artikel-3-222-pantai parangtritis (5).jpg', '2017-11-21', 'contoh-artikel-3', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `blog_kategori`
--

CREATE TABLE `blog_kategori` (
  `id_blog_kategori` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL,
  `seo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_kategori`
--

INSERT INTO `blog_kategori` (`id_blog_kategori`, `nama`, `image`, `seo`) VALUES
(6, 'contoh kategori 1', 'kategori-1-82-air terjun sri gethuk (2).jpg', 'contoh-kategori-1'),
(7, 'contoh kategori 2', 'contoh-kategori-2-22-hutan pinus (3).jpg', 'contoh-kategori-2');

-- --------------------------------------------------------

--
-- Table structure for table `car_rent`
--

CREATE TABLE `car_rent` (
  `id_car_rent` int(11) NOT NULL,
  `nama` varchar(300) NOT NULL,
  `seo` varchar(300) NOT NULL,
  `gambar` varchar(300) NOT NULL,
  `deskripsi` text,
  `harga1` varchar(10) NOT NULL,
  `harga2` int(10) NOT NULL,
  `harga3` int(10) NOT NULL,
  `harga4` int(10) NOT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_rent`
--

INSERT INTO `car_rent` (`id_car_rent`, `nama`, `seo`, `gambar`, `deskripsi`, `harga1`, `harga2`, `harga3`, `harga4`, `status`) VALUES
(1, 'Avanza', 'avanza', 'avanza-578-avanza.png', NULL, '300000', 400000, 450000, 550000, 'Y'),
(2, 'All New Avanza', 'all-new-avanza', 'all-new-avanza-708-all new avanza.png', NULL, '350000', 500000, 500000, 600000, 'Y'),
(3, 'Grand Innova', 'grand-innova', 'grand-innova-665-grand innova.png', NULL, '500000', 650000, 650000, 750000, 'Y'),
(4, 'Grand All New Innova', 'grand-all-new-innova', 'grand-all-new-innova-663-grand all new innova.png', NULL, '600000', 750000, 750000, 850000, 'Y'),
(5, 'Elf Short', 'elf-short', 'elf-short-540-elf short.png', NULL, '650000', 750000, 750000, 900000, 'Y'),
(6, 'Elf Long', 'elf-long', 'elf-long-93-elf long.jpg', NULL, '950000', 1100000, 1200000, 1500000, 'Y'),
(7, 'Hiace', 'hiace', 'hiace-341-Hiace.png', NULL, '1100000', 1200000, 1400000, 1700000, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_title`) VALUES
(3, 'Daerah Irigasi'),
(4, 'Daerah Irigasi Rawa'),
(5, 'Daerah Irigasi Tambak');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id_client`, `nama`, `image`) VALUES
(9, 'Gucci', 'gucci-281-brand1.png'),
(10, 'Chanel', 'chanel-411-brand2.png'),
(11, 'Donna Karan New York', 'donna-karan-new-york-582-brand3.png'),
(12, 'Dolce & Gabbana', 'dolce--gabbana-881-brand4.png'),
(13, 'Christian Dior SE', 'christian-dior-se-320-brand5.png'),
(14, 'Hermes', 'hermes-30-brand6.png'),
(15, 'Louis Vuitton', 'louis-vuitton-544-brand7.png'),
(16, 'Prada', 'prada-230-brand8.png'),
(17, 'Versace', 'versace-916-brand9.png');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL,
  `comment_rel_id` bigint(20) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `desc` text,
  `url` varchar(512) DEFAULT NULL,
  `comment_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') DEFAULT '0',
  `comment_embed` text,
  `latlon` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment_rel_id`, `name`, `email`, `desc`, `url`, `comment_timestamp`, `status`, `comment_embed`, `latlon`) VALUES
(1, NULL, 'sinur', '3s0c9m7@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2019-04-13 04:42:24', '0', 'RE1FBhB_1920x10801.jpg', '{\"lat\":-3.63951773,\"lng\":115.2053833}'),
(2, NULL, 'tes dua', 'tesdua@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2019-04-13 04:42:29', '0', 'RE1FBhB_1920x10802.jpg', '{\"lat\":-3.57373032,\"lng\":115.82611084}'),
(3, 1, 'tes tiga', 'testiga@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2019-04-10 04:18:17', '0', NULL, NULL),
(4, 2, 'tes 4', 'tes4@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2019-04-10 04:39:41', '0', NULL, NULL),
(7, NULL, 'Tes 6', 'tes6@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2019-04-13 04:50:05', '0', 'RE1FBhB_1920x1080.jpg', '{\"lat\":-7.8341725,\"lng\":110.3824654}');

-- --------------------------------------------------------

--
-- Table structure for table `galeri_foto`
--

CREATE TABLE `galeri_foto` (
  `id_galeri_foto` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri_foto`
--

INSERT INTO `galeri_foto` (`id_galeri_foto`, `nama`, `image`) VALUES
(18, 'Karaoke Big Room Diklat Kalasan Yogyakarta', 'akustik-ruang-mystudio513614karaoke-big-room-diklat-kalasan-yogyakartaIMG-20181002-WA0017.jpg'),
(19, 'Studio Musik PJB Bekasi', 'akustik-ruang-mystudio587914studio-musik-pjb-bekasiIMG-20180927-WA0018.jpg'),
(20, 'Studio Musik PJB Bekasi', 'akustik-ruang-mystudio779725studio-musik-pjb-bekasiIMG-20180927-WA0022.jpg'),
(21, 'Karaoke Room pribadi Bapak Hiras Pulomas Jakarta Timur', 'akustik-ruang-mystudio52533karaoke-room-pribadi-bapak-hiras-pulomas-jakarta-timurIMG-20180924-WA0012.jpg'),
(22, 'Karaoke Room Pribadi Pak Rizky Padang Sumatera Barat', 'akustik-ruang-mystudio405447karaoke-room-pribadi-pak-rizky-padang-sumatera-baratview-B-alternatif-2-copy-(1).jpg'),
(23, 'Gedung Pertunjukan SMA N 1 PURWOREJO Jawa Tengah', 'akustik-ruang-mystudio126094gedung-pertunjukan-sma-n-1-purworejo-jawa-tengahview-C-concert-hall-purworejo-copy.jpg'),
(24, 'Teleconference Room Poltekkes Colomadu Solo Jawa Tengah', 'akustik-ruang-mystudio722510teleconference-room-poltekkes-colomadu-solo-jawa-tengahview-A-alternatif-2-copy.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `galeri_foto_porto`
--

CREATE TABLE `galeri_foto_porto` (
  `id_galeri_foto_porto` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL,
  `id_portofolio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri_foto_porto`
--

INSERT INTO `galeri_foto_porto` (`id_galeri_foto_porto`, `nama`, `image`, `id_portofolio`) VALUES
(5, 'Saluran Pasangan Batu', 'sijari-tanah-bumbu-1.png', 33),
(6, 'Saluran Pasangan Batu', 'sijari-tanah-bumbu-2.jpg', 33),
(7, 'Saluran Pasangan Batu', 'sijari-tanah-bumbu-3.jpg', 33),
(8, 'Saluran Pasangan Batu', 'sijari-tanah-bumbu-4.jpg', 33),
(9, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air.jpg', 34),
(10, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer.jpg', 34),
(11, 'Saluran sekunder 1', 'sijari-tanah-bumbu-saluran-sekunder-1.jpg', 34),
(12, 'Saluran Sekunder 2', 'sijari-tanah-bumbu-saluran-sekunder-2.jpg', 34),
(13, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong.jpg', 35),
(14, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air1.jpg', 35),
(17, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer1.jpg', 35),
(18, 'Saluran Saluran Primer', 'sijari-tanah-bumbu-saluran-saluran-primer.jpg', 35),
(19, 'Jembatan', 'sijari-tanah-bumbu-jembatan.jpg', 36),
(20, 'Saluran Primer Alami', 'sijari-tanah-bumbu-saluran-primer-alami.jpg', 36),
(21, 'Saluran Primer Pasangan Batu', 'sijari-tanah-bumbu-saluran-primer-pasangan-batu.jpg', 36),
(22, 'Saluran Sekunder Pasangan Batu', 'sijari-tanah-bumbu-saluran-sekunder-pasangan-batu.jpg', 36),
(23, 'Embung', 'sijari-tanah-bumbu-embung.jpg', 37),
(24, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air2.jpg', 37),
(25, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier.jpg', 37),
(26, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder.jpg', 37),
(27, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air3.jpg', 38),
(28, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier1.jpg', 38),
(29, 'Jembatan Kayu', 'sijari-tanah-bumbu-jembatan-kayu.jpg', 38),
(30, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder1.jpg', 38),
(31, 'Pintu Air 1', 'sijari-tanah-bumbu-pintu-air4.jpg', 39),
(32, 'Pintu Air 2', 'sijari-tanah-bumbu-pintu-air-2.jpg', 39),
(33, 'Gorong gorong 1', 'sijari-tanah-bumbu-gorong-gorong1.jpg', 39),
(34, 'Gorong gorong 2', 'sijari-tanah-bumbu-gorong-gorong-2.jpg', 39),
(35, 'Jembatan Kayu', 'sijari-tanah-bumbu-jembatan-kayu1.jpg', 39),
(36, 'Jembatan', 'sijari-tanah-bumbu-jembatan1.jpg', 39),
(37, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer2.jpg', 39),
(38, 'Saluran Primer Pasangan Batu', 'sijari-tanah-bumbu-saluran-primer-pasangan-batu1.jpg', 39),
(39, 'Saluran Sekunder Pasangan Batu', 'sijari-tanah-bumbu-saluran-sekunder-pasangan-batu1.jpg', 39),
(40, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder2.jpg', 39),
(41, 'Saluran Tersier 1', 'sijari-tanah-bumbu-saluran-tersier2.jpg', 39),
(42, 'Saluran Tersier 2', 'sijari-tanah-bumbu-saluran-tersier-2.jpg', 39),
(43, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer3.jpg', 40),
(44, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder3.jpg', 40),
(45, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer4.jpg', 41),
(46, 'Saluran Sekunder Alami', 'sijari-tanah-bumbu-saluran-sekunder-alami.jpg', 41),
(47, 'Saluran Sekunder Pasangan Batu', 'sijari-tanah-bumbu-saluran-sekunder-pasangan-batu2.jpg', 41),
(48, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air5.jpg', 41),
(49, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air6.jpg', 42),
(50, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder4.jpg', 42),
(51, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer5.jpg', 42),
(52, 'Jembatan di Saluran Sekunder', 'sijari-tanah-bumbu-jembatan-di-saluran-sekunder.jpg', 42),
(53, 'Embung', 'sijari-tanah-bumbu-embung1.jpg', 43),
(54, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air7.jpg', 43),
(55, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder5.jpg', 43),
(56, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier3.jpg', 43),
(57, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer6.jpg', 44),
(58, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong2.jpg', 44),
(59, 'Tanggul', 'sijari-tanah-bumbu-tanggul.jpg', 44),
(60, 'Saluran Tanah', 'sijari-tanah-bumbu-saluran-tanah.jpg', 44),
(61, 'Pintu Air Kayu', 'sijari-tanah-bumbu-pintu-air-kayu.jpg', 45),
(62, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer7.jpg', 45),
(63, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder6.jpg', 45),
(65, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier4.jpg', 45),
(66, 'Embung', 'sijari-tanah-bumbu-embung2.jpg', 46),
(67, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air8.jpg', 46),
(68, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier5.jpg', 46),
(69, 'Embung', 'sijari-tanah-bumbu-embung3.jpg', 47),
(70, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air9.jpg', 47),
(71, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier6.jpg', 47),
(72, 'Jembatan', 'sijari-tanah-bumbu-jembatan2.jpg', 48),
(73, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air10.jpg', 48),
(74, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer8.jpg', 48),
(75, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder7.jpg', 48),
(76, 'Saluran Tersier', 'sijari-tanah-bumbu-saluran-tersier7.jpg', 48),
(77, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong3.jpg', 48),
(78, 'Jembatan', 'sijari-tanah-bumbu-jembatan3.jpg', 49),
(79, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air11.jpg', 49),
(80, 'Pintu Air', 'sijari-tanah-bumbu-saluran-primer9.jpg', 49),
(81, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder8.jpg', 49),
(82, 'Jembatan', 'sijari-tanah-bumbu-jembatan4.jpg', 50),
(83, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air12.jpg', 50),
(84, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer10.jpg', 50),
(85, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder9.jpg', 50),
(86, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air13.jpg', 51),
(87, 'Dam Parit', 'sijari-tanah-bumbu-dam-parit.jpg', 51),
(88, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong4.jpg', 51),
(89, 'Embung', 'sijari-tanah-bumbu-embung4.jpg', 51),
(90, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer11.jpg', 51),
(91, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder10.jpg', 51),
(92, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air14.jpg', 52),
(93, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer12.jpg', 52),
(95, 'Saluran Sekunder Pasangan Batu', 'sijari-tanah-bumbu-saluran-sekunder12.jpg', 52),
(96, 'Saluran Sekunder Tanah', 'sijari-tanah-bumbu-saluran-sekunder-tanah.jpg', 52),
(97, 'Gorong-gorong', 'sijari-tanah-bumbu-Gorong-gorong.jpg', 53),
(98, 'Jembatan', 'sijari-tanah-bumbu-Jembatan.jpg', 53),
(99, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air.jpg', 53),
(100, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer.jpg', 53),
(101, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder.jpg', 53),
(102, 'Saluran Tersier', 'sijari-tanah-bumbu-Saluran-Tersier.jpg', 53),
(103, 'Gorong-gorong', 'sijari-tanah-bumbu-Gorong-gorong1.jpg', 55),
(104, 'Jembatan', 'sijari-tanah-bumbu-Jembatan1.jpg', 55),
(105, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air1.jpg', 55),
(106, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer1.jpg', 55),
(107, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder1.jpg', 55),
(108, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer13.jpg', 73),
(109, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder13.jpg', 73),
(110, 'Embung', 'sijari-tanah-bumbu-Embung.jpg', 56),
(111, 'Pintu Air Beton', 'sijari-tanah-bumbu-Pintu-Air-Beton.jpg', 56),
(112, 'Pintu Air Kayu Otomatis', 'sijari-tanah-bumbu-Pintu-Air-Kayu-Otomatis.jpg', 56),
(113, 'Pintu Air Kayu', 'sijari-tanah-bumbu-Pintu-Air-Kayu.jpg', 56),
(114, 'Jembatan', 'sijari-tanah-bumbu-Jembatan2.jpg', 57),
(115, 'Pintu Air Kayu', 'sijari-tanah-bumbu-Pintu-Air-Kayu1.jpg', 57),
(116, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder2.jpg', 57),
(117, 'Saluran Tersier', 'sijari-tanah-bumbu-Saluran-Tersier1.jpg', 57),
(118, 'Jembatan', 'sijari-tanah-bumbu-Jembatan3.jpg', 58),
(119, 'Pintu Air Kayu', 'sijari-tanah-bumbu-Pintu-Air-Kayu2.jpg', 58),
(120, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder3.jpg', 58),
(121, 'Saluran Tersier', 'sijari-tanah-bumbu-Saluran-Tersier2.jpg', 58),
(122, 'Jembatan', 'sijari-tanah-bumbu-Jembatan4.jpg', 59),
(123, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air2.jpg', 59),
(124, 'Jembatan', 'sijari-tanah-bumbu-Jembatan5.jpg', 60),
(125, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air3.jpg', 60),
(126, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer2.jpg', 60),
(127, 'Tanggul', 'sijari-tanah-bumbu-Tanggul.jpg', 60),
(128, 'Jembatan', 'sijari-tanah-bumbu-Jembatan6.jpg', 61),
(129, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer3.jpg', 61),
(130, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air15.jpg', 74),
(131, 'Jembatan', 'sijari-tanah-bumbu-jembatan5.jpg', 74),
(132, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder4.jpg', 61),
(133, 'Saluran Tersier', 'sijari-tanah-bumbu-Saluran-Tersier3.jpg', 61),
(134, 'Gorong gorong 1', 'sijari-tanah-bumbu-gorong-gorong5.jpg', 74),
(135, 'Gorong gorong 2', 'sijari-tanah-bumbu-gorong-gorong-21.jpg', 74),
(136, 'Jembatan', 'sijari-tanah-bumbu-Jembatan7.jpg', 62),
(137, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air4.jpg', 62),
(138, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer4.jpg', 62),
(139, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder5.jpg', 62),
(140, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer14.jpg', 74),
(141, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder14.jpg', 74),
(142, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air16.jpg', 75),
(143, 'Jembatan', 'sijari-tanah-bumbu-jembatan6.jpg', 75),
(144, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder15.jpg', 75),
(145, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air17.jpg', 76),
(146, 'Jembatan', 'sijari-tanah-bumbu-jembatan7.jpg', 76),
(147, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong6.jpg', 76),
(148, 'Tanggul', 'sijari-tanah-bumbu-tanggul1.jpg', 76),
(149, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer.png', 76),
(150, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder16.jpg', 76),
(151, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air18.jpg', 77),
(152, 'Jembatan', 'sijari-tanah-bumbu-jembatan8.jpg', 77),
(153, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer15.jpg', 77),
(154, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder17.jpg', 77),
(155, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air19.jpg', 78),
(156, 'Jembatan Kayu 1', 'sijari-tanah-bumbu-jembatan-kayu2.jpg', 78),
(157, 'Jembatan Kayu 2', 'sijari-tanah-bumbu-jembatan-kayu-2.jpg', 78),
(158, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong7.jpg', 78),
(159, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer16.jpg', 78),
(160, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder18.jpg', 78),
(161, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer17.jpg', 79),
(162, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder19.jpg', 79),
(163, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer18.jpg', 80),
(164, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air20.jpg', 81),
(165, 'Jembatan', 'sijari-tanah-bumbu-jembatan9.jpg', 81),
(166, 'Pipa', 'sijari-tanah-bumbu-pipa.jpg', 81),
(167, 'Tanggul', 'sijari-tanah-bumbu-tanggul2.jpg', 81),
(168, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer19.jpg', 81),
(169, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder20.jpg', 81),
(170, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air21.jpg', 83),
(171, 'Jembatan', 'sijari-tanah-bumbu-jembatan10.jpg', 83),
(172, 'Pipa', 'sijari-tanah-bumbu-pipa1.jpg', 83),
(173, 'Tanggul', 'sijari-tanah-bumbu-tanggul3.jpg', 83),
(174, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer20.jpg', 83),
(175, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder21.jpg', 83),
(176, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air22.jpg', 84),
(177, 'Jembatan', 'sijari-tanah-bumbu-jembatan11.jpg', 84),
(178, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong8.jpg', 84),
(179, 'Jembatan Beton', 'sijari-tanah-bumbu-jembatan-beton.jpg', 84),
(180, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer21.jpg', 84),
(181, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder22.jpg', 84),
(182, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer22.jpg', 85),
(183, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder23.jpg', 85),
(184, 'Jembatan', 'sijari-tanah-bumbu-jembatan-1.jpg', 86),
(185, 'Jembatan Kayu', 'sijari-tanah-bumbu-jembatan-kayu3.jpg', 86),
(186, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong9.jpg', 86),
(187, 'Tanggul', 'sijari-tanah-bumbu-tanggul4.jpg', 86),
(188, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer23.jpg', 86),
(189, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder24.jpg', 86),
(190, 'Kolam Ikan 1', 'sijari-tanah-bumbu-kolam-ikan.jpg', 87),
(191, 'Kolam Ikan 2', 'sijari-tanah-bumbu-kolam-ikan-1.jpg', 87),
(192, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer24.jpg', 87),
(193, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air23.jpg', 88),
(194, 'Tanggul', 'sijari-tanah-bumbu-tanggul5.jpg', 88),
(195, 'Saluran Primer', 'sijari-tanah-bumbu-primer.jpg', 88),
(196, 'Saluran Sekunder', 'sijari-tanah-bumbu-sekunder.jpg', 88),
(197, 'Gorong-gorong', 'sijari-tanah-bumbu-Gorong-gorong2.jpg', 63),
(198, 'Jembatan Beton', 'sijari-tanah-bumbu-Jembatan-Beton.jpg', 63),
(199, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer5.jpg', 63),
(200, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder6.jpg', 63),
(201, 'Jembatan', 'sijari-tanah-bumbu-Jembatan8.jpg', 64),
(202, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air5.jpg', 64),
(203, 'Saluran Pasangan Batu', 'sijari-tanah-bumbu-Saluran-Pasangan-Batu.jpg', 64),
(204, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer6.jpg', 64),
(205, 'Jembatan', 'sijari-tanah-bumbu-Jembatan9.jpg', 65),
(206, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air6.jpg', 65),
(207, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder7.jpg', 65),
(208, 'Saluran Tersier', 'sijari-tanah-bumbu-Saluran-Tersier4.jpg', 65),
(209, 'Jembatan', 'sijari-tanah-bumbu-Jembatan10.jpg', 66),
(210, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air7.jpg', 66),
(211, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer7.jpg', 66),
(212, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder8.jpg', 66),
(213, 'Jembatan', 'sijari-tanah-bumbu-Jembatan11.jpg', 67),
(214, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air8.jpg', 67),
(215, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder9.jpg', 67),
(216, 'Tanggul', 'sijari-tanah-bumbu-Tanggul1.jpg', 67),
(217, 'Bangunan Bagi', 'sijari-tanah-bumbu-Bangunan-Bagi.jpg', 68),
(218, 'Jembatan Beton', 'sijari-tanah-bumbu-Jembatan-Beton1.jpg', 68),
(219, 'Jembatan Kayu', 'sijari-tanah-bumbu-Jembatan-Kayu.jpg', 68),
(220, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air9.jpg', 68),
(221, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer8.jpg', 68),
(222, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder10.jpg', 68),
(223, 'Gorong-gorong 2', 'sijari-tanah-bumbu-Gorong-gorong-2.jpg', 69),
(224, 'Gorong-gorong', 'sijari-tanah-bumbu-Gorong-gorong3.jpg', 69),
(225, 'Pintu Air 2', 'sijari-tanah-bumbu-Pintu-Air-2.jpg', 69),
(226, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air10.jpg', 69),
(227, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer9.jpg', 69),
(228, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder11.jpg', 69),
(229, 'Jembatan', 'sijari-tanah-bumbu-Jembatan12.jpg', 70),
(230, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air11.jpg', 70),
(231, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer10.jpg', 70),
(232, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder12.jpg', 70),
(233, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air12.jpg', 71),
(234, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer11.jpg', 71),
(235, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder13.jpg', 71),
(236, 'Saluran Tersier', 'sijari-tanah-bumbu-Saluran-tersier.jpg', 71),
(237, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air13.jpg', 72),
(238, 'Saluran Primer', 'sijari-tanah-bumbu-Saluran-Primer12.jpg', 72),
(239, 'Jembatan', 'sijari-tanah-bumbu-Jembatan13.jpg', 54),
(240, 'Pintu Air', 'sijari-tanah-bumbu-Pintu-Air14.jpg', 54),
(241, 'Saluran Sekunder', 'sijari-tanah-bumbu-Saluran-Sekunder14.jpg', 54),
(242, 'Tanggul', 'sijari-tanah-bumbu-Tanggul2.jpg', 54),
(243, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air24.jpg', 90),
(244, 'Jembatan Kayu', 'sijari-tanah-bumbu-jembatan12.jpg', 90),
(245, 'Gorong gorong', 'sijari-tanah-bumbu-gorong-gorong10.jpg', 90),
(246, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer25.jpg', 90),
(247, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder25.jpg', 90),
(248, 'Saluran Sekunder Pasangan Batu', 'sijari-tanah-bumbu-saluran-sekunder-pasangan-batu3.jpg', 90),
(249, 'Saluran Sekunder 1', 'sijari-tanah-bumbu-saluran-sekunder26.jpg', 91),
(251, 'Saluran Sekunder 2', 'sijari-tanah-bumbu-saluran-sekunder-21.jpg', 91),
(252, 'Pintu Air', 'sijari-tanah-bumbu-pintu-air25.jpg', 92),
(253, 'Saluran Primer', 'sijari-tanah-bumbu-saluran-primer26.jpg', 92),
(254, 'Saluran Sekunder', 'sijari-tanah-bumbu-saluran-sekunder27.jpg', 92),
(255, 'Saluran Sekunder Pasangan Batu', 'sijari-tanah-bumbu-saluran-sekunder-pasangan-batu4.jpg', 92);

-- --------------------------------------------------------

--
-- Table structure for table `galeri_video`
--

CREATE TABLE `galeri_video` (
  `id_galeri_video` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL,
  `seo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri_video`
--

INSERT INTO `galeri_video` (`id_galeri_video`, `nama`, `image`, `seo`) VALUES
(10, '026OeYh7Q04', '', NULL),
(11, 'NhBkuqVSSNk', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_port`
--

CREATE TABLE `gallery_port` (
  `id_gallery` int(11) NOT NULL,
  `id_portofolio` int(11) DEFAULT NULL,
  `image` varchar(200) NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_port`
--

INSERT INTO `gallery_port` (`id_gallery`, `id_portofolio`, `image`, `dateTime`) VALUES
(526, 143, '143-contoh-album-foto-1-724-borobudur (1).jpg', '2017-11-20 07:57:05'),
(527, 143, '143-contoh-album-foto-1-746-kalisuci (3).jpg', '2017-11-20 07:57:06'),
(528, 143, '143-contoh-album-foto-1-53-kalibiru.jpg', '2017-11-20 07:57:07'),
(529, 143, '143-contoh-album-foto-1-991-kraton yk (1).jpg', '2017-11-20 07:57:08'),
(530, 143, '143-contoh-album-foto-1-45-goa pindul (1).jpg', '2017-11-20 07:57:08'),
(531, 143, '143-contoh-album-foto-1-816-lava tour merapi (2).jpg', '2017-11-20 07:57:09'),
(532, 144, '144-contoh-album-foto-2-747-tamansari (1).jpg', '2017-11-20 08:00:19'),
(533, 144, '144-contoh-album-foto-2-497-prambanan (3).jpg', '2017-11-20 08:00:20'),
(534, 144, '144-contoh-album-foto-2-213-pantai indrayanti (2).jpg', '2017-11-20 08:00:20'),
(535, 144, '144-contoh-album-foto-2-645-kraton yk (3).jpg', '2017-11-20 08:00:21'),
(536, 144, '144-contoh-album-foto-2-560-goa pindul (2).jpg', '2017-11-20 08:00:22'),
(537, 144, '144-contoh-album-foto-2-216-borobudur (2).jpg', '2017-11-20 08:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id_hotel` int(11) NOT NULL,
  `nm_hotel` text NOT NULL,
  `des` text NOT NULL,
  `image` text,
  `link` text,
  `seo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id_hotel`, `nm_hotel`, `des`, `image`, `link`, `seo`) VALUES
(21, 'Candi Borobudur', '<p><strong>Borobudur</strong>, or&nbsp;<strong>Barabudur</strong>&nbsp;(<a title=\"Indonesian language\" href=\"https://en.wikipedia.org/wiki/Indonesian_language\">Indonesian</a>:&nbsp;<span lang=\"id\" xml:lang=\"id\"><em>Candi Borobudur</em></span>,&nbsp;<a title=\"Javanese language\" href=\"https://en.wikipedia.org/wiki/Javanese_language\">Javanese</a>:&nbsp;<span lang=\"jv\" xml:lang=\"jv\">????????????</span>,&nbsp;<small><abbr title=\"transliteration\">translit.</abbr>&nbsp;</small><em><span class=\"Unicode\" title=\"Javanese transliteration\">Candhi Barabudhur</span></em>) is a 9th-century&nbsp;<a title=\"Mahayana\" href=\"https://en.wikipedia.org/wiki/Mahayana\">Mahayana</a>&nbsp;<a title=\"Buddhism\" href=\"https://en.wikipedia.org/wiki/Buddhism\">Buddhist</a>&nbsp;<a title=\"Temple\" href=\"https://en.wikipedia.org/wiki/Temple\">temple</a>&nbsp;in&nbsp;<a title=\"Magelang Regency\" href=\"https://en.wikipedia.org/wiki/Magelang_Regency\">Magelang</a>,&nbsp;<a title=\"Central Java\" href=\"https://en.wikipedia.org/wiki/Central_Java\">Central Java</a>,&nbsp;<a title=\"Indonesia\" href=\"https://en.wikipedia.org/wiki/Indonesia\">Indonesia</a>, and the&nbsp;<a title=\"List of Buddhist temples\" href=\"https://en.wikipedia.org/wiki/List_of_Buddhist_temples\">world\'s largest Buddhist temple</a>.<sup id=\"cite_ref-Guiness_1-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-Guiness-1\">[1]</a></sup><sup id=\"cite_ref-JakartaPost1_2-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-JakartaPost1-2\">[2]</a></sup>&nbsp;<sup id=\"cite_ref-unesco-whc_3-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-unesco-whc-3\">[3]</a></sup>&nbsp;The temple consists of nine stacked platforms, six square and three circular, topped by a central dome. It is decorated with 2,672&nbsp;<a title=\"Relief\" href=\"https://en.wikipedia.org/wiki/Relief\">relief</a>&nbsp;panels and 504&nbsp;<a title=\"Buddhist art\" href=\"https://en.wikipedia.org/wiki/Buddhist_art\">Buddha statues</a>. The central dome is surrounded by 72 Buddha statues, each seated inside a perforated&nbsp;<a title=\"Stupa\" href=\"https://en.wikipedia.org/wiki/Stupa\">stupa</a>.<sup id=\"cite_ref-p35-36_4-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-p35-36-4\">[4]</a></sup></p>\r\n<p>Built in the 9th century during the reign of the&nbsp;<a class=\"mw-redirect\" title=\"Sailendra Dynasty\" href=\"https://en.wikipedia.org/wiki/Sailendra_Dynasty\">Sailendra Dynasty</a>, the temple design follows&nbsp;<a class=\"mw-redirect\" title=\"Javanese temple architecture\" href=\"https://en.wikipedia.org/wiki/Javanese_temple_architecture\">Javanese</a>&nbsp;<a title=\"Buddhist architecture\" href=\"https://en.wikipedia.org/wiki/Buddhist_architecture\">Buddhist architecture</a>, which blends the&nbsp;<a title=\"Architecture of Indonesia\" href=\"https://en.wikipedia.org/wiki/Architecture_of_Indonesia\">Indonesian</a>&nbsp;indigenous cult of&nbsp;<a class=\"mw-redirect\" title=\"Ancestor worship\" href=\"https://en.wikipedia.org/wiki/Ancestor_worship\">ancestor worship</a>&nbsp;and the Buddhist concept of attaining&nbsp;<a title=\"Nirvana\" href=\"https://en.wikipedia.org/wiki/Nirvana\">Nirvana</a>.<sup id=\"cite_ref-unesco-whc_3-1\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-unesco-whc-3\">[3]</a></sup>&nbsp;The temple demonstrates the influences of&nbsp;<a title=\"Gupta Empire\" href=\"https://en.wikipedia.org/wiki/Gupta_Empire\">Gupta</a>&nbsp;art that reflects&nbsp;<a title=\"India\" href=\"https://en.wikipedia.org/wiki/India\">India</a>\'s influence on the region, yet there are enough indigenous scenes and elements incorporated to make Borobudur uniquely Indonesian.<sup id=\"cite_ref-5\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-5\">[5]</a></sup><sup id=\"cite_ref-6\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-6\">[6]</a></sup>&nbsp;The monument is a&nbsp;<a title=\"Shrine\" href=\"https://en.wikipedia.org/wiki/Shrine\">shrine</a>&nbsp;to the&nbsp;<a class=\"mw-redirect\" title=\"Lord Buddha\" href=\"https://en.wikipedia.org/wiki/Lord_Buddha\">Lord Buddha</a>&nbsp;and a place for&nbsp;<a title=\"Buddhist pilgrimage\" href=\"https://en.wikipedia.org/wiki/Buddhist_pilgrimage\">Buddhist pilgrimage</a>. The pilgrim journey begins at the base of the monument and follows a path around the monument, ascending to the top through three levels symbolic of&nbsp;<a title=\"Buddhist cosmology\" href=\"https://en.wikipedia.org/wiki/Buddhist_cosmology\">Buddhist cosmology</a>:&nbsp;<em><a title=\"Desire realm\" href=\"https://en.wikipedia.org/wiki/Desire_realm\">K?madh?tu</a></em>&nbsp;(the world of desire),&nbsp;<em><a title=\"Rupajhana\" href=\"https://en.wikipedia.org/wiki/Rupajhana\">Rupadhatu</a></em>&nbsp;(the world of forms) and&nbsp;<em><a title=\"Ar?pajh?na\" href=\"https://en.wikipedia.org/wiki/Ar%C5%ABpajh%C4%81na\">Arupadhatu</a></em>&nbsp;(the world of formlessness). The monument guides pilgrims through an extensive system of stairways and corridors with 1,460 narrative relief panels on the walls and the&nbsp;<a class=\"mw-redirect\" title=\"Balustrade\" href=\"https://en.wikipedia.org/wiki/Balustrade\">balustrades</a>. Borobudur has the largest and most complete ensemble of Buddhist reliefs in the world.<sup id=\"cite_ref-unesco-whc_3-2\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-unesco-whc-3\">[3]</a></sup></p>\r\n<p>Evidence suggests Borobudur was constructed in the 9th century and abandoned following the 14th-century decline of&nbsp;<a title=\"Hinduism in Indonesia\" href=\"https://en.wikipedia.org/wiki/Hinduism_in_Indonesia\">Hindu</a>&nbsp;kingdoms in Java and the&nbsp;<a title=\"Javanese people\" href=\"https://en.wikipedia.org/wiki/Javanese_people\">Javanese</a>&nbsp;conversion to Islam.<sup id=\"cite_ref-Soekmono4_7-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-Soekmono4-7\">[7]</a></sup>&nbsp;Worldwide knowledge of its existence was sparked in 1814 by&nbsp;<a class=\"mw-redirect\" title=\"Sir Thomas Stamford Raffles\" href=\"https://en.wikipedia.org/wiki/Sir_Thomas_Stamford_Raffles\">Sir Thomas Stamford Raffles</a>, then the&nbsp;<a class=\"mw-redirect\" title=\"British Java\" href=\"https://en.wikipedia.org/wiki/British_Java\">British ruler of Java</a>, who was advised of its location by native Indonesians.&nbsp;<sup id=\"cite_ref-8\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-8\">[8]</a></sup>&nbsp;Borobudur has since been preserved through several restorations. The largest restoration project was undertaken between 1975 and 1982 by the&nbsp;<a title=\"Government of Indonesia\" href=\"https://en.wikipedia.org/wiki/Government_of_Indonesia\">Indonesian government</a>&nbsp;and&nbsp;<a title=\"UNESCO\" href=\"https://en.wikipedia.org/wiki/UNESCO\">UNESCO</a>, followed by the monument\'s listing as a UNESCO&nbsp;<a title=\"World Heritage Site\" href=\"https://en.wikipedia.org/wiki/World_Heritage_Site\">World Heritage Site</a>.<sup id=\"cite_ref-unesco-whc_3-3\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-unesco-whc-3\">[3]</a></sup></p>\r\n<p>Borobudur remains popular for pilgrimage. Once a year, Buddhists in Indonesia celebrate&nbsp;<a title=\"Vesak\" href=\"https://en.wikipedia.org/wiki/Vesak\">Vesak</a>&nbsp;at the monument, and Borobudur is Indonesia\'s single most visited&nbsp;<a title=\"Tourism in Indonesia\" href=\"https://en.wikipedia.org/wiki/Tourism_in_Indonesia\">tourist attraction</a>.<sup id=\"cite_ref-9\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-9\">[9]</a></sup><sup id=\"cite_ref-Hampton2004_10-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-Hampton2004-10\">[10]</a></sup><sup id=\"cite_ref-Sedyawati1997_11-0\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Borobudur#cite_note-Sedyawati1997-11\">[11]</a></sup></p>\r\n<p>&nbsp;</p>\r\n<div id=\"toc\" class=\"toc\">&nbsp;</div>', 'candi-borobudur-35-Borobudur-Sunrise-Tour-16_.jpg', NULL, 'candi-borobudur'),
(22, 'Candi Prambanan', '<p><strong>Prambanan</strong>&nbsp;or&nbsp;<strong>Rara Jonggrang</strong>&nbsp;(<a title=\"Javanese language\" href=\"https://en.wikipedia.org/wiki/Javanese_language\">Javanese</a>:&nbsp;<span lang=\"jv\" xml:lang=\"jv\">?????????</span>,&nbsp;<small><abbr title=\"transliteration\">translit.</abbr>&nbsp;</small><em><span class=\"Unicode\" title=\"Javanese transliteration\">Rara Jonggrang</span></em>) is a 9th-century&nbsp;<a title=\"Hindu temple\" href=\"https://en.wikipedia.org/wiki/Hindu_temple\">Hindu temple</a>&nbsp;compound in&nbsp;<a title=\"Central Java\" href=\"https://en.wikipedia.org/wiki/Central_Java\">Central Java</a>,&nbsp;<a title=\"Indonesia\" href=\"https://en.wikipedia.org/wiki/Indonesia\">Indonesia</a>, dedicated to the&nbsp;<a title=\"Trimurti\" href=\"https://en.wikipedia.org/wiki/Trimurti\">Trimurti</a>, the expression of God as the Creator (<a title=\"Brahma\" href=\"https://en.wikipedia.org/wiki/Brahma\">Brahma</a>), the Preserver (<a title=\"Vishnu\" href=\"https://en.wikipedia.org/wiki/Vishnu\">Vishnu</a>) and the Destroyer (<a title=\"Shiva\" href=\"https://en.wikipedia.org/wiki/Shiva\">Shiva</a>). The temple compound is located approximately 17 kilometres (11&nbsp;mi) northeast of the city of&nbsp;<a title=\"Yogyakarta\" href=\"https://en.wikipedia.org/wiki/Yogyakarta\">Yogyakarta</a>&nbsp;on the boundary between Central Java and&nbsp;<a title=\"Special Region of Yogyakarta\" href=\"https://en.wikipedia.org/wiki/Special_Region_of_Yogyakarta\">Yogyakarta provinces</a>.<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Prambanan#cite_note-1\">[1]</a></sup></p>\r\n<p>The temple compound, a&nbsp;<a title=\"UNESCO\" href=\"https://en.wikipedia.org/wiki/UNESCO\">UNESCO</a>&nbsp;<a title=\"World Heritage Site\" href=\"https://en.wikipedia.org/wiki/World_Heritage_Site\">World Heritage Site</a>, is the largest Hindu temple site in Indonesia, and one of the biggest in&nbsp;<a title=\"Southeast Asia\" href=\"https://en.wikipedia.org/wiki/Southeast_Asia\">Southeast Asia</a>. It is characterized by its tall and pointed architecture, typical of&nbsp;<a class=\"mw-redirect\" title=\"Hindu architecture\" href=\"https://en.wikipedia.org/wiki/Hindu_architecture\">Hindu architecture</a>, and by the towering 47-metre-high (154&nbsp;ft) central building inside a large complex of individual temples.<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Prambanan#cite_note-2\">[2]</a></sup>&nbsp;Prambanan attracts many visitors from around the world.<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Prambanan#cite_note-3\">[3]</a></sup><sup id=\"cite_ref-4\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Prambanan#cite_note-4\">[4]</a></sup></p>', 'candi-prambanan-786-Prambanan_Java245.jpg', NULL, 'candi-prambanan'),
(23, 'Merapi Lava Tour', '<p>Merapi lava tour description</p>', 'merapi-lava-tour-755-10608331_914654208563230_5743242442243955754_o-1.jpg', NULL, 'merapi-lava-tour'),
(24, 'Kalibiru', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'kalibiru-245-kalibiru.jpg', NULL, 'kalibiru'),
(25, 'Hutan Pinus', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'hutan-pinus-270-Hutan-pinus-dlingo-bantul-imogiri.jpg', NULL, 'hutan-pinus'),
(26, 'Pantai Parangtritis', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'parangtritis-651-Ikon-Baru-Pantai-Parangtritis.jpg', NULL, 'pantai-parangtritis'),
(27, 'Kraton Yogyakarta', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'kraton-yogyakarta-593-Screenshot.jpg', NULL, 'kraton-yogyakarta'),
(28, 'Taman Sari', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'taman-sari-517-Istana-Air-Taman-Sari-Jogja-2.jpg', NULL, 'taman-sari'),
(29, 'Malioboro', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'malioboro-749-malioboro.jpg', NULL, 'malioboro'),
(30, 'Alun-Alun Kidul (ALKID)', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'alunalun-kidul-alkid-63-Gemerlap-Sepeda-Hias-di-Alun-alun-Kidul-Yogyakarta.jpg', NULL, 'alunalun-kidul-alkid'),
(31, 'Air Terjun Sri Gethuk', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'air-terjun-sri-gethuk-955-Air-Terjun-Sri-Gethuk-Terbaru.jpg', NULL, 'air-terjun-sri-gethuk'),
(32, 'Goa Pindul / Goa Jomblang / Kalisuci', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'goa-pindul--goa-jomblang--kalisuci-248-DSC_0033edt.jpg', NULL, 'goa-pindul--goa-jomblang--kalisuci'),
(33, 'Pantai Indrayanti', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'pantai-indrayanti-442-Pantai-Indrayanti-Gunung-Kidul-Yogyakarta.jpg', NULL, 'pantai-indrayanti'),
(34, 'Pusat Oleh-Oleh', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'pusat-oleholeh-744-IMG_4218-73022.jpg', NULL, 'pusat-oleholeh'),
(35, 'Drop Off Bandara / Stasiun / Terminal', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'drop-off-bandara--stasiun--terminal-805-IMG_1718.jpg', NULL, 'drop-off-bandara--stasiun--terminal');

-- --------------------------------------------------------

--
-- Table structure for table `img_header`
--

CREATE TABLE `img_header` (
  `id_img` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `image` text,
  `urutan` int(11) NOT NULL,
  `seo` varchar(350) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `img_header`
--

INSERT INTO `img_header` (`id_img`, `name`, `image`, `urutan`, `seo`) VALUES
(1, 'Home - Paket Wisata Terbaru', 'home--paket-wisata-terpopuler-373-1imghdr.jpg', 1, 'home--paket-wisata-terbaru'),
(2, 'Home - Testimoni', 'home--testimoni-901-2imghdr.jpg', 2, 'home--testimoni'),
(3, 'Profil', 'profil-206-3imghdr.jpg', 3, 'profil'),
(4, 'Sewa Mobil', 'sewa-mobil-253-4imghdr.jpg', 4, 'sewa-mobil'),
(5, 'Paket Wisata', 'paket-wisata-162-5imghdr.jpg', 5, 'paket-wisata'),
(6, 'Artikel', 'artikel-677-6imghdr.jpg', 6, 'artikel'),
(7, 'Galeri', 'galeri-667-8imghdr.jpg', 7, 'galeri'),
(8, 'Kontak', 'kontak-729-7imghdr.jpg', 8, 'kontak');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id_kontak` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `image` varchar(200) NOT NULL,
  `deskripsi` tinytext CHARACTER SET latin1 COLLATE latin1_general_ci,
  `tgl_posting` date NOT NULL,
  `jenis` enum('address','phone','email') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id_kontak`, `nama`, `judul`, `image`, `deskripsi`, `tgl_posting`, `jenis`) VALUES
(7, 'Email', 'email@gmail.com', 'email-599-email.png', '', '2018-11-28', 'email'),
(8, 'Phone', '62812 3456 789', 'call-248-phone-receiver.png', '', '2018-12-21', 'phone'),
(10, 'Address', 'Yogyakarta, Daerah Istimewa Yogyakarta, Indonesia.', '', '', '2018-11-28', 'address');

-- --------------------------------------------------------

--
-- Table structure for table `layers`
--

CREATE TABLE `layers` (
  `layer_id` int(11) NOT NULL,
  `layer_title` varchar(225) DEFAULT NULL,
  `layer_name` varchar(225) DEFAULT NULL,
  `relation_categories` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layers`
--

INSERT INTO `layers` (`layer_id`, `layer_title`, `layer_name`, `relation_categories`) VALUES
(2, 'Administrai Batu Licin AR', '1549443060_Administrasi_Batulicin_AR.kmz', 3),
(3, 'Administrasi DI Tanah Bumbu AR', '1549511993_Administrasi_DI_Tanah_Bumbu_AR.kmz', 3),
(4, 'Administrasi Tanah Bumbu AR', '1549512273_Administrasi_Tanah_Bumbu_AR.kmz', 3),
(5, 'Administrasi Tanah Bumbu LN', '1549512307_Administrasi_Tanah_Bumbu_LN.kmz', 3),
(6, 'Bangunan Air DI Tanah Bumbu 2 PT', '1549512628_Bangunan_Air_DI_Tanah_Bumbu_2_PT.kmz', 3),
(7, 'Bangunan Air DI Tanah Bumbu PT', '1549513030_Bangunan_Air_DI_Tanah_Bumbu_PT.kmz', 3),
(9, 'Saluran Irigasi DI Tanah Bumbu LN', '1549513128_Saluran_Irigasi_DI_Tanah_Bumbu_LN.kmz', 3),
(10, 'Administrasi DIR Tanah Bumbu AR', '1549513409_Administrasi_DIR_Tanah_Bumbu_AR.kmz', 4),
(11, 'Bangunan Air DIR Tanah Bumbu 2 PT', '1549513457_Bangunan_Air_DIR_Tanah_Bumbu_2_PT.kmz', 4),
(12, 'Bangunan Air DIR Tanah Bumbu PT', '1549513535_Bangunan_Air_DIR_Tanah_Bumbu_PT.kmz', 4),
(13, 'Saluran Irigasi DIR Tanah Bumbu 2 LN', '1549513677_Saluran_Irigasi_DIR_Tanah_Bumbu_2_LN.kmz', 4),
(14, 'Saluran Irigasi DIR Tanah Bumbu LN', '1549513829_Saluran_Irigasi_DIR_Tanah_Bumbu_LN.kmz', 4),
(15, 'Administrasi DIT Tanah Bumbu AR', '1549513884_Administrasi_DIT_Tanah_Bumbu_AR.kmz', 5),
(16, 'Bangunan Air DIT Tanah Bumbu PT', '1549513939_Bangunan_Air_DIT_Tanah_Bumbu_PT.kmz', 5),
(17, 'Saluran Irigasi DIT Tanah Bumbu LN', '1549513991_Saluran_Irigasi_DIT_Tanah_Bumbu_LN.kmz', 5);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `gambar` varchar(150) NOT NULL,
  `deskripsi` varchar(350) NOT NULL,
  `link` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama`, `gambar`, `deskripsi`, `link`) VALUES
(20, 'Beranda', 'akustik-ruang-beranda---Copy.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'akustik-ruang'),
(21, 'Tentang Kami', 'akustik-ruang-beranda---Copy1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'tentang-akustik-ruang'),
(22, 'Portofolio', 'akustik-ruang-beranda---Copy2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'portofolio-akustik-ruang'),
(23, 'Produk', 'akustik-ruang-beranda---Copy3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'produk-akustik-ruang'),
(24, 'Blog', 'akustik-ruang-beranda---Copy4.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'blog-akustik-ruang'),
(25, 'Konsultasi', 'akustik-ruang-beranda---Copy5.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'konsultasi-akustik-ruang'),
(26, 'Galeri', 'akustik-ruang-beranda---Copy6.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'galeri-foto-akustik-ruang'),
(27, 'Testimoni', 'akustik-ruang-beranda---Copy7.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'testimoni-akustik-ruang'),
(28, 'Kontak', 'akustik-ruang-beranda---Copy8.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'kontak-akustik-ruang');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id_messages` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `attachment` varchar(250) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id_messages`, `name`, `email`, `phone`, `subject`, `message`, `attachment`, `status`, `dateTime`) VALUES
(14, 'ivan', 'ivan@email.com', '0274', 'coba pesan', 'heheheh', '', '1', '2017-11-24 08:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `messages_konsul`
--

CREATE TABLE `messages_konsul` (
  `id_messages_konsul` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `attachment` varchar(250) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages_konsul`
--

INSERT INTO `messages_konsul` (`id_messages_konsul`, `name`, `email`, `phone`, `subject`, `message`, `attachment`, `status`, `dateTime`) VALUES
(14, 'ivan', 'ivan@email.com', '0274', 'coba pesan', 'heheheh', '', '1', '2017-11-24 08:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `modul`
--

CREATE TABLE `modul` (
  `id_modul` int(5) NOT NULL,
  `nama_modul` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `link` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `static_content` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `static_content_2` text NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `publish` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `status` enum('user','admin') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `urutan` int(5) NOT NULL,
  `link_seo` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modul`
--

INSERT INTO `modul` (`id_modul`, `nama_modul`, `link`, `static_content`, `static_content_2`, `gambar`, `publish`, `status`, `aktif`, `urutan`, `link_seo`) VALUES
(2, 'Web Keyword', '?module=keyword', 'Sistem Jaringan Irigasi', '', '', 'Y', 'admin', 'Y', 0, ''),
(4, 'Home / Welcome', '', '', '', '', 'Y', 'admin', 'Y', 0, ''),
(1, 'Web Tittle', '?module=tittle', 'Sistem Jaringan Irigasi Kabupaten Tanah Bumbu Kalimantan Selatan', '', '', 'Y', 'admin', 'Y', 0, ''),
(3, 'Web Diskripsi', '?module=diskripsi', 'Sistem Jaringan Irigasi', '', '', 'Y', 'admin', 'Y', 0, ''),
(5, 'Tentang Kami', '', '<div class=\"&quot;about-title&quot;\">Bidang SDA mempunyai tugas penyiapan perumusan kebijakan teknis dan penyelenggaraan kegiatan di bidang SDA.</div>\r\n<p>Bidang SDA dalam melaksanakan tugas menyelenggarakan fungsi:</p>\r\n<ol>\r\n<li>Penyiapan perumusan kebijakan teknis bidang SDA;</li>\r\n<li>Pelaksanaan program, pembinaan dan penyelenggaraan kegiatan di bidang sungai dan pantai;</li>\r\n<li>Pelaksanaan program, pembinaan dan penyelenggaraan kegiatan di bidang irigasi dan rawa;</li>\r\n<li>Koordinasi penyelenggaraan kegiatan;</li>\r\n<li>Evaluasi dan pelaporan;</li>\r\n<li>Pelaksanaan fungsi lain yang diberikan oleh atasan</li>\r\n</ol>\r\n<p>Bidang SDA mempunyai uraian tugas sebagai berikut:</p>\r\n<ol>\r\n<li>Menyiapkan bahan penyusunan petunjuk teknis di bidang SDA;</li>\r\n<li>Pengelolaan SDA dan bangunan pengaman pantai pada wilayan sungai dalam 1 (satu) daerah;</li>\r\n<li>Pengembangan dan pengelolaan sistem irigasi primer dan sekunder pada daerah irigasi yang luasnya kurang dari 1000 ha dalam 1 (satu) daerah;</li>\r\n<li>Penyusunan perencanaan teknis, pembinaan dan pengelolaan prasarana sumber daya air;</li>\r\n<li>Penyiapan, pembinaan, pengaturan teknis dan pelaksanaan pembangunan serta pengembangan prasarana;</li>\r\n<li>Penyiapan bahan pembinaan, pengaturan teknis dan pelaksanaan pembangunan serta pengembangan sarana;</li>\r\n<li>Penyiapan bahan pembinaan, pengaturan teknis, pelaksanaan operasi dan pemulihan;</li>\r\n<li>Penataan pengelolaan prasarana dan pemeliharaan operasi prasarana sumber daya air;</li>\r\n<li>Melaksanakan koordinasi dengan instansi terkait;</li>\r\n<li>Melaksanakan evaluasi dan pelaporan kegiatan; dan</li>\r\n<li>Melaksanakan tugas lain yang diberikan oleh atasan sesuai dengan bidang tugas.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p>Bidang SDA terdiri dari:</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>a. Seksi Sungai dan Pantai; dan</strong></span></p>\r\n<p>Seksi Sungai dan Pantai mempunyai tugas menyiapkan bahan penyusunan program, petunjuk teknis dan pengoordinasian penyelenggaraan kegiatan di bidang sungai dan pantai.</p>\r\n<p>Untuk menjabarkan tugas pokok Seksi Sungai dan Pantai mempunyai uraian tugas sebagai berikut:</p>\r\n<ol>\r\n<li>Menyiapkan bahan penyusunan petunjuk teknis pelaksanaan kebijakan dibidang sungai dan pantai;</li>\r\n<li>Melakukan penyiapan bahan penyusunan kebijakan di bidang sungai dan pantai.</li>\r\n<li>Menyiapkan bahan penelitian/survey, inventarisasi, perencanaan teknis pembangunan, pengembangan, pemeliharaan, pemanfaatan sungai dan pantai;</li>\r\n<li>Menyiapkan bahan pelaksanaan pembangunan, pengembangan, pemeliharaan dan pemanfaatan prasarana sungai dan pantai;</li>\r\n<li>Pengelolaan SDA dan bangunan pengaman pantai pada wilayah sungai dalam 1 (satu) daerah;</li>\r\n<li>Menyiapkan bahan fasilitasi penerapan sistem manajemen mutu dan sistem manajemen keselamatan dan kesehatan kerja (SMK3);</li>\r\n<li>Melaksanakan pengelolaan sistem hidrologi dan sitem peringatan dini di bidang sungai dan pantai;</li>\r\n<li>Melaksanakan penanggulangan kerusakan akibat bencana dan pemberdayaan masyarakat di bidang operasi dan pemeliharaan;</li>\r\n<li>Menyiapkan bahan koordinasi pengelolaan sumber daya air dan penyelenggaraan pemantauan dan pengawasan penggunaan sumber daya air serta penyelenggaraan penyidikan tindak pidana bidang sumber daya air;</li>\r\n<li>Menyiapkan bahan pengawasan dan pengendalian pelaksanaan pembangunan, pengembangan, pemeliharaan dan pemanfaatan prasarana sungai dan pantai;</li>\r\n<li>Menyiapkan bahan kerjasama dengan instansi terkait;</li>\r\n<li>Melaksanakan evaluasi dan pelaporan pelaksanaan tugas; dan</li>\r\n<li>Melaksanakan tugas lain yang diberikan oleh atasan sesuai dengan bidang tugas.</li>\r\n</ol>\r\n<p><span style=\"text-decoration: underline;\"><strong>b. Seksi Irigasi dan Rawa</strong></span></p>\r\n<p>Seksi Irigasi dan Rawa mempunyai tugas menyiapkan bahan penyusunan program, petunjuk teknis dan pengoordinasian penyelenggaraan kegiatan di bidang irigasi dan rawa.</p>\r\n<p>Seksi Irigasi dan Rawa mempunyai uraian tugas sebagai berikut:</p>\r\n<ol>\r\n<li>Menyiapkan bahan penelitian/survey, inventarisasi, perencanaan teknis pembangnan, pengembangan, pemeliharaan dan pemanfaatan prasarana irigasi dan rawa;</li>\r\n<li>Menyiapkan bahan penyusunan program pembangunan, pengembangan, pemeliharaan dan pemanfaatan prasarana irigasi dan rawa;</li>\r\n<li>Menyiapkan bahan penyusunan petunjuk teknis evaluasi, pemeliharaan dan pengelolaan operasi serta pemulihan prasarana irigasi dan rawa;</li>\r\n<li>Pengembangan dan pengelolaan sistem irigasi primer dan sekunder pada daerah irigasi yang luasnya kurang dari 1000 ha dalam 1 (satu) daerah;</li>\r\n<li>Melaksanakan pengelolaan sistem hidrologi dan sistem&nbsp;peringatan dini di bidang irigasi dan rawa;</li>\r\n<li>Melaksanakan penanggulangan kerusakan akibat bencana dan pemberdayaan masyarakat di bidang operasi dan pemeliharaan di bidang irigasi dan rawa;</li>\r\n<li>Melaksanakan evaluasi, pemeliharaan dan pengelolaan operasi serta pemulihan prasarana irigasi dan rawa;</li>\r\n<li>Menyiapkan bahan pemantauan dan evaluasi pelaksanaan program penelitian/survey, pembangunan, pengembangan, pemeliharaan dan pemanfaatan;</li>\r\n<li>Menyiapkan bahan koordinasi dengan instansi terkait;</li>\r\n<li>Melaksanakan evaluasi dan pelaporan kegiatan; dan</li>\r\n<li>Melaksanakan tugas lain yang diberikan oleh atasan sesuai dengan bidang tugas.</li>\r\n</ol>', '<div class=\"about-title\" style=\"text-align: justify;\">contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi.</div>\r\n<div class=\"about-title\" style=\"text-align: justify;\">&nbsp;</div>\r\n<div class=\"about-title\" style=\"text-align: justify;\">contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi,&nbsp;contoh isi tupoksi irigasi.</div>', '', 'Y', 'admin', 'Y', 0, ''),
(9, 'Profil Singkat', '', '<h2 style=\"text-align: center;\"><strong>Welcome</strong></h2>\r\n<p>&nbsp;</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(10, 'Welcome', '', '<h2><strong>Welcome to Anestesiugm</strong></h2>\r\n<h3>Departemen Anestesiologi,Resusitasi dan Terapi Intensif FK UGM-RSUP Dr Sardjito / PERDATIN Yogyakarta&nbsp;</h3>', '', '', 'Y', 'admin', 'Y', 0, ''),
(6, 'Maps', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d415.47071650711007!2d110.37921559296592!3d-7.827255400227396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac8d90905871c1b5!2sJogjaSite.com!5e0!3m2!1sen!2sid!4v1511343510977', '', '', 'Y', 'admin', 'Y', 0, ''),
(7, 'Info Kontak', '', '<p style=\"text-align: left;\"><strong>info@otoplus.co.id</strong></p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(8, 'Testimoni', '', '<p>Testimoni&nbsp;</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(11, 'Career', '', '<h4><em>Caarer sedang dalam proses penginputan data</em></h4>', '', '', 'Y', 'admin', 'Y', 0, ''),
(12, 'Car Additional Content', '', '<h3>Nb :</h3>\r\n<p>Dengan biaya sewa diatas,</p>\r\n<ol>\r\n<li>1. Biaya sewa belum termasuk biaya parkir dan makan driver</li>\r\n<li>2. Biaya overtime 10% dari sewa</li>\r\n<li>3. Harga berubah pada hari raya dan tahun baru</li>\r\n</ol>', '', '', 'Y', 'admin', 'Y', 0, ''),
(21, 'Testimoni Short Description', '', '<p>contoh deskripsi singkat testimoni</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(22, 'Client Short Description', '', '<p>contoh deskripsi singkat klien</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(23, 'Profile Short Description', '', '<p>contoh deskripsi singkat profil</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(24, 'Car Short Description', '', '<p>contoh deskripsi singkat sewa mobil</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(25, 'Tour Package Short Decription', '', '<p>contoh deskripsi singkat paket wisata</p>', '', '', 'Y', 'admin', 'Y', 0, ''),
(26, 'Article Short Description', '', 'contoh deskripsi singkat artikel', '', '', 'Y', 'admin', 'Y', 0, ''),
(27, 'Gallery Short Description', '', 'contoh deskripsi singkat galeri', '', '', 'Y', 'admin', 'Y', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `portofolio`
--

CREATE TABLE `portofolio` (
  `id_portofolio` int(11) NOT NULL,
  `id_portofolio_kategori` int(11) DEFAULT NULL,
  `judul` text NOT NULL,
  `nama` varchar(100) NOT NULL,
  `popular` enum('Y','N') DEFAULT NULL,
  `short_desc` text,
  `konten` text,
  `image` text NOT NULL,
  `image_2` text NOT NULL,
  `image_3` text,
  `date` date NOT NULL,
  `seo` text NOT NULL,
  `status` enum('Y','N') DEFAULT NULL,
  `stat` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portofolio`
--

INSERT INTO `portofolio` (`id_portofolio`, `id_portofolio_kategori`, `judul`, `nama`, `popular`, `short_desc`, `konten`, `image`, `image_2`, `image_3`, `date`, `seo`, `status`, `stat`) VALUES
(33, 27, 'DI Baru Gelang', '', NULL, NULL, '<p>Baru Gelang merupakan salah satu desa yang terletak di kecamatan Kusan Hilir, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Baru Gelang mencapai 360.3 Ha. Daerah irigasi Barugelang memiliki luas 262,63 Ha lahan potensial dari total 340 Ha lahan baku yang tersedia.Baru gelang menggunakan tadah hujan sebagai sumber utama pengairan dan sudah di dukung dengan bangunan irigasi seperti pintu air dan saluran. Namun keadaan pintu air di Baru Gelang sudah rusak dan memerlukan perbaikan.</p>', 'sijari-tanahbumbu-A1.-DI-BARU-GELANG-1.jpg', 'sijari-tanahbumbu-A1.-DI-BARU-GELANG-11.jpg', 'sijari-tanahbumbu-1.-DI-BARUGELANG-1.jpg', '2019-04-08', 'di-baru-gelang', 'Y', 3),
(34, 13, 'DI Karang Bintang', '', NULL, NULL, '<p>Daerah irigasi Karang Bintang terletak di Kecamatan Karang Bintang. Luas Desa Karang Bintang yaitu 4252.61 Ha. Luas fungsional persawahan mencapai 25.69 Ha dan dikelola oleh 125 Petani. Persawahan di Karang Bintang menggunakan tadah hujan sebagai sumber pengairan utama. Untuk beberapa titik lokasi persawahan dekat dengan sungai, tetapi sumber pengairan tetap dengan tadah hujan dikarenakan letak sungai lebih rendah dibandingkan dengan petak sawah. Tahun ini di Karang Bintang mencoba pengairan yang berasal dari pabrik perkebunan karet. Bangunan irigasi di Karang Bintang masih alami dan hanya terdapat 1 pintu air yang sudah tidak berfungsi lagi.</p>', 'sijari-tanahbumbu-A2.-DI-KARANG-BINTANG-1.jpg', 'sijari-tanahbumbu-A2.-DI-KARANG-BINTANG-11.jpg', 'sijari-tanahbumbu-2.-DI-KARANG-BINTANG-1.jpg', '2019-04-08', 'di-karang-bintang', 'Y', 3),
(35, 13, 'DI Kersik Putih', '', NULL, NULL, '<p>Daerah irigasi Kersik Putih terletak di Desa Kersik Putih, Kecamatan Batulicin. Luas Desa Kersik Putih mencapai 1127.53 Ha. Area persawahan mencapai &plusmn;42.6 Ha yang dikelola oleh &plusmn;180 petani. Sumber air utama persawahan ini adalah tadah hujan, sedangkan untuk penunjang menggunakan embung. Tetapi embung ini hanya untuk mengalirkan ke beberapa petak sawah saja, karena lokasinya yang tidak strategis. Di Kersik Putih sudah di dukung dengan bangunan irigasi yang cukup memadai. Hanya perlu perhatian di beberapa titik, seperti pintu air yang rusak, butuh perpanjangan saluran, dan perawatan untuk asset yang sudah ada.</p>', 'sijari-tanahbumbu-A3.-DI-KERSIK-PUTIH-1.jpg', 'sijari-tanahbumbu-A3.-DI-KERSIK-PUTIH-11.jpg', 'sijari-tanahbumbu-3.-DI-KERSIK-PUTIH-1.jpg', '2019-04-08', 'di-kersik-putih', 'Y', 3),
(36, 12, 'DI Mekar Jaya', '', NULL, NULL, '<p>Luas Desa Mekar Jaya mencapai 136.76 Ha. Daerah irigasi Mekar Jaya memiliki lahan fungsional seluas 39.17 Ha. Lahan fungsional tersebut dimanfaatkan menjadi persawahan. Persawahan di Mekar Jaya mendapatkan pengairan dari Sungai Kusan. Di Mekar jaya sudah didukung dengan bangunan irigasi untuk mengatur debit dan suplai air yang masuk. Hanya perlu pembuatan gorong-gorong agar pengairan lebih lancar dan perawatan terhadap adet yang sudah ada.</p>', 'sijari-tanahbumbu-A4.-DI-MEKAR-JAYA-1.jpg', 'sijari-tanahbumbu-A4.-DI-MEKAR-JAYA-11.jpg', 'sijari-tanahbumbu-4.-DI-MEKAR-JAYA-1.jpg', '2019-04-08', 'di-mekar-jaya', 'Y', 3),
(37, 13, 'DI Pondok Butun Gunung Tinggi', '', NULL, NULL, '<p>Pondok Butun terletak di Kecamatan Batulicin, Kabupaten Tanah Bumbu, Kalimantan Selatan. Luas desa Pondok Butun mencapai 1306.9 Ha. Dari luas desa tersebut terdapat 234.08 Ha luas potensial dan hanya berfungsi seluas 68.27 Ha. Di Pondok Butun sudah di dukung dengan jaringan irigasi yang cukup memadai. Hanya terjadi beberapa kerusakan pada pintu air dan adanya pendangkalan pada saluran yang menyebabkan meluapnya air ketika hujan tiba.</p>', 'sijari-tanahbumbu-A5.-DI-PONDOK-BUTUN-GUNUNG-TINGGI-1.jpg', 'sijari-tanahbumbu-A5.-DI-PONDOK-BUTUN-GUNUNG-TINGGI-11.jpg', NULL, '2019-02-12', 'di-pondok-butun-gunung-tinggi', 'Y', 3),
(38, 13, 'DI Polewali Marajae', '', NULL, NULL, '<p>Luas Desa Polewali Marajae mencapai 432.49 Ha. Daerah irigasi Polewali Marajae memiliki luas fungsional persawahan mencapai 173.26 Ha. Daerah ini sudah di dukung dengan adanya bangunan irigasi, seperti pintu air, bangunan bagi, saluran, gorong-gorong dan jembatan. Namun, kondisi aset kurang diperhatikan. Pintu air yang terbuat dari besi sudah berkarat dan tidak bisa digunakan lagi. Beberapa jembatan mengalami kerusakan seperti gelagar yang sudah keropos dan ambles menutupi saluran. Kondisi saluran juga sudah ada yang ditumbuhi rumput liar. Kurangnya perawatan ini menyebabkan di beberapa titik mengalami kebanjiran jika musim penghujan datang.</p>', 'sijari-tanahbumbu-A6.-DI-PULAU-WALI-1.jpg', 'sijari-tanahbumbu-A6.-DI-PULAU-WALI-11.jpg', NULL, '2019-02-13', 'di-polewali-marajae', 'Y', 3),
(39, 27, 'DI Salimuran', '', NULL, NULL, '<p>Daerah irigasi Salimuran memiliki luas fungsional seluas 414.37 Ha dari luas Desa Salimuran 2333.04 Ha. Sumber air utama berasal dari Sungai Kusan dan tadah hujan. Bangunan irigasi di Salimuran sudah cukup mendukung pertanian disana. Dibeberapa titik perlu peninggian tanggul dikarenakan sering terjadinya hujan ketika musim penghujan. Selain itu juga perlu pendalaman saluran sekunder dan penambahan saluran untuk masuk ke petak sawah yang tidak bisa terjangkau.</p>', 'sijari-tanahbumbu-A7.-DI-SALIMURAN-1.jpg', 'sijari-tanahbumbu-A7.-DI-SALIMURAN-11.jpg', NULL, '2019-02-12', 'di-salimuran', 'Y', 3),
(40, 15, 'DI Satui Timur', '', NULL, NULL, '<p>Satui Timur merupakan salah satu desa yang terletak di kecamatan Satui, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Satui Timur mencapai 3970.5 Ha. Daerah irigasi Satui Timur memiliki luas 8,2 Ha lahan potensial dari total 1672,43 Ha lahan baku yang tersedia. Di Satui Timur cukup di dukung dengan jaringan irigasi seperti Saluran Sekunder dan Saluran Primer. Keadaan saluran cukup baik, hanya pad saluran sekunder mengalami pendangkalan dikarenakan di tumbuhi dengan semak-semak.</p>', 'sijari-tanahbumbu-A8.-DI-SATUI-TIMUR-1.jpg', 'sijari-tanahbumbu-A8.-DI-SATUI-TIMUR-11.jpg', 'sijari-tanahbumbu-8.-DI-SATUI-TIMUR-1.jpg', '2019-04-08', 'di-satui-timur', 'Y', 3),
(41, 13, 'DI Segumbang', '', NULL, NULL, '<p>Daerah irigasi Segumbang terletak di Desa Segumbang, Kecamatan Batulicin dengan luas desa 426.05 Ha. Luas fungsional area persawahan mencapai 105.25 Ha yang dikelola oleh 6 kelompok tani. Sumber air utama area persawahan ini adalah tadah hujan. Selain itu, lahan pertanian juga mendapatkan suplai air dari sungai yang mengalir diperbatasan dengan desa Kersik Putih. Persawahan di Desa Segumbang ini dapat melakukan 2 kali musim tanam setiap tahunnya. Dibeberapa titik saluran sekunder masih alami dan mengalami pendangkalan akibat sedimentasi tanah.Kondisi tersebut juga berdampak kurang maksimalnya pasokan air ke lahan-lahan persawahan.</p>', 'sijari-tanahbumbu-A9.-DI-SEGUMBANG-1.jpg', 'sijari-tanahbumbu-A9.-DI-SEGUMBANG-11.jpg', 'sijari-tanahbumbu-9.-DI-SEGUMBANG-1.jpg', '2019-04-08', 'di-segumbang', 'Y', 3),
(42, 15, 'DI Sei Danau', '', NULL, NULL, '<p>Sei Danau merupakan salah satu desa yang terletak di kecamatan Satui, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Sei Danau mencapai 1868.95 Ha. Daerah irigasi Sei Danau memiliki luas 17,69 Ha lahan potensial dari total 172,92 Ha lahan baku yang tersedia. Di Sei Danau sudah di dukung dengan jaringan irigasi untuk suplesi dan pembuangan air. Keadaan saluran cukup baik, tidak dengan bangunan air yang sudah tidak berfungsi lagi dan diusulkan untuk dilakukan perbaikan.</p>', 'sijari-tanahbumbu-A10.-DI-SEI-DANAU-1.jpg', 'sijari-tanahbumbu-A10.-DI-SEI-DANAU-11.jpg', 'sijari-tanahbumbu-10.-DI-SEI-DANAU-1.jpg', '2019-04-08', 'di-sei-danau', 'Y', 3),
(43, 32, 'DI Sei Loban', '', NULL, NULL, '<p>Desa Sungai Loban terletak di Kecamatan Sei Loban dan masuk dalam kategori Daerah Irigasi Tambak. Di Sei Loban memiliki luas fungsional seluas 7.7 Ha dari total luas Desa 2900.19 Ha. Namun Pertambakan di daerah ini sudah tidak berfungsi sejak tahun 2000an dikarenakan penduduk sudah beralih profesi menjadi penambang. Di Sei Loban juga belum ada jaringan irigasi yang baik dan sebagian besar petak tambak sudah di tumbuhi bakau. Menurut perangkat desa setempat, di Sei Loban sangat berpotensi untuk di jadikan tambak karena letaknya yang dekat dengan laut dan lahan potensial yang luas.</p>', 'sijari-tanahbumbu-A11.-DI-SEI-LOBAN-1.jpg', 'sijari-tanahbumbu-A11.-DI-SEI-LOBAN-11.jpg', 'sijari-tanahbumbu-11.-DI-SEI-LOBAN-1.jpg', '2019-04-08', 'di-sei-loban', 'Y', 3),
(44, 27, 'DI Sepunggur', '', NULL, NULL, '<p>Daerah irigasi Sepunggur terletak di Kecamatan Kusan Hilir dengan luas Desa mencapai 3724.34 Ha. Luas fungsional persawahan mencapai 341.15 Ha yang dikelola sebanyak 172 petani dalam 17 kelompok tani. Sumber air utama dari persawahan di Desa Sepunggur yaitu tadah hujan. Irigasi di Desa Sepunggur belum maksimal karena belum adanya pintu air.</p>', 'sijari-tanahbumbu-A12.-DI-SEPUNGGUR-1.jpg', 'sijari-tanahbumbu-A12.-DI-SEPUNGGUR-11.jpg', 'sijari-tanahbumbu-12.-DI-SEPUNGGUR-1.jpg', '2019-04-08', 'di-sepunggur', 'Y', 3),
(45, 27, 'DI Serdangan', '', NULL, NULL, '<p>Daerah irigasi Serdangan terletak di Kusan Hilir dengan luas desa 4013.35 Ha. Luas baku di Sardangan mencapai 1973.62 Ha, tetapi hanya 57.83 Ha saja yang di manfaatkan menjadi lahan persawahan. Namun di Sardangan mengalami kendala yaitu banjir. Kurangnya tinggi tanggul dan menjadi hilir dari beberapa titik saluran menyebabkan banjir sering melanda. Bangunan irigasi di Sardangan sudah mendukung dengan adanya pintu air dan saluran yang bersih.</p>', 'sijari-tanahbumbu-A13.-DI-SERDANGAN-1.jpg', 'sijari-tanahbumbu-A13.-DI-SERDANGAN-11.jpg', 'sijari-tanahbumbu-13.-DI-SARDANGAN-1.jpg', '2019-04-08', 'di-serdangan', 'Y', 3),
(46, 12, 'DI Sumber Baru', '', NULL, NULL, '<p>Sumber Baru merupakan salah satu desa yang terletak di kecamatan Angsana, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Sumber Baru mencapai 2306.11 Ha. Daerah irigasi Sumber Baru memiliki luas 191,17 Ha lahan potensial dari total 471,3 Ha lahan baku yang tersedia. Di Desa Sumber Baru sudah di dukung dengan jaringan irigasi yang mendukung suplesi dan pembuangan air. Kondisi saluran cuku baik, tetapi kondisi bangunan air tidak dapat di fungsikan.</p>', 'sijari-tanahbumbu-A14.-DI-SUMBER-BARU-1.jpg', 'sijari-tanahbumbu-A14.-DI-SUMBER-BARU-11.jpg', 'sijari-tanahbumbu-14.-DI-SUMBER-BARU-1.jpg', '2019-04-08', 'di-sumber-baru', 'Y', 3),
(47, 27, 'DI Beringin', '', NULL, NULL, '<p>Beringin merupakan salah satu desa yang terletak di kecamatan Kusan Hilir, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Beringin mencapai 659.38 Ha. Daerah irigasi Beringin memiliki luas 289,77 Ha yang di garap dari total 463,97 Ha lahan baku yang tersedia. Di desa Beringin sudah di dukung dengan jaringan irigasi untuk mendukung suolesi dan pembuangan air seperti, saluran sekunder, saluran primer, jembatan, pintu air dan gorong-gorong. Keadaan tidak cukup baik, terjadi beberapa kerusakan pada saluran seperti ditumbuhi rumput dan pendangkalan. Untuk bangunan air tidak dapat difungsikan lagi.</p>', 'sijari-tanahbumbu-A15.-DI-BERINGIN-1.jpg', 'sijari-tanahbumbu-A15.-DI-BERINGIN-11.jpg', NULL, '2019-02-12', 'di-beringin', 'Y', 3),
(48, 29, 'DI Karya Bakti', '', NULL, NULL, '<p>Luas Desa Karya Bakti mencapai 547.95 Ha dan memiliki luas persawahan 42.6 Ha yang dikelola oleh 90 petani. Akses jalan masuk ke Karya Bakti sangat sulit karena tidak ada jalan melalui aspal. Untuk persawahan di Karya Bakti di dukung pengairan dari Sungai Kusan. Selain itu di Karya Bakti sudah dukung oleh bangunan air yang cukup memadai dan perbaikan pintu air.</p>', 'sijari-tanahbumbu-A16.-DI-KARYA-BAKTI-1.jpg', 'sijari-tanahbumbu-A16.-DI-KARYA-BAKTI-11.jpg', 'sijari-tanahbumbu-16.-DI-KARYA-BAKTI-1.jpg', '2019-04-08', 'di-karya-bakti', 'Y', 3),
(49, 27, 'DI Pulau Tanjung', '', NULL, NULL, '<p>Daerah irigasi Pulau Tanjung terletak di Kusan Hilir dengan luas desa sebesar 625.6 Ha. Untuk mencapai daerah tersebut dapat melalui Derasan Binjai. Luas areal persawahan aktif mencapai 182.81 Ha yang dikelola 135 petani. Persawahan di Pulau Tanjung didukung dengan pengairan berasal dari Sungai Kusan. Di Pulau Tanjung sudah di dukung oleh bangunan irigasi. Di beberapa titik perlu perbaikan pintu air dan pendalaman saluran. Dari seluruh saluran yang ada hanya 1 titik saluran saja yang sudah di siring. Kendala yang di hadapi yaitu kebanjiran ketika musim hujan datang.</p>', 'sijari-tanahbumbu-A17.-DI-PULAU-TANJUNG-1.jpg', 'sijari-tanahbumbu-A17.-DI-PULAU-TANJUNG-11.jpg', 'sijari-tanahbumbu-17.-DI-PULAU-TANJUNG-1.jpg', '2019-04-08', 'di-pulau-tanjung', 'Y', 3),
(50, 27, 'DI Satiung', '', NULL, NULL, '<p>Daerah irigasi Satiung termasuk ke dalam daerah irigasi permukaan. Terletak pada Kecamatan Kusan Hilir, Kabupaten Tanah Bumbu, Kalimantan Selatan. Memiliki luas fungsional seluas 134.76 Ha dari total luas desa 3311.51 Ha. Di desa Satiung sudah di dukung dengan jaringan irigasi untu suplesi dan pembuangan air seperti bangunan air, dorong-gorong, jembatan kayu, dan tanggul penahan banjir.</p>', 'sijari-tanahbumbu-A18.-DI-SATIUNG-1.jpg', 'sijari-tanahbumbu-A18.-DI-SATIUNG-11.jpg', 'sijari-tanahbumbu-18.-DI-SATIUNG-1.jpg', '2019-04-08', 'di-satiung', 'Y', 3),
(51, 27, 'DI Sei Bubu', '', NULL, NULL, '<p>Daerah irigasi Sungai Bubu (Sei Bubu) terletak di Desa Sei Bubu, Kecamatan Kusan Hilir dengan luas desa sebesar 2439.82 Ha. Luas persawahan di desa ini mencapai 162.79 Ha. Sumber utama pengairan di sawah ini menggunakan DAM, sehingga di desa ini bisa mengalami musim tanam sebanyak 2 kali dalam setahun. Untuk meningkatkan pertanian dari pihak desa sudah membuatkan embung seluas 200x50 m dan diusulkan untuk dibuat bangunan. Aset irigasi seperti pintu air masih dalam kondisi baik dan saluran-saluran masih dalam bentuk alami, belum ada penyiringan.</p>', 'sijari-tanahbumbu-A19.-DI-SEI-BUBU-1.jpg', 'sijari-tanahbumbu-A19.-DI-SEI-BUBU-11.jpg', 'sijari-tanahbumbu-19.-DI-SEI-BUBU-1.jpg', '2019-04-08', 'di-sei-bubu', 'Y', 3),
(52, 27, 'DI Sei Lembu', '', NULL, NULL, '<p>Sei Lembu merupakan salah satu desa yang terletak di kecamatan Kusan Hilir, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Sei Lembu mencapai 246.92 Ha. Daerah irigasi Sei Lembu memiliki luas 131,06 Ha yang di garap dari total 221,76 Ha lahan baku yang tersedia.</p>', 'sijari-tanahbumbu-A20.-DI-SEI-LEMBU-1.jpg', 'sijari-tanahbumbu-A20.-DI-SEI-LEMBU-11.jpg', 'sijari-tanahbumbu-20.-DI-SEI-LEMBU-1.jpg', '2019-04-08', 'di-sei-lembu', 'Y', 3),
(53, 19, 'DIR Anjir Baru', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Anjir Baru terletak di Kecamatan Kusan Hulu dengan luas desa sebesar 1471.4 Ha dan memiliki luas fungsional 184.88 Ha. Sumber utama pengairan berasal dari tadah hujan. Bangunan irigasi di Anjir Baru perlu unruk di perbaiki dan dibangun. Banyak saluran air yang sudah di tumbuhi rumput-rumput sehingga menghambat aliran air. Oleh karena itu perlu adanya normalisasi sungai agar aliran sungai tidak terhambat dan dapat menjadi penunjang persawahan di Anjir Baru.</p>', 'sijari-tanahbumbu-B1.-ANJIR-BARUpdf-1.jpg', 'sijari-tanahbumbu-B1.-DIR-ANJIR-BARU-1.jpg', 'sijari-tanahbumbu-1.-DIR-ANJIR-BARU-1.jpg', '2019-04-09', 'dir-anjir-baru', 'Y', 4),
(54, 19, 'DIR Bakarangan', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Bakarangan memiliki lahan fungsional seluas 48.05 Ha dari total luas desa sebesar 5070.6 Ha dan termasuk ke dalam daerah irigasi rawa (D.I.R). Area persawahan ini di kelola oleh 72 orang yang tergabung dalam 4 kelompok tani. Sumber air yang digunakan di Bekarangan menggunakan Sungai Kusan sebagai sumber utama dan mata air. Di Bakarangan sudah didukung oleh bangunan air seperti pintu air, saluran pembuang, taggul dan jembatan.</p>', 'sijari-tanahbumbu-B2.DIR-BAKARANGAN-1.jpg', 'sijari-tanahbumbu-B2.-BEKARANGAN-1.jpg', 'sijari-tanahbumbu-2.-DIR-BAKARANGAN-1.jpg', '2019-04-09', 'dir-bakarangan', 'Y', 4),
(55, 19, 'DIR Binawara', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Binawara memiliki luas fungsional 92.51 Ha dari total luas desa 1599 Ha. Sumber air di Binawara berasal dari tadah hujan. Para petani setempat meminta untuk penormalisasian sungai dikarenakan aliran air menjadi tidak maksimal. Di beberapa titik perlu adanya peninggian tanggul karena banjir ketika musim penghujan dan aliran air juga bertambah dikarenakan pembuangan dari perkebunan sawit.</p>', 'sijari-tanahbumbu-B3.-DIR-BINAWARA-1.jpg', 'sijari-tanahbumbu-B3.-BINAWARA-1.jpg', 'sijari-tanahbumbu-3.-DIR-BINAWARA-1.jpg', '2019-04-09', 'dir-binawara', 'Y', 4),
(56, 19, 'DIR Derasan Binjai', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Sei Binjai memiliki luas fungsional 25.68 Ha yang di garap sebanyak 360 petani dan tergabung dalam 18 kelompok tani. Bangunan irigasi yang ada di Sei Binjai belum cukup mendukung. Pintu air yang ada sudah rusak. Kondisi jalan perlu untuk di cor karena tergerus ketika banjir datang. Saluran air yang ada belum bisa menyalurkan air ke seluruh petak sawah. Perlu adanya pembangunan saluran menuju sumber air yaitu sungai.</p>', 'sijari-tanahbumbu-B4.DIR-DERASAN-BINJAI-1.jpg', 'sijari-tanahbumbu-B4.-DERASAN-BINJAI-1.jpg', 'sijari-tanahbumbu-4.-DIR-DERESAN-BINJAI-1.jpg', '2019-04-09', 'dir-derasan-binjai', 'Y', 4),
(57, 18, 'DIR Girimulya-Kuranji', '', NULL, NULL, '<p style=\"text-align: justify;\">Giri Mulya masuk ke dalam Kecamatan Kuranji dan memiliki lahan fungsional seluas 475 Ha dari total luas desa 1842.5 Ha. Lahan tersebut dikelola sebanyak 390 petani dan tergabung dalam 10 kelompok tani. Sumber air utama yang digunakan adalah tadah hujan. Di Giri Mulya sudah di dukung dengan bangunan irigasi seperti pintu air, saluran intake/pembuangan, bangunan bagi, jembatan dan gorong-gorong.</p>', 'sijari-tanahbumbu-B5.DIR-GIRIMULYA-KURANJI-1.jpg', 'sijari-tanahbumbu-B5.GIRI-MULYA-1.jpg', 'sijari-tanahbumbu-5.-DIR-GIRI-MULYA-1.jpg', '2019-04-09', 'dir-girimulyakuranji', 'Y', 4),
(58, 19, 'DIR Guntung', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Guntung termasuk ke dalam Daerah Irigasi Rawa (D.I.R). Guntung memiliki luasan sawah fungsional 50.96 Ha dari total luas desa 12851.8 Ha. Sumber air utama persawahan berasal dari tadah hujan dan penunjang yaitu Sungai Kusan. Pertanian di Guntung kurang didukung dengan Bangunan Irigasi, sehingga pengaturan debit dan suplai air kurang terkendali. Hal tersebut yang sering menyebabkan banjir. Usulan yang diberikan para petani berupa pembuatan tunggul dan pintu air.</p>', 'sijari-tanahbumbu-B6.DIR-GUNTUNG-1.jpg', 'sijari-tanahbumbu-B6.-GUNTUNG-1.jpg', 'sijari-tanahbumbu-6.-DIR-GUNTUNG-1.jpg', '2019-04-09', 'dir-guntung', 'Y', 4),
(59, 19, 'DIR Hantiif', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Hantiif merupakan daerah irigasi rawa (D.I.R) yang memanfaatkan tadah hujan sebagai sumber air utama. Kawasan petanian yang fungsional mencapai 58.98 Ha dari total luas desa 4682.5 Ha. Kawasan pertanian di daerah irigasi Hati&rsquo;if belum di dukung oleh bangunan irigasi yang memadai sehingga suplai air dan pengaturan debit masih terkendala. Hal ini yang menyebabkan areal pertanian tergenang air ketika musim penghujan tiba dan kekeringan jika musim kemarau.</p>', 'sijari-tanahbumbu-B7.-DIR-HANTIIF-1.jpg', 'sijari-tanahbumbu-B7.HANTIIF-1.jpg', 'sijari-tanahbumbu-7.-DIR-HATI-IF-1.jpg', '2019-04-09', 'dir-hantiif', 'Y', 4),
(60, 19, 'DIR Harapan Jaya', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Harapan Jaya terletak di Desa Harapan Jaya, Kecamatan Kusan Hulu dengan luas desa 1017.09 Ha. Areal persawahan di desa harapan jaya menggunakan 2 sumber air yaitu Sungai Salimuran dan Sungai Satiung. Luas fungsional dari persawahan mencapai 65.44 Ha saja dari luas baku 206.04 Ha. Untuk bangunan irigasi, sudah terdapat pintu air, tetapi dalam kondisi yang rusak ringan.</p>', 'sijari-tanahbumbu-B8.DIR-HARAPAN-JAYA-1.jpg', 'sijari-tanahbumbu-B8.-HARAPAN-JAYApdf-1.jpg', 'sijari-tanahbumbu-8.-DIR-HARAPANA-JAYA-1.jpg', '2019-04-09', 'dir-harapan-jaya', 'Y', 4),
(61, 20, 'DIR Karang Jawa-Barokah', '', NULL, NULL, '<p style=\"text-align: justify;\">Karang jawa merupakan salah satu desa yang terletak di kecamatan Simpang Empat, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa mencapai 703.50 Ha. Daerah irigasi Karang Jawa memiliki luas 21,43 Ha yang di garap dari total 111,98 Ha lahan baku yang tersedia. Di daerah irigasi Karang Jawa sudah di dukung dengan jaringan irigasi seperti pintu air, saluran primer, saluran sekunder, saluran tersier, dan jembatan dan dalam kondisi cukup baik.</p>', 'sijari-tanahbumbu-B9.DIR-KARANG-JAWA-BAROKAHpdf-1.jpg', 'sijari-tanahbumbu-B9.-KARANGJAWA-BAROKAH-1.jpg', 'sijari-tanahbumbu-9.-DIR-KARANG-JAWA-1.jpg', '2019-04-09', 'dir-karang-jawabarokah', 'Y', 4),
(62, 19, 'DIR Karang Mulya', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Karang Mulya terletak di Kecamatan Kusan Hulu dan memiliki luas fungsional 37.17 Ha dari total luas desa 1071.39 Ha. Sumber air utama di persawahan tersebut adalah tadah hujan. Melalui sumber air tersebut mendukung musim tanam sawah sebanyak 2 kali dalam setahun. Petani mengusulkan dibuatkannya embung seluas &frac14; Ha untuk menunjang sumber air. Di Karang Mulya belum ada bangunan irigasi yang memadai. Belum adanya pintu air untuk mengatur suplai dan debit air yang masuk. Saluran masih alami, berupa galian. Untuk jembatan masih banyak yang membutuhkan perbaikan.</p>', 'sijari-tanahbumbu-B10.DIR-KARANG-MULYA-1.jpg', 'sijari-tanahbumbu-B10.-KARANGMULYA-1.jpg', 'sijari-tanahbumbu-10.-DIR-KARANG-MULYA-1.jpg', '2019-04-09', 'dir-karang-mulya', 'Y', 4),
(63, 19, 'DIR Kepayang', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Teluk Kepayang dikategorikan Daerah Irigasi Rawa (D.I.R). Luas fungsional Teluk Kepayang mencapai 63.96 Ha dari total luas desa 14758.56 Ha. Sumber air utama Teluk Kepayang yaitu tadah hujan dan penunjang Sungai Buran. Peninggian tanggul di perlukan, supaya air yang berasal dari Sungai Buran dan air pembuangan perkebunan sawit.</p>', 'sijari-tanahbumbu-B11.DIR-KEPAYANG-1.jpg', 'sijari-tanahbumbu-B11.-KEPAYANG-1.jpg', NULL, '2019-02-13', 'dir-kepayang', 'Y', 4),
(64, 17, 'DIR Kuranji Kodeco-Maju Bersama', '', NULL, NULL, '<p style=\"text-align: justify;\">Kuranji Kodeco (Maju Bersama) merupakan salah satu desa yang terletak di kecamatan Batulicin, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Kuranji Kudeco mencapai 2968.30 Ha. Daerah irigasi Kuranji Kodeco memiliki luas 38,87 Ha yang di garap dari total 324,01 Ha lahan baku yang tersedia.</p>', 'sijari-tanahbumbu-B12.DIR-KURANJI-KODECO-MAJU-BERSAMA-1.jpg', 'sijari-tanahbumbu-B12.KURANJI-KUDECO-MAJU-BERSAMApdf-1.jpg', NULL, '2019-02-13', 'dir-kuranji-kodecomaju-bersama', 'Y', 4),
(65, 17, 'DIR Kusambi-Maju Makmur', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Maju Makmur merupakan daerah irigasi rawa (D.I.R) yang memiliki lahan fungsional sawah seluas 93.66 Ha dari total luas desa 1181.26 Ha. Lahan tersebut diolah oleh petani sebanyak 101 orang dan dibagi ke dalam 6 kelompok tani. Selain itu juga pengairan di daerah irigasi maju makmur didukung dari Sungai Kusambi dan Sungai Tempurung. Persawahan di maju makmur belum di dukung oleh bangunan irigasi yang memadai sehingga sering terjadi banjir ketika musim penghujan datang.</p>', 'sijari-tanahbumbu-B14.DIR-KUSAMBI-MAJU-MAKMUR-1.jpg', 'sijari-tanahbumbu-B14.-MAJU-MAKMUR--1.jpg', 'sijari-tanahbumbu-13.-DIR-MAJU-MAKMUR--KUSAMBI-1.jpg', '2019-04-09', 'dir-kusambimaju-makmur', 'Y', 4),
(66, 19, 'DIR Manuntung', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Manuntung terletak di Kecamatan Kusan Hulu, Kalimantan Selatan. Luas desa tersebut mencapai 1228.24 Ha. Luas potensial daerah irigasi Manuntung sebesar 51.136 Ha dan hanya di fungsionalkan seluas 51.14 Ha. Di Manuntu g sudah di dukung dengan jaringan irigasi seperti pintu air, jembatan, saluran primer dan saluran sekaunder.</p>', 'sijari-tanahbumbu-B15.DIR-MANUNTUNG-1.jpg', 'sijari-tanahbumbu-B15.-MANUNTUNG-1.jpg', 'sijari-tanahbumbu-14.-DIR-MANUNTUNG-1.jpg', '2019-04-09', 'dir-manuntung', 'Y', 4),
(67, 19, 'DIR Pacakan', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Pacakan masuk ke dalam daerah irigasi rawa (D.I.R) dan terletak di Desa Pacakan, Kecamatan Kusan Hulu dengan luasan desa sebesar 5253.95 Ha. Luas fungsional mencapai 136.25 Ha. Untuk sumber air yang digunakan yaitu luapan Sungai Kusan ketika musim penghujan. Luapan air ini akan masuk ke dalam saluran yang primer yang selanjutnya akan di bagi melalui bangunan bagi dan masuk ke dalam saluran sekunder. Namun, masih terjadi masalah kebanjiran ketika musim penghujan di area persawahan karena kondisi tanggul yang kurang tinggi dan beberapa pintu air kurang berfungsi maksimal.</p>', 'sijari-tanahbumbu-B16.DIR-PACAKAN-1.jpg', 'sijari-tanahbumbu-B16.-PACAKAN-1.jpg', 'sijari-tanahbumbu-15.-DIR-PACAKAN-1.jpg', '2019-04-09', 'dir-pacakan', 'Y', 4),
(68, 18, 'DIR Ringkit', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Ringkit termasuk Daerah Irigasi Rawa (D.I.R). luas fungsional di Ringkit mencapai 77.87 Ha dari total luasan desa 897.5 Ha. Sumber utama pengairan yaitu Sungai Ringkit dan tadah hujan. Di Ringkit belum ada bangunan irigasi yaitu Pintu Air, pintu air yang sudah ada mengalami kerusakan. Usulan dari petani untuk dibuatkannya Pintu Air pada sungai utama, peninggian tanggul untuk menghambat air pembuangan dari perkebunan sawit, dan memperpanjang saluran ke petak sawah yang jauh dari sumber air.</p>', 'sijari-tanahbumbu-B17.DIR-RINGKIT-1.jpg', 'sijari-tanahbumbu-B17.-RINGKIT-1.jpg', 'sijari-tanahbumbu-16.-DIR-RINGKIT-1.jpg', '2019-04-09', 'dir-ringkit', 'Y', 4),
(69, 20, 'DIR Sari Gadung', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Sarigadung masuk kedalam kategori Daerah Irigasi Rawa (D.I.R). Luas area persawahan fungsional mencapai 80.06 Ha dari luasan desa 4964.6 Ha. Persawahan ini didukung dengan sumber air PDAM dan penunjang tadah hujan. Jenis pertanian yang dikembangkan yaitu padi-palawija. Di Sarigadung sudah di dukung dengan bangunan irigasi seperti pintu air, gorong-gorong, bangunan bagi, saluran intake/pembuang dan jembatan. Namun di beberapa titik aset mengalami keruskan dan butuh perbaikan.</p>', 'sijari-tanahbumbu-B18.DIR-SARI-GADUNG-1.jpg', 'sijari-tanahbumbu-B18.-SARIGADUNG-1.jpg', 'sijari-tanahbumbu-17.-DIR-SARIGADUNG-1.jpg', '2019-04-09', 'dir-sari-gadung', 'Y', 4),
(70, 19, 'DIR Sei Rukam', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Sungai Rukam terletak di Kecamatan Kusan Hulu dan memiliki luas fungsional 106.99 Ha dengan luas desa 1700.33 Ha. Persawahan di kelola oleh 154 petani yang tergabung dalam 6 kelompok tani. Sumber air utama di Sungai Rukam yaitu dari tadah hujan. Selain itu sumber air di tunjang oleh sungai yang dinaikkan dengan pompa air. Bangunan irigasi di Sungai Rukam sudah cukup mendukung pertanian, sehingga produktifitas petani meningkat.</p>', 'sijari-tanahbumbu-B19.DIR-SEI-RUKAM-1.jpg', 'sijari-tanahbumbu-B19.-SEI-RUKAM-1.jpg', 'sijari-tanahbumbu-18.-DIR-SUNGAI-RUKAM-1.jpg', '2019-04-09', 'dir-sei-rukam', 'Y', 4),
(71, 19, 'DIR Lasung', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Lasung termasuk ke dalam Daerah Irigasi Rawa. Luas areal persawahan fungsional mencapai 367.21 Ha yang di kelola oleh 120 petani. Pengairan di Lasung berasal dari tadah hujan. Sistem irigasi di Lasung sudah cukup memadai. Hanya perlu perbaikan pada tanggul untuk dilakukannya peninggian &plusmn;1.5 m. Hal ini perlu dilakukan karena persawahan di lasung akan terendam banjir ketika musim penghujan dan ditambanya aliran air pembuangan dari perkebunan sawit.</p>', 'sijari-tanahbumbu-B20.DIR-LASUNG-1.jpg', 'sijari-tanahbumbu-B20.TANGGUL-LASUNG-1.jpg', 'sijari-tanahbumbu-19.-DIR-TANGGUL-LASUNG-1.jpg', '2019-04-09', 'dir-lasung', 'Y', 4),
(72, 19, 'DIR Tapus', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Tapus terletak di Desa Tapus Kecamatan Kusan Hulu. Desa Tapus memiliki potensial pertanian seperti padi seluas 132.33 Ha dengan luasan desa mencapai 953.8 Ha. Kawasan pertanian padi ini di dukung oleh pengairan tadah hujan, dimana hujan menjadi sumber utama air. Di daerah irigasi Tapus sudah didukung dengan bangunan irigasi seperti pintu air, bangunan bagi dan saluran air untuk mengatur debit suplai dan buang air sehingga dapat mengontrol debit air yang masuk ke dalam areal persawahan.</p>', 'sijari-tanahbumbu-B21.DIR-TAPUS-1.jpg', '', 'sijari-tanahbumbu-20.-DIR-TAPUS-1.jpg', '2019-04-09', 'dir-tapus', 'Y', 4),
(73, 24, 'DIT Pandamaran', '', NULL, NULL, '<p>Daerah irigasiPandamarantermasukkedalamkategori Daerah IrigasiTambak yang terletak di KecamatanSatui dan memilikiluasdesa 1293.24 Ha. Luas fungsionaldaridaerahirigasitambakinimencapai 344 Ha.</p>', 'sijari-tanahbumbu-C1-.PANDAMARAN-1.jpg', 'sijari-tanahbumbu-C1.-PANDAMARAN-1.jpg', 'sijari-tanahbumbu-1.-DIT-PANDAMARAN-1.jpg', '2019-04-09', 'dit-pandamaran', 'Y', 5),
(74, 23, 'DIT Betung', '', NULL, NULL, '<p>Desa Betung masuk kedalam kategori Daerah Irigasi Tambak yang terletak di Kecamatan Kusan Hilir. Luas area tambak fungsional hanya 7.31 Ha dari luas desa 2196.8 Ha. Pada tahun 2010 akhirpertambakan di Betung sudah banyak yang tidak aktif dikarenakan pertambakan terserang penyakit yang menyebabkan produktivitas warga sangat berkurang. Untuk bangunan irigasi sudah cukup baik hanya perlu di perbaikan di beberapa titik</p>', 'sijari-tanahbumbu-C2.DIT-BETUNG-1.jpg', 'sijari-tanahbumbu-C2.BETUNG-1.jpg', 'sijari-tanahbumbu-2.-DIT-BETUNG-1.jpg', '2019-04-09', 'dit-betung', 'Y', 5),
(75, 21, 'DIT Bunati', '', NULL, NULL, '<p>Daerah irigasi Bunati merupakan Daerah Irigasi Tambak (D.I.T). Luas daerah pertambakkan di Bunati mencapai 10.8 Ha dengan luas desa 1262.27 Ha. Pertambakkan di Bunati sudah tidak berfungsi, dikarenakan masyarakat sudah tidak berminat dan banyak beralih profesi menjadi pekerja tambang yang menjajikan. Selain itu juga muara sungai sudah mati tertutupi oleh pohon bakau. Hanya ada 0,5 Ha saja tambak yang masih aktif dengan mengandalkan air hujan.</p>', 'sijari-tanahbumbu-C3.BUNATI-1.jpg', 'sijari-tanahbumbu-C3.-BUNATI--1.jpg', 'sijari-tanahbumbu-3.-DIT-BUNATI-1.jpg', '2019-04-09', 'dit-bunati', 'Y', 5),
(76, 22, 'DIT Kersik Putih', '', NULL, NULL, '<p>Daerah Irigasi Tambak (D.I.T) Kresik Putih memiliki luas fungsional 215.05 Ha yang dikelola sebanyak 80 petani tambak dan tergabung dalam 6 kelompok tani. Luas desa Kersik Putih mencapai 1127.53 Ha. Produk tambak yang dibudidayakan adalah ikan dan udang. Setiap petani tambak mengelola seluas 3 Ha. Bangunan irigasi di Kersik Putih masih berfungsi dengan baik, sehingga pengairan di daerah tersebut tidak mengalami kendala dan produktivitas petani tambak pun tergolong maksimal.</p>', 'sijari-tanahbumbu-C4.DIT-KERSIK-PUTIH-1.jpg', 'sijari-tanahbumbu-C4.KERSIK-PUTIHpdf-1.jpg', 'sijari-tanahbumbu-4.-DIT-KERSIK-PUTIH-1.jpg', '2019-04-09', 'dit-kersik-putih', 'Y', 5),
(77, 23, 'DIT Muara Pagatan', '', NULL, NULL, '<p>Daerah irigasi Muara Pagatan masuk kedalam Daerah Irigasi Tambak (D.I.T). Terletak di desa Muara Pagatan, Kecamatan Kusan Hilir. Luas desa Muara Pagatan Mencapai 550.9 Ha. Luas daerah tambak mencapai 71.45 Ha dan dekelola oleh 174 petani tambak. Sumber air utama berasal dari laut dan penunjang berasal dari Sungai Kusan. Di beberapa titik ada tanggul yang jebol, sehingga debit air yang masuk tidak dapat dikendalikan. Selain itu juga terdapat masalah meluapnya air ketika musim penghujan datang.</p>', 'sijari-tanahbumbu-C5.DIT-MUARA-PAGATAN-1.jpg', 'sijari-tanahbumbu-C5.-MUARA-PAGATAN-1.jpg', 'sijari-tanahbumbu-6.-DIT-MUARA-PAGATAN-1.jpg', '2019-04-09', 'dit-muara-pagatan', 'Y', 5),
(78, 23, 'DIT Muara Pagatan Tengah', '', NULL, NULL, '<p>Desa Muara Pagatan Tengah masuk ke dalam kategori Daerah Irigasi Tambak (D.I.T). Luas fungsional lahan tambak mencapai 73.57 Ha dengan luas desa 270.37 Ha. Budidaya yang dikembangkan berupa ikan dan udang. Di Muara Pagatan Tengah sudah didukung dengan jaringan irigasi yang cukup baik, sehingga pengairan untuk tambak tidak mengalami kendala. Sumber utama pengairan berasal dari laut dan sungai kusan. Kendala yang dihadapi adalah tanggul / pematang tambak yang kurang tinggi. Sehingga ketika hujan datang, air akan meluap yang menyebabkan gagal panen.</p>', 'sijari-tanahbumbu-C6.DIT-MUARA-PAGATAN-TENGAH-1.jpg', 'sijari-tanahbumbu-C6.-MUARA-PAGATAN-TENGAH-1.jpg', 'sijari-tanahbumbu-5.-DIT-MUARA-PAGATAN-TENGAH-1.jpg', '2019-04-09', 'dit-muara-pagatan-tengah', 'Y', 5),
(79, 26, 'DIT Sebambam Baru', '', NULL, NULL, '<p>Sebamban Baru merupakan Daerah Irigasi Tambak (D.I.T). Dari luas potensial sebanyak 65.56 Ha dengan luas desa 5681.8 Ha. Sumber air yang digunakan adalah pasang surut air laut. Dibeberapa titik saluran tambak tercermar oleh aktifitas pertambangan. Sehingga seluruh tambak yang sudah tercemar di beli oleh PT STU selaku pemilik pertambangan. Usul dari perangkat desa, untuk dibukakan lahan pertanian di lahan yang belum tercemar oleh pertambangan.</p>', 'sijari-tanahbumbu-C7.DIT-SEBAMBAM-BARU-1.jpg', 'sijari-tanahbumbu-C7.-SEBAMBAM-BARU-1.jpg', 'sijari-tanahbumbu-7.-DIT-SEBAMBAN-BARU-1.jpg', '2019-04-09', 'dit-sebambam-baru', 'Y', 5),
(80, 26, 'DIT Sebambam Lama', '', NULL, NULL, '<p>Daerah Irigasi Tambak Sebamban Lama masuk ke dalam Kecamatan Sei Loban. Luas fungsional tambak hanya 14.23 Ha dari luas desa 5681.8 Ha. Budidaya yang dikembangkan adalah udang. Sumber air utama yang digunakan untuk pengairan yaitu pasang surut air laut. Air di Sebamban Lama pun juga tercemar seperti di Sebamban Baru dikarenakan aktivitas pertambangan, hal tersebut juga mempengaruhi produktifitas dari petani tambak. Selain itu juga di Sebamban lama belum di dukung bangunan irigasi untuk mengatur suplai dan debit air.</p>', 'sijari-tanahbumbu-C8.-DIT-SEBAMBAM-LAMA-1.jpg', 'sijari-tanahbumbu-C8.-SEBAMBAM-LAMA-1.jpg', 'sijari-tanahbumbu-8.-DIT-SEBAMBAN-LAMA-1.jpg', '2019-04-09', 'dit-sebambam-lama', 'Y', 5),
(81, 22, 'DIT Segumbang', '', NULL, NULL, '<p>Daerah irigasi Segumbang terletak di Kecamatan Batulicin dengan luas desa 426.04 Ha. Luas fungsional area persawahan mencapai 172.37 Ha yang dikelola oleh 6 kelompok tani. Sumber air utama area persawahan ini adalah tadah hujan. Walaupun hanya menggunakan tadah hujan, namun persawahan di Desa Segumbang ini bisa mengalami 2 kali musim tanam setiap tahunnya. Dibeberapa titik saluran sekunder masih alami dan mengalami pendangkalan karena belum di cor, sehingga untuk kondisi irigasi di Desa Segumbang masih kurang maksimal.</p>', 'sijari-tanahbumbu-C9.DIT-SEGUMBANG-1.jpg', 'sijari-tanahbumbu-C9.-SEGUMBANG-1.jpg', 'sijari-tanahbumbu-9.-DIT-SEGUMBANG-1.jpg', '2019-04-09', 'dit-segumbang', 'Y', 5),
(83, 25, 'DIT Sei Dua', '', NULL, NULL, '<p>Daerah irigasi Sei Dua dikategorikan sebagai Daerah Irigasi Tambak dengan jenis tambak tawar dan tambak asin. Tambak tawar di Sei Dua mempunyai luas fungsional 10.53 Ha dari luas baku 88,5 Ha. Untuk tambak asin, memiliki luas potensial sebesar 560,1 Ha dengan luas fungsionalnya sebesar 60-70%. Tambak tawarnya memanfaatkan adanya bendungan sebagai sumber air, sedangkan tambak asin memanfaatkan pasang surut air laut.</p>', 'sijari-tanahbumbu-C10.DIT-SEI-DUA-1.jpg', 'sijari-tanahbumbu-C10.-SEI-DUA-11.jpg', 'sijari-tanahbumbu-10.-DIT-SEI-DUA-1.jpg', '2019-04-09', 'dit-sei-dua', 'Y', 5),
(84, 26, 'DIT Sei Dua Laut', '', NULL, NULL, '<p>Daerah irigasi Sei Dua dikategorikan sebagai Daerah Irigasi Tambak dengan luas fungsional 10.53 Ha dari luas desa 1752.82 Ha. Pertambakan Sei Dua Laut sudah tidak aktif lama dikarenakan lokasi petak lahannya terlalu dekat dengan laut sehingga setiap pasang air laut, tambak akan rusak dan ikan gagal untuk dipanen. Selain itu juga sudah tidak ada petani tambak lagi karena pekerjaan nelayan lebih menjanjikan.</p>', 'sijari-tanahbumbu-C11.DIT-SEI-DUA-LAUT-1.jpg', 'sijari-tanahbumbu-C11.-SEI-DUA-LAUT-1.jpg', 'sijari-tanahbumbu-11.-DIT-SEI-DUA-LAUT-1.jpg', '2019-04-09', 'dit-sei-dua-laut', 'Y', 5),
(85, 26, 'DIT Sei Loban', '', NULL, NULL, '<p>Desa Sungai Loban terletak di Kecamatan Sei Loban dan masuk dalam kategori Daerah Irigasi Tambak dengan luas desa 2900.18 Ha. Namun Pertambakan di daerah ini sudah tidak berfungsi sejak tahun 2000an dikarenakan penduduk sudah beralih profesi menjadi penambang. Di Sei Loban juga belum ada jaringan irigasi yang baik dan sebagian besar petak tambak sudah di tumbuhi bakau. Menurut perangkat desa setempat, di Sei Loban sangat berpotensi untuk dijadikan tambak karena letaknya yang dekat dengan laut dan lahan potensial yang luas.</p>', 'sijari-tanahbumbu-C12.DIT-SEI-LOBAN-1.jpg', 'sijari-tanahbumbu-C12.-SEI-LOBAN-1.jpg', 'sijari-tanahbumbu-12.-DIT-SEI-LOBAN-1.jpg', '2019-04-09', 'dit-sei-loban', 'Y', 5),
(86, 23, 'DIT Sepunggur', '', NULL, NULL, '<p>Daerah Irigasi Tambak (D.I.T) Sepunggur memiliki luas desa 3724.3 Ha dan merupakan daerah irigasi tambak terluas, mencapai 478.74 Ha dan dikelola sebanyak 160 petani yang tergabung dalam 8 kelompok petani tambak. Budidaya yang dikembangkan berupa tambak ikan dan udang. Sumber utama pengairan dari laut dan untuk penunjang menggunakan pasang surut air laut yang masuk ke sungai dan dialirkan ke petak-petak tambak. Proses pengairan pun tidak mengalami kendala yang berarti, sehingga produktivitas petani tidak terganggu.</p>', 'sijari-tanahbumbu-C13.DIT-SEPUNGGUR-1.jpg', 'sijari-tanahbumbu-C13.1.SEPUNGGUR-1.jpg', 'sijari-tanahbumbu-13.-DIT-SEPUNGGUR-1.jpg', '2019-04-09', 'dit-sepunggur', 'Y', 5),
(87, 23, 'DIT Serdangan', '', NULL, NULL, '<p>Daerah irigasi Sardangan juga termasuk kedalam Daerah Irigasi Tambak (D.I.T). Luas daerah tambak mencapai 17.7 Ha dari luas desa 4013.3 Ha. Tambak di Serdangan termasuk kedalam tambak tawar. Warga sekitar menyebutnya sebagai kolam jebakan. Tidak ada penebaran bibit, ikan di dapatkan pada saat air sungai meluap karna banjir. Setelah itu akan dibuat petak-petak untuk membudidayakan ikan. Panen ikan dilakukan pada saat musim kemarau, untuk mempermudah penangkapan.</p>', 'sijari-tanahbumbu-C14.DIT-SERDANGAN-1.jpg', 'sijari-tanahbumbu-C14.SERDANGAN-1.jpg', 'sijari-tanahbumbu-14.-DIT-SERDANGAN-1.jpg', '2019-04-09', 'dit-serdangan', 'Y', 5),
(88, 24, 'DIT Setarap', '', NULL, NULL, '<p>Daerah irigasi Setarap masuk kedalam Daerah Irigasi Tambak (D.I.T). DIT Satarap terletak di Desa Setarap, Kecamatan Satui. Luasan desa Setarap mencapai 2428.8 Ha dengan luas fungsional 11.99 Ha. Sumber utama pengairan yaitu dari laut langsung, dikarenakan lokasi tambak berdekatan dengan laut. Kendala yang dihadapi di pertambakan ini yaitu meluapnya air tambak dikarenakan pasang air laut. Air laut yang meluap ke sungai akan langsung masuk ke dalam petak-petak tambak. Pintu air yang ada pun dalam keadaan yang tidak baik dan perlu untuk diperbaiki.</p>', 'sijari-tanahbumbu-C15.DIT-SETARAP-1.jpg', 'sijari-tanahbumbu-C15.SETARAP-1.jpg', 'sijari-tanahbumbu-15.-DIT-SETARAP-1.jpg', '2019-04-09', 'dit-setarap', 'Y', 5),
(89, 17, 'DIR Suka Maju', '', NULL, NULL, '<p>contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar, contoh isi komentar.</p>', '', 'sijari-tanahbumbu-B13.SUKA-MAJU-1.jpg', NULL, '2019-01-03', 'dir-suka-maju', 'Y', 4),
(90, 15, 'DI Tegal Sari ', '', NULL, NULL, '<p>Daerah irigasi Tegal Sari terletak di Kecamatan Satui yang memiliki luas desa 704.7 Ha dengan luas fungsional persawahan mencapai 71.05 Ha. Sumber air utama yaitu sungai. Bangunan irigasi yang ada sudah cukup memadai, hanya perlu perbaikan di beberapa titik. Seperti peninggian tanggul dan perbaikan pintu air. Hal tersebut perlu diperhatikan karena ketika musim penghujan akan terjadi banjir dikarenakan debit air juga bertambah dari aliran air perkebunan sawit.</p>', 'sijari-tanahbumbu-SKEMA-DI-TEGAL-SARI-1.jpg', 'sijari-tanahbumbu-PETA-DI-TEGAL-SARI-1.jpg', 'sijari-tanahbumbu-21.-DI-TEGAL-SARI-1.jpg', '2019-04-08', 'di-tegal-sari-', 'Y', 3),
(91, 15, 'DI Makmur Mulya ', '', NULL, NULL, '<p>Makmur Mulya merupakan salah satu desa yang terletak di kecamatan Satui, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Makmur Mulya mencapai 1620.4 Ha. Daerah irigasi Makmur Mulya memiliki luas 12,09 Ha lahan potensial dari total 134,89 Ha lahan baku yang tersedia.</p>', 'sijari-tanahbumbu-A22.-DI-MAKMUR-MULYA-1.jpg', 'sijari-tanahbumbu-A22.-DI-MAKMUR-MULYA-KONTUR-1.jpg', 'sijari-tanahbumbu-22.-MAKMUR-MULIA-1.jpg', '2019-04-08', 'di-makmur-mulya-', 'Y', 3),
(92, 15, 'DI Sinar Bulan', '', NULL, NULL, '<p>Sinar Bulan merupakan salah satu desa yang terletak di kecamatan Satui, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Daerah irigasi Sinar Bulan memiliki luas 23,43 Ha lahan potensial dari total 69,76 Ha lahan baku yang tersedia. Di persawahan Sinar Bulan sudah di dukung dengan jaringan irigasi seperti pintu air, saluran sekunder, saluran tersier, saluran primer, dan pintu air. Kondisi pintu air bias dikatakan baik tetapi utnuk saluran banyak ditumbuhi rumput dan gulma, sedangkan pada saluran sekunder mengalami pendangkalan.</p>', 'sijari-tanahbumbu-DI-SINAR-BULAN-1.jpg', 'sijari-tanahbumbu-A23.-DI-SINAR-BULAN-1.jpg', 'sijari-tanahbumbu-23.-DI-SINAR-BULAN-1.jpg', '2019-04-08', 'di-sinar-bulan', 'Y', 3),
(93, 33, 'DIR Wonorejo', '', NULL, NULL, '<p style=\"text-align: justify;\">Wonorejo merupakan salah satu desa yang terletak di kecamatan Kusan Hulu, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan. Luas desa Wonorejo mencapai 1534.6 Ha. Daerah irigasi Wonorejo memiliki luas 131,06 Ha yang di garap dari total 221,76 Ha lahan baku yang tersedia.</p>', 'sijari-tanahbumbu-0001.jpg', 'sijari-tanahbumbu-0001-(2).jpg', 'sijari-tanahbumbu-22.-DIR-WONOEREJO-1.jpg', '2019-04-09', 'dir-wonorejo', 'Y', 4),
(94, 19, 'Tibarau Panjang', '', NULL, NULL, '<p style=\"text-align: justify;\">Daerah irigasi Tibarau Panjang terletak di Desa Tibarau Panjang, Kecamatan Kusan Hulu dan memiliki liuas desa 2096.4 Ha. Desa Tibarau Panjang memiliki potensial pertanian padi seluas 283.8 Ha, namun hanya di fungsionalkan seluas 166.97 Ha. Daerah Irigasi di Tibarau Panjang sudah di dukung dengan jaringan irigasi seperti bangunan air, saluran primer, saluran sekunder, saluran tersier dan tanggul. Keadaan bangunan air tidak berfungsi dikarenakan tidak adanya pintu air dan sudah jebol. Saluran-saluran disana pun sudah banyak di tumbuhi rerumputan.</p>', 'sijari-tanahbumbu-00011.jpg', 'sijari-tanahbumbu-0001-(2)1.jpg', 'sijari-tanahbumbu-21.-DIR-TIBARAU-PANJANG-1.jpg', '2019-04-09', 'tibarau-panjang', 'Y', 4);

-- --------------------------------------------------------

--
-- Table structure for table `portofolio_kategori`
--

CREATE TABLE `portofolio_kategori` (
  `id_portofolio_kategori` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL,
  `seo` text,
  `stat` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portofolio_kategori`
--

INSERT INTO `portofolio_kategori` (`id_portofolio_kategori`, `nama`, `image`, `seo`, `stat`) VALUES
(12, 'Kecamatan Angsana', '', 'kecamatan-angsana', 3),
(13, 'Kecamatan Batulicin', '', 'kecamatan-batulicin', 3),
(14, 'Kecamatan Satui Karangbintang', '', 'kecamatan-satui-karangbintang', 3),
(15, 'Kecamatan Satui', '', 'kecamatan-satui', 3),
(16, 'Kecamatan Sungai Hulu', '', 'kecamatan-sungai-hulu', 3),
(17, 'Kecamatan Batulicin', '', 'kecamatan-batulicin', 4),
(18, 'Kecamatan Kuranji', '', 'kecamatan-kuranji', 4),
(19, 'Kecamatan Kusan Hulu', '', 'kecamatan-kusan-hulu', 4),
(20, 'Kecamatan Simpangempat', '', 'kecamatan-simpangempat', 4),
(21, 'Kecamatan Angsana', '', 'kecamatan-angsana', 5),
(22, 'Kecamatan Batulicin', '', 'kecamatan-batulicin', 5),
(23, 'Kecamatan Kusan Hilir', '', 'kecamatan-kusan-hilir', 5),
(24, 'Kecamatan Satui', '', 'kecamatan-satui', 5),
(25, 'Kecamatan Simpangempat', '', 'kecamatan-simpangempat', 5),
(26, 'Kecamatan Sungai Loban', '', 'kecamatan-sungai-loban', 5),
(27, 'Kecamatan Kusan Hilir', '', 'kecamatan-kusan-hilir', 3),
(29, 'Kecamatan Mantewe', '', 'kecamatan-mantewe', 3),
(32, 'Kecamatan Sungai Loban', '', 'kecamatan-sungai-loban', 3),
(33, 'Kecamatan Satui', '', 'kecamatan-satui', 4);

-- --------------------------------------------------------

--
-- Table structure for table `portofolio_subkategori`
--

CREATE TABLE `portofolio_subkategori` (
  `id_portofolio_subkategori` int(11) NOT NULL,
  `id_portofolio_kategori` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL,
  `seo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id_posts` bigint(20) NOT NULL,
  `id_author` int(10) NOT NULL,
  `id_member` int(5) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `seo` varchar(100) NOT NULL,
  `harga` int(10) NOT NULL,
  `kondisi` enum('Baru','Bekas') NOT NULL,
  `kategori` enum('Mobil','Truck dan Mobil Niaga','Motor','Asesoris dan Sparepart','Velg dan Ban') NOT NULL,
  `deskripsi` text NOT NULL,
  `id_provinsi` int(5) NOT NULL,
  `id_kabupaten` int(5) NOT NULL,
  `id_merk` int(5) NOT NULL,
  `id_type` int(5) NOT NULL,
  `id_promo` varchar(250) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `hits` int(10) NOT NULL,
  `favorit` int(10) NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `id_produk_kategori` int(11) DEFAULT NULL,
  `judul` text NOT NULL,
  `nama` varchar(100) NOT NULL,
  `popular` enum('Y','N') DEFAULT NULL,
  `short_desc` text,
  `konten` text,
  `code` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `image` text NOT NULL,
  `date` date NOT NULL,
  `seo` text NOT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_produk_kategori`, `judul`, `nama`, `popular`, `short_desc`, `konten`, `code`, `price`, `image`, `date`, `seo`, `status`) VALUES
(10, 8, 'Panel Skyline Diffuser 50 x 50 cm', '', NULL, NULL, '', 'Skyline Diffuser', 850000, 'akustik-ruang-mystudio412panel-skyline-diffuser-50x50cmskyline-diffuser-50x50cm-low.jpg', '2018-11-27', 'panel-skyline-diffuser-50-x-50-cm', 'Y'),
(11, 8, 'Panel Linear Diffuser 120 x 60 cm', '', NULL, NULL, '', 'Linear Diffuser', 850000, 'akustik-ruang-mystudio389panel-linear-diffuser-120x60cmlinear-diffuser-60x120cm-low.jpg', '2018-11-27', 'panel-linear-diffuser-120-x-60-cm', 'Y'),
(12, 9, 'Lantai Parquet Solid Engineered', '', NULL, NULL, '', 'LP', 175000, 'akustik-ruang-mystudio331lantai-parquetIMG-20140808-01298.jpg', '2018-11-27', 'lantai-parquet-solid-engineered', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `produk_kategori`
--

CREATE TABLE `produk_kategori` (
  `id_produk_kategori` int(11) NOT NULL,
  `nama` text NOT NULL,
  `image` text NOT NULL,
  `seo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_kategori`
--

INSERT INTO `produk_kategori` (`id_produk_kategori`, `nama`, `image`, `seo`) VALUES
(8, 'Bahan Material Akustik', '', 'bahan-material-akustik'),
(9, 'Lantai Kayu', '', 'lantai-kayu');

-- --------------------------------------------------------

--
-- Table structure for table `reservasi_destination`
--

CREATE TABLE `reservasi_destination` (
  `id_reservasi_destination` int(11) NOT NULL,
  `id_tour` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `checkin` date NOT NULL,
  `checkout` date DEFAULT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `catatan` text,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi_destination`
--

INSERT INTO `reservasi_destination` (`id_reservasi_destination`, `id_tour`, `nama`, `checkin`, `checkout`, `telp`, `email`, `alamat`, `catatan`, `waktu`, `status`) VALUES
(8, 34, 'ivan', '2017-11-29', NULL, '123456', 'ivan@email.com', 'jogja', '', '2017-11-24 06:57:11', '1'),
(9, 32, 'ivan', '0000-00-00', NULL, '0274', 'ivan@email.com', 't', 't', '2017-12-06 08:29:23', '1'),
(10, 32, 'sdgs', '0000-00-00', NULL, '2', 'email@email.com', 'df', 'sdf', '2017-12-06 08:29:02', '1'),
(11, 34, 'sdf', '0000-00-00', NULL, '45', 'email@email.com', 'teetu', 'etueu', '2017-12-13 07:10:50', '1'),
(12, 32, 'ivan', '0000-00-00', NULL, '0274', 'email@email.com', 'htjyjtyjtyjtyj', 'jtyjsrthjfjfjjyjy', '2018-11-26 06:42:44', '1');

-- --------------------------------------------------------

--
-- Table structure for table `reservasi_special_interest`
--

CREATE TABLE `reservasi_special_interest` (
  `id_reservasi_special_interest` int(11) NOT NULL,
  `id_special_interest` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `checkin` date NOT NULL,
  `checkout` date DEFAULT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `catatan` text,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi_special_interest`
--

INSERT INTO `reservasi_special_interest` (`id_reservasi_special_interest`, `id_special_interest`, `nama`, `checkin`, `checkout`, `telp`, `email`, `alamat`, `catatan`, `waktu`, `status`) VALUES
(1, 12, 'Mastopp', '2017-08-15', NULL, '4336373', 'asd@gmail.com', 'dfhdfha', 'dfhadfh', '2017-08-15 09:17:43', '1');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id_review` int(11) NOT NULL,
  `id_special` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `comment` text NOT NULL,
  `category` enum('destination','special_interest') NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id_review`, `id_special`, `name`, `email`, `rating`, `comment`, `category`, `status`) VALUES
(1, 26, 'Mastopp', 'anu@gmail.com', 5, 'anu', 'destination', '0'),
(2, 13, 'Mastopp', 'anu@gmail.com', 5, 'rgsdg', 'destination', '0'),
(3, 13, 'Mastopp', 'anu@gmail.com', 3, 'sdrysdfg', 'destination', '0'),
(4, 13, 'Mastopp', 'anu@gmail.com', 1, 'efyhadfh', 'destination', '0'),
(5, 9, 'Mastopp', 'anu@gmail.com', 3, 'sdfhafh', 'destination', '1'),
(6, 9, 'Mastopp', 'anu@gmail.com', 5, 'sdgSDG', 'destination', '1'),
(7, 1, 'Mastopp', 'anu@gmail.com', 4, 'dfsdfhg', 'special_interest', '1'),
(8, 1, 'Mastopp', 'anu@gmail.com', 5, 'erua', 'special_interest', '1'),
(9, 19, 'Mastopp', 'anu@gmail.com', 5, 'dfjhADf', 'special_interest', '1'),
(10, 18, 'mastopp', 'anu@gmail.com', 5, 'asdfa', 'special_interest', '1');

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id_slide` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(150) NOT NULL,
  `deskripsi` varchar(350) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`id_slide`, `nama`, `gambar`, `deskripsi`) VALUES
(21, 'Slide 1', 'akustik-ruang-slide.jpg', ''),
(22, 'Slide 2', 'akustik-ruang-slide1.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `sosmed`
--

CREATE TABLE `sosmed` (
  `id_sosmed` int(3) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `link` varchar(150) NOT NULL,
  `gambar` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sosmed`
--

INSERT INTO `sosmed` (`id_sosmed`, `nama`, `link`, `gambar`) VALUES
(2, 'Facebook', 'https://www.facebook.com/', 'facebook-140-socmed-facebook.png'),
(3, 'Twitter', 'https://twitter.com/', 'twitter-308-socmed-twitter.png'),
(6, 'Instagram', 'https://instagram.com/', 'instagram-597-socmed-instagram.png');

-- --------------------------------------------------------

--
-- Table structure for table `special_interest`
--

CREATE TABLE `special_interest` (
  `id_special_interest` int(11) NOT NULL,
  `id_special_interest_kategori` int(11) NOT NULL,
  `id_hotel` text,
  `nama` varchar(255) NOT NULL,
  `length` varchar(100) DEFAULT NULL,
  `harga` varchar(255) NOT NULL,
  `deskripsi` text,
  `konten` text,
  `konten_harga` text,
  `image` text NOT NULL,
  `peta` text,
  `date` date NOT NULL,
  `seo` text NOT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `special_interest`
--

INSERT INTO `special_interest` (`id_special_interest`, `id_special_interest_kategori`, `id_hotel`, `nama`, `length`, `harga`, `deskripsi`, `konten`, `konten_harga`, `image`, `peta`, `date`, `seo`, `status`) VALUES
(1, 1, NULL, 'Yogyakarta Out Ketapang Harbour', '9 Day', '1000 K', NULL, '<p><strong>Day 01 / ARRIVAL &ndash; PRAMBANAN</strong></p>\r\n<p>Welcome to Jogjakarta, arrival meets and greet by our staff. Straight we start our tour visit</p>\r\n<p><strong>Prambanan temple</strong>.</p>\r\n<p>In the evening drive to Borobudur Temple area and check-in to Borobudur area hotel&nbsp;(1.5hr)</p>\r\n<p>optional : <strong>sunset visit to Borobudur Temple</strong></p>\r\n<p><span style=\"color: #3366ff;\">Hotel : Borobudur area hotel</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Day 02 / BOROBUDUR SUNRISE &ndash; YOGYAKARTA CITY TOUR</strong></p>\r\n<p>4:45am climb up Borobudur Temple to see sunrise. Back to hotel for breakfast and check</p>\r\n<p>out</p>\r\n<p>Visit to <strong>Kasultanan Yogyakarta, Taman Sari water castle, Pasty Bird market</strong></p>\r\n<p><span style=\"color: #3366ff;\">Hotel : Yogyakarta area hotel</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Day 03 / PRAMBANAN &ndash; MANGKUNEGARAN &ndash; JOMBANG</strong></p>\r\n<p>Breakfast at hotel and check out at 07am Heading to Solo (1.5h) visit to <strong>Mangkunegaran</strong></p>\r\n<p><strong>Solo Palace, Sukuh temple</strong> passing tea plantation.</p>\r\n<p>Drive to Jombang Sub-District (4h drives) for check in.</p>\r\n<p><span style=\"color: #3366ff;\">Hotel : Jombang area hotel</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Day 04 / TRANSFER TO BROMO</strong></p>\r\n<p>Breakfast at hotel and check out. Drive to Mt. Bromo (5h driving)</p>\r\n<p>Optional : visiting Madakaripura waterfall</p>\r\n<p>Free and easy (hotel activities)</p>\r\n<p><span style=\"color: #3366ff;\">Hotel : Mt. Bromo area hotel</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Day 05/ MT. BROMO SUNRISE</strong></p>\r\n<p>3:00am start with jeep tour to see <strong>sunrise Mt. Bromo</strong> from Penanjakan Hill. Visit to Bromo</p>\r\n<p>Crater by horse (personal account) Breakfast at hotel and check out</p>\r\n<p>Drive to Sempol for check in (drive 6h)</p>\r\n<p><span style=\"color: #3366ff;\">Hotel : Sempol area hotel</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Day 06 / IJEN SUNRISE &ndash; TRANSFER OUT KETAPANG HARBOR</strong></p>\r\n<p>Early morning call, start at 4am heading to Paltuding and start trekking <strong>to the top of Mt. Ijen</strong></p>\r\n<p><strong>for have sunrise</strong>.</p>\r\n<p>Drive to ferry at Ketapang Harbor. TOUR ENDS</p>', NULL, 'yogyakarta-out-ketapang-harbour-279-bali.jpg', NULL, '2017-08-04', 'yogyakarta-out-ketapang-harbour', 'Y'),
(2, 1, NULL, 'Jakarta to Yogyakarta', '5 Days', 'Rp 1000 K', NULL, '<p>Under Maintenance</p>', NULL, 'jakarta-to-yogyakarta-464-IMG-20141108-WA0001.jpg', NULL, '2017-08-04', 'jakarta-to-yogyakarta', 'Y'),
(3, 1, NULL, 'Yogyakarta to Ketapang Harbor', '6 Days', 'Rp 1000 K', NULL, '<p>Under Maintenance</p>', NULL, 'yogyakarta-to-ketapang-harbor-501-20140914_072122.jpg', NULL, '2017-08-04', 'yogyakarta-to-ketapang-harbor', 'Y'),
(4, 1, NULL, 'Yogyakarta to Pemuteran', '10 Days', 'Rp 1000 K', NULL, '<p>Under MAintenance</p>', NULL, 'yogyakarta-to-pemuteran-413-20140920_064448.jpg', NULL, '2017-08-04', 'yogyakarta-to-pemuteran', 'Y'),
(5, 2, NULL, 'Semarang Old City Tour', '-', 'Rp 1000 K', NULL, '<p>under maintenance</p>', NULL, 'semarang-old-city-tour-319-Lawang-Sewu.jpg', NULL, '2017-08-04', 'semarang-old-city-tour', 'Y'),
(6, 2, NULL, 'Borobudur Temple', '-', 'Rp 1000 K', NULL, '<p>under maintenenace</p>', NULL, 'borobudur-temple-297-IMG-20170322-WA0013.jpg', NULL, '2017-08-04', 'borobudur-temple', 'Y'),
(7, 2, NULL, 'Prambanan Temple', '-', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'prambanan-temple-693-Candi-Prambanan.jpg', NULL, '2017-08-04', 'prambanan-temple', 'Y'),
(8, 2, NULL, 'Gedong Songo Temple and Ambarawa Train Museum', '-', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'gedong-songo-temple-and-ambarawa-train-museum-51-IMG-20170322-WA0013.jpg', NULL, '2017-08-04', 'gedong-songo-temple-and-ambarawa-train-museum', 'Y'),
(9, 2, NULL, 'Mt. Bromo Tour', '-', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'mt-bromo-tour-938-20140920_064448.jpg', NULL, '2017-08-04', 'mt-bromo-tour', 'Y'),
(10, 3, NULL, 'Mt. Merapi Trekking', '3 Days', 'Rp 100 K', NULL, '<p>Under maintenance</p>', NULL, 'mt-merapi-trekking-444-20140914_052213_LLS.jpg', NULL, '2017-08-04', 'mt-merapi-trekking', 'Y'),
(11, 3, NULL, 'Blue Fire at Mt. Ijen & Sunrise at Mt. Bromo', '4 Days', 'Rp 100 K', NULL, '<p>Under Maintenance</p>', NULL, 'blue-fire-at-mt-ijen--sunrise-at-mt-bromo-569-20140914_053548.jpg', NULL, '2017-08-04', 'blue-fire-at-mt-ijen--sunrise-at-mt-bromo', 'Y'),
(12, 4, NULL, 'Yogyakarta City Tour', '4 Days', 'Rp 100 K', NULL, '<p>Under maintenance</p>', NULL, 'yogyakarta-city-tour-519-keraton.jpg', NULL, '2017-08-04', 'yogyakarta-city-tour', 'Y'),
(13, 5, NULL, 'Lava Tour at Mt. Merapi', '-', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'lava-tour-at-mt-merapi-585-20141005_051958.jpg', NULL, '2017-08-04', 'lava-tour-at-mt-merapi', 'Y'),
(14, 5, NULL, 'Tea Plantation tour with jeep 4wd', '-', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'tea-plantation-tour-with-jeep-4wd-891-20140920_064448.jpg', NULL, '2017-08-04', 'tea-plantation-tour-with-jeep-4wd', 'Y'),
(15, 5, NULL, 'Rappeling Jomblang cave', '2 Days', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'rappeling-jomblang-cave-223-20141005_050905.jpg', NULL, '2017-08-04', 'rappeling-jomblang-cave', 'Y'),
(16, 5, NULL, 'White rafting Elo river', '2 Days', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'white-rafting-elo-river-674-20140920_054235.jpg', NULL, '2017-08-04', 'white-rafting-elo-river', 'Y'),
(17, 5, NULL, 'Cycling Through Borobudur Temple and Prambanan Temple', '3 Days', 'Rp 100 K', NULL, '<p>Under maintenance</p>', NULL, 'cycling-through-borobudur-temple-and-prambanan-temple-268-Candi-Prambanan.jpg', NULL, '2017-08-04', 'cycling-through-borobudur-temple-and-prambanan-temple', 'Y'),
(18, 7, '4,3,', 'Borobudur Yoga Escape', '3 Days', 'Rp 100 K', '<p>Healty Yoga at Borobudur.</p>', '<p>under maintenance</p>', NULL, 'borobudur-yoga-escape-338-IMG-20170322-WA0013.jpg', NULL, '2017-09-08', 'borobudur-yoga-escape', 'Y'),
(19, 7, NULL, 'Karimun Jawa Diving', '5 Days', 'Rp 100 K', NULL, '<p>under maintenance</p>', NULL, 'karimun-jawa-diving-640-20141025_145207.jpg', NULL, '2017-08-04', 'karimun-jawa-diving', 'Y'),
(20, 6, '7,6,5,4,3,', 'WALI SONGO ', '5 Days', 'Rp 100 K', '', '<p>under maintenance</p>', '', 'wali-songo--331-20140914_072122.jpg', NULL, '2017-09-26', 'wali-songo-', 'Y'),
(21, 7, '7,', 'Anu', '5 Day', 'USD  280', '', '', '', 'anu-916-atmaja-tour-545-pantai-depok-depok.jpg', 'anu-203-botswana-map.jpg', '2017-09-27', 'anu', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `special_interest_kategori`
--

CREATE TABLE `special_interest_kategori` (
  `id_special_interest_kategori` int(11) NOT NULL,
  `nama` text NOT NULL,
  `seo` text NOT NULL,
  `gambar` text NOT NULL,
  `deskripsi` text,
  `konten` text,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `special_interest_kategori`
--

INSERT INTO `special_interest_kategori` (`id_special_interest_kategori`, `nama`, `seo`, `gambar`, `deskripsi`, `konten`, `status`) VALUES
(1, 'OVERLAND TOURS', 'overland-tours', 'java-overland-75-20140914_112701.jpg', 'Special interest for you', NULL, 'Y'),
(2, 'CRUISE SHIP', 'cruise-ship', 'cruise-ship-490-IMG-20141108-WA0001.jpg', 'Cruise special interest', NULL, 'Y'),
(3, 'HIKING & WALKS', 'hiking--walks', 'hiking--walks-199-20141005_051845.jpg', 'Hiking & Walks special interest', NULL, 'Y'),
(4, 'CULTURE, NATURE, HERITAGE', 'culture-nature-heritage', 'culture-nature-heritage-61-IMG-20170322-WA0007.jpg', 'special interest for you', NULL, 'Y'),
(5, 'ADVENTURE & NATURE', 'adventure--nature', 'adventure--nature-412-20170305_080932.jpg', 'special interest for you', NULL, 'Y'),
(6, 'MOSLEM TOUR', 'moslem-tour', 'wali-songo-moslem-tour-906-20141005_051514.jpg', 'special interest moslem tour ', NULL, 'Y'),
(7, 'ESCAPE', 'escape', 'escape-932-20140914_112701.jpg', 'Special Interest ', '<p>under maintenance</p>', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `statistik`
--

CREATE TABLE `statistik` (
  `ip` varchar(20) NOT NULL DEFAULT '',
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL DEFAULT '1',
  `online` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistik`
--

INSERT INTO `statistik` (`ip`, `tanggal`, `hits`, `online`) VALUES
('::1', '2017-12-27', 45, '1514363330'),
('::1', '2018-02-08', 4, '1518071591'),
('::1', '2018-02-12', 13, '1518426757'),
('::1', '2018-05-18', 10, '1526631266'),
('::1', '2018-11-19', 1, '1542609931'),
('::1', '2018-11-21', 107, '1542791427'),
('::1', '2018-11-22', 187, '1542879814'),
('::1', '2018-11-23', 144, '1542965275'),
('::1', '2018-11-24', 125, '1543047677'),
('::1', '2018-11-26', 135, '1543223711'),
('::1', '2018-11-27', 72, '1543311256'),
('::1', '2018-11-28', 95, '1543395323'),
('::1', '2018-11-29', 76, '1543483464'),
('::1', '2018-11-30', 37, '1543551635'),
('::1', '2018-12-01', 44, '1543650760'),
('::1', '2018-12-03', 27, '1543812874'),
('::1', '2018-12-04', 5, '1543905975'),
('::1', '2018-12-05', 3, '1543977522'),
('::1', '2018-12-06', 61, '1544081944'),
('::1', '2018-12-10', 6, '1544423175'),
('::1', '2018-12-11', 1, '1544494151'),
('::1', '2018-12-13', 13, '1544689000'),
('::1', '2018-12-19', 14, '1545212869'),
('::1', '2018-12-20', 10, '1545279674'),
('::1', '2018-12-21', 28, '1545383720'),
('::1', '2018-12-26', 4, '1545814286'),
('::1', '2018-12-28', 4, '1545968055'),
('::1', '2018-12-29', 66, '1546071614'),
('::1', '2018-12-31', 157, '1546249461'),
('::1', '2019-01-02', 103, '1546421568'),
('::1', '2019-01-03', 63, '1546497443'),
('::1', '2019-01-23', 53, '1548233187'),
('::1', '2019-01-24', 88, '1548322610'),
('::1', '2019-01-26', 40, '1548490404'),
('::1', '2019-02-06', 10, '1549440983'),
('::1', '2019-02-07', 84, '1549531608'),
('::1', '2019-02-09', 17, '1549697368'),
('::1', '2019-02-11', 5, '1549874889'),
('::1', '2019-02-14', 95, '1550135556'),
('::1', '2019-02-15', 22, '1550221353'),
('::1', '2019-02-16', 67, '1550288639'),
('36.81.82.176', '2019-02-16', 12, '1550305249'),
('66.220.149.26', '2019-02-16', 1, '1550297390'),
('173.252.127.11', '2019-02-16', 1, '1550297404'),
('66.220.149.3', '2019-02-16', 2, '1550305334'),
('66.220.149.16', '2019-02-16', 1, '1550297415'),
('66.220.149.31', '2019-02-16', 1, '1550297419'),
('66.220.149.2', '2019-02-16', 2, '1550305246'),
('66.220.149.22', '2019-02-16', 1, '1550305237'),
('66.220.149.8', '2019-02-16', 1, '1550305268'),
('66.220.149.35', '2019-02-16', 1, '1550305275'),
('66.220.149.13', '2019-02-16', 1, '1550305287'),
('66.220.149.27', '2019-02-16', 1, '1550305303'),
('36.81.82.176', '2019-02-18', 2, '1550456351'),
('182.1.170.166', '2019-02-18', 1, '1550481305'),
('36.81.82.176', '2019-02-19', 12, '1550565137'),
('66.220.149.25', '2019-02-19', 1, '1550562528'),
('66.220.149.1', '2019-02-19', 1, '1550562533'),
('66.220.149.36', '2019-02-19', 1, '1550562535'),
('66.220.149.35', '2019-02-19', 2, '1550562547'),
('66.220.149.19', '2019-02-19', 1, '1550562580'),
('64.233.173.35', '2019-02-19', 1, '1550589661'),
('64.233.173.32', '2019-02-19', 2, '1550589699'),
('66.220.149.11', '2019-02-19', 1, '1550589712'),
('36.80.241.81', '2019-02-19', 3, '1550593843'),
('182.1.161.58', '2019-02-20', 1, '1550639008'),
('64.233.173.45', '2019-02-20', 1, '1550658399'),
('36.68.53.71', '2019-02-22', 1, '1550834817'),
('105.108.183.57', '2019-02-23', 1, '1550945600'),
('104.37.31.240', '2019-02-23', 1, '1550964033'),
('36.72.212.41', '2019-02-25', 2, '1551082392'),
('185.106.213.196', '2019-02-25', 3, '1551133952'),
('38.79.214.119', '2019-03-02', 1, '1551566966'),
('36.73.91.34', '2019-03-03', 3, '1551613987'),
('66.220.149.14', '2019-03-03', 1, '1551613960'),
('66.220.149.34', '2019-03-03', 1, '1551613978'),
('36.78.42.22', '2019-03-04', 7, '1551705582'),
('66.220.149.34', '2019-03-04', 1, '1551705558'),
('13.231.186.100', '2019-03-04', 1, '1551707110'),
('66.102.6.88', '2019-03-05', 1, '1551787467'),
('36.78.42.22', '2019-03-06', 4, '1551846872'),
('66.220.149.32', '2019-03-06', 1, '1551844736'),
('66.220.149.19', '2019-03-06', 1, '1551846691'),
('202.67.33.37', '2019-03-08', 3, '1552071467'),
('64.233.173.43', '2019-03-09', 1, '1552117335'),
('114.4.215.138', '2019-03-09', 3, '1552134383'),
('66.102.6.151', '2019-03-11', 1, '1552314574'),
('66.102.6.153', '2019-03-12', 1, '1552401242'),
('125.163.246.128', '2019-03-13', 1, '1552443543'),
('36.81.53.101', '2019-03-14', 1, '1552538501'),
('66.102.6.87', '2019-03-14', 1, '1552567018'),
('54.36.150.186', '2019-03-15', 1, '1552636654'),
('114.5.213.111', '2019-03-17', 1, '1552839027'),
('128.199.165.82', '2019-03-20', 3, '1553051126'),
('::1', '2019-03-20', 1, '1553053518'),
('::1', '2019-03-27', 5, '1553660654'),
('::1', '2019-04-05', 1, '1554433610'),
('::1', '2019-04-08', 20, '1554716388'),
('::1', '2019-04-09', 119, '1554800536'),
('::1', '2019-04-10', 91, '1554886264'),
('::1', '2019-04-11', 57, '1554971858'),
('::1', '2019-04-12', 78, '1555062978'),
('::1', '2019-04-13', 22, '1555141743');

-- --------------------------------------------------------

--
-- Table structure for table `subimg_destination`
--

CREATE TABLE `subimg_destination` (
  `id_sub` int(11) NOT NULL,
  `id_tour` int(11) NOT NULL,
  `judul` text,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subimg_destination`
--

INSERT INTO `subimg_destination` (`id_sub`, `id_tour`, `judul`, `image`) VALUES
(10, 8, NULL, '8-634-bromo.jpg'),
(14, 7, NULL, '7-327-bromo.jpg'),
(15, 7, NULL, '7-268-Tangkuban-perahu.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `subimg_special_interest`
--

CREATE TABLE `subimg_special_interest` (
  `id_sub` int(11) NOT NULL,
  `id_special_interest` int(11) NOT NULL,
  `judul` text,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subimg_special_interest`
--

INSERT INTO `subimg_special_interest` (`id_sub`, `id_special_interest`, `judul`, `image`) VALUES
(1, 18, NULL, '18-761-IMG-20170322-WA0015.jpg'),
(3, 18, NULL, '18-358-IMG-20170322-WA0010.jpg'),
(4, 18, NULL, '18-358-Jakarta.jpg'),
(5, 18, NULL, '18-739-keraton.jpg'),
(6, 18, NULL, '18-140-IMG-20170322-WA0007.jpg'),
(7, 18, NULL, '18-211-Candi-Prambanan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tailor_make`
--

CREATE TABLE `tailor_make` (
  `id_tailor_make` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `checkin` date NOT NULL,
  `contact1` varchar(15) NOT NULL,
  `contact2` varchar(15) DEFAULT NULL,
  `email` varchar(25) NOT NULL,
  `destination` varchar(225) NOT NULL,
  `duration` varchar(150) NOT NULL,
  `adults` varchar(10) DEFAULT NULL,
  `children` varchar(10) DEFAULT NULL,
  `catatan` text,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tailor_make`
--

INSERT INTO `tailor_make` (`id_tailor_make`, `nama`, `checkin`, `contact1`, `contact2`, `email`, `destination`, `duration`, `adults`, `children`, `catatan`, `waktu`, `status`) VALUES
(2, 'Mastopp', '2017-09-22', '890746734', '88483468', 'mastopp@anu.com', 'Yogyakarta', '4 Day', '5', '1', 'nothing', '2017-09-22 07:57:46', '1'),
(3, 'anu', '2017-09-22', '890746734', '88483468', 'mastopp@aau.com', 'Yogyakarta', '4 Day', '5', '1', 'afcasf', '2017-11-17 06:59:31', '1'),
(4, 'Mastopp', '2017-09-23', '890746734', '', 'dfsfs@asaf.com', 'Yogyakarta', '4 Day', '5', '1', '', '2017-09-27 02:30:28', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE `tb_bank` (
  `id_bank` int(5) NOT NULL,
  `nomor_rekening` char(20) DEFAULT NULL,
  `bank` varchar(20) DEFAULT NULL,
  `pemilik` varchar(30) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bank`
--

INSERT INTO `tb_bank` (`id_bank`, `nomor_rekening`, `bank`, `pemilik`, `tgl`) VALUES
(3, '12323', 'MANDIRI', 'Santoso Dwi Nugroho', '2016-06-23 04:27:12'),
(4, '123456544', 'BNI', 'Toyar', '2016-06-23 05:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `seo` varchar(100) NOT NULL,
  `deskripsi` text,
  `gambar` varchar(150) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `status` enum('Y','N') DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama`, `seo`, `deskripsi`, `gambar`, `datetime`, `status`) VALUES
(1, 'Desain Interior', ' desain-interior', 'PT. Datum Global Mandiri dalam jasa pembuatan design interior rumah, kantor, apartement dll', 'design-interior-69-interior.jpg', '2016-07-19 09:11:42', 'Y'),
(2, 'Desain Pendidikan', ' desain-pendidikan', 'PT. Datum Global Mandiri dalam jasa pembuatan design pendidikan', 'design-pendidikan-563-pendidikan.jpg', '2016-07-19 09:11:49', 'Y'),
(3, 'Desain Arsitektur', ' desain-arsitektur', 'PT. Datum Global Mandiri dalam jasa pembuatan design arsitektur ', 'design-arsitektur-259-arsitektur.jpg', '2016-07-19 09:11:53', 'Y'),
(4, 'Desain Rumah Sakit', ' desain-rumah-sakit', 'PT. Datum Global Mandiri dalam jasa pembuatan design rumah sakit ', 'design-rumah-sakit-183-rumahsakit.jpg', '2016-07-19 09:11:58', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tb_portofolio`
--

CREATE TABLE `tb_portofolio` (
  `id_portofolio` int(11) NOT NULL,
  `judul` varchar(250) NOT NULL,
  `seo` varchar(250) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_portofolio`
--

INSERT INTO `tb_portofolio` (`id_portofolio`, `judul`, `seo`, `status`, `dateTime`) VALUES
(143, 'contoh album foto 1', 'contoh-album-foto-1', '1', '2017-11-20 07:57:04'),
(144, 'contoh album foto 2', 'contoh-album-foto-2', '1', '2017-11-20 08:00:19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_testimoni`
--

CREATE TABLE `tb_testimoni` (
  `id_testimoni` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `website` varchar(25) NOT NULL,
  `image` varchar(150) NOT NULL,
  `comment` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_testimoni`
--

INSERT INTO `tb_testimoni` (`id_testimoni`, `nama`, `company`, `website`, `image`, `comment`, `status`, `dateTime`) VALUES
(17, 'Ibu Ami', 'Studio Musik Ibu Ami Mojokerto Jawa Timur', '', 'akustik-ruang-628371Screenshot_20181029-112603.png', '<p>Studio musik Bu Ami Mojokerto Jawa Timur kedatangan artis ibukota Denada :D</p>', '0', '2018-11-28 07:44:56'),
(18, 'Ibu Lidya', 'Studio Musik Ibu Lidya Jakarta', '', 'akustik-ruang-5188Screenshot_2017-10-07-16-27-56.png', '<p>Hasilnya bagus dan memuaskan :)</p>', '0', '2018-11-28 08:14:08'),
(19, 'Bapak Sandy', 'Green Lake Tangerang', '', 'akustik-ruang-854478Screenshot_2016-10-24-13-36-42.jpg', '<p>Semoga next year dapat bekerja sama kembali. :)</p>', '0', '2018-11-28 08:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `tour`
--

CREATE TABLE `tour` (
  `id_tour` int(11) NOT NULL,
  `id_tour_kategori` int(11) NOT NULL,
  `id_hotel` tinytext,
  `nama` varchar(255) NOT NULL,
  `length` varchar(100) DEFAULT NULL,
  `harga` varchar(255) NOT NULL,
  `deskripsi` text,
  `konten_harga` text,
  `konten` text,
  `image` text NOT NULL,
  `peta` text,
  `date` date NOT NULL,
  `seo` text NOT NULL,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour`
--

INSERT INTO `tour` (`id_tour`, `id_tour_kategori`, `id_hotel`, `nama`, `length`, `harga`, `deskripsi`, `konten_harga`, `konten`, `image`, `peta`, `date`, `seo`, `status`) VALUES
(29, 0, '23,22,21,', 'Paket 1', '12 jam', 'Pukul 08.00 WIB / Kondisional', '<p>Private car, Air Conditioner, Driver + BBM, Air Mineral (Aqua), Charge HP, Sewa kamera*</p>', NULL, NULL, 'paket-1-254-paket1.jpg', NULL, '2017-11-21', 'paket-1', 'Y'),
(30, 0, '26,25,24,', 'Paket 2', '12 jam', 'Pukul 08.00 WIB / Kondisional', '<p>Private car, Air Conditioner, Driver + BBM, Air Mineral (Aqua), Charge HP, Sewa kamera*</p>', NULL, NULL, 'paket-2-794-paket2.jpg', NULL, '2017-11-21', 'paket-2', 'Y'),
(31, 0, '30,29,28,27,', 'Paket 3', '12 jam', 'Pukul 08.00 WIB / Kondisional', '<p>Private car, Air Conditioner, Driver + BBM, Air Mineral (Aqua), Charge HP, Sewa kamera*</p>', NULL, NULL, 'paket-3-276-paket3.jpg', NULL, '2017-11-21', 'paket-3', 'Y'),
(32, 0, '33,32,31,', 'Paket 4', '12 jam', 'Pukul 08.00 WIB / Kondisional', '<p>Private car, Air Conditioner, Driver + BBM, Air Mineral (Aqua), Charge HP, Sewa kamera*</p>', NULL, NULL, 'paket-4-244-paket4.jpg', NULL, '2017-11-21', 'paket-4', 'Y'),
(34, 0, '35,34,', 'Paket 5', '-', '-', '<p>Private car, Air Conditioner, Driver + BBM, Air Mineral (Aqua), Charge HP, Sewa kamera*</p>', NULL, NULL, 'paket-5-78-paket5.jpg', NULL, '2017-11-22', 'paket-5', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tour_kategori`
--

CREATE TABLE `tour_kategori` (
  `id_tour_kategori` int(11) NOT NULL,
  `nama` text NOT NULL,
  `seo` text NOT NULL,
  `gambar` text NOT NULL,
  `deskripsi` text,
  `konten` text,
  `status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour_kategori`
--

INSERT INTO `tour_kategori` (`id_tour_kategori`, `nama`, `seo`, `gambar`, `deskripsi`, `konten`, `status`) VALUES
(19, 'Kalimantan Island', 'kalimantan-island', 'kalimantan-island-514-20141025_145207.jpg', '<p>Trip to Kalimantan</p>', NULL, 'Y'),
(20, 'Sumatra Island', 'sumatra-island', 'surabaya-602-bromo.jpg', 'Trip to Sumatra', NULL, 'Y'),
(21, 'Sulawesi Island', 'sulawesi-island', 'bandung-680-Tangkuban-perahu.jpg', 'Trip to Sulawesi', NULL, 'Y'),
(22, 'Karimun Jawa Island', 'karimun-jawa-island', 'karimun-jawa-island-308-20141025_143216.jpg', '<p>Trip to Karimun Jawa</p>', NULL, 'Y'),
(23, 'Lombok Island', 'lombok-island', 'lombok-island-475-20141025_145254.jpg', 'Trip to Lombok ', NULL, 'Y'),
(24, 'Bali Island', 'bali-island', 'bali-island-959-bali.jpg', 'Trip to Bali', NULL, 'Y'),
(25, 'Java Island', 'java-island', 'java-island-744-Candi-Prambanan.jpg', 'Trip to Java', NULL, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` int(2) NOT NULL,
  `foto` varchar(150) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `level` varchar(20) NOT NULL,
  `blokir` enum('Y','N') NOT NULL,
  `id_session` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `foto`, `username`, `password`, `nama_lengkap`, `email`, `no_telp`, `level`, `blokir`, `id_session`) VALUES
(2, 'admin-161-20161014_58006bf6e7079.png', 'admin', 'OXb+tIyblnqPY1hQCyd6akP3xxqAHBRwmxWaBEuOaHM=', 'Admin SIJARI', 'no-reply@sijari.com', '0812-3456-789', 'admin', 'N', '71mcdpt1roi6ji90m1sdsfa2n5');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id_video` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `seo` varchar(100) DEFAULT NULL,
  `video` text,
  `tgl_posting` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id_video`, `judul`, `seo`, `video`, `tgl_posting`) VALUES
(38, 'UGM', 'ugm', 'https://www.youtube.com/watch?v=A9JNF1bHhFE', '2017-06-20 09:39:24'),
(39, 'UGM', 'ugm', 'https://www.youtube.com/watch?v=JXpGz382gGM', '2017-06-20 09:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `workshop`
--

CREATE TABLE `workshop` (
  `id_workshop` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `seo` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `image` varchar(150) NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL,
  `autor` varchar(100) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workshop`
--

INSERT INTO `workshop` (`id_workshop`, `title`, `seo`, `deskripsi`, `image`, `dateTime`, `status`, `autor`, `subject`) VALUES
(10, 'Sample Workshop test Deskripsi', 'sample-workshop-test-deskripsi', '<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'sample-agenda-test-deskripsi-214-sample.jpg', '2017-06-22 02:29:34', '1', 'Admin', 'Testing'),
(9, 'Sample Workshop tgl 22 juni', 'sample-workshop-tgl-22-juni', '<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'sample-agenda-722-image-not-found-800x416.png', '2017-06-22 02:30:02', '1', 'Admin', 'Pelaksanaan test ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `blog_detail`
--
ALTER TABLE `blog_detail`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `blog_kategori`
--
ALTER TABLE `blog_kategori`
  ADD PRIMARY KEY (`id_blog_kategori`);

--
-- Indexes for table `car_rent`
--
ALTER TABLE `car_rent`
  ADD PRIMARY KEY (`id_car_rent`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri_foto`
--
ALTER TABLE `galeri_foto`
  ADD PRIMARY KEY (`id_galeri_foto`);

--
-- Indexes for table `galeri_foto_porto`
--
ALTER TABLE `galeri_foto_porto`
  ADD PRIMARY KEY (`id_galeri_foto_porto`);

--
-- Indexes for table `galeri_video`
--
ALTER TABLE `galeri_video`
  ADD PRIMARY KEY (`id_galeri_video`);

--
-- Indexes for table `gallery_port`
--
ALTER TABLE `gallery_port`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indexes for table `img_header`
--
ALTER TABLE `img_header`
  ADD PRIMARY KEY (`id_img`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id_kontak`);

--
-- Indexes for table `layers`
--
ALTER TABLE `layers`
  ADD PRIMARY KEY (`layer_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id_messages`);

--
-- Indexes for table `messages_konsul`
--
ALTER TABLE `messages_konsul`
  ADD PRIMARY KEY (`id_messages_konsul`);

--
-- Indexes for table `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`id_modul`);

--
-- Indexes for table `portofolio`
--
ALTER TABLE `portofolio`
  ADD PRIMARY KEY (`id_portofolio`);

--
-- Indexes for table `portofolio_kategori`
--
ALTER TABLE `portofolio_kategori`
  ADD PRIMARY KEY (`id_portofolio_kategori`);

--
-- Indexes for table `portofolio_subkategori`
--
ALTER TABLE `portofolio_subkategori`
  ADD PRIMARY KEY (`id_portofolio_subkategori`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id_posts`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `produk_kategori`
--
ALTER TABLE `produk_kategori`
  ADD PRIMARY KEY (`id_produk_kategori`);

--
-- Indexes for table `reservasi_destination`
--
ALTER TABLE `reservasi_destination`
  ADD PRIMARY KEY (`id_reservasi_destination`);

--
-- Indexes for table `reservasi_special_interest`
--
ALTER TABLE `reservasi_special_interest`
  ADD PRIMARY KEY (`id_reservasi_special_interest`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `sosmed`
--
ALTER TABLE `sosmed`
  ADD PRIMARY KEY (`id_sosmed`);

--
-- Indexes for table `special_interest`
--
ALTER TABLE `special_interest`
  ADD PRIMARY KEY (`id_special_interest`);

--
-- Indexes for table `special_interest_kategori`
--
ALTER TABLE `special_interest_kategori`
  ADD PRIMARY KEY (`id_special_interest_kategori`);

--
-- Indexes for table `subimg_destination`
--
ALTER TABLE `subimg_destination`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `subimg_special_interest`
--
ALTER TABLE `subimg_special_interest`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `tailor_make`
--
ALTER TABLE `tailor_make`
  ADD PRIMARY KEY (`id_tailor_make`);

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_portofolio`
--
ALTER TABLE `tb_portofolio`
  ADD PRIMARY KEY (`id_portofolio`);

--
-- Indexes for table `tb_testimoni`
--
ALTER TABLE `tb_testimoni`
  ADD PRIMARY KEY (`id_testimoni`);

--
-- Indexes for table `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id_tour`);

--
-- Indexes for table `tour_kategori`
--
ALTER TABLE `tour_kategori`
  ADD PRIMARY KEY (`id_tour_kategori`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id_video`);

--
-- Indexes for table `workshop`
--
ALTER TABLE `workshop`
  ADD PRIMARY KEY (`id_workshop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `blog_detail`
--
ALTER TABLE `blog_detail`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `blog_kategori`
--
ALTER TABLE `blog_kategori`
  MODIFY `id_blog_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `car_rent`
--
ALTER TABLE `car_rent`
  MODIFY `id_car_rent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `galeri_foto`
--
ALTER TABLE `galeri_foto`
  MODIFY `id_galeri_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `galeri_foto_porto`
--
ALTER TABLE `galeri_foto_porto`
  MODIFY `id_galeri_foto_porto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `galeri_video`
--
ALTER TABLE `galeri_video`
  MODIFY `id_galeri_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `gallery_port`
--
ALTER TABLE `gallery_port`
  MODIFY `id_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=538;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id_hotel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `img_header`
--
ALTER TABLE `img_header`
  MODIFY `id_img` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `layers`
--
ALTER TABLE `layers`
  MODIFY `layer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id_messages` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `messages_konsul`
--
ALTER TABLE `messages_konsul`
  MODIFY `id_messages_konsul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `modul`
--
ALTER TABLE `modul`
  MODIFY `id_modul` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `portofolio`
--
ALTER TABLE `portofolio`
  MODIFY `id_portofolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `portofolio_kategori`
--
ALTER TABLE `portofolio_kategori`
  MODIFY `id_portofolio_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `portofolio_subkategori`
--
ALTER TABLE `portofolio_subkategori`
  MODIFY `id_portofolio_subkategori` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id_posts` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `produk_kategori`
--
ALTER TABLE `produk_kategori`
  MODIFY `id_produk_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `reservasi_destination`
--
ALTER TABLE `reservasi_destination`
  MODIFY `id_reservasi_destination` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reservasi_special_interest`
--
ALTER TABLE `reservasi_special_interest`
  MODIFY `id_reservasi_special_interest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id_review` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id_slide` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `sosmed`
--
ALTER TABLE `sosmed`
  MODIFY `id_sosmed` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `special_interest`
--
ALTER TABLE `special_interest`
  MODIFY `id_special_interest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `special_interest_kategori`
--
ALTER TABLE `special_interest_kategori`
  MODIFY `id_special_interest_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subimg_destination`
--
ALTER TABLE `subimg_destination`
  MODIFY `id_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `subimg_special_interest`
--
ALTER TABLE `subimg_special_interest`
  MODIFY `id_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tailor_make`
--
ALTER TABLE `tailor_make`
  MODIFY `id_tailor_make` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `id_bank` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_portofolio`
--
ALTER TABLE `tb_portofolio`
  MODIFY `id_portofolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `tb_testimoni`
--
ALTER TABLE `tb_testimoni`
  MODIFY `id_testimoni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tour`
--
ALTER TABLE `tour`
  MODIFY `id_tour` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tour_kategori`
--
ALTER TABLE `tour_kategori`
  MODIFY `id_tour_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `workshop`
--
ALTER TABLE `workshop`
  MODIFY `id_workshop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
