<?php
	function youtube_data($url,$key=null) {
		$id 		= substr($url, -11, 11);
		$content 	= file_get_contents("http://youtube.com/get_video_info?video_id={$id}");
		parse_str($content, $ytdata);
		return  empty($key) ? $ytdata : $ytdata[ $key ] ;
	}
?>