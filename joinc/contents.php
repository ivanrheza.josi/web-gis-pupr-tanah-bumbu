<?php
#===================== MODUL CONTENTS : BEGIN =====================

# beranda
if($_GET['mod']=='beranda') {
	include_once "joinc/contents/home.php";
}

# daerah irigasi
elseif($_GET['mod']=='daerah_irigasi') {
	include_once "joinc/contents/portofolio_kategori.php" ;
}
elseif($_GET['mod']=='detail_daerah_irigasi') {
	include_once "joinc/contents/portofolio_detail.php" ;
}



# tentang kami
elseif($_GET['mod']=='tentang_kami') {
	include_once "joinc/contents/tentang_kami.php";
}

# portofolio
elseif($_GET['mod']=='portofolio') {
	include_once "joinc/contents/portofolio.php";
}
elseif($_GET['mod']=='portofolio_kategori') {
	include_once "joinc/contents/portofolio_kategori.php" ;
}
elseif($_GET['mod']=='portofolio_detail') {
	include_once "joinc/contents/portofolio_detail.php" ;
}

# produk
elseif($_GET['mod']=='produk') {
	include_once "joinc/contents/produk.php";
}
elseif($_GET['mod']=='produk_kategori') {
	include_once "joinc/contents/produk_kategori.php" ;
}
elseif($_GET['mod']=='produk_detail') {
	include_once "joinc/contents/produk_detail.php" ;
}

# blog
elseif($_GET['mod']=='blog') {
	include_once "joinc/contents/blog.php";
}
elseif($_GET['mod']=='blog_detail') {
	include_once "joinc/contents/blog_detail.php" ;
}

# konsultasi
elseif($_GET['mod']=='konsultasi') {
	include_once "joinc/contents/konsultasi.php" ;
}
elseif($_GET['mod']=='send_messages_konsul') {
	include_once "joinc/contents/action/send_messages_konsul.php" ;
}

# galeri
elseif($_GET['mod']=='gallery') {
	include_once "joinc/contents/gallery.php" ;
}
elseif($_GET['mod']=='gallery_video') {
	include_once "joinc/contents/gallery_video.php" ;
}

# testimoni
if($_GET['mod']=='testimoni') {
	include_once "joinc/contents/testimoni.php";
}

# kontak
elseif($_GET['mod']=='kontak') {
	include_once "joinc/contents/kontak.php" ;
}
elseif($_GET['mod']=='send_messages') {
	include_once "joinc/contents/action/send_messages.php" ;
}

elseif ( $_GET['mod']=='map-sijari' ) {
	include_once "joinc/contents/map_sijari.php";
}




#===================== MODUL CONTENTS : END ====================

?>