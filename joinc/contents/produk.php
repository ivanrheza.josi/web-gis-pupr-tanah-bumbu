<div class="container">
    <section class="header header-bg-8" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Produk</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Produk</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="blog-wrapper">
        <!-- <div class="container"> -->
            <div class="row roww">
                <!-- <div class="col-md-offset-2 col-md-8">
                    <div class="section-title text-center">
                        <i class="flaticon-sidebar"></i>
                        <h2>Artikel</h2>
                        <?php 
                            // $about = $database->select($fields = "*", $table = "modul", $where_clause = "WHERE id_modul = '26'");
                            // echo $about['static_content'];
                        ?>
                    </div>
                </div> -->
                <div class="col-md-9 col-sm-8">
                    <div class="row">
                        <?php 
                            require 'josys/class/Pagination.php';
                            $pagination = new Paging_by_id_blog();
                            $batas      = 9;
                            $posisi     = $pagination->cariPosisi($batas);

                            $produk = $database->select($fields = "*", $table = "produk", $where_clause = "WHERE status = 'Y' LIMIT $posisi, $batas", $fetch = "all");
                            $tanggal = new Tanggal();
                            foreach ($produk as $key => $val_por) {
                                echo '
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="blog-content">
                                            <a href="'.$config['link_produk_det'].'-'.$val_por['seo'].'-'.$val_por['id_produk'].'" title="'.$val_por['judul'].'">
                                                <div class="blog-img image-hover" style="background-image:url(\'joimg/produk/'.$val_por['image'].'\')"></div>
                                            </a>
                                            <h4>
                                                <a href="'.$config['link_produk_det'].'-'.$val_por['seo'].'-'.$val_por['id_produk'].'" title="'.$val_por['judul'].'">'.$retVal = (strlen($val_por['judul']) > 20) ? substr($val_por['judul'], 0, 20).'...' : $val_por['judul'] ;
                                                echo '</a>
                                            </h4>
                                            <p><i class="fas fa-money-bill"></i>&nbsp; '.$rupiah->koma_strip($val_por['price']).'</p>
                                        </div>
                                    </div>
                                ';
                            }
                            $jml_data   = $database->count_rows($table="produk", $where_clause="WHERE status = 'Y'");
                            if($jml_data > $batas){
                                echo "
                                    <div class='row'>
                                        <div class='col-md-12 pagination-inner' style='text-align: center;'>
                                            <ul class='pagination'>";
                                            $jmlhalaman = $pagination->jumlahHalaman($jml_data, $batas);
                                            $linkHalaman = $pagination->navHalaman($_GET['halaman'], $jmlhalaman);
                                            echo " $linkHalaman";
                                        echo"</ul>
                                        </div>
                                    </div>
                                ";
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="sidber-box cats-widget">
                        <div class="cats-title">Kategori Produk</div>
                        <ul>    
                            <?php 
                                $sb_por_kat = $database->select($fields = "*", $table = "produk_kategori", $where_clause = "ORDER BY nama ASC", $fetch="all");
                                foreach ($sb_por_kat as $key => $val_sb_por_kat) {
                                    $count = $database->count_rows($table = "produk", $where_clause = "WHERE id_produk_kategori = '$val_sb_por_kat[id_produk_kategori]' AND status = 'Y'");
                                    echo '
                                        <li><a href="'.$config['link_produk_kat'].'-'.$val_sb_por_kat['seo'].'-'.$val_sb_por_kat['id_produk_kategori'].'">'.$val_sb_por_kat['nama'].'</a> <span>'.$count.'</span></li>
                                    ';
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
</div>