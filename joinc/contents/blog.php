<div class="container">
    <section class="header header-bg-8" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Blog</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Blog</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="blog-wrapper">
        <!-- <div class="container"> -->
            <div class="row roww">
                <!-- <div class="col-md-offset-2 col-md-8">
                    <div class="section-title text-center">
                        <i class="flaticon-sidebar"></i>
                        <h2>Artikel</h2>
                        <?php 
                            // $about = $database->select($fields = "*", $table = "modul", $where_clause = "WHERE id_modul = '26'");
                            // echo $about['static_content'];
                        ?>
                    </div>
                </div> -->
                <div class="col-md-12">
                    <div class="row">
                        <?php 
                            require 'josys/class/Pagination.php';
                            $pagination = new Paging_by_id_blog();
                            $batas      = 9;
                            $posisi     = $pagination->cariPosisi($batas);

                            $blog       = $database->select($fields = "*", $table = "blog", $where_clause = "WHERE status = 'Y' LIMIT $posisi, $batas", $fetch = "all");
                            $tanggal    = new Tanggal();
                            foreach ($blog as $key => $val_por) {
                                echo '
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="blog-content">
                                            <a href="'.$config['link_blog_det'].'-'.$val_por['seo'].'-'.$val_por['id_blog'].'" title="'.$val_por['judul'].'">
                                                <div class="blog-img blog-imgg image-hover" style="background-image:url(\'joimg/blog/'.$val_por['image'].'\')">
                                                    <span class="post-date">'.$tanggal->indo($val_por['date']).'</span>
                                                </div>
                                            </a>
                                            <h4>
                                                <a href="'.$config['link_blog_det'].'-'.$val_por['seo'].'-'.$val_por['id_blog'].'" title="'.$val_por['judul'].'">'.$retVal = (strlen($val_por['judul']) > 30) ? substr($val_por['judul'], 0, 30).'...' : $val_por['judul'] ;
                                                echo '</a>
                                            </h4>
                                            <p><i class="fas fa-user-alt"></i> '.$val_por['nama'].'</p>
                                        </div>
                                    </div>
                                ';
                            }
                            $jml_data   = $database->count_rows($table="blog", $where_clause="WHERE status = 'Y'");
                            if($jml_data > $batas){
                                echo "
                                    <div class='row'>
                                        <div class='col-md-12 pagination-inner' style='text-align: center;'>
                                            <ul class='pagination'>";
                                            $jmlhalaman = $pagination->jumlahHalaman($jml_data, $batas);
                                            $linkHalaman = $pagination->navHalaman($_GET['halaman'], $jmlhalaman);
                                            echo " $linkHalaman";
                                        echo"</ul>
                                        </div>
                                    </div>
                                ";
                            }
                        ?>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
</div>