<?php 
    $id_portofolio_kategori = $_GET['id'];
    $portofolio_kategori    = $database->select($fields = "*", $table = "portofolio_kategori", $where_clause = "WHERE id_portofolio_kategori = '$id_portofolio_kategori'", $fetch = "");
?>
<div class="containerXXX">
    <section class="header header-bg-8" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1><?php echo $portofolio_kategori['nama']; ?></h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <!-- <a href="<?php //echo $config['link_portofolio']; ?>" class="section">Portofolio</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div> -->
                                <div class="active section">DI / DIP <?php echo $portofolio_kategori['nama']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="blog-wrapper">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-3 col-sm-4">
                    <div class="sidber-box cats-widget">
                        <div class="cats-title">Daerah Irigasi</div>
                        <ul>    
                            <?php 
                                $sb_por_kat = $database->select($fields = "*", $table = "portofolio", $where_clause = "WHERE id_portofolio_kategori = '$portofolio_kategori[id_portofolio_kategori]' ORDER BY judul ASC", $fetch="all");
                                foreach ($sb_por_kat as $key => $val_sb_por_kat) {
                                    // $count = $database->count_rows($table = "portofolio", $where_clause = "WHERE id_portofolio_kategori = '$val_sb_por_kat[id_portofolio_kategori]' AND status = 'Y'");
                                    echo '
                                        <li><a href="'.$config['link_portofolio_det'].'-'.$val_sb_por_kat['seo'].'-'.$val_sb_por_kat['id_portofolio'].'">'.$val_sb_por_kat['judul'].'</a></li>
                                    ';
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="row">
                    <?php 
                        require 'josys/class/Pagination.php';
                        $pagination = new Paging_by_id_blog();
                        $batas      = 50;
                        $posisi     = $pagination->cariPosisi($batas);

                        $portofolio = $database->select($fields = "*", $table = "portofolio", $where_clause = "WHERE id_portofolio_kategori = '$id_portofolio_kategori' AND status = 'Y' LIMIT $posisi, $batas", $fetch = "all");
                        $tanggal = new Tanggal();
                        foreach ($portofolio as $key => $val_por) {
                            echo '
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="blog-content">
                                        <a href="'.$config['link_portofolio_det'].'-'.$val_por['seo'].'-'.$val_por['id_portofolio'].'" title="'.$val_por['judul'].'">
                                            <div class="blog-img image-hover" style="background-image:url(\'joimg/portofolio/'.$val_por['image'].'\')"></div>
                                        </a>
                                        <h4>
                                            <a href="'.$config['link_portofolio_det'].'-'.$val_por['seo'].'-'.$val_por['id_portofolio'].'" title="'.$val_por['judul'].'">'.$retVal = (strlen($val_por['judul']) > 20) ? substr($val_por['judul'], 0, 20).'...' : $val_por['judul'] ;
                                            echo '</a>
                                        </h4>
                                    </div>
                                </div>
                            ';
                        }
                        $jml_data   = $database->count_rows($table="portofolio", $where_clause="WHERE id_portofolio_kategori = '$id_portofolio_kategori' AND status = 'Y'");
                        if($jml_data > $batas){
                            echo "
                                <div class='rowXXX'>
                                    <div class='col-md-12 pagination-inner' style='text-align: center;'>
                                        <ul class='pagination'>";
                                        $jmlhalaman = $pagination->jumlahHalaman($jml_data, $batas);
                                        $linkHalaman = $pagination->navHalaman($_GET['halaman'], $jmlhalaman);
                                        echo " $linkHalaman";
                                    echo"</ul>
                                    </div>
                                </div>
                            ";
                        }
                    ?>
                        <!-- <div class="clearfix"></div> -->
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
</div>