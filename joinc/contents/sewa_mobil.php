<!-- page header -->
    <?php
        $imghdr = $database->select($fields = "image", $table = "img_header", $where_clause = "WHERE id_img = '4'");
    ?>
    <section class="header header-bg-2" style="background-image: url(joimg/imgheader/<?php echo $imghdr['image']; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Sewa Mobil</h1>
                            <div class="ui breadcrumb">
                                <a href="ab-sewa-mobil-jogja" class="section">Home</a>
                                <div class="divider"> / </div>
                                <div class="active section">Sewa Mobil</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about section -->
    <section class="about-section">
        <!-- about section -->
        <div class="about-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="section-title text-center">
                            <i class="flaticon-car"></i>
                            <h2>Sewa Mobil</h2>
                            <?php 
                                $about = $database->select($fields = "*", $table = "modul", $where_clause = "WHERE id_modul = '24'");
                                echo $about['static_content'];
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <?php 
                            // $about = $database->select($fields = "*", $table = "modul", $where_clause = "WHERE id_modul = '5'");
                            // echo $about['static_content'];
                        ?>

                        <h3>Daftar Harga Sewa Mobil</h3>
                        <div class="table-responsive">
                        <table class="table table-responsive table-striped table-bordered itabelth">
                            <thead>
                                <tr class="warning">
                                    <th class="itabeltdno" rowspan="2" style="vertical-align: middle;">No</th>
                                    <th rowspan="2" style="vertical-align: middle;">Jenis Mobil</th>
                                    <th colspan="2"><strong>Mobil + Driver</strong></th>
                                    <th colspan="2"><strong>Mobil + Driver + BBM</strong></th>
                                </tr>
                                <tr class="warning">
                                    <th>12 Jam</th>
                                    <th>Full Day</th>
                                    <th>12 Jam</th>
                                    <th>Full Day</th>
                                </tr>
                            </thead>
                            <tbody class="itengahtrtd">
                            <?php
                                $no = 1;
                                $price = new Rupiah();
                                $car = $database->select($fields="*", $tables="car_rent", $where_clause="WHERE status = 'Y' ORDER BY harga1 ASC", $fetch="all");
                                foreach ($car as $key => $c) {
                            ?>
                                    <tr>
                                        <td class="itabeltd"><?php echo $no; ?></td>
                                        <td class="inamamobil"><img src="joimg/car_rent/<?php echo $c['gambar']; ?>" class="ifotomobil"><br><?php echo $c['nama']; ?></td>
                                        <td class="itabeltd" style="vertical-align: middle;"><?php echo $price->koma_strip($c['harga1']) ; ?></td>
                                        <td class="itabeltd" style="vertical-align: middle;"><?php echo $price->koma_strip($c['harga2']) ; ?></td>
                                        <td class="itabeltd" style="vertical-align: middle;"><?php echo $price->koma_strip($c['harga3']) ; ?></td>
                                        <td class="itabeltd" style="vertical-align: middle;"><?php echo $price->koma_strip($c['harga4']) ; ?></td>
                                    </tr>
                            <?php
                                    $no++;
                                }
                            ?>
                            </tbody>
                        </table>
                        </div>
                        <div class="about-title">
                        <?php 
                            $about = $database->select($fields = "*", $table = "modul", $where_clause = "WHERE id_modul = '12'");
                            echo $about['static_content'];
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>