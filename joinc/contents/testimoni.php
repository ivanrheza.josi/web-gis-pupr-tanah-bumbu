<div class="container">
    <section class="header header-bg-7" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Testimoni</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Home</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Testimoni</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="about-section" style="padding-bottom: 30px;">
        <div class="gallery">
            <div class="gallery-inner">
                <!-- <div class="container"> -->
                    <div class="row roww gallery-margin">
                        <?php
                            $count_gallery = $database->count_rows($tables="tb_testimoni", $where_clause="");
                            if ($count_gallery < 1) {
                                echo '
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <b>Ups Sorry!</b> <br>
                                        <b>No Image been found here</b>.
                                    </div>
                                ';
                            } 
                            else{
                        ?>
                                <div class="portfolio-items list-unstyled zoom-gallery" id="grid">
                                    <?php 
                                        $testimoni = $database->select($fields="*", $tables="tb_testimoni", $where_clause="ORDER BY id_testimoni DESC", $fetch="all");
                                        $col = array(6);
                                        foreach ($testimoni as $key => $val_tes) {
                                            echo '
                                                <div class="col-md-'.$col[array_rand($col)].' col-sm-'.$col[array_rand($col)].' col-xs-12 gallery-padding">
                                                    <table>
                                                        <tr>
                                                            <td class="testi-imagee"><img src="joimg/testimoni/'.$val_tes['image'].'" class="img-responsive" alt=""></td>
                                                            <td>
                                                                <div style="padding: 10px 20px;">
                                                                    <div class="testimoniii"><i class="fas fa-quote-left"></i>'.$val_tes['comment'].'</div>
                                                                    <span class="author">'.$val_tes['nama'].', '.$val_tes['company'].'</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            ';
                                        }
                                     ?>
                                    <div class="col-md-3 col-sm-3 col-xs-12 shuffle_sizer"></div>
                                </div>
                        <?php                         
                            }
                        ?>
                    </div>
                <!-- </div>  -->
            </div>
        </div>
    </section>
</div>