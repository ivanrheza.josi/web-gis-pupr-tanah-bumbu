<style type="text/css">
  /* left navigation */
  #content-window {
    float: left;
    font-family: 'Roboto','sans-serif';
    padding-left: 10px;
    display: none;
    font-size: 0.8rem;
    background-color: #fff;
    z-index: 1;
    height: 80vh;
    overflow: auto;
    padding-top: 1rem;
  }
  /* end left navigation */

  /*filter map*/
  #filterMap .card-body {
    height: 45vh;
    overflow: auto;
  }
  /*end filter map*/

  /* komentar */
  #lastComments{
    z-index: 2;
  }
  /* komentar */


</style>
<div id="fb-root"></div>


<div class="container-fluid">
  <div class="row">
    <div id="filterMap" class="d-flex flex-row-reverse mt-4 position-absolute">
      <div class="card bg-white">
        <div class="card-header">
          <h4><a href="#demoLayers" data-toggle="collapse" class="btn btn-block btn-outline-info"><img width="15%" src="https://img.icons8.com/wired/64/000000/layers.png"> Pilih Daerah Irigasi</a></h4>
        </div>
        <div id="demoLayers" class="card-body collapse">
          <ul class="list-group"></ul>
        </div>
      </div>
    </div>
    <div id="content-window" class="">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 id="content-title">
            <a id="hideNav" href="javascript: void(0)"><img src="https://img.icons8.com/ios/50/000000/circled-left-2.png" class="icon-arroww"> </a>  
            <label for="" style="font-size: inherit;">{Title In Here}</label>
          </h4>';
        </div>
        <div class="panel-body">
      <div id="content-description"></div>
          <hr>
          <div class="fb-comments" data-href="" data-numposts="5" data-width="100%" ></div>
        </div>
      </div>
    </div>
    <div id="map" class=""></div>
  </div>
  <!-- /.row -->
</div>
<!-- /.container-fluid -->

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      
      <h3 class="text-center"><img width="32px" src="https://img.icons8.com/windows/32/000000/speech-bubble-with-dots.png"> Kolom Komentar</h3>
      <hr>
      <div>
      <!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU"></script> -->
        <form method="post" id="formComments" enctype="multipart/form-data">
          <input type="hidden" name="a" value="new-comment">
          <input type="hidden" name="latlon" id="latlon">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nama" required="">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="email@gmail.com" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea name="desc" id="" rows="5" class="form-control" placeholder="Tambahkan Komentar" required=""></textarea>
          </div>
          <div class="form-group">
            <label for="">Lokasi Kamu</label>
            <div id="mapHolder"></div>
          </div>
          <div class="form-group">
            <label for="">Upload Foto</label>
            <input type="file" name="fupload" required="">
          </div>
          <div class="form-group">
            <button class="btn btn-default" type="submit">Submit</button>
          </div>
        </form>
      </div>

      <!-- <hr>
      <div class="row">
        <div class="col-sm-10 col-xs-4">
          <strong>0 Komentar</strong>
        </div>
        <div class="col-sm-2 col-xs-8">
          <strong>Urut Berdasarkan</strong>
          <select name="" id="">
            <option value="">Terbaru</option>
            <option value="">Paling Lama</option>
          </select>
        </div>
      </div> -->
      <hr>

      <div class="panel panel-default">
        <div class="panel-body">
          <!-- push Comments -->
          <div id="lastComments">

          </div>
          <!-- <div class="btn btn-default form-control">
            Tampilkan Lebih Banyak
          </div> -->
        </div>
      </div>
      <hr>
      
    </div>
  </div>
</div>
<!-- /.container-fluid kolom komentar -->


<!-- Replace the value of the key parameter with your own API key. -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs-Udhr3Yu9oItwZnIDL-fNGfrIef4tCE&callback=initMap"></script>
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs-Udhr3Yu9oItwZnIDL-fNGfrIef4tCE"></script> -->
<script type="text/javascript">
  // <!-- start js comments -->
    (function(j){
      var row_counts= 9;
      var row_start= 0;
      var initialComments = {
        "view-all"  : getComments(),
        "new"       : postComment()
      };

      function getComments(){
        j.get('map-comments',{"a":"all", "start":row_start,"length":row_counts,"order":"DESC"},function(d){
          j('#lastComments').html(d)
          // console.log(d)
        },'html')
      }

      function postComment(){
        j('#formComments').submit(function(e){
          e.preventDefault()
          var formData = new FormData(this);
          $.ajax({
              url: 'map-comments',
              type: 'POST',
              data: formData,
              success: function (data) {
                  alert(data)
                  $('#formComments')[0].reset()
                  getComments()
              },
              cache: false,
              contentType: false,
              processData: false
          });
        })
      }

    })(jQuery)
  // <!-- end js comments-->
  
  // call map utama
  var parent= JSON.parse( $.ajax({type: "GET", url: "map-json", async: false, data: {"c":"categories"} }).responseText );
  var maps= JSON.parse( $.ajax({type: "GET", url: "map-json", async: false, data: {"c":"layers"} }).responseText );

  var semantics={};

  function initMap() {
    
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: {lat: -3.316694, lng: 114.590111},
      fullscreenControl : 0,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    });

    var x;
    var ctaLayer= [];
    for( x in maps) {
      if ( window.location.host == 'localhost' ) {
        ctaLayer[ maps[x].layer_id ] = new google.maps.KmlLayer({
          url: 'http://sijari.site.co.id/src/maps/kmz/'+maps[x].layer_name,
          suppressInfoWindows: true,
          map: map,
        });
      } else {
        ctaLayer[ maps[x].layer_id ] = new google.maps.KmlLayer({
          url: window.location.protocol+'//'+window.location.host+'/src/maps/kmz/'+maps[x].layer_name,
          suppressInfoWindows: true,
          map: map,
        }); 
      }
    }

    var x_p;
    for( x_p in parent ){
      $('#filterMap').css({
        "z-index" : "1",
        "right" : "1rem",
        "margin-top" : "1rem",
        "background-color" : "#fff"
      }).find('ul').append('<li id="parent_'+parent[x_p].category_id+'" class="list-group-item"><a class="a-layers" data-id="'+parent[x_p].category_id+'" href="javascript: void(0)">'+parent[x_p].category_title+'</a><a class="pl-2 float-right" href="#demoLayersSub'+parent[x_p].category_id+'" data-toggle="collapse"><img width="30px" src="https://img.icons8.com/ios/50/000000/plus.png"></a></li>');            
      $('#parent_'+parent[x_p].category_id).append( function(){
        var parent_sub= '';
        parent_sub += '<div id="demoLayersSub'+parent[x_p].category_id+'" class="collapse ml-3" >';
        for( x in maps ){
          if ( maps[x].relation_categories == parent[x_p].category_id ) {
            parent_sub += ' <label class="d-block"><a class="btn btn-outline-info a-layers-sub" data-id="'+maps[x].layer_id+'" href="javascript: void(0)">'+maps[x].layer_title+'</a></label>';
          }
        }
        parent_sub += '</div>';
        return parent_sub; 
      } );
    }

    $(document).on('click', '.a-layers', function(){
      for( x in maps)
      {
        if ( ( $(this).attr('data-id') == maps[x].relation_categories ) )
        {
          console.log(maps[x].relation_categories);
          ctaLayer[$(this).attr('data-id')].setMap(map);
        }else{
          ctaLayer[ maps[x].layer_id ].setMap(null);
        }
      }
    });

    $(document).on('click', '.a-layers-sub', function(){
      for( x in maps)
      {
        if ( ( $(this).attr('data-id') == maps[x].layer_id ) )
        {
          ctaLayer[$(this).attr('data-id')].setMap(map);
        }else{
          ctaLayer[ maps[x].layer_id ].setMap(null);
        }
      }
    });

    var y;
    for ( y in ctaLayer ){
      ctaLayer[y].addListener('click', function(kmlEvent) {
        var text = kmlEvent.featureData.description;
        showInContentWindow(text, kmlEvent.featureData.name, kmlEvent.featureData.id);
      });
    }
        
    function showInContentWindow(text,judul,id)
    {
      
      var sidediv = document.getElementById('content-window');
      window.location.hash= '#'+id;
      $('.fb-comments').attr('data-href', window.location.href );
      $('#content-title label').html(judul);
      $('#content-description').html(text);
      $('table').addClass('table');
      $("table tr table tr:first-child").html('');
      
      $('#content-window').addClass('col-lg-4 col-xs-12 show');
      $('#map').addClass('col-lg-8 col-xs-12');

      $('#hideNav').on('click', function(){
        $('#content-window').removeClass('col-lg-4 col-xs-12 show').css({"display": "none"});
        $('#map').removeClass('col-lg-8 col-xs-12');
      });  

      comments_plugin(); //call comments plugin
    }

    function comments_plugin()
    {
      // initialize sdk
      window.fbAsyncInit = function() {
        FB.init({
          appId            : '707444802710788',
          autoLogAppEvents : true,
          xfbml            : true,
          version          : 'v3.2'
        });
      };
      
      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/id_ID/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
      // end initialize sdk

      FB.XFBML.parse($('.comments').get(0),function(){
          $(".FB_Loader").remove();
      });
    }

    /* map Holder */
      markerMap()
      function markerMap(){
        var image = {
          url: 'https://img.icons8.com/color/48/000000/speech-bubble-with-dots.png',
          // This marker is 20 pixels wide by 32 pixels high.
          // size: new google.maps.Size(20, 32),
          // The origin for this image is (0, 0).
          // origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          // anchor: new google.maps.Point(0, 32)
        };
        $.get('map-comments',{"a":"comments"},function(d){
          $.each(d,function(i,items){
            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading"><img width="32px" src="https://img.icons8.com/windows/32/000000/speech-bubble-with-dots.png"> '+items.name+'</h1>'+
                '<div id="bodyContent">'+items.desc+
                '<img class="img-responsive" src="joimg/comment/'+items.comment_embed+'" alt="thumbnail">'
                // '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
                // 'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
                // '(last visited June 22, 2009).</p>'+
                '</div>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });

            var marker = new google.maps.Marker({
              position: JSON.parse(items.latlon),
              // position: {'lat':-3.63951773,'lng':115.2053833},
              icon: image,
              map: map,
              title: 'Post by '+items.name
            });

            marker.addListener('click', function() {
              infowindow.open(map, marker);
            });
            // console.log(JSON.parse(items.latlon))
          })  
        },'json')
        
      }

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getPosition,errorPosition);
      } else { 
        $('#formComments').css("display","none")
      }

      function getPosition(position) {
        var latlon= {
          'lat': position.coords.latitude,
          'lng': position.coords.longitude
        };
        $('#latlon').val(JSON.stringify(latlon))
        // console.log(latlon)

        var map2= document.getElementById("mapHolder");
        map2.style.height='400px';
        map2.style.width='100%';
        // {lat: -3.316694, lng: 114.590111}
        // var latlon=new google.maps.LatLng(position.coords.latitude, position.coords.longitude);


        var mapHolder = new google.maps.Map(map2, {
          center: latlon,
          zoom: 20,
          mapTypeId:google.maps.MapTypeId.ROADMAP,
          mapTypeControl:false,
          navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
        });
        var marker=new google.maps.Marker({
          position: latlon,
          map: mapHolder,
          title:"Your are here :)"
        });

        /* info windows */
        // var contentString = '<div id="content">'+
        //     '<div id="siteNotice">'+
        //     '</div>'+
        //     '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
        //     '<div id="bodyContent">'+
        //     '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
        //     'sandstone rock formation in the southern part of the '+
        //     'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
        //     'south west of the nearest large town, Alice Springs; 450&#160;km '+
        //     '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
        //     'features of the Uluru - Kata Tjuta National Park. Uluru is '+
        //     'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
        //     'Aboriginal people of the area. It has many springs, waterholes, '+
        //     'rock caves and ancient paintings. Uluru is listed as a World '+
        //     'Heritage Site.</p>'+
        //     '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
        //     'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
        //     '(last visited June 22, 2009).</p>'+
        //     '</div>'+
        //     '</div>';

        // var infowindow = new google.maps.InfoWindow({
        //   content: contentString
        // });

        // var marker = new google.maps.Marker({
        //   position: uluru,
        //   map: map,
        //   title: 'Uluru (Ayers Rock)'
        // });
        // marker.addListener('click', function() {
        //   infowindow.open(map, marker);
        // });
        /* info windows */
        
      }

      function errorPosition(position) {
          $('#formComments').css("display","none")
      }


    /* map Holder */
  
}

</script>
