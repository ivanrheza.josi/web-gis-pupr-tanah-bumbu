<div class="container">
    <section class="header header-bg-7" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Galeri Foto</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Home</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Galeri Foto</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="about-section" style="padding-bottom: 30px;">
        <div class="gallery">
            <div class="gallery-inner">
                <!-- <div class="container"> -->
                    <div class="row roww gallery-margin">
                        <?php
                            $count_gallery = $database->count_rows($tables="galeri_foto", $where_clause="");
                            if ($count_gallery < 1) {
                                echo '
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <b>Ups Sorry!</b> <br>
                                        <b>No Image been found here</b>.
                                    </div>
                                ';
                            } 
                            else{
                        ?>
                                <div class="portfolio-items list-unstyled zoom-gallery" id="grid">
                                    <?php 
                                        $gallery = $database->select($fields="*", $tables="galeri_foto", $where_clause="ORDER BY id_galeri_foto DESC", $fetch="all");
                                        $col = array(3,3,3);
                                        foreach ($gallery as $key => $valueg) {
                                            echo '
                                                <div class="col-md-'.$col[array_rand($col)].' col-sm-'.$col[array_rand($col)].' col-xs-12 gallery-padding">
                                                    <div class="img-thumb">
                                                        <a href="joimg/galeri/'.$valueg['image'].'" data-source="joimg/galeri/'.$valueg['image'].'" title="'.$valueg['nama'].'">
                                                            <img src="joimg/galeri/'.$valueg['image'].'" class="img-responsive" alt="">
                                                            <div class="gallery-hover">
                                                                <span class="plus-link dfd-top-right" href="#">
                                                                    <span class="plus-link-container">
                                                                        <span class="plus-link-out"></span>
                                                                        <span class="plus-link-come"></span>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                     ?>
                                    <div class="col-md-3 col-sm-3 col-xs-12 shuffle_sizer"></div>
                                </div>
                        <?php                         
                            }
                        ?>
                    </div>
                <!-- </div>  -->
            </div>
        </div>
    </section>
</div>