<?php 
	$id                     = $_GET['id'];
	$portofolio             = $database->select($fields = "*", $table = "portofolio", $where_clause = "WHERE id_portofolio= ".$id, $fetch ="");
	$portofolio_kategori    = $database->select($fields = "*", $table = "portofolio_kategori", $where_clause = "WHERE id_portofolio_kategori = '$portofolio[id_portofolio_kategori]'", $fetch = "");
	$tanggal                = new Tanggal();
?>
<div class="containerXXX">
	<section class="header header-bg-3" style="background-image: url(assets/images/bg-menu.jpg);">
		<div class="row roww">
			<div class="col-md-12">
				<div class="header-content">
					<div class="header-content-inner">
						<h1><?php echo $portofolio['judul']; ?></h1>
						<div class="ui breadcrumb">
							<a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
							<div class="divider"> <i class="fas fa-angle-right"></i> </div>
							<a href="<?php echo $config['link_portofolio_kat'].'-'.$portofolio_kategori['seo'].'-'.$portofolio_kategori['id_portofolio_kategori']; ?>" class="section">DI / DIP <?php echo $portofolio_kategori['nama']; ?></a>
							<div class="divider"> <i class="fas fa-angle-right"></i> </div>
							<div class="active section"><?php echo $portofolio['judul']; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="blog-details-inner">
		<div class="row roww">
			<div class="col-md-3 col-sm-4">
				<div class="sidber-box cats-widget">
					<div class="cats-title">Daerah Irigasi</div>
					<ul>    
						<?php 
							$sb_por_kat = $database->select($fields = "*", $table = "portofolio", $where_clause = "WHERE id_portofolio_kategori = '$portofolio_kategori[id_portofolio_kategori]' ORDER BY judul ASC", $fetch="all");
							foreach ($sb_por_kat as $key => $val_sb_por_kat) {
								if ($val_sb_por_kat['id_portofolio'] == $id) {
									$kelas = 'class="aktiff"';
								}
								else {
									$kelas = '';
								}
								echo '
									<li '.$kelas.'><a href="'.$config['link_portofolio_det'].'-'.$val_sb_por_kat['seo'].'-'.$val_sb_por_kat['id_portofolio'].'">'.$val_sb_por_kat['judul'].'</a></li>
								';
							}
						?>
					</ul>
				</div>
			</div>
			<div class="col-md-9 col-sm-8">
				<div class="blog-post">
					<h3 style="margin: 0 0 20px;"><?php echo $portofolio['judul'];?></h3>
					<div style="padding-bottom: 10px;">
						<img src="joimg/portofolio/<?php echo $portofolio['image']; ?>" class="img-responsive" alt="<?php echo $config['web_name']; ?>">
						<p class="text-center">Peta <?php echo $portofolio['judul'];?></p>
					</div>
					<div style="padding-bottom: 10px;">
						<img src="joimg/portofolio/<?php echo $portofolio['image_2']; ?>" class="img-responsive" alt="<?php echo $config['web_name']; ?>">
						<p class="text-center">Skema <?php echo $portofolio['judul'];?></p>
					</div>
					<div>
						<img style="width:840px !important" src="joimg/portofolio/<?php echo $portofolio['image_3']; ?>" class="img-responsive" alt="<?php echo $config['web_name']; ?>">
					</div>
					<div style="padding-bottom: 10px;">
						<h4>Galeri Foto</h4>
						<div class="gallery">
							<div class="gallery-inner">
								<div class="row roww gallery-margin">
									<div class="portfolio-items list-unstyled zoom-gallery" id="grid">
										<?php 
											$gallery = $database->select($fields="*", $tables="galeri_foto_porto", $where_clause="WHERE id_portofolio = '$portofolio[id_portofolio]' ORDER BY id_galeri_foto_porto ASC", $fetch="all");
											foreach ($gallery as $key => $valueg) {
												echo '
													<div class="col-md-4 col-xs-12 gallery-padding">
														<div class="img-thumb">
															<a href="joimg/portofolio/'.$valueg['image'].'" data-source="joimg/portofolio/'.$valueg['image'].'" title="'.$valueg['nama'].'">
																<img src="joimg/portofolio/'.$valueg['image'].'" class="img-responsive" alt="'.$valueg['nama'].'">
																<div class="gallery-hover">
																	<span class="plus-link dfd-top-right" href="#">
																		<span class="plus-link-container">
																			<span class="plus-link-out"></span>
																			<span class="plus-link-come"></span>
																		</span>
																	</span>
																</div>
															</a>
														</div>
													</div>
												';
											}
										 ?>
										<div class="col-md-4 col-sm-4 col-xs-12 shuffle_sizer"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="blog-post-inner">
						<div class="panel panel-default">
							<div id="fb-root"></div>
							<!-- <script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
									js = d.createElement(s); js.id = id;
									js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2&appId=707444802710788&autoLogAppEvents=1';
									fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));
							</script> -->
							<script async="1" defer="1" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&amp;version=v3.2&amp;appId=387679478714474&amp;autoLogAppEvents=1"></script>
							<div class="fb-comments" data-href="<?php echo $_SERVER['HTTP_REFERER'] ?>" data-numposts="5" width="100%"></div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>