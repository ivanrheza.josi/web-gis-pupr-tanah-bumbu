<div class="container">
    <section class="header header-bg-8" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Konsultasi</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Konsultasi</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="contact-inner">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-sm-12">
                    <div class="contact-form">
                        <form action="send-messages-konsul" method="post">
                            <h2>Form Konsultasi</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Your Name</label>
                                        <input type="text" class="form-control" id="f_name" name="name" placeholder="Your name" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Your phone number" onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Your email" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter subject" required="">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea class="form-control" id="message" name="message" placeholder="Enter message" rows="5" required=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php 
                                            $a = create_captcha(1,3);
                                            $b = create_captcha(1,3);
                                        ?>
                                            <label>Human verification</label>
                                            <input type="text" class="form-control" id="verify_booking" name="captcha" placeholder="Are you human? <?php echo $a.' + '.$b; ?> =" required="" onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="a" value="<?php echo $a; ?>">
                                    <input type="hidden" name="b" value="<?php echo $b; ?>">
                                </div>
                            </div>
                            <button type="submit" class="thm-btn">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
</div>