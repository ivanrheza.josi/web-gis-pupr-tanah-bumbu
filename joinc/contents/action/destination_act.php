<?php
	if ($_POST['id_tour']) {
		function isEmail($email) {
			return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
		}
		$id_tour	= $_POST['id_tour'];
		// $checkin	= stripcslashes($_POST['checkin']);
		$nama 		= stripslashes($_POST['name']);
		$email 		= stripslashes($_POST['email']);
		$telp 		= stripslashes($_POST['phone']);
		$alamat		= stripslashes($_POST['address']);
		$catatan 	= stripslashes($_POST['note']);
		$captcha 	= stripcslashes($_POST['captcha']);
		$a 			= $_POST['a'];
		$b 			= $_POST['b'];
		$c 			= $a+$b;
		//echo $c.''.$id_tour.' '.$checkin.' '.$nama.' '.$email.' '.$telp.' '.$alamat.' '.$catatan.' '.$captcha.' '.$a.' '.$b;die();

		if(trim($nama) == '')
		{
			echo "<script>alert('Sorry! Nama is Empty.'); window.location = document.referrer;</script>";
		}
		// else if(trim($checkin) == '')
		// {
		// 	echo "<script>alert('Sorry! Tanggal Check In is Empty.'); window.location = document.referrer;</script>";
		// }
		else if(trim($email) == '')
		{
			echo "<script>alert('Sorry! Email is Empty.'); window.location = document.referrer;</script>";
		}
		else if(!isEmail($email))
		{
			echo "<script>alert('Sorry! invalid Email. Thanks'); window.location = document.referrer;</script>";
		}
		else if(trim($telp) == '')
		{
			echo "<script>alert('Sorry! Phone is Empty.'); window.location = document.referrer;</script>";
		}
		else if(trim($alamat) == '')
		{
			echo "<script>alert('Sorry! alamat is Empty.'); window.location = document.referrer;</script>";
		}
		 else
		 {

			if($captcha != '')
			{
				if(($c) != $captcha)
				{
					// Captcha verification is incorrect.
					echo "<script>alert('Sorry! invalid Captcha. Please Try again. Thanks.'); window.location = document.referrer;</script>";
				}
				else
				{
					$tour = $database->select($fields="*", $tables="tour", $where_clause="WHERE id_tour = '$id_tour'");
					//data yang akan di insert berbentuk array
					$form_data = array(
					    "nama" 			=> "$nama",
					    "id_tour"		=> "$id_tour",
					    // "checkin"		=> "$checkin",
					    "email"			=> "$email",
						"telp"			=> "$telp",
						"alamat"		=> "$alamat",
						"catatan"		=> "$catatan",
						"status"		=> "0"
					);

					//proses insert ke database
		            $database->insert($table="reservasi_destination", $array=$form_data);
					
					$emailadm 	= $database->select($fields="*", $table="users", $where_clause="WHERE id_users = '2'");

					// send mail
					$to = "".$emailadm['email']."";
					$subject = "[AB Sewa Mobil Jogja] Reservasi Paket Wisata dari ".$form_data['nama']."";

					$message = "
					<html>
						<head>
							<title>[AB Sewa Mobil Jogja] Reservasi Paket Wisata dari ".$form_data['nama']."</title>
						</head>
						<body>
							<p>Reservasi Paket Wisata dari Website AB Sewa Mobil Jogja</p>
							<table>
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td>".$form_data['nama']."</td>
								</tr>
								<tr>
									<td>Paket wisata yang dipesan</td>
									<td>:</td>
									<td>".$tour['nama']."</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>:</td>
									<td>".$form_data['email']."</td>
								</tr>
								<tr>
									<td>Telepon</td>
									<td>:</td>
									<td>".$form_data['telp']."</td>
								</tr>
								<tr>
									<td>Alamat</td>
									<td>:</td>
									<td>".$form_data['alamat']."</td>
								</tr>
								<tr>
									<td>Catatan</td>
									<td>:</td>
									<td>".$form_data['catatan']."</td>
								</tr>
							</table>
						</body>
					</html>
					";

					// Always set content-type when sending HTML email
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					// More headers
					$headers .= 'From: <'.$form_data['email'].'>' . "\r\n";
					// $headers .= 'Cc: myboss@example.com' . "\r\n";

					mail($to,$subject,$message,$headers);
		            // end send mail

					echo "<script>alert('Thank you $nama, Your reservation Delivered Successfully. We will soon be able to contact you.'); window.location = document.referrer;</script>";
				}
			}
			else
			{
				// echo "<script>alert('Sorry! Captcha is Empty.'); window.location = document.referrer;</script>";
			}
		 }
	} else {
		// echo "<script>alert('Ohh Snapp! Something Huge may coming soon'); window.location = 'ab-sewa-mobil-jogja';</script>";
	}
	
?>
