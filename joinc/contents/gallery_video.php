<div class="container">
    <section class="header header-bg-7" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Galeri Video</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Home</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Galeri Video</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="about-section" style="padding-bottom: 30px;">
        <div class="gallery">
            <div class="gallery-inner">
                <!-- <div class="container"> -->
                    <div class="row roww gallery-margin">
                        <?php 
                            $count_gallery = $database->count_rows($tables="galeri_video", $where_clause="");
                            if ($count_gallery < 1) {
                                echo '
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <b>Ups Sorry!</b> <br>
                                        <b>No Image been found here</b>.
                                    </div>
                                ';
                            } 
                            else{
                                // $yt_link = "https://www.youtube.com/watch?v=7Vj5M0qKh8g";
                                // echo "<pre>";
                                // print_r( youtube_data($yt_link) );
                                // echo "</pre>";

                                // $channel_id = "UC4fVw6tmNRqZGNXnxq8FkZw";
                                // // Change channelid value to match your YouTube channel ID
                                // $url = 'https://www.youtube.com/subscribe_embed?channelid='.$channel_id;
                                // // Fetch the Subscribe button HTML
                                // $button_html = file_get_contents( $url );
                                // // Extract the subscriber count
                                // $found_subscribers = preg_match( '/="0">(\d+)</i', $button_html, $matches );
                                // if ( $found_subscribers && isset( $matches[1] ) ) {
                                //     echo intval( $matches[1] );
                                // }

                             ?>
                                    <div class="portfolio-items list-unstyled zoom-gallery">
                                        <?php 
                                            $gallery = $database->select($fields="*", $tables="galeri_video", $where_clause="ORDER BY id_galeri_video DESC", $fetch="all");
                                            foreach ($gallery as $key => $val_gal) {
                                                $yt_link        = "https://www.youtube.com/watch?v=".$val_gal['nama'];
                                                $yt_title       = youtube_data($yt_link,'title');
                                                $yt_thumb       = youtube_data($yt_link,'thumbnail_url');
                                                $yt_thumbnail   = str_replace("default", "mqdefault", $yt_thumb);
                                                echo '
                                                    <div class="col-md-6 col-sm-6 col-xs-12 gallery-padding video-galleryy">
                                                        <iframe src="https://www.youtube.com/embed/'.$val_gal['nama'].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        <h4>'.$yt_title.'</h4>
                                                    </div>
                                                ';
                                            }
                                         ?>
                                        <div class="col-md-3 col-sm-3 col-xs-12 shuffle_sizer"></div>
                                    </div>
                            <?php                         
                                }
                             ?>
                    </div>
                <!-- </div>  -->
            </div>
        </div>
    </section>
</div>