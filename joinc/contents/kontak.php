<div class="container">
    <section class="header header-bg-8" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Kontak</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section">Kontak</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="contact-inner">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-sm-6">
                <?php
                    $img_kontak = $database->select($fields="gambar", $table="menu", $where_clause="WHERE id_menu = '21'");
                ?>
                    <div class="image-menuu">
                        <img src="joimg/menu/<?php echo $img_kontak['gambar']; ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="contact-details">
                        <h2 style="padding-top: 15px;">Detail Kontak</h2>
                        <div class="contact-content">
                            <h4>Alamat</h4>
                        <?php 
                            $address = $database->select($fields = "judul", $table = "kontak", $where_clause = "WHERE jenis = 'address'", $fetch = "all");
                            foreach ($address as $key => $vphone) {
                                echo '
                                    <p>'.$vphone['judul'].'</p>
                                ';
                            }
                        ?>
                        </div>
                        <div class="contact-content">
                            <h4>Telepon</h4>
                        <?php 
                            $phone = $database->select($fields = "judul", $table = "kontak", $where_clause = "WHERE jenis = 'phone'", $fetch = "all");
                            foreach ($phone as $key => $vphone) {
                                echo '
                                    <p>'.$vphone['judul'].'</p>
                                ';
                            }
                        ?>
                        </div>
                        <div class="contact-content">
                            <h4>Email</h4>
                        <?php 
                            $email = $database->select($fields = "judul", $table = "kontak", $where_clause = "WHERE jenis = 'email'", $fetch = "all");
                            foreach ($email as $key => $vemail) {
                                echo '
                                    <p>'.$vemail['judul'].'</p>
                                ';
                            }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="contact-form" style="margin-bottom: 30px;">
                        <form action="send-messages" method="post">
                            <h2 style="padding-top: 15px;">Form Kirim Pesan</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Your Name</label>
                                        <input type="text" class="form-control" id="f_name" name="name" placeholder="Your name" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Your phone number" onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Your email" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter subject" required="">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea class="form-control" id="message" name="message" placeholder="Enter message" rows="5" required=""></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php 
                                            $a = create_captcha(1,3);
                                            $b = create_captcha(1,3);
                                        ?>
                                            <label>Human verification</label>
                                            <input type="text" class="form-control" id="verify_booking" name="captcha" placeholder="Are you human? <?php echo $a.' + '.$b; ?> =" required="" onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="a" value="<?php echo $a; ?>">
                                    <input type="hidden" name="b" value="<?php echo $b; ?>">
                                </div>
                            </div>
                            <button type="submit" class="thm-btn">Send</button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-details">
                        <div class="contact-content">
                            <h2 style="padding-top: 15px;">Ikuti Kami</h2>
                            <?php 
                                $sosm = $database->select($fields = "*", $table = "sosmed", $where_clause = "ORDER BY id_sosmed DESC", $fetch = "all");
                                foreach ($sosm as $key => $vsosm) {
                                    echo '
                                        <a href="'.$vsosm['link'].'" target="_blank"><img src="joimg/sosmed/'.$vsosm['gambar'].'" alt="'.$vsosm['nama'].'" title="'.$vsosm['nama'].'" style="width: 45px; height: auto;"></a>&nbsp;&nbsp;&nbsp;
                                    ';
                                }
                             ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="contact-details">
                        <div class="contact_map">
                            <h2 style="padding-top: 15px;">Lokasi</h2>
                        <?php 
                            $map = $database->select($fields="static_content", $table="modul", $where_clause="WHERE id_modul = 6", $fetch='');
                        ?>
                            <div id="map">
                                <iframe src="<?php echo $map['static_content']; ?>" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
</div>