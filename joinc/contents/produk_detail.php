<?php 
    $id                 = $_GET ['id'];
    $produk             = $database->select($fields = "*", $table = "produk", $where_clause = "WHERE id_produk= ".$id, $fetch ="");
    $produk_kategori    = $database->select($fields = "*", $table = "produk_kategori", $where_clause = "WHERE id_produk_kategori = '$produk[id_produk_kategori]'", $fetch = "");
    $tanggal            = new Tanggal();
?>
<div class="container">
    <section class="header header-bg-3" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1><?php echo $produk['judul']; ?></h1>
                            <!-- <p><?php //echo $produk['short_desc']; ?></p> -->
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <a href="<?php echo $config['link_produk']; ?>" class="section">Produk</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <a href="<?php echo $config['link_produk_kat'].'-'.$produk_kategori['seo'].'-'.$produk_kategori['id_produk_kategori']; ?>" class="section"><?php echo $produk_kategori['nama']; ?></a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section"><?php echo $produk['judul']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="blog-details-inner">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-9 col-sm-8">
                    <div class="blog-post">
                        <img src="joimg/produk/<?php echo $produk['image']; ?>" class="img-responsive" alt="<?php echo $config['web_name']; ?>">
                        <div class="blog-post-inner">
                            <h3><?php echo $produk['judul'];?></h3>
                            <p class="date-clientt" style="font-size: 15px; margin: 15px 0 20px 3px !important;">
                                <i class="far fa-id-badge fa-lg"></i>&nbsp; <?php echo $produk['code']; ?> &nbsp;&nbsp;&nbsp;&nbsp;
                                <i class="fas fa-money-bill fa-lg"></i>&nbsp; <?php echo $rupiah->koma_strip($produk['price']); ?>
                            </p>
                            <?php
                                echo $produk['konten'];
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="sidber-box cats-widget">
                        <div class="cats-title">Kategori Produk</div>
                        <ul>    
                            <?php 
                                $sb_por_kat = $database->select($fields = "*", $table = "produk_kategori", $where_clause = "ORDER BY nama ASC", $fetch="all");
                                foreach ($sb_por_kat as $key => $val_sb_por_kat) {
                                    $count = $database->count_rows($table = "produk", $where_clause = "WHERE id_produk_kategori = '$val_sb_por_kat[id_produk_kategori]' AND status = 'Y'");
                                    echo '
                                        <li><a href="'.$config['link_produk_kat'].'-'.$val_sb_por_kat['seo'].'-'.$val_sb_por_kat['id_produk_kategori'].'">'.$val_sb_por_kat['nama'].'</a> <span>'.$count.'</span></li>
                                    ';
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-sm-12">
                    <div class="separator"></div>
                    <h3>Artikel Terkait</h3>
                </div>
                <div class="clearfix"></div> -->
                <?php 
                    // $blog_relate = $database->select($fields = "*", $table = "produk", $where_clause = "WHERE status = 'Y' AND id_produk_kategori = '$produk[id_produk_kategori]' AND id_produk != $id ORDER BY id_produk DESC LIMIT 4", $fetch = "all");
                    //     $tanggal = new Tanggal();
                    //     foreach ($blog_relate as $key => $valuebr) {
                    //         echo '
                    //             <div class="col-xs-12 col-sm-6 col-md-3">
                    //                 <div class="blog-content">
                    //                     <a href="detail-artikel-'.$valuebr['seo'].'-'.$valuebr['id_produk'].'" title="'.$valuebr['judul'].'">
                    //                     <div class="blog-img image-hover" style="background-image:url(\'joimg/blog_detail/'.$valuebr['image'].'\')">
                                            
                    //                         <span class="post-date">'.$tanggal->indo($valuebr['date']).'</span>
                    //                     </div>
                    //                     </a>
                    //                     <h4>
                    //                         <a href="detail-artikel-'.$valuebr['seo'].'-'.$valuebr['id_produk'].'" title="'.$valuebr['judul'].'">'.$retVal = (strlen($valuebr['judul']) > 20) ? substr($valuebr['judul'], 0, 20).'...' : $valuebr['judul'] ;
                    //                         echo '</a></h4>
                    //                     <p>'.substr(strip_tags($valuebr['konten']), 0, 150).'...</p>
                    //                 </div>
                    //             </div>
                    //         ';
                    //     }
                 ?>
            </div>
        <!-- </div> -->
    </section>
</div>