<?php 
    $about = $database->select($fields = "*", $table = "modul", $where_clause = "WHERE id_modul = '5'");
?>
<div class="container">
    <section class="header header-bg-2" style="background-image: url(assets/images/bg-menu.jpg);">
        <div class="row roww">
            <div class="col-md-12">
                <div class="header-content">
                    <div class="header-content-inner">
                        <h1>Tentang Kami</h1>
                        <div class="ui breadcrumb">
                            <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                            <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                            <div class="active section">Tentang Kami</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-section">
        <div class="about-inner">
            <div class="row roww">
                <div class="col-sm-6">
                <?php
                    $img_tentang = $database->select($fields="gambar", $table="menu", $where_clause="WHERE id_menu = '21'");
                ?>
                    <div class="image-menuu">
                        <img src="joimg/menu/<?php echo $img_tentang['gambar']; ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                <?php
                    echo $about['static_content'];
                ?>
                </div>
                <div class="col-sm-12">
                <?php
                    echo $about['static_content_2'];
                ?>
                </div>
            </div>
        </div>
    </section>
</div>