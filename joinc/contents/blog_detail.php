<?php 
    $id         = $_GET ['id'];
    $blog       = $database->select($fields = "*", $table = "blog", $where_clause = "WHERE id_blog= ".$id, $fetch ="");
    $tanggal    = new Tanggal();
?>
<div class="container">
    <section class="header header-bg-3" style="background-image: url(assets/images/bg-menu.jpg);">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1>Blog Detail</h1>
                            <div class="ui breadcrumb">
                                <a href="<?php echo $config['link_home']; ?>" class="section">Beranda</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <a href="<?php echo $config['link_blog']; ?>" class="section">Blog</a>
                                <div class="divider"> <i class="fas fa-angle-right"></i> </div>
                                <div class="active section"><?php echo $blog['judul']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="blog-details-inner">
        <!-- <div class="container"> -->
            <div class="row roww">
                <div class="col-md-9 col-sm-8">
                    <div class="blog-post">
                        <img src="joimg/blog/<?php echo $blog['image']; ?>" class="img-responsive" alt="<?php echo $config['web_name']; ?>">
                        <div class="blog-post-inner">
                            <h3><?php echo $blog['judul']; ?></h3>
                            <p class="date-clientt">
                                <i class="fas fa-user-alt"></i> <?php echo $blog['nama']; ?> &nbsp;&nbsp;
                                <i class="fas fa-table"></i> <?php echo $tanggal->indo($blog['date']); ?>
                            </p>
                            <?php
                                echo $blog['konten'];
                            ?>
                            <?php
                                $req_url        = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                $twitter_acc    = "akun_twitter"
                            ?>
                            <div class="share-buttonn">
                                Share article : 
                                <a href="https://www.facebook.com/sharer.php?u=<?php echo $req_url; ?>" target="_blank" title="Facebook" id="facebook"><i class="fab fa-facebook-square fa-2x"></i></a>
                                <a href="https://twitter.com/share?url=<?php echo $req_url; ?>&text=<?php echo $blog['judul']; ?>&via=<?php echo $twitter_acc; ?>" target="_blank" title="Twitter" id="twitter"><i class="fab fa-twitter-square fa-2x"></i></a>
                                <a href="https://api.whatsapp.com/send?text=Hey!%20I%20see%20this%20and%20will%20share%20it%20with%20you,%20<?php echo $blog['judul']; ?>%20<?php echo $req_url; ?>" target="_blank" title="Whatsapp" id="whatsapp"><i class="fab fa-whatsapp-square fa-2x"></i></a>
                                <a href="https://telegram.me/share/url?url=<?php echo $req_url; ?>&text=Hey!%20I%20see%20this%20and%20will%20share%20it%20with%20you,%20<?php echo $blog['judul']; ?>" target="_blank" title="Telegram" id="telegram"><i class="fab fa-telegram fa-2x"></i></a>
                                <a href="https://social-plugins.line.me/lineit/share?url=<?php echo $req_url; ?>" target="_blank" title="Line" id="line"><i class="fab fa-line fa-2x"></i></a>
                                <a href="https://plus.google.com/share?url=<?php echo $req_url; ?>" target="_blank" title="Google+" id="gplus"><i class="fab fa-google-plus-square fa-2x"></i></a>
                                <a href="mailto:?Subject=Blog%20<?php echo $config['web_name']; ?>%20<?php echo"[".$config['web_link']."]"; ?>&Body=I%20see%20this%20and%20will%20share%20it%20with%20you,%20<?php echo $blog['judul']; ?>%20<?php echo $req_url; ?>" target="_blank" title="Email" id="email"><i class="fas fa-envelope-square fa-2x"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="sidber-box cats-widget">
                        <div class="cats-title">Blog Lain</div>
                        <ul>    
                            <?php 
                                $sb_blo_othe = $database->select($fields = "*", $table = "blog", $where_clause = "WHERE id_blog != '$id' LIMIT 3", $fetch ="all");
                                foreach ($sb_blo_othe as $key => $val_sb_blo_othe) {
                                    echo '
                                        <li><a href="'.$config['link_blog_det'].'-'.$val_sb_blo_othe['seo'].'-'.$val_sb_blo_othe['id_blog'].'">'.$val_sb_blo_othe['judul'].'</a></li>
                                    ';
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
</div>