<?php
/****************************************************************************************************
*					Search Engine Optimization - Title - Description - Keyword 						*
****************************************************************************************************/

// Source : http://ogp.me/

$default 			= $config['web_name'];
$domain				= $config['web_link'];
$seo_title			= $database->select($fields="static_content", $table="modul", $where_clause="WHERE id_modul = '1'");
$seo_keyword		= $database->select($fields="static_content", $table="modul", $where_clause="WHERE id_modul = '2'");
$seo_description	= $database->select($fields="static_content", $table="modul", $where_clause="WHERE id_modul = '3'");
$title				= $seo_title['static_content'];
$description 		= str_replace("&nbsp;", " ", $seo_description['static_content']);
// $keyword			= "akustik, ruang, akustik ruang, ruang akustik, ".$seo_keyword['static_content'];
$keyword			= $seo_keyword['static_content'];
$req_url 			= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$twitter_acc		= "@sijari";

echo '
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="index follow" />
	<meta name="description" content="'.$description.' | '.$default.'" />
    <meta name="keywords" content="'.$keyword.'" />
	<meta name="author" content="'.$default.'" />
	<meta name="generator" content="'.$default.'" />
	<meta name="copyright" content="'.$default.'" />
';

if ($_GET['mod']=='portofolio_detail') {
	$page 				= $database->select($fields="*", $table="portofolio", $where_clause="WHERE id_portofolio = '$_GET[id]'", $fetch="");
	$title_page			= $page['judul'];
	$description_page	= strip_tags(str_replace("&nbsp;", " ", $page['konten']));
	$image_page			= $page['image'];
	echo '
		<title>'.$title_page.' | '.$default.'</title>

		<meta property="og:url" content="http://'.$domain.'/'.$config['link_portofolio_det'].'-'.$page['seo'].'-'.$page['id_portofolio'].'" />
		<meta property="og:site_name" content="'.$default.'" />
		<meta property="og:locale" content="id_ID" />
	    <meta property="og:title" content="'.$title_page.' | '.$title.'" />
	    <meta property="og:type" content="product" />
	    <meta property="og:image" content="http://'.$domain.'/joimg/portofolio/'.$image_page.'" />
    	<meta property="og:image:alt" content="'.$title_page.'" />
		<meta property="og:description" content="'.substr($description_page, 0, 180).'" />
	    
	    <meta name="twitter:site" content="'.$twitter_acc.'" />
		<meta name="twitter:site:id" content="'.$twitter_acc.'" />
		<meta name="twitter:creator" content="'.$twitter_acc.'" />
		<meta name="twitter:title" content="'.$title_page.'">
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:description" content="'.substr($description_page, 0, 180).'" />
		<meta name="twitter:image" content="http://'.$domain.'/joimg/portofolio/'.$image_page.'" />

		<link rel="canonical" href="http://'.$domain.'/'.$config['link_portofolio_det'].'-'.$page['seo'].'-'.$page['id_portofolio'].'" />
	';
}

elseif ($_GET['mod']=='produk_detail') {
	$page 				= $database->select($fields="*", $table="produk", $where_clause="WHERE id_produk = '$_GET[id]'", $fetch="");
	$title_page			= $page['judul'];
	$description_page	= strip_tags(str_replace("&nbsp;", " ", $page['konten']));
	$image_page			= $page['image'];
	echo '
		<title>'.$title_page.' | '.$default.'</title>

		<meta property="og:url" content="http://'.$domain.'/'.$config['link_produk_det'].'-'.$page['seo'].'-'.$page['id_produk'].'" />
		<meta property="og:site_name" content="'.$default.'" />
		<meta property="og:locale" content="id_ID" />
	    <meta property="og:title" content="'.$title_page.' | '.$title.'" />
	    <meta property="og:type" content="product" />
	    <meta property="og:image" content="http://'.$domain.'/joimg/produk/'.$image_page.'" />
    	<meta property="og:image:alt" content="'.$title_page.'" />
		<meta property="og:description" content="'.substr($description_page, 0, 180).'" />
	    
	    <meta name="twitter:site" content="'.$twitter_acc.'" />
		<meta name="twitter:site:id" content="'.$twitter_acc.'" />
		<meta name="twitter:creator" content="'.$twitter_acc.'" />
		<meta name="twitter:title" content="'.$title_page.'">
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:description" content="'.substr($description_page, 0, 180).'" />
		<meta name="twitter:image" content="http://'.$domain.'/joimg/produk/'.$image_page.'" />

		<link rel="canonical" href="http://'.$domain.'/'.$config['link_produk_det'].'-'.$page['seo'].'-'.$page['id_produk'].'" />
	';
}

elseif ($_GET['mod']=='blog_detail') {
	$page 				= $database->select($fields="*", $table="blog", $where_clause="WHERE id_blog = '$_GET[id]'", $fetch="");
	$title_page			= $page['judul'];
	$description_page	= strip_tags(str_replace("&nbsp;", " ", $page['konten']));
	$image_page			= $page['image'];
	echo '
		<title>'.$title_page.' | '.$default.'</title>

		<meta property="og:url" content="http://'.$domain.'/'.$config['link_blog_det'].'-'.$page['seo'].'-'.$page['id_blog'].'" />
		<meta property="og:site_name" content="'.$default.'" />
		<meta property="og:locale" content="id_ID" />
	    <meta property="og:title" content="'.$title_page.' | '.$title.'" />
	    <meta property="og:type" content="product" />
	    <meta property="og:image" content="http://'.$domain.'/joimg/blog/'.$image_page.'" />
    	<meta property="og:image:alt" content="'.$title_page.'" />
		<meta property="og:description" content="'.substr($description_page, 0, 180).'" />
	    
	    <meta name="twitter:site" content="'.$twitter_acc.'" />
		<meta name="twitter:site:id" content="'.$twitter_acc.'" />
		<meta name="twitter:creator" content="'.$twitter_acc.'" />
		<meta name="twitter:title" content="'.$title_page.'">
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:description" content="'.substr($description_page, 0, 180).'" />
		<meta name="twitter:image" content="http://'.$domain.'/joimg/blog/'.$image_page.'" />

		<link rel="canonical" href="http://'.$domain.'/'.$config['link_blog_det'].'-'.$page['seo'].'-'.$page['id_blog'].'" />
	';
}

else
{
	if ($_GET['mod']=='tentang_kami') {
		echo '
			<title>Tentang Kami | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_tentang'].'" />
		';
	}
	elseif ($_GET['mod']=='portofolio') {
		echo '
			<title>Portofolio | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_portofolio'].'" />
		';
	}
	elseif ($_GET['mod']=='portofolio_kategori') {
		$page 		= $database->select($fields = "*", $table = "portofolio_kategori", $where_clause = "WHERE id_portofolio_kategori = '$_GET[id]'", $fetch = "");
		$title_page	= $page['nama'];
		echo '
			<title>Portofolio '.$title_page.' | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_portofolio_kat'].'-'.$page['seo'].'-'.$page['id_portofolio_kategori'].'" />
		';
	}
	elseif ($_GET['mod']=='produk') {
		echo '
			<title>Produk | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_produk'].'" />
		';
	}
	elseif ($_GET['mod']=='produk_kategori') {
		$page 		= $database->select($fields = "*", $table = "produk_kategori", $where_clause = "WHERE id_produk_kategori = '$_GET[id]'", $fetch = "");
		$title_page	= $page['nama'];
		echo '
			<title>Produk '.$title_page.' | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_produk_kat'].'-'.$page['seo'].'-'.$page['id_produk_kategori'].'" />
		';
	}
	elseif ($_GET['mod']=='blog') {
		echo '
			<title>Blog | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_blog'].'" />
		';
	}
	elseif ($_GET['mod']=='konsultasi') {
		echo '
			<title>Konsultasi | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_konsultasi'].'" />
		';
	}
	elseif ($_GET['mod']=='gallery') {
		echo '
			<title>Galeri Foto | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_galeri_foto'].'" />
		';
	}
	elseif ($_GET['mod']=='gallery_video') {
		echo '
			<title>Galeri Video | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_galeri_video'].'" />
		';
	}
	elseif ($_GET['mod']=='testimoni') {
		echo '
			<title>Testimoni | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_testimoni'].'" />
		';
	}
	elseif ($_GET['mod']=='kontak') {
		echo '
			<title>Kontak | '.$default.'</title>
			<link rel="canonical" href="http://'.$domain.'/'.$config['link_kontak'].'" />
		';
	}
	else {
		echo '
			<title>'.$title.' | '.$default.' | '.$domain.'</title>
			<link rel="canonical" href="http://'.$domain.'" />
		';
	}

	echo '
		<meta property="og:url" content="http://'.$domain.'" />
		<meta property="og:site_name" content="'.$default.'" />
		<meta property="og:locale" content="id_ID" />
		<meta property="og:title" content="'.$title.'" />
		<meta property="og:type" content="website" />
	    <meta property="og:image" content="http://'.$domain.'/img/icon/icon.png" />
	    <meta property="og:image:alt" content="'.$default.'" />
		<meta property="og:description" content="'.$description.'" />		
		
		<meta name="twitter:site" content="'.$twitter_acc.'" />
		<meta name="twitter:site:id" content="'.$twitter_acc.'" />
		<meta name="twitter:creator" content="'.$twitter_acc.'" />
		<meta name="twitter:title" content="'.$title.'">
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:description" content="'.strip_tags(substr($description, 0, 180)).'" />
		<meta name="twitter:image" content="http://'.$domain.'/img/icon/icon.png" />
	';
}
?>
