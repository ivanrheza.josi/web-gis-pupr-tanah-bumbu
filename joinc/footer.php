<footer>
    <div class="container">
        <div class="row">
            <!-- Address -->
            <div class="col-sm-4X col-md-4 itopfooter">
                <h4 class="footer-title">AB Sewa Mobil Jogja</h4>
                <hr class="ihrfooter">
                <div class="footer-box address-inner">
                    <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the.  </p> -->
                    <div class="address">
                        <i class="flaticon-placeholder"></i>
                        <?php 
                            $address = $database->select($fields = "judul", $table = "kontak", $where_clause = "WHERE jenis = 'address'", $fetch = "all");
                            foreach ($address as $key => $vphone) {
                                echo '
                                    <p>'.$vphone['judul'].'</p>
                                ';
                            }
                         ?>
                    </div>
                    <div class="address">
                        <i class="flaticon-customer-service"></i>
                        <?php 
                            $phone = $database->select($fields = "judul", $table = "kontak", $where_clause = "WHERE jenis = 'phone'", $fetch = "all");
                            foreach ($phone as $key => $vphone) {
                                echo '
                                    <p class="ihoverfooter"><a href="https://api.whatsapp.com/send?phone='.$vphone['judul'].'&text=Halo%20AB%20Sewa%20Mobil%20Jogja%2C%20" class="phone" target="_blank">'.$vphone['judul'].'</a></p>
                                ';
                            }
                         ?>
                    </div>
                    <div class="address">
                        <i class="flaticon-mail"></i>
                        <?php 
                            $email = $database->select($fields = "judul", $table = "kontak", $where_clause = "WHERE jenis = 'email'", $fetch = "all");
                            foreach ($email as $key => $vemail) {
                                echo '
                                    <p class="ihoverfooter"><a href="mailto:'.$vphone['judul'].'">'.$vemail['judul'].'</a></p>
                                ';
                            }
                         ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-8X col-md-4">
                <div class="footer-box itopfooter">
                    <h4 class="footer-title">Artikel Terpopuler</h4>
                    <hr class="ihrfooter">
                    <ul class="categoty iblogfooter">
                        <?php 
                            $blog = $database->select($fetch = "*", $table = "blog_detail", $where_clause = "WHERE popular = 'Y' LIMIT 5", $fetch="all");
                            foreach ($blog as $key => $value_b) {
                                echo '
                                    <li>
                                        <a href="detail-artikel-'.$value_b['seo'].'-'.$value_b['id_blog_detail'].'"><img src="joimg/blog_detail/'.$value_b['image'].'"> '.ucwords(strtolower($value_b['judul'])).'</a>
                                    </li>
                                ';
                            }
                         ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-6X hidden-sm">
                <div class="footer-box itopfooter">
                    <h4 class="footer-title">Galeri</h4>
                    <hr class="ihrfooter">
                    <ul class="gallery-list popup-gallery igalerifooter">
                        <?php 
                            $gal = $database->select($fields = "*", $table = "gallery_port", $where_clause = "ORDER BY RAND() LIMIT 9", $fetch = "all");
                            foreach ($gal as $key => $v_gal) {
                                echo '
                                    <li> <a href="joimg/galeri/'.$v_gal['image'].'" data-source="joimg/galeri/'.$v_gal['image'].'" title="'.$v_gal['image'].'"><img src="joimg/galeri/'.$v_gal['image'].'" alt=""></a></li>
                                ';
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 ifooter">
                    <p>Copyright &copy; 2017 <font style="color: #242121;"> AB Sewa Mobil Jogja</font>. All rights reserved<br>
                        Developed by <a href="http://jogjasite.com/">JogjaSite</a></p>
                </div>
                <div class="col-sm-7">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="ab-sewa-mobil-jogja">Home</a></li>
                            <li><a href="<?php echo $config['link_about']; ?>">Profil</a></li>
                            <li><a href="sewa-mobil-jogja">Sewa Mobil</a></li>
                            <li><a href="paket-wisata-jogja">Paket Wisata</a></li>
                        </ul>
                        <?php
                            $ip         = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
                            $tanggal    = date("Y-m-d");// Mendapatkan tanggal sekarang
                            $waktu      = time(); //

                            $pengunjung       = $database->count_rows($table="statistik", $where_clause="WHERE tanggal='$tanggal' GROUP BY ip");
                            $totalpengunjung  = $database->select($fields="COUNT(hits)", $table="statistik", $where_clause="", $fetch="");
                            $hits             = $database->select($fields="SUM(hits)", $table="statistik", $where_clause="WHERE tanggal='$tanggal'", $fetch="");
                            $totalhits        = $database->select($fields="SUM(hits)", $table="statistik", $where_clause="", $fetch="");
                            $bataswaktu       = time() - 300;
                            $pengunjungonline = $database->count_rows($table="statistik", $where_clause="WHERE online > '$bataswaktu'");
                        ?>
                        <ul style="padding-top: 8px;">
                            <li>Visitor Today <span class="badge ibadge"><?php if(empty($pengunjung)){echo "0";}else{echo $pengunjung;} ?></span> | </li>
                            <li>Total Visitor <span class="badge ibadge"><?php echo $totalpengunjung['0']; ?></span> | </li>
                            <li>Hits Today <span class="badge ibadge"><?php if(empty($hits['0'])){echo "0";}else{echo $hits['0'];} ?></span> | </li>
                            <li>Total Hits <span class="badge ibadge"><?php echo $totalhits['0']; ?></span> | </li>
                            <li>Online Visitor <span class="badge ibadge"><?php echo $pengunjungonline; ?></span> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>