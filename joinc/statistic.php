<?php
	// Statistik Visitor / User
    $ip      = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
    $tanggal = date("Y-m-d"); // Mendapatkan tanggal sekarang
    $waktu   = time();

    // Mengecek berdasarkan IPnya, apakah user sudah pernah mengakses hari ini
    $statistik = $database->count_rows_new($table="statistik", $where_clause="WHERE ip='$ip' AND tanggal='$tanggal'");
    // Jika belum, tambahkan data baru
    if ($statistik == 0) {
        $form_data = array(
            "ip"        => "$ip",
            "tanggal"   => "$tanggal",
            "hits"      => "1",
            "online"    => "$waktu"
        );
        $database->insert($table="statistik", $array=$form_data);
    }
    // Jika sudah, update data lama
    else {
        $cek_stat = $database->select($fields="ip, hits, tanggal", $table="statistik", $where_clause="WHERE ip = '$ip' AND tanggal = '$tanggal'", $fetch='');
        $hits   = $cek_stat['hits'] + 1;

        $form_data = array(
            "hits"      => "$hits",
            "online"    => "$waktu"
        );
        $database->update_stat($table="statistik", $array=$form_data, $fields_key1="ip", $fields_key2="tanggal", $id_1="$ip", $id_2="$tanggal");
    }
?>